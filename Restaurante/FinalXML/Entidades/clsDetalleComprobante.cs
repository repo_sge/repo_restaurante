﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Entidades
{
    public class clsDetalleComprobante
    {
        private int iddetallecomprobante;
        private clsComprobante comprobante;
        private clsTipoImpuesto tipoimpuesto;
        private decimal subtotal;
        private decimal igv;
        private decimal total;
        private int estado;
        private clsDetallePedido detallepedido;

        public int Iddetallecomprobante
        {
            get
            {
                return iddetallecomprobante;
            }

            set
            {
                iddetallecomprobante = value;
            }
        }

        internal clsComprobante Comprobante
        {
            get
            {
                return comprobante;
            }

            set
            {
                comprobante = value;
            }
        }

        internal clsTipoImpuesto Tipoimpuesto
        {
            get
            {
                return tipoimpuesto;
            }

            set
            {
                tipoimpuesto = value;
            }
        }

        public decimal Subtotal
        {
            get
            {
                return subtotal;
            }

            set
            {
                subtotal = value;
            }
        }

        public decimal Igv
        {
            get
            {
                return igv;
            }

            set
            {
                igv = value;
            }
        }

        public decimal Total
        {
            get
            {
                return total;
            }

            set
            {
                total = value;
            }
        }

        public int Estado
        {
            get
            {
                return estado;
            }

            set
            {
                estado = value;
            }
        }

        internal clsDetallePedido Detallepedido
        {
            get
            {
                return detallepedido;
            }

            set
            {
                detallepedido = value;
            }
        }
    }
}
