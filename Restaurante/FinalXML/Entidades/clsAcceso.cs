﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Entidades
{
    class clsAcceso
    {
        private int idacceso;
        private DateTime fecha;
        private clsUsuario usuario;
        private string pc;
        private string usuariopc;

        public int Idacceso
        {
            get
            {
                return idacceso;
            }

            set
            {
                idacceso = value;
            }
        }

        public DateTime Fecha
        {
            get
            {
                return fecha;
            }

            set
            {
                fecha = value;
            }
        }

        public clsUsuario Usuario
        {
            get
            {
                return usuario;
            }

            set
            {
                usuario = value;
            }
        }

        public string Pc
        {
            get
            {
                return pc;
            }

            set
            {
                pc = value;
            }
        }

        public string Usuariopc
        {
            get
            {
                return usuariopc;
            }

            set
            {
                usuariopc = value;
            }
        }
    }
}
