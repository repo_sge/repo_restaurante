﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Entidades
{
   public  class clsProveedor
    {
        private int idproveedor;
        private clsTipoDocumentoIndentidad tipodocumentoindentidad;
        private string documento;
        private string razonsocial;
        private string direccion;
        private string descripcion;
        private int estado;

        public int Idproveedor
        {
            get
            {
                return idproveedor;
            }

            set
            {
                idproveedor = value;
            }
        }

        internal clsTipoDocumentoIndentidad Tipodocumentoindentidad
        {
            get
            {
                return tipodocumentoindentidad;
            }

            set
            {
                tipodocumentoindentidad = value;
            }
        }

        public string Documento
        {
            get
            {
                return documento;
            }

            set
            {
                documento = value;
            }
        }

        public string Razonsocial
        {
            get
            {
                return razonsocial;
            }

            set
            {
                razonsocial = value;
            }
        }

        public string Direccion
        {
            get
            {
                return direccion;
            }

            set
            {
                direccion = value;
            }
        }

        public string Descripcion
        {
            get
            {
                return descripcion;
            }

            set
            {
                descripcion = value;
            }
        }

        public int Estado
        {
            get
            {
                return estado;
            }

            set
            {
                estado = value;
            }
        }
    }
}
