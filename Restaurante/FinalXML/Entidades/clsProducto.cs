﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace FinalXML.Entidades
{
    public class clsProducto
    {
        private int idProducto;
        private clsCategoria categoria;
        private string nombre;
        private string descripcion;
        private decimal preciounitario;
        private int estado;
        private string codsunat;

        public Boolean Icbper { get; set; }

        public int IdProducto
        {
            get
            {
                return idProducto;
            }

            set
            {
                idProducto = value;
            }
        }

        internal clsCategoria Categoria
        {
            get
            {
                return categoria;
            }

            set
            {
                categoria = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public string Descripcion
        {
            get
            {
                return descripcion;
            }

            set
            {
                descripcion = value;
            }
        }

        public decimal Preciounitario
        {
            get
            {
                return preciounitario;
            }

            set
            {
                preciounitario = value;
            }
        }

        public int Estado
        {
            get
            {
                return estado;
            }

            set
            {
                estado = value;
            }
        }

        public string Codsunat { get => codsunat; set => codsunat = value; }
    }
}
