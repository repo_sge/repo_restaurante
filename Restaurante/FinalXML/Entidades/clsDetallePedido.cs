﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Entidades
{
    public class clsDetallePedido
    {
        private int iddetallepedido;
        private clsProducto producto;
        private decimal cantidad;
        private decimal preciounitario;
        private decimal descuento;
        private clsPedido pedido;
        private int parallevar;
        private int atendido;
        private int estado;
        private string descripcion;
        private clsUnidadMedida unidad;

        public int Iddetallepedido
        {
            get
            {
                return iddetallepedido;
            }

            set
            {
                iddetallepedido = value;
            }
        }

        public clsProducto Producto
        {
            get
            {
                return producto;
            }

            set
            {
                producto = value;
            }
        }

        public decimal Cantidad
        {
            get
            {
                return cantidad;
            }

            set
            {
                cantidad = value;
            }
        }

        public decimal Preciounitario
        {
            get
            {
                return preciounitario;
            }

            set
            {
                preciounitario = value;
            }
        }

        public decimal Descuento
        {
            get
            {
                return descuento;
            }

            set
            {
                descuento = value;
            }
        }

        public int Estado
        {
            get
            {
                return estado;
            }

            set
            {
                estado = value;
            }
        }

        internal clsPedido Pedido
        {
            get
            {
                return pedido;
            }

            set
            {
                pedido = value;
            }
        }

        public clsUnidadMedida Unidad
        {
            get
            {
                return unidad;
            }

            set
            {
                unidad = value;
            }
        }

        public int Parallevar
        {
            get
            {
                return parallevar;
            }

            set
            {
                parallevar = value;
            }
        }

        public int Atendido
        {
            get
            {
                return atendido;
            }

            set
            {
                atendido = value;
            }
        }

        public string Descripcion
        {
            get
            {
                return descripcion;
            }

            set
            {
                descripcion = value;
            }
        }
    }
}
