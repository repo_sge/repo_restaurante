﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Entidades
{
    class clsTipoCuentaBancaria
    {
        private int idtipocuentabancaria;
        private string descripcion;
        private int estado;

        public int Idtipocuentabancaria
        {
            get
            {
                return idtipocuentabancaria;
            }

            set
            {
                idtipocuentabancaria = value;
            }
        }

        public string Descripcion
        {
            get
            {
                return descripcion;
            }

            set
            {
                descripcion = value;
            }
        }

        public int Estado
        {
            get
            {
                return estado;
            }

            set
            {
                estado = value;
            }
        }
    }
}
