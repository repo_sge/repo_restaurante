﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Entidades
{
    public class clsMesa
    {
        private int idmesa;
        private string nombre;
        private string descripcion;
        private clsPiso piso;
        private int estado;

        public int Idmesa
        {
            get
            {
                return idmesa;
            }

            set
            {
                idmesa = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public string Descripcion
        {
            get
            {
                return descripcion;
            }

            set
            {
                descripcion = value;
            }
        }

        public int Estado
        {
            get
            {
                return estado;
            }

            set
            {
                estado = value;
            }
        }

        internal clsPiso Piso
        {
            get
            {
                return piso;
            }

            set
            {
                piso = value;
            }
        }
    }
}
