﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Entidades
{
    public class clsNumeracion
    {
        public Int32 IDDocumento { get; set; }
        public String TipoDocumento { get; set; }
        public String NombreDocumento { get; set; }
        public String Serie { get; set; }
        public Int32 Numeracion { get; set; }

        public DateTime FechaIni { get; set; }
        public DateTime FechaFin { get; set; }
        public int Estado { get; set; }
    }
}
