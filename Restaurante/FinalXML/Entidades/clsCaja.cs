﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Entidades
{
    class clsCaja
    {
        private int idcaja;
        private DateTime fechaapertura;
        private decimal montoapertura;
        private DateTime fechacierre;
        private decimal montocierre;
        private clsUsuario usuario;
        private decimal totalefectivo;
        private decimal totaldeposito;
        private decimal totaltransferencia;
        private decimal totaldisponible;
        private decimal totaltarjeta;
        private decimal totalnota;
        private int estado;

        public int Idcaja
        {
            get
            {
                return idcaja;
            }

            set
            {
                idcaja = value;
            }
        }

        public DateTime Fechaapertura
        {
            get
            {
                return fechaapertura;
            }

            set
            {
                fechaapertura = value;
            }
        }

        public decimal Montoapertura
        {
            get
            {
                return montoapertura;
            }

            set
            {
                montoapertura = value;
            }
        }

        public DateTime Fechacierre
        {
            get
            {
                return fechacierre;
            }

            set
            {
                fechacierre = value;
            }
        }

        public decimal Montocierre
        {
            get
            {
                return montocierre;
            }

            set
            {
                montocierre = value;
            }
        }

        public clsUsuario Usuario
        {
            get
            {
                return usuario;
            }

            set
            {
                usuario = value;
            }
        }

        public decimal Totalefectivo
        {
            get
            {
                return totalefectivo;
            }

            set
            {
                totalefectivo = value;
            }
        }

        public decimal Totaldeposito
        {
            get
            {
                return totaldeposito;
            }

            set
            {
                totaldeposito = value;
            }
        }

        public decimal Totaltransferencia
        {
            get
            {
                return totaltransferencia;
            }

            set
            {
                totaltransferencia = value;
            }
        }

        public decimal Totaldisponible
        {
            get
            {
                return totaldisponible;
            }

            set
            {
                totaldisponible = value;
            }
        }

        public decimal Totaltarjeta
        {
            get
            {
                return totaltarjeta;
            }

            set
            {
                totaltarjeta = value;
            }
        }

        public decimal Totalnota
        {
            get
            {
                return totalnota;
            }

            set
            {
                totalnota = value;
            }
        }

        public int Estado
        {
            get
            {
                return estado;
            }

            set
            {
                estado = value;
            }
        }
    }
}
