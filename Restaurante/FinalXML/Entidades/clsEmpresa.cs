﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Entidades
{
    public class clsEmpresa
    {
        private int idempresa;
        private string razonsocial;
        private string ruc;
        private string ubigeo;
        private string direccion;
        private string departamento;
        private string provincia;
        private string distrito;        
        private int estado;
        private clsTipoDocumentoIndentidad tipodocidentidad;
        private string usuariosol;
        private string clavesol;
        private string nombrecertificado;
        private string rutacertificado;
        private string clavecertificado;
        private string urlenvio;

      

        public string Usuariosol
        {
            get
            {
                return usuariosol;
            }

            set
            {
                usuariosol = value;
            }
        }

        public string Clavesol
        {
            get
            {
                return clavesol;
            }

            set
            {
                clavesol = value;
            }
        }

        public string Rutacertificado
        {
            get
            {
                return rutacertificado;
            }

            set
            {
                rutacertificado = value;
            }
        }

        public string Clavecertificado
        {
            get
            {
                return clavecertificado;
            }

            set
            {
                clavecertificado = value;
            }
        }

        public string Urlenvio
        {
            get
            {
                return urlenvio;
            }

            set
            {
                urlenvio = value;
            }
        }

        public int Idempresa
        {
            get
            {
                return idempresa;
            }

            set
            {
                idempresa = value;
            }
        }

        public string Razonsocial
        {
            get
            {
                return razonsocial;
            }

            set
            {
                razonsocial = value;
            }
        }

       

        public string Ubigeo
        {
            get
            {
                return ubigeo;
            }

            set
            {
                ubigeo = value;
            }
        }

        public string Direccion
        {
            get
            {
                return direccion;
            }

            set
            {
                direccion = value;
            }
        }

        public string Departamento
        {
            get
            {
                return departamento;
            }

            set
            {
                departamento = value;
            }
        }

        public string Provincia
        {
            get
            {
                return provincia;
            }

            set
            {
                provincia = value;
            }
        }

        public string Distrito
        {
            get
            {
                return distrito;
            }

            set
            {
                distrito = value;
            }
        }

        public int Estado
        {
            get
            {
                return estado;
            }

            set
            {
                estado = value;
            }
        }

        public string Ruc
        {
            get
            {
                return ruc;
            }

            set
            {
                ruc = value;
            }
        }

        public string Nombrecertificado
        {
            get
            {
                return nombrecertificado;
            }

            set
            {
                nombrecertificado = value;
            }
        }

        internal clsTipoDocumentoIndentidad Tipodocidentidad
        {
            get
            {
                return tipodocidentidad;
            }

            set
            {
                tipodocidentidad = value;
            }
        }
    }
}
