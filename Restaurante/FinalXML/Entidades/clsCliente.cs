﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Entidades
{
    public class clsCliente
    {
        private int idcliente;
        private clsTipoDocumentoIndentidad tipodocidentidad;
        private string razonsocial;
        private string documento;
        private string direccion;
        private int estado;

        public int Idcliente
        {
            get
            {
                return idcliente;
            }

            set
            {
                idcliente = value;
            }
        }

        internal clsTipoDocumentoIndentidad Tipodocidentidad
        {
            get
            {
                return tipodocidentidad;
            }

            set
            {
                tipodocidentidad = value;
            }
        }

        public string Razonsocial
        {
            get
            {
                return razonsocial;
            }

            set
            {
                razonsocial = value;
            }
        }

        public string Documento
        {
            get
            {
                return documento;
            }

            set
            {
                documento = value;
            }
        }

        public string Direccion
        {
            get
            {
                return direccion;
            }

            set
            {
                direccion = value;
            }
        }

        public int Estado
        {
            get
            {
                return estado;
            }

            set
            {
                estado = value;
            }
        }
    }
}
