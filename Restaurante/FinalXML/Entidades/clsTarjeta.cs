﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Entidades
{
    class clsTarjeta
    {
        private int idtarjeta;
        private string descripcion;
        private int estado;

        public int Idtarjeta
        {
            get
            {
                return idtarjeta;
            }

            set
            {
                idtarjeta = value;
            }
        }

        public string Descripcion
        {
            get
            {
                return descripcion;
            }

            set
            {
                descripcion = value;
            }
        }

        public int Estado
        {
            get
            {
                return estado;
            }

            set
            {
                estado = value;
            }
        }
    }
}
