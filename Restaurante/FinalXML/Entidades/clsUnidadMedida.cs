﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Entidades
{
    public class clsUnidadMedida
    {
        private int unidadmedidaid;
        private string nombre;
        private string sigla;
        private int estado;

        public int Unidadmedidaid
        {
            get
            {
                return unidadmedidaid;
            }

            set
            {
                unidadmedidaid = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public string Sigla
        {
            get
            {
                return sigla;
            }

            set
            {
                sigla = value;
            }
        }

        public int Estado
        {
            get
            {
                return estado;
            }

            set
            {
                estado = value;
            }
        }
    }
}
