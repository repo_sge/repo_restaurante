﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Entidades
{
    public class clsIngrediente
    {
        private int idingrediente;
        private string nombre;
        private string descripcion;
        private decimal preciounitario;
        private clsUnidadMedida unidadmedida;
        private int estado;

        public int Idingrediente
        {
            get
            {
                return idingrediente;
            }

            set
            {
                idingrediente = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public string Descripcion
        {
            get
            {
                return descripcion;
            }

            set
            {
                descripcion = value;
            }
        }

        public decimal Preciounitario
        {
            get
            {
                return preciounitario;
            }

            set
            {
                preciounitario = value;
            }
        }

        public int Estado
        {
            get
            {
                return estado;
            }

            set
            {
                estado = value;
            }
        }

        public clsUnidadMedida Unidadmedida
        {
            get
            {
                return unidadmedida;
            }

            set
            {
                unidadmedida = value;
            }
        }
    }
}
