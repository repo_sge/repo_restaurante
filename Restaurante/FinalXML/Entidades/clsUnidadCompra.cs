﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Entidades
{
    public class clsUnidadCompra
    {
        private int idunidadcompra;
        private clsIngrediente ingrediente;
        private clsUnidadMedida unidadmedida;
        private decimal equivalencia;
        private int estado;

        public int Idunidadcompra
        {
            get
            {
                return idunidadcompra;
            }

            set
            {
                idunidadcompra = value;
            }
        }

        public clsIngrediente Ingrediente
        {
            get
            {
                return ingrediente;
            }

            set
            {
                ingrediente = value;
            }
        }

        public clsUnidadMedida Unidadmedida
        {
            get
            {
                return unidadmedida;
            }

            set
            {
                unidadmedida = value;
            }
        }

        public decimal Equivalencia
        {
            get
            {
                return equivalencia;
            }

            set
            {
                equivalencia = value;
            }
        }

        public int Estado
        {
            get
            {
                return estado;
            }

            set
            {
                estado = value;
            }
        }
    }
}
