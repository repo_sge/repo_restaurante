﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Entidades
{
    class clsSerieFactura
    {
        private int idserie;
        private string tipodocumento;
        private string nombredocumento;
        private string serie;
        private int correlativo;
        private int estado;

        public int Idserie
        {
            get
            {
                return idserie;
            }

            set
            {
                idserie = value;
            }
        }

        public string Tipodocumento
        {
            get
            {
                return tipodocumento;
            }

            set
            {
                tipodocumento = value;
            }
        }

        public string Nombredocumento
        {
            get
            {
                return nombredocumento;
            }

            set
            {
                nombredocumento = value;
            }
        }

        public string Serie
        {
            get
            {
                return serie;
            }

            set
            {
                serie = value;
            }
        }

        public int Estado
        {
            get
            {
                return estado;
            }

            set
            {
                estado = value;
            }
        }

        public int Correlativo
        {
            get
            {
                return correlativo;
            }

            set
            {
                correlativo = value;
            }
        }
    }
}
