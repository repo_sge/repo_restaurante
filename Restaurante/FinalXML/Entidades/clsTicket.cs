﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Entidades
{
    public class clsTicket
    {
        private int idticket;
        private DateTime fecha;
        private clsPedido pedido;
        private string numero;
        private int estado;

        public int Idticket
        {
            get
            {
                return idticket;
            }

            set
            {
                idticket = value;
            }
        }

        public DateTime Fecha
        {
            get
            {
                return fecha;
            }

            set
            {
                fecha = value;
            }
        }

        public clsPedido Pedido
        {
            get
            {
                return pedido;
            }

            set
            {
                pedido = value;
            }
        }

        public string Numero
        {
            get
            {
                return numero;
            }

            set
            {
                numero = value;
            }
        }

        public int Estado
        {
            get
            {
                return estado;
            }

            set
            {
                estado = value;
            }
        }
    }
}
