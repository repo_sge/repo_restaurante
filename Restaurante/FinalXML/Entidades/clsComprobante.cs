﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Entidades
{
   public class clsComprobante
    {
        private int idcomprobante;
        private clsPedido pedido;
        private clsSerieFactura seriefactura;
        private string numero;
        private DateTime fechaemsion;
        private clsCliente cliente;
        private clsTipoVenta tipoventa;
        private clsTipoOperacion tipooperacion;
        private string descripcion;
        private clsMoneda moneda;
        private decimal descuento;
        private decimal subtotal;
        private decimal igv;
        private decimal total;
        private decimal gravada;
        private decimal exonerada;
        private decimal inafecta;
        private decimal gratuita;
        private DateTime fechapago;
        private int situacionpago;
        private decimal totalcancelado;
        private clsComprobante comprobanterelacionado;
        private clsUsuario usuario;
        private int estado;
        private List<clsDetalleComprobante> lista_detallecomprobante;
        private clsCaja caja;

        public int Idcomprobante
        {
            get
            {
                return idcomprobante;
            }

            set
            {
                idcomprobante = value;
            }
        }

        public clsPedido Pedido
        {
            get
            {
                return pedido;
            }

            set
            {
                pedido = value;
            }
        }

        internal clsSerieFactura Seriefactura
        {
            get
            {
                return seriefactura;
            }

            set
            {
                seriefactura = value;
            }
        }

        public string Numero
        {
            get
            {
                return numero;
            }

            set
            {
                numero = value;
            }
        }

        public DateTime Fechaemsion
        {
            get
            {
                return fechaemsion;
            }

            set
            {
                fechaemsion = value;
            }
        }

        public clsCliente Cliente
        {
            get
            {
                return cliente;
            }

            set
            {
                cliente = value;
            }
        }

        internal clsTipoVenta Tipoventa
        {
            get
            {
                return tipoventa;
            }

            set
            {
                tipoventa = value;
            }
        }

        internal clsTipoOperacion Tipooperacion
        {
            get
            {
                return tipooperacion;
            }

            set
            {
                tipooperacion = value;
            }
        }

        public string Descripcion
        {
            get
            {
                return descripcion;
            }

            set
            {
                descripcion = value;
            }
        }

        public clsMoneda Moneda
        {
            get
            {
                return moneda;
            }

            set
            {
                moneda = value;
            }
        }

        public decimal Descuento
        {
            get
            {
                return descuento;
            }

            set
            {
                descuento = value;
            }
        }

        public decimal Subtotal
        {
            get
            {
                return subtotal;
            }

            set
            {
                subtotal = value;
            }
        }

        public decimal Igv
        {
            get
            {
                return igv;
            }

            set
            {
                igv = value;
            }
        }

        public decimal Total
        {
            get
            {
                return total;
            }

            set
            {
                total = value;
            }
        }

        public decimal Gravada
        {
            get
            {
                return gravada;
            }

            set
            {
                gravada = value;
            }
        }

        public decimal Exonerada
        {
            get
            {
                return exonerada;
            }

            set
            {
                exonerada = value;
            }
        }

        public decimal Inafecta
        {
            get
            {
                return inafecta;
            }

            set
            {
                inafecta = value;
            }
        }

        public decimal Gratuita
        {
            get
            {
                return gratuita;
            }

            set
            {
                gratuita = value;
            }
        }

        public DateTime Fechapago
        {
            get
            {
                return fechapago;
            }

            set
            {
                fechapago = value;
            }
        }

        public int Situacionpago
        {
            get
            {
                return situacionpago;
            }

            set
            {
                situacionpago = value;
            }
        }

        public decimal Totalcancelado
        {
            get
            {
                return totalcancelado;
            }

            set
            {
                totalcancelado = value;
            }
        }

        internal clsComprobante Comprobanterelacionado
        {
            get
            {
                return comprobanterelacionado;
            }

            set
            {
                comprobanterelacionado = value;
            }
        }

        public clsUsuario Usuario
        {
            get
            {
                return usuario;
            }

            set
            {
                usuario = value;
            }
        }

        public int Estado
        {
            get
            {
                return estado;
            }

            set
            {
                estado = value;
            }
        }

        internal List<clsDetalleComprobante> Lista_detallecomprobante
        {
            get
            {
                return lista_detallecomprobante;
            }

            set
            {
                lista_detallecomprobante = value;
            }
        }

        internal clsCaja Caja
        {
            get
            {
                return caja;
            }

            set
            {
                caja = value;
            }
        }
    }
}
