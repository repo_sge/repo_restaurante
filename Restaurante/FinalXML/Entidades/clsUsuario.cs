﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Entidades
{
    public class clsUsuario
    {
        private int idusuario;
        private clsTipoUsuario tipousuario;
        private string nombreyapellido;
        private string documentoidentidad;
        private string telefono;
        private string cuenta;
        private string clave;
        private int estado;

       

        internal clsTipoUsuario Tipousuario
        {
            get
            {
                return tipousuario;
            }

            set
            {
                tipousuario = value;
            }
        }

        public string Nombreyapellido
        {
            get
            {
                return nombreyapellido;
            }

            set
            {
                nombreyapellido = value;
            }
        }

        public string Documentoidentidad
        {
            get
            {
                return documentoidentidad;
            }

            set
            {
                documentoidentidad = value;
            }
        }

        public string Cuenta
        {
            get
            {
                return cuenta;
            }

            set
            {
                cuenta = value;
            }
        }

        public string Clave
        {
            get
            {
                return clave;
            }

            set
            {
                clave = value;
            }
        }

        public int Estado
        {
            get
            {
                return estado;
            }

            set
            {
                estado = value;
            }
        }

        public string Telefono
        {
            get
            {
                return telefono;
            }

            set
            {
                telefono = value;
            }
        }

        public int Idusuario
        {
            get
            {
                return idusuario;
            }

            set
            {
                idusuario = value;
            }
        }
    }
}
