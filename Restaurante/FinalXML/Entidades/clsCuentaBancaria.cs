﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Entidades
{
    class clsCuentaBancaria
    {
        private int idcuentabancaria;
        private clsBanco banco;
        private clsTipoCuentaBancaria tipocuenta;
        private clsMoneda moneda;
        private string numero;
        private int estado;

        public int Idcuentabancaria
        {
            get
            {
                return idcuentabancaria;
            }

            set
            {
                idcuentabancaria = value;
            }
        }

        internal clsBanco Banco
        {
            get
            {
                return banco;
            }

            set
            {
                banco = value;
            }
        }

        internal clsTipoCuentaBancaria Tipocuenta
        {
            get
            {
                return tipocuenta;
            }

            set
            {
                tipocuenta = value;
            }
        }

        public clsMoneda Moneda
        {
            get
            {
                return moneda;
            }

            set
            {
                moneda = value;
            }
        }

        public string Numero
        {
            get
            {
                return numero;
            }

            set
            {
                numero = value;
            }
        }

        public int Estado
        {
            get
            {
                return estado;
            }

            set
            {
                estado = value;
            }
        }
    }
}
