﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Entidades
{
    public class clsCobro
    {
        private int idcobro;
        private DateTime fechacobro;
        private clsComprobante comprobante;
        private clsTipoCobro tipocobro;
        private clsTarjeta tarjetacobro;
        private clsBanco banco;
        private clsCuentaBancaria cuentabancaria;
        private string noperacion;
        private clsComprobante notacredito;
        private decimal monto;
        private clsCaja caja;
        private string observacion;
        private int estado;

        public int Idcobro
        {
            get
            {
                return idcobro;
            }

            set
            {
                idcobro = value;
            }
        }

        public DateTime Fechacobro
        {
            get
            {
                return fechacobro;
            }

            set
            {
                fechacobro = value;
            }
        }

        public clsComprobante Comprobante
        {
            get
            {
                return comprobante;
            }

            set
            {
                comprobante = value;
            }
        }

        internal clsTipoCobro Tipocobro
        {
            get
            {
                return tipocobro;
            }

            set
            {
                tipocobro = value;
            }
        }

        internal clsTarjeta Tarjetacobro
        {
            get
            {
                return tarjetacobro;
            }

            set
            {
                tarjetacobro = value;
            }
        }

        internal clsBanco Banco
        {
            get
            {
                return banco;
            }

            set
            {
                banco = value;
            }
        }

        internal clsCuentaBancaria Cuentabancaria
        {
            get
            {
                return cuentabancaria;
            }

            set
            {
                cuentabancaria = value;
            }
        }

        public string Noperacion
        {
            get
            {
                return noperacion;
            }

            set
            {
                noperacion = value;
            }
        }

        public clsComprobante Notacredito
        {
            get
            {
                return notacredito;
            }

            set
            {
                notacredito = value;
            }
        }

        public decimal Monto
        {
            get
            {
                return monto;
            }

            set
            {
                monto = value;
            }
        }

        internal clsCaja Caja
        {
            get
            {
                return caja;
            }

            set
            {
                caja = value;
            }
        }

        public string Observacion
        {
            get
            {
                return observacion;
            }

            set
            {
                observacion = value;
            }
        }

        public int Estado
        {
            get
            {
                return estado;
            }

            set
            {
                estado = value;
            }
        }
    }
}
