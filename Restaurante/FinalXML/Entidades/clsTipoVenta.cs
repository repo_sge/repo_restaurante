﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Entidades
{
    class clsTipoVenta
    {
        private int idtipoventa;
        private string descripcion;
        private int estado;

        

        public string Descripcion
        {
            get
            {
                return descripcion;
            }

            set
            {
                descripcion = value;
            }
        }

        public int Estado
        {
            get
            {
                return estado;
            }

            set
            {
                estado = value;
            }
        }

        public int Idtipoventa
        {
            get
            {
                return idtipoventa;
            }

            set
            {
                idtipoventa = value;
            }
        }
    }
}
