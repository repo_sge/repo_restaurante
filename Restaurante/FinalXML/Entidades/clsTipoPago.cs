﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Entidades
{
    public class clsTipoPago
    {
        private int idtipopago;
        private string descripcion;
        private int numerodias;
        private int estado;

        public int Idtipopago
        {
            get
            {
                return idtipopago;
            }

            set
            {
                idtipopago = value;
            }
        }

        public string Descripcion
        {
            get
            {
                return descripcion;
            }

            set
            {
                descripcion = value;
            }
        }

        public int Numerodias
        {
            get
            {
                return numerodias;
            }

            set
            {
                numerodias = value;
            }
        }

        public int Estado
        {
            get
            {
                return estado;
            }

            set
            {
                estado = value;
            }
        }
    }
}
