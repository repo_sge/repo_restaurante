﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Entidades
{
    public class clsPedido
    {
        private int idpedido;
        private DateTime fechapedido;
        private clsUsuario usuario;
        private clsMesa mesa;
        private List<clsDetallePedido> lista_detallepedido;
        private int estado;

        public int Idpedido
        {
            get
            {
                return idpedido;
            }

            set
            {
                idpedido = value;
            }
        }

        public DateTime Fechapedido
        {
            get
            {
                return fechapedido;
            }

            set
            {
                fechapedido = value;
            }
        }

        public clsUsuario Usuario
        {
            get
            {
                return usuario;
            }

            set
            {
                usuario = value;
            }
        }

        internal clsMesa Mesa
        {
            get
            {
                return mesa;
            }

            set
            {
                mesa = value;
            }
        }

        public int Estado
        {
            get
            {
                return estado;
            }

            set
            {
                estado = value;
            }
        }

        internal List<clsDetallePedido> Lista_detallepedido
        {
            get
            {
                return lista_detallepedido;
            }

            set
            {
                lista_detallepedido = value;
            }
        }
    }
}
