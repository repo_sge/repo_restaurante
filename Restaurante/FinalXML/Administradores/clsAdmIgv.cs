﻿using FinalXML.Entidades;
using FinalXML.Interfaces;
using FinalXML.InterMySql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Administradores
{
    class clsAdmIgv
    {
        private IIgv iig = new MysqlIgv();
        public clsIgv listar_igv_anual() {

            return iig.listar_igv_anual();
        }
    }
}
