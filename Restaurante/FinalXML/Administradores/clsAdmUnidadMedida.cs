﻿using FinalXML.Entidades;
using FinalXML.Interfaces;
using FinalXML.InterMySql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Administradores
{
    class clsAdmUnidadMedida
    {
        private IUnidadMedida iunime = new MysqlUnidadMedida();
        public int registrar_unidadmedida(clsUnidadMedida unidadmedida, clsUsuario usureg) {

            return iunime.registrar_unidadmedida(unidadmedida, usureg);
        }

        public int actualizar_unidadmedida(clsUnidadMedida unidadmedida, clsUsuario usureg) {

            return iunime.actualizar_unidadmedida(unidadmedida, usureg);
        }

        public DataTable listar_unidadmedida()
        {
            return iunime.listar_unidadmedida();
        }

        public List<clsUnidadMedida> listar_unidadmedida_xestado() {

            return iunime.listar_unidadmedida_xestado();
        }

      }
}
