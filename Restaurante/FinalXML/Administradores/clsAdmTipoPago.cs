﻿using FinalXML.Entidades;
using FinalXML.Interfaces;
using FinalXML.InterMySql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Administradores
{
    class clsAdmTipoPago
    {
        private ITipoPago itipopag = new MysqlTipoPago();

        public int registrar_tipo_pago(clsTipoPago tipopago, clsUsuario usureg) {

            return itipopag.registrar_tipo_pago(tipopago, usureg);
        }

        public int actualizar_tipo_pago(clsTipoPago tipopago, clsUsuario usureg)
        {
            return itipopag.actualizar_tipo_pago(tipopago, usureg);
        }

        public DataTable listar_tipo_pago() {

            return itipopag.listar_tipo_pago();
        }

        public List<clsTipoPago> listar_tipo_pago_estado() {

            return itipopag.listar_tipo_pago_estado();
        }
    }
}
