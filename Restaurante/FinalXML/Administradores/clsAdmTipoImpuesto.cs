﻿using FinalXML.Entidades;
using FinalXML.Interfaces;
using FinalXML.InterMySql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Administradores
{
    class clsAdmTipoImpuesto
    {
        private ITipoImpuesto itipo = new MysqlTipoImpuesto();
        public List<clsTipoImpuesto> listar_tipoimpuesto_xestado() {

            return itipo.listar_tipoimpuesto_xestado();
        }
    }
}
