﻿using FinalXML.Entidades;
using FinalXML.Interfaces;
using FinalXML.InterMySql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Administradores
{
    class clsAdmPedido
    {
        private IPedido ipe = new MysqlPedido();
        public clsPedido listar_idpedidoxmesa(clsMesa mesa) {

            return ipe.listar_idpedidoxmesa(mesa);
        }

        public int registrar_pedido(clsPedido pedido, clsUsuario usureg) {

            return ipe.registrar_pedido(pedido, usureg);
        }

        public clsPedido listar_pedido_xidpedido(clsPedido pedido) {

            return ipe.listar_pedido_xidpedido(pedido);
        }

        public int registrar_detallepedido(clsPedido pedido, clsUsuario usureg) {

            return ipe.registrar_detallepedido(pedido, usureg);
        }

        public int actualizar_detallepedido(clsPedido pedido, clsUsuario usureg) {

            return ipe.actualizar_detallepedido(pedido, usureg);
        }

        public int eliminar_detallepedido(clsPedido pedido, clsUsuario usureg) {

            return ipe.eliminar_detallepedido(pedido, usureg);
        }

        public int actualizar_pedidoydetalle_anular(clsPedido pedido, clsUsuario usureg) {

            return ipe.actualizar_pedidoydetalle_anular(pedido, usureg);
        }

        public int actualizar_detallepedido_parallevar(clsPedido pedido, clsUsuario usureg)
        {
            return ipe.actualizar_detallepedido_parallevar(pedido, usureg);
        }

        public clsPedido listar_pedido_xidpedidoxestado(clsPedido pedido) {

            return ipe.listar_pedido_xidpedidoxestado(pedido);
        }

        /***************************Ordenes de Cocina*****************************/
         public DataTable listar_idordencocina() {

            return ipe.listar_idordencocina();
        }
        public DataTable listar_detalle_ordencocina(int idordencocina) {

            return ipe.listar_detalle_ordencocina(idordencocina);
        }
        public int actualizar_detalleordencocina_atendido(clsPedido pedido, clsUsuario usureg) {

            return ipe.actualizar_detalleordencocina_atendido(pedido, usureg);
        }
        public string listar_descripcion_xiddetallepedido(clsDetallePedido detallepedido) {

            return ipe.listar_descripcion_xiddetallepedido(detallepedido);
        }

        public int actualizar_descripcion_xdetallepedidoid(clsDetallePedido detallepedido, clsUsuario usureg) {

            return ipe.actualizar_descripcion_xdetallepedidoid(detallepedido, usureg);
        }

        /**********************Comandas***********************/
        public DataSet imprimir_comanda(clsPedido pedido) {

            return ipe.imprimir_comanda(pedido);
        }
    }
}
