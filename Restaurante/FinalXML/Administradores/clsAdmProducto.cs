﻿using FinalXML.Entidades;
using FinalXML.Interfaces;
using FinalXML.InterMySql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Administradores
{
    class clsAdmProducto
    {
        private IProducto ipro = new MysqlProducto();

        public int actualizar_producto(clsProducto producto, clsUsuario usureg) {

            return ipro.actualizar_producto(producto, usureg);
        }
        public DataTable listar_producto() {

            return ipro.listar_producto();
        }

        public DataTable listar_productoxnombre(clsProducto producto) {

            return ipro.listar_productoxnombre(producto);
        }

        public clsProducto listar_productoxid(int id)
        {
            return null;
        }

        public DataTable listar_productoxnombreestado(clsProducto producto) {

            return ipro.listar_productoxnombreestado(producto);
        }

        public int registrar_producto(clsProducto producto, clsUsuario usureg) {

            return ipro.registrar_producto(producto,usureg);
        }

        public DataTable listar_productoxcategorianombre(clsProducto producto) {

            return ipro.listar_productoxcategorianombre(producto);
        }

        public int producto_paracocina(clsProducto producto) {

            return ipro.producto_paracocina(producto);
        }

        public DataTable listar_codproductosunatxdesc(String desc)
        {
            return ipro.listar_codproductosunatxdesc(desc);
        }

        public int validaProductoBolsa()
        {

            return ipro.validaProductoBolsa();
        }
    }
}
