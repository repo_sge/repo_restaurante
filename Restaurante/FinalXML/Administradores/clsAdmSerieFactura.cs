﻿using FinalXML.Entidades;
using FinalXML.Interfaces;
using FinalXML.InterMySql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Administradores
{
    class clsAdmSerieFactura
    {
        private ISerieFactura iser = new MysqlSerieFactura();

        public int actualizar_seriexnumeracion(clsSerieFactura serie, clsUsuario usureg) {

            return iser.actualizar_seriexnumeracion(serie, usureg);
        }

        public DataTable listar_seriefacturacion() {

            return iser.listar_seriefacturacion();
        }

        public List<clsSerieFactura> _listar_seriefacturacion() {

            return iser._listar_seriefacturacion();
        }
    }
}
