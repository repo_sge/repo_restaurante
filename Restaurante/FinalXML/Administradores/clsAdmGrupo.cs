﻿using FinalXML.Entidades;
using FinalXML.Interfaces;
using FinalXML.InterMySql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Administradores
{
    class clsAdmGrupo
    {
        private IGrupo igru = new MysqlGrupo();
        public int registrar_grupo(clsUsuario usureg) {

            return igru.registrar_grupo(usureg);
        }

        public DataTable listar_grupoxestado() {

            return igru.listar_grupoxestado();
        }

        public int registrar_mesagrupo(clsGrupo grupo, clsMesa mesa, clsUsuario usureg) {

            return igru.registrar_mesagrupo(grupo, mesa, usureg);
        }

        public int eliminar_mesagrupo(clsGrupo grupo, clsUsuario usureg) {

            return igru.eliminar_mesagrupo(grupo, usureg);
        }
    }
}
