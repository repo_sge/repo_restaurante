﻿using FinalXML.Entidades;
using FinalXML.Interfaces;
using FinalXML.InterMySql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Administradores
{
    class clsAdmTicket
    {
        private ITicket itick = new MysqlTicket();

        public int registrar_ticket(clsPedido pedido, clsUsuario usureg) {

            return itick.registrar_ticket(pedido, usureg);
        }

        public clsTicket listar_ticket_xid(clsTicket ticket) {

            return itick.listar_ticket_xid(ticket);
        }

        public int anular_ticket(clsTicket ticket, clsUsuario usureg) {

            return itick.anular_ticket(ticket, usureg);
        }

        public DataTable listar_ticket_generado() {

            return itick.listar_ticket_generado(); 
        }

        public DataSet imprimir_ticket(clsTicket ticket) {

            return itick.imprimir_ticket(ticket);
        }

        public DataTable ticket_afacturar(clsTicket ticket) {

            return itick.ticket_afacturar(ticket);
        }

        public int actualizar_estadoticket_anofacturado(clsTicket ticket, clsUsuario usureg) {

            return itick.actualizar_estadoticket_anofacturado(ticket, usureg);
        }

        public DataTable listar_ticket_nofacturado(DateTime fecha) {

            return itick.listar_ticket_nofacturado(fecha);
        }

        public DataTable listar_tickets_xmesa(clsTicket ticket, clsMesa mesa, DateTime fechaini, DateTime fechafin) {

            return itick.listar_tickets_xmesa(ticket, mesa, fechaini, fechafin);
        }

        public DataTable listar_tickets(DateTime fechaini, DateTime fechafin) {

            return itick.listar_tickets(fechaini, fechafin);
        }

        public int eliminar_ticket_nofacturado(clsTicket ticket, clsUsuario usureg) {

            return itick.eliminar_ticket_nofacturado(ticket, usureg);
        }

        public int actualizar_ticket_xidcomprobante(clsComprobante comprobante, clsUsuario usureg) {

            return itick.actualizar_ticket_xidcomprobante(comprobante, usureg);
        }

        public DataTable listar_tickets_xnumero(clsTicket ticket) {

            return itick.listar_tickets_xnumero(ticket);
        }

        public int actualizar_ticket(clsTicket ticket, clsUsuario usureg) {

            return itick.actualizar_ticket(ticket, usureg);
        }
    }
}
