﻿using FinalXML.Entidades;
using FinalXML.Interfaces;
using FinalXML.InterMySql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Administradores
{
    class clsAdmComprobante
    {
        private IComprobante icom = new MysqlComprobante();

        public int registrar_comprobante(clsComprobante comprobante, clsUsuario usureg) {

            return icom.registrar_comprobante(comprobante, usureg);
        }
        public string listar_numerocomprobante_xidcomprobante(clsComprobante comprobante) {

            return icom.listar_numerocomprobante_xidcomprobante(comprobante);

        }
        public DateTime listar_fecha_actual() {

            return icom.listar_fecha_actual();
        }
        public DataSet imprimir_comprobante(clsComprobante comprobante) {

            return icom.imprimir_comprobante(comprobante);
        }

        public DataTable listar_ventas_xestado_xfecha(DateTime fechaincio, DateTime fechafin, int estado) {

            return icom.listar_ventas_xestado_xfecha(fechaincio, fechafin, estado);
        }

        public clsComprobante listar_comprobante_xid(clsComprobante comprobante) {

            return icom.listar_comprobante_xid(comprobante);
        }

        public DataTable listar_detalle_comprobante_xidcomprobante(clsComprobante comprobante) {

            return icom.listar_detalle_comprobante_xidcomprobante(comprobante);
        }

        public int anular_comprobante(clsComprobante comprobante, clsUsuario usureg) {

            return icom.anular_comprobante(comprobante, usureg);
        }

        public DataTable listar_notas_xestado_xfecha(DateTime fechaincio, DateTime fechafin, int estado) {

            return icom.listar_notas_xestado_xfecha(fechaincio, fechafin, estado);
        }

        public DataTable listar_ventas_xestado_xfecha_xcomprobante(DateTime fechaincio, DateTime fechafin, int estado, clsComprobante comprobante) {

            return icom.listar_ventas_xestado_xfecha_xcomprobante(fechaincio, fechafin, estado, comprobante);
        }

        /****************Notas de credito******************/
        public DataTable listar_notas_xestado_xcliente(clsCliente cliente) {

            return icom.listar_notas_xestado_xcliente(cliente);
        }

        /*****************Ventas***************************/
        public DataTable listar_ventas_mesas_xmozo(DateTime fechaincio, DateTime fechafin) {

            return icom.listar_ventas_mesas_xmozo(fechaincio, fechafin);
        }

        public DataSet reporte_ventas_mesas_xmozo(DateTime fechaincio, DateTime fechafin) {

            return icom.reporte_ventas_mesas_xmozo(fechaincio, fechafin);
        }
    }
}
