﻿using FinalXML.Entidades;
using FinalXML.Interfaces;
using FinalXML.InterMySql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Administradores
{
     class clsAdmTipoOperacion
    {
        private ITipoOperacion itipoo = new MysqlTipoOperacion();
        public List<clsTipoOperacion> listar_tipooeracion_xestado() {

            return itipoo.listar_tipooeracion_xestado();
        }

        public clsTipoOperacion listar_tipooeracion_xid(int id)
        {
            return itipoo.listar_tipooeracion_xid(id);
        }
    }
}
