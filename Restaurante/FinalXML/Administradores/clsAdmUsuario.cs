﻿using FinalXML.Entidades;
using FinalXML.Interfaces;
using FinalXML.InterMySql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Administradores
{
    class clsAdmUsuario
    {
        private IUsuario iusu = new MysqlUsuario();

        public clsUsuario buscar_usuarioxdocumento(clsUsuario usuario) {

            return iusu.buscar_usuarioxdocumento(usuario);
        }

        public int registrar_usuario(clsUsuario usuario, clsUsuario usureg) {

            return iusu.registrar_usuario(usuario, usureg);
        }

        public int actualizar_usuario(clsUsuario usuario, clsUsuario usureg)
        {
            return iusu.actualizar_usuario(usuario, usureg);
        }

        public DataTable listar_usuario() {

            return iusu.listar_usuario();
        }

        public DataTable buscar_usuarioxnombreapellido(clsUsuario usuario) {

            return iusu.buscar_usuarioxnombreapellido(usuario);
        }

        public clsUsuario buscar_usuarioxcuentaclave(clsUsuario usuario) {

            return iusu.buscar_usuarioxcuentaclave(usuario);
        }
        public clsUsuario buscar_usuarioxcuentaclave_autorizado(clsUsuario usuario) {

            return iusu.buscar_usuarioxcuentaclave_autorizado(usuario);
        }
    }
}
