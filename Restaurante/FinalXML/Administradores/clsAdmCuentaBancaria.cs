﻿using FinalXML.Entidades;
using FinalXML.Interfaces;
using FinalXML.InterMySql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Administradores
{
    class clsAdmCuentaBancaria
    {
        private ICuentaBancaria icue = new MysqlCuentaBancaria();

        public int registrar_cuenta(clsCuentaBancaria cuenta, clsUsuario usureg) {

            return icue.registrar_cuenta(cuenta, usureg);
        }

        public int actualizar_cuenta(clsCuentaBancaria cuenta, clsUsuario usureg) {

            return icue.actualizar_cuenta(cuenta, usureg);
        }

        public DataTable listar_cuenta() {

            return icue.listar_cuenta();
        }

        public DataTable listar_cuenta_xbanco(clsBanco banco) {

            return icue.listar_cuenta_xbanco(banco);
        }

        public List<clsCuentaBancaria> listar_cuenta_banco_xestado() {

            return icue.listar_cuenta_banco_xestado();
        }

        public List<clsCuentaBancaria> listar_cuenta_x_banco(clsBanco banco) {

            return icue.listar_cuenta_x_banco(banco);
        }
    }
}
