﻿using FinalXML.Entidades;
using FinalXML.Interfaces;
using FinalXML.InterMySql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Administradores
{
    class clsAdmBanco
    {
        private IBanco iban = new MysqlBanco();

        public int registrar_banco(clsBanco banco, clsUsuario usureg) {

            return iban.registrar_banco(banco, usureg);
        }

        public int actualizar_banco(clsBanco banco, clsUsuario usureg)
        {
            return iban.actualizar_banco(banco, usureg);
        }
        public DataTable listar_banco() {

            return iban.listar_banco();
        }

        public DataTable listar_banco_xnombre(clsBanco banco) {

            return iban.listar_banco_xnombre(banco);
        }

        public List<clsBanco> listar_banco_xestado() {

            return iban.listar_banco_xestado();
        }
    }
}
