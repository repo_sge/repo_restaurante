﻿using FinalXML.Entidades;
using FinalXML.Interfaces;
using FinalXML.InterMySql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Administradores
{
    class clsAdmTipoCuentaBancaria
    {
        private ITipoCuentaBancaria iticu = new MysqlTipoCuentaBancaria();

        public int registrar_tipocuenta(clsTipoCuentaBancaria tipocuenta, clsUsuario usureg)
        {
            return iticu.registrar_tipocuenta(tipocuenta, usureg);
        }
        public int actualizar_tipocuenta(clsTipoCuentaBancaria tipocuenta, clsUsuario usureg) {

            return iticu.actualizar_tipocuenta(tipocuenta, usureg);
        }
        public DataTable listar_tipocuenta() {

            return iticu.listar_tipocuenta();
        }
        public DataTable listar_tipocuenta_xdescripcion(clsTipoCuentaBancaria tipocuenta) {

            return iticu.listar_tipocuenta_xdescripcion(tipocuenta);
        }
        public List<clsTipoCuentaBancaria> listar_tipocuenta_xestado() {

            return iticu.listar_tipocuenta_xestado();
        }
    }
}
