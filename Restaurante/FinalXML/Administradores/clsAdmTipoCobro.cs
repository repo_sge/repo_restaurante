﻿using FinalXML.Entidades;
using FinalXML.Interfaces;
using FinalXML.InterMySql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Administradores
{
    class clsAdmTipoCobro
    {
        private ITipoCobro itico = new MysqlTipoCobro();

        public int existe_sigla(clsTipoCobro tipocobro)
        {
            return itico.existe_sigla(tipocobro);
        }

        public int registrar_tipo_cobro(clsTipoCobro tipocobro, clsUsuario usureg) {

            return itico.registrar_tipo_cobro(tipocobro, usureg);
        }

        public int actualizar_tipo_cobro(clsTipoCobro tipocobro, clsUsuario usureg) {

            return itico.actualizar_tipo_cobro(tipocobro, usureg);
        }

        public DataTable listar_tipocobro() {

            return itico.listar_tipocobro();
        }

        public DataTable listar_tipocobro_xnombre(clsTipoCobro tipocobro) {

            return itico.listar_tipocobro_xnombre(tipocobro);
        }

        public List<clsTipoCobro> listar_tipo_cobro() {

            return itico.listar_tipo_cobro();
        }

        public List<clsTipoCobro> listar_tipocobro_xestado() {

            return itico.listar_tipocobro_xestado();
        }
    }
}
