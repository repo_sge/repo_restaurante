﻿using FinalXML.Entidades;
using FinalXML.InterMySql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Administradores
{
    class clsAdmDiscrepancia
    {
        private IDiscrepancia idis = new MysqlDiscrepancia();

        public List<clsDiscrepancia> listar_discrepancia_ncc() {

            return idis.listar_discrepancia_ncc();
        }
    }
}
