﻿using FinalXML.Entidades;
using FinalXML.Interfaces;
using FinalXML.InterMySql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Administradores
{
    class clsAdmTipoDocumentoIdentidad
    {
        private ITipoDocumentoIndentidad itipodociden = new MysqlTipoDocumentoIdentidad();

        public List<clsTipoDocumentoIndentidad> listar_tipo_documento_identidad()
        {
            return itipodociden.listar_tipo_documento_identidad();
        }
    }
}
