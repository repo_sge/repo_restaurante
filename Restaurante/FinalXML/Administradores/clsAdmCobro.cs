﻿using FinalXML.Entidades;
using FinalXML.Interfaces;
using FinalXML.InterMySql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Administradores
{
    class clsAdmCobro
    {
        private ICobro ico = new MysqlCobro();
        public int registrar_cobro(clsCobro cobro, clsUsuario usureg) {

            return ico.registrar_cobro(cobro, usureg);
        }

        public int anular_cobro(clsCobro cobro, clsUsuario usureg) {

            return ico.anular_cobro(cobro, usureg);
        }
    }
}
