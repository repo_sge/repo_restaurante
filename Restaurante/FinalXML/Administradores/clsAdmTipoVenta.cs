﻿using FinalXML.Entidades;
using FinalXML.Interfaces;
using FinalXML.InterMySql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Administradores
{
    class clsAdmTipoVenta
    {
        private ITipoVenta itipv = new MysqlTipoVenta();

        public List<clsTipoVenta> listar_tipoventa() {

            return itipv.listar_tipoventa();
        }
    }
}
