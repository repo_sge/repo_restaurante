﻿using FinalXML.Entidades;
using FinalXML.Interfaces;
using FinalXML.InterMySql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Administradores
{
    class clsAdmIngrediente
    {
        private IIngrediente iing = new MysqlIngrediente();
        public int registrar_ingrediente(clsIngrediente ingrediente, clsUsuario usureg) {

            return iing.registrar_ingrediente(ingrediente, usureg);
        }

        public int actualizar_ingrediente(clsIngrediente ingrediente, clsUsuario usureg) {

            return iing.actualizar_ingrediente(ingrediente, usureg);
        }

        public DataTable listar_ingrediente() {

            return iing.listar_ingrediente();
        }

        public DataTable listar_ingrediente_xnombre(clsIngrediente ingrediente) {

            return iing.listar_ingrediente_xnombre(ingrediente);
        }

        public DataTable listar_ingrediente_xnombre_xestado(clsIngrediente ingrediente) {

            return iing.listar_ingrediente_xnombre_xestado(ingrediente);
        }
    }
}
