﻿using FinalXML.Entidades;
using FinalXML.Interfaces;
using FinalXML.InterMySql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Administradores
{
    class clsAdmAcceso
    {
        private IAcceso iacc = new MysqlAcceso();

        public int registrar_acceso(clsAcceso acceso) {

            return iacc.registrar_acceso(acceso);
        }
    }
}
