﻿using FinalXML.Entidades;
using FinalXML.Interfaces;
using FinalXML.InterMySql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Administradores
{
    class clsAdmProveedor
    {
        private IProveedor iprovee = new MysqlProveedor();

        public int registrar_proveedor(clsProveedor proveedor, clsUsuario usureg) {

            return iprovee.registrar_proveedor(proveedor, usureg);
        }

        public int actualizar_proveedor(clsProveedor proveedor, clsUsuario usureg)
        {

            return iprovee.actualizar_proveedor(proveedor, usureg);
        }

        public DataTable listar_proveedor() {

            return iprovee.listar_proveedor();
        }

        public DataTable listar_proveedor_xrazonsocial(clsProveedor proveedor) {

            return iprovee.listar_proveedor_xrazonsocial(proveedor);
        }
    }

}
