﻿using FinalXML.Entidades;
using FinalXML.Interfaces;
using FinalXML.InterMySql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Administradores
{
    class clsAdmMesa
    {
        private IMesa ime = new MysqlMesa();

        public int registrar_mesa(clsMesa mesa, clsUsuario usureg) {

            return ime.registrar_mesa(mesa, usureg);
        }

        public int actualizar_mesa(clsMesa mesa, clsUsuario usureg) {

            return ime.actualizar_mesa(mesa, usureg);
        }

        public DataTable listar_mesa() {

            return ime.listar_mesa();
        }

        public List<clsMesa> listar_mesaxestado(clsPiso piso) {

            return ime.listar_mesaxestado(piso);
        }

        public List<clsMesa> listar_mesa_noenmesagrupo(clsPiso piso) {

            return ime.listar_mesa_noenmesagrupo(piso);
        }

        public DataTable listar_mesaxgrupo(clsGrupo grupo, clsPiso piso) {

            return ime.listar_mesaxgrupo(grupo, piso);
        }

        public DataTable listar_mesaxgruposinpedido(clsGrupo grupo, clsPiso piso) {

            return ime.listar_mesaxgruposinpedido(grupo, piso);
        }

        public int cambiar_mesa(clsMesa mesa, clsPedido pedido, clsUsuario usureg) {

            return ime.cambiar_mesa(mesa, pedido, usureg);
        }

        public DataTable listar_grupodemesas(clsMesa mesa) {

            return ime.listar_grupodemesas(mesa);
        }
    }
}
