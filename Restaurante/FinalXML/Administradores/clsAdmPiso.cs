﻿using FinalXML.Entidades;
using FinalXML.Interfaces;
using FinalXML.InterMySql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Administradores
{
    class clsAdmPiso
    {
        private IPiso ipi = new MysqlPiso();

        public int registrar_piso(clsPiso piso, clsUsuario usureg) {

            return ipi.registrar_piso(piso, usureg);
        }

        public int actualizar_piso(clsPiso piso, clsUsuario usureg) {

            return ipi.actualizar_piso(piso, usureg);
        }

        public DataTable listar_piso() {

            return ipi.listar_piso();
        }

        public List<clsPiso> listar_piso_xestado() {

            return ipi.listar_piso_xestado();
        }
    }
}
