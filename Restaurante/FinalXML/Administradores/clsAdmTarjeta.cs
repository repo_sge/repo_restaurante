﻿using FinalXML.Entidades;
using FinalXML.Interfaces;
using FinalXML.InterMySql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Administradores
{
    class clsAdmTarjeta
    {
        private ITarjeta itar = new MysqlTarjeta();

        public int registrar_tarjeta_cobro(clsTarjeta tarjetacobro, clsUsuario usureg) {

            return itar.registrar_tarjeta_cobro(tarjetacobro, usureg);
        }

        public int actualizar_tarjeta_cobro(clsTarjeta tarjetacobro, clsUsuario usureg) {

            return itar.actualizar_tarjeta_cobro(tarjetacobro, usureg);
        }

        public DataTable listar_tarjeta_cobro() {

            return itar.listar_tarjeta_cobro();
        }

        public List<clsTarjeta> listar_tarjetacobro() {

            return itar.listar_tarjetacobro();
        }
    }
}
