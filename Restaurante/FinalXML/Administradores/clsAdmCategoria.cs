﻿using FinalXML.Entidades;
using FinalXML.Interfaces;
using FinalXML.InterMySql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Administradores
{
    class clsAdmCategoria
    {
        private ICategoria icat = new MysqlCategoria();

        public int registrar_categoria(clsCategoria categoria, clsUsuario usureg) {

            return icat.registrar_categoria(categoria, usureg);
        }

        public int actualizar_categoria(clsCategoria categoria, clsUsuario usureg) {

            return icat.actualizar_categoria(categoria, usureg);
        }

        public List<clsCategoria> listar_categoria() {

            return icat.listar_categoria();
        }

        public DataTable _listar_categoria() {

            return icat._listar_categoria();
        }
    }
}
