﻿using FinalXML.Entidades;
using FinalXML.Interfaces;
using FinalXML.InterMySql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Administradores
{
    class clsAdmUnidadCompra
    {
        private IUnidadCompra iunico = new MysqlUnidadCompra();

        public int registrar_unidad_compra(clsUnidadCompra unidadmedidacompra, clsUsuario usureg) {

            return iunico.registrar_unidad_compra(unidadmedidacompra, usureg);
        }

        public int actualizar_unidad_compra(clsUnidadCompra unidadmedidacompra, clsUsuario usureg) {

            return iunico.actualizar_unidad_compra(unidadmedidacompra, usureg);
        }

        public DataTable listar_unidad_compra_xingrediente(clsIngrediente ingrediente) {

            return iunico.listar_unidad_compra_xingrediente(ingrediente);
        }

        public List<clsUnidadCompra> listar_unidad_compra_xingrediente_xestado(clsIngrediente ingrediente) {

            return iunico.listar_unidad_compra_xingrediente_xestado(ingrediente);
        }
    }
}
