﻿using FinalXML.Entidades;
using FinalXML.Interfaces;
using FinalXML.InterMySql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Administradores
{
    class clsAdmCaja
    {
        private ICaja ica = new MysqlCaja();

        public int buscar_caja_abierta(clsUsuario usuario) {

            return ica.buscar_caja_abierta(usuario);
        }

        public int registrar_caja(clsCaja caja, clsUsuario usureg) {

            return ica.registrar_caja(caja, usureg);
        }

        public DataTable listar_caja_apertura() {

            return ica.listar_caja_apertura();
        }

        public int anular_caja(clsCaja caja, clsUsuario usureg) {

            return ica.anular_caja(caja, usureg);
        }

        public DataTable listar_caja_movimiento(clsCaja caja) {

            return ica.listar_caja_movimiento(caja);
        }

        public clsCaja total_caja(clsCaja caja) {

            return ica.total_caja(caja);
        }

        public clsCaja buscar_caja_cerrada(clsCaja caja) {

            return ica.buscar_caja_cerrada(caja);
        }

        public int cerrar_caja(clsCaja caja, clsUsuario usureg) {

            return ica.cerrar_caja(caja, usureg);
        }

        public DataSet reporte_caja_movimiento(clsCaja caja) {

            return ica.reporte_caja_movimiento(caja);
        }

        public DataTable listar_caja_cerrada(DateTime fechaini, DateTime fechafin) {

            return ica.listar_caja_cerrada(fechaini, fechafin);
        }

        public DataTable listar_movimiento_caja_xcomprobante(clsCaja caja, clsComprobante comprobante) {

            return ica.listar_movimiento_caja_xcomprobante(caja, comprobante);
        }

        public int abrir_caja(clsCaja caja, clsUsuario usureg) {

            return ica.abrir_caja(caja, usureg);
        }
        
    }
}
