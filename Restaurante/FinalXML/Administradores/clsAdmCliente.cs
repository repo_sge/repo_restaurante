﻿using FinalXML.Entidades;
using FinalXML.Interfaces;
using FinalXML.InterMySql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Administradores
{
    class clsAdmCliente
    {
        private ICliente icli = new MysqlCliente();

        public clsCliente buscar_clientexnumerodocumento(clsCliente cliente) {

            return icli.buscar_clientexnumerodocumento(cliente);
        }
        public int registrar_cliente(clsCliente cliente, clsUsuario usureg) {

            return icli.registrar_cliente(cliente, usureg);
        }

        public int actualizar_cliente(clsCliente cliente, clsUsuario usureg) {

            return icli.actualizar_cliente(cliente, usureg);
        }

        public DataTable buscar_clientexnombreyapellido(clsCliente cliente) {

            return icli.buscar_clientexnombreyapellido(cliente);
        }

        public DataTable listar_cliente() {

            return icli.listar_cliente();
        }
    }
}
