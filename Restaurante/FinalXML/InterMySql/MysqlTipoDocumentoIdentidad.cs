﻿using FinalXML.Conexion;
using FinalXML.Interfaces;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinalXML.Entidades;

namespace FinalXML.InterMySql
{
    class MysqlTipoDocumentoIdentidad : ITipoDocumentoIndentidad
    {
        private clsConexionMysql con = new clsConexionMysql();
        private MySqlCommand cmd = null;
        private MySqlDataReader dr = null;
        private MySqlDataAdapter adap = null;
        private MySqlTransaction tra = null;
        private DataTable tabla = null;

        public List<clsTipoDocumentoIndentidad> listar_tipo_documento_identidad()
        {
            List<clsTipoDocumentoIndentidad> lista_tipodociden = null;
            clsTipoDocumentoIndentidad tipodociden = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("listar_tipo_documento_identidad", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    lista_tipodociden = new List<clsTipoDocumentoIndentidad>();

                    while (dr.Read())
                    {
                        tipodociden = new clsTipoDocumentoIndentidad()
                        {
                            Idtipodocumentoidentidad = (int)dr["idtipodocumentoidentidad"],
                            Descripcion = (string)dr["descripcion"],
                            Codsunat= (string)dr["codsunat"]
                        };

                        lista_tipodociden.Add(tipodociden);
                    }
                    dr.Close();
                }
                return lista_tipodociden;
            }
            catch (MySqlException ex)
            {
                return lista_tipodociden;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }
    }
}
