﻿using FinalXML.Conexion;
using FinalXML.Interfaces;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinalXML.Entidades;

namespace FinalXML.InterMySql
{
    class MysqlTicket : ITicket
    {
        private clsConexionMysql con = new clsConexionMysql();
        private MySqlCommand cmd = null;
        private MySqlDataReader dr = null;
        private MySqlDataAdapter adap = null;
        private MySqlTransaction tra = null;
        private DataTable tabla = null;
        private DataSet data = null;
        private Herramientas her = new Herramientas();
        private string ruta = "";

        public int registrar_ticket(clsPedido pedido, clsUsuario usureg)
        {
            int id = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("registrar_ticket", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;
                cmd.Parameters.Add(new MySqlParameter("@_idpedido", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));               

                cmd.Parameters[0].Value = pedido.Idpedido;               
                cmd.Parameters[1].Value = usureg.Idusuario;

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        id = Convert.ToInt32(dr["_id"]);
                    }
                    dr.Close();
                }
                tra.Commit(); tra.Dispose();
                return id;
            }
            catch (MySqlException ex)
            {
                id = -1;
                tra.Rollback();
                return id;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public clsTicket listar_ticket_xid(clsTicket ticket)
        {
            clsTicket tick = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("listar_ticket_xid", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_idticket", MySqlDbType.Int32));
                cmd.Parameters[0].Value = ticket.Idticket;
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        tick = new clsTicket()
                        {
                            Idticket = (int)dr["idticket"],
                            Fecha=(DateTime)dr["fecha"],
                            Pedido=new clsPedido() {
                                Idpedido= (int)dr["idpedido"]
                            },
                            Numero=(string)dr["numero"],
                            Estado=(int)dr["estado"]                            
                        };

                    }
                    dr.Close();
                }
                return tick;
            }
            catch (MySqlException ex)
            {
                return tick;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public int anular_ticket(clsTicket ticket, clsUsuario usureg)
        {
            int filas_afectadas = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("anular_ticket", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;
                cmd.Parameters.Add(new MySqlParameter("@_idticket", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));

                cmd.Parameters[0].Value = ticket.Idticket;               
                cmd.Parameters[1].Value = usureg.Idusuario;

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        filas_afectadas = Convert.ToInt32(dr["_filas_afectadas"]);
                    }
                    dr.Close();
                }
                tra.Commit(); tra.Dispose();
                return filas_afectadas;
            }
            catch (MySqlException ex)
            {
                filas_afectadas = -1;
                tra.Rollback();
                return filas_afectadas;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable listar_ticket_generado()
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_ticket_generado", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;            
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }

        public DataSet imprimir_ticket(clsTicket ticket)
        {
            try
            {
                con.conectarBD();
                data = new DataSet();               
                cmd = new MySqlCommand("imprimir_ticket", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;           
                cmd.Parameters.Add(new MySqlParameter("@_idticket", MySqlDbType.Int32));       
                cmd.Parameters[0].Value = ticket.Idticket;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(data, "comprobante_ticket");

                ruta = her.GetResourcesPath4()+ "\\ticket.xml";
                data.WriteXml(ruta, XmlWriteMode.WriteSchema);

                return data;
            }
            catch (MySqlException ex)
            {                
                return data;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable ticket_afacturar(clsTicket ticket)
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("ticket_afacturar", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_idticket", MySqlDbType.Int32));
                cmd.Parameters[0].Value = ticket.Idticket;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }

        public int actualizar_estadoticket_anofacturado(clsTicket ticket, clsUsuario usureg)
        {
            int filas_afectadas = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("actualizar_estadoticket_anofacturado", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;
                cmd.Parameters.Add(new MySqlParameter("@_estado", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idticket", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));

                cmd.Parameters[0].Value = ticket.Estado;
                cmd.Parameters[1].Value = ticket.Idticket;
                cmd.Parameters[2].Value = usureg.Idusuario;

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        filas_afectadas = Convert.ToInt32(dr["_filas_afectadas"]);
                    }
                    dr.Close();
                }
                tra.Commit(); tra.Dispose();
                return filas_afectadas;
            }
            catch (MySqlException ex)
            {
                filas_afectadas = -1;
                tra.Rollback();
                return filas_afectadas;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable listar_ticket_nofacturado(DateTime fecha)    
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_ticket_nofacturado", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_fecha", MySqlDbType.Date));
                cmd.Parameters[0].Value = fecha;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }

        public DataTable listar_tickets_xmesa(clsTicket ticket, clsMesa mesa, DateTime fechaini, DateTime fechafin)
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_tickets_xmesa", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new MySqlParameter("@_estado", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idmesa", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_fechaini", MySqlDbType.Date));
                cmd.Parameters.Add(new MySqlParameter("@_fechafin", MySqlDbType.Date));

                cmd.Parameters[0].Value = ticket.Estado;
                cmd.Parameters[1].Value = mesa.Idmesa;
                cmd.Parameters[2].Value = fechaini;
                cmd.Parameters[3].Value = fechafin;

                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }

        public DataTable listar_tickets(DateTime fechaini, DateTime fechafin)
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_tickets", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
         
                cmd.Parameters.Add(new MySqlParameter("@_fechaini", MySqlDbType.Date));
                cmd.Parameters.Add(new MySqlParameter("@_fechafin", MySqlDbType.Date));
              
                cmd.Parameters[0].Value = fechaini;
                cmd.Parameters[1].Value = fechafin;

                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }

        public int eliminar_ticket_nofacturado(clsTicket ticket, clsUsuario usureg)
        {
            int filas_afectadas = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("eliminar_ticket_nofacturado", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;
       
                cmd.Parameters.Add(new MySqlParameter("@_idticket", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));
            
                cmd.Parameters[0].Value = ticket.Idticket;
                cmd.Parameters[1].Value = usureg.Idusuario;

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        filas_afectadas = Convert.ToInt32(dr["_filas_afectadas"]);
                    }
                    dr.Close();
                }
                tra.Commit(); tra.Dispose();
                return filas_afectadas;
            }
            catch (MySqlException ex)
            {
                filas_afectadas = -1;
                tra.Rollback();
                return filas_afectadas;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public int actualizar_ticket_xidcomprobante(clsComprobante comprobante, clsUsuario usureg)
        {
            int filas_afectadas = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("actualizar_ticket_xidcomprobante", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;
                cmd.Parameters.Add(new MySqlParameter("@_idcomprobante", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));

                cmd.Parameters[0].Value = comprobante.Idcomprobante;
                cmd.Parameters[1].Value = usureg.Idusuario;

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        filas_afectadas = Convert.ToInt32(dr["_filas_afectadas"]);
                    }
                    dr.Close();
                }
                tra.Commit(); tra.Dispose();
                return filas_afectadas;
            }
            catch (MySqlException ex)
            {
                filas_afectadas = -1;
                tra.Rollback();
                return filas_afectadas;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable listar_tickets_xnumero(clsTicket ticket)
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_tickets_xnumero", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_numero", MySqlDbType.VarChar));
                cmd.Parameters[0].Value = ticket.Numero;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }

        public int actualizar_ticket(clsTicket ticket, clsUsuario usureg)
        {
            int filas_afectadas = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("actualizar_ticket", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;

                cmd.Parameters.Add(new MySqlParameter("@_idticket", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));

                cmd.Parameters[0].Value = ticket.Idticket;
                cmd.Parameters[1].Value = usureg.Idusuario;

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        filas_afectadas = Convert.ToInt32(dr["_filas_afectadas"]);
                    }
                    dr.Close();
                }
                tra.Commit(); tra.Dispose();
                return filas_afectadas;
            }
            catch (MySqlException ex)
            {
                filas_afectadas = -1;
                tra.Rollback();
                return filas_afectadas;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }
    }
}
