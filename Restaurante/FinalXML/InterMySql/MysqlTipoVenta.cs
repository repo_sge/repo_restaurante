﻿using FinalXML.Conexion;
using FinalXML.Interfaces;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinalXML.Entidades;

namespace FinalXML.InterMySql
{
    class MysqlTipoVenta:ITipoVenta
    {
        private clsConexionMysql con = new clsConexionMysql();
        private MySqlCommand cmd = null;
        private MySqlDataReader dr = null;
        private MySqlDataAdapter adap = null;
        private MySqlTransaction tra = null;
        private DataTable tabla = null;

        public List<clsTipoVenta> listar_tipoventa()
        {
            List<clsTipoVenta> lista_tipoventa = null;
            clsTipoVenta tipov = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("listar_tipoventa", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;              
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    lista_tipoventa = new List<clsTipoVenta>();

                    while (dr.Read())
                    {
                        tipov = new clsTipoVenta
                        {
                          Idtipoventa=(int)dr["idtipoventa"],
                          Descripcion=(string)dr["descripcion"],
                          Estado=(int)dr["estado"]
                        };

                        lista_tipoventa.Add(tipov);
                    }
                    dr.Close();
                }
                return lista_tipoventa;
            }
            catch (MySqlException ex)
            {
                return lista_tipoventa;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }
    }
}
