﻿using FinalXML.Conexion;
using FinalXML.Interfaces;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinalXML.Entidades;

namespace FinalXML.InterMySql
{
    class MysqlUnidadCompra:IUnidadCompra
    {
        private clsConexionMysql con = new clsConexionMysql();
        private MySqlCommand cmd = null;
        private MySqlDataReader dr = null;
        private MySqlDataAdapter adap = null;
        private MySqlTransaction tra = null;
        private DataTable tabla = null;

        public int registrar_unidad_compra(clsUnidadCompra unidadmedidacompra, clsUsuario usureg)
        {
            int id = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("registrar_unidad_compra", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;
                cmd.Parameters.Add(new MySqlParameter("@_idingrediente", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_unidadmedidaid", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_equivalencia", MySqlDbType.Decimal));
                cmd.Parameters.Add(new MySqlParameter("@_estado", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));

                cmd.Parameters[0].Value = unidadmedidacompra.Ingrediente.Idingrediente;
                cmd.Parameters[1].Value = unidadmedidacompra.Unidadmedida.Unidadmedidaid;
                cmd.Parameters[2].Value = unidadmedidacompra.Equivalencia;
                cmd.Parameters[3].Value = unidadmedidacompra.Estado;
                cmd.Parameters[4].Value = usureg.Idusuario;

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        id = Convert.ToInt32(dr["_id"]);
                    }
                    dr.Close();
                }
                tra.Commit(); tra.Dispose();
                return id;
            }
            catch (MySqlException ex)
            {
                id = -1;
                tra.Rollback();
                return id;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public int actualizar_unidad_compra(clsUnidadCompra unidadmedidacompra, clsUsuario usureg)
        {
            int filas_afectadas = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("actualizar_unidad_compra", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;
                cmd.Parameters.Add(new MySqlParameter("@_idunidadcompra", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idingrediente", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_unidadmedidaid", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_equivalencia", MySqlDbType.Decimal));
                cmd.Parameters.Add(new MySqlParameter("@_estado", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));

                cmd.Parameters[0].Value = unidadmedidacompra.Idunidadcompra;
                cmd.Parameters[1].Value = unidadmedidacompra.Ingrediente.Idingrediente;
                cmd.Parameters[2].Value = unidadmedidacompra.Unidadmedida.Unidadmedidaid;
                cmd.Parameters[3].Value = unidadmedidacompra.Equivalencia;
                cmd.Parameters[4].Value = unidadmedidacompra.Estado;
                cmd.Parameters[5].Value = usureg.Idusuario;

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        filas_afectadas = Convert.ToInt32(dr["_filas_afectadas"]);
                    }
                    dr.Close();
                }
                tra.Commit(); tra.Dispose();
                return filas_afectadas;
            }
            catch (MySqlException ex)
            {
                filas_afectadas = -1;
                tra.Rollback();
                return filas_afectadas;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable listar_unidad_compra_xingrediente(clsIngrediente ingrediente)
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_unidad_compra_xingrediente", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_idingrediente", MySqlDbType.Int32));
                cmd.Parameters[0].Value = ingrediente.Idingrediente;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }

        public List<clsUnidadCompra> listar_unidad_compra_xingrediente_xestado(clsIngrediente ingrediente)
        {
            List<clsUnidadCompra> lista_unidadcompra = null;
            clsUnidadCompra unidadcompra = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("listar_unidad_compra_xingrediente_xestado", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_idingrediente", MySqlDbType.Int32));
                cmd.Parameters[0].Value = ingrediente.Idingrediente;
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    lista_unidadcompra = new List<clsUnidadCompra>();

                    while (dr.Read())
                    {
                        unidadcompra = new clsUnidadCompra
                        {
                            Idunidadcompra = (int)dr["idunidadcompra"],
                            Ingrediente= new clsIngrediente() {
                                Idingrediente = (int)dr["idingrediente"],
                            },
                            Unidadmedida=new clsUnidadMedida() {
                                Unidadmedidaid= (int)dr["unidadmedidaid"],
                                Sigla= (string)dr["sigla"],
                            },
                            Equivalencia= (decimal)dr["equivalencia"],
                            Estado= (int)dr["estado"],
                        };

                        lista_unidadcompra.Add(unidadcompra);
                    }
                    dr.Close();
                }
                return lista_unidadcompra;
            }
            catch (MySqlException ex)
            {
                return lista_unidadcompra;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }
    }
}
