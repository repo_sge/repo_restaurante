﻿using FinalXML.Conexion;
using FinalXML.Interfaces;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinalXML.Entidades;

namespace FinalXML.InterMySql
{
    class MysqlCobro:ICobro
    {
        private clsConexionMysql con = new clsConexionMysql();
        private MySqlCommand cmd = null;
        private MySqlDataReader dr = null;
        private MySqlDataAdapter adap = null;
        private MySqlTransaction tra = null;
        private DataTable tabla = null;

        public int registrar_cobro(clsCobro cobro, clsUsuario usureg)
        {
            int id = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("registrar_cobro", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;
                cmd.Parameters.Add(new MySqlParameter("@_fechacobro", MySqlDbType.Date));
                cmd.Parameters.Add(new MySqlParameter("@_comprobanteid", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_tipocobroid", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_tarjetaid", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_bancoid", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_cuentaid", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_noperacion", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_notacreditoid", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_monto", MySqlDbType.Decimal));
                cmd.Parameters.Add(new MySqlParameter("@_cajaid", MySqlDbType.Int32));    
                cmd.Parameters.Add(new MySqlParameter("@_observacion", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_estado", MySqlDbType.Int32));      
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));

                cmd.Parameters[0].Value = cobro.Fechacobro;
                cmd.Parameters[1].Value = cobro.Comprobante.Idcomprobante;
                cmd.Parameters[2].Value = cobro.Tipocobro.Idtipocobro;
                cmd.Parameters[3].Value = cobro.Tarjetacobro.Idtarjeta;
                cmd.Parameters[4].Value = cobro.Banco.Idbanco;
                cmd.Parameters[5].Value = cobro.Cuentabancaria.Idcuentabancaria;
                cmd.Parameters[6].Value = cobro.Noperacion;
                cmd.Parameters[7].Value = cobro.Notacredito.Idcomprobante;
                cmd.Parameters[8].Value = cobro.Monto;
                cmd.Parameters[9].Value = cobro.Comprobante.Caja.Idcaja;                
                cmd.Parameters[10].Value = cobro.Observacion;
                cmd.Parameters[11].Value = cobro.Estado;
                cmd.Parameters[12].Value = usureg.Idusuario;

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        id = Convert.ToInt32(dr["_id"]);
                    }
                    dr.Close();
                }
                tra.Commit(); tra.Dispose();
                return id;
            }
            catch (MySqlException ex)
            {
                id = -1;
                tra.Rollback();
                return id;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public int anular_cobro(clsCobro cobro, clsUsuario usureg)
        {
            int filas_afectadas = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("anular_cobro_xid", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;
                cmd.Parameters.Add(new MySqlParameter("@_idcobro", MySqlDbType.Int32));               
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));

                cmd.Parameters[0].Value = cobro.Idcobro;
                cmd.Parameters[1].Value = usureg.Idusuario;

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        filas_afectadas = Convert.ToInt32(dr["_filas_afectadas"]);
                    }
                    dr.Close();
                }
                tra.Commit(); tra.Dispose();
                return filas_afectadas;
            }
            catch (MySqlException ex)
            {
                filas_afectadas = -1;
                tra.Rollback();
                return filas_afectadas;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }
    }
}
