﻿using FinalXML.Conexion;
using FinalXML.Interfaces;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinalXML.Entidades;

namespace FinalXML.InterMySql
{
    class MysqlProveedor:IProveedor
    {
        private clsConexionMysql con = new clsConexionMysql();
        private MySqlCommand cmd = null;
        private MySqlDataReader dr = null;
        private MySqlDataAdapter adap = null;
        private MySqlTransaction tra = null;
        private DataTable tabla = null;

        public int registrar_proveedor(clsProveedor proveedor, clsUsuario usureg)
        {
            int id = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("registrar_proveedor", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;
                cmd.Parameters.Add(new MySqlParameter("@_idtipodocumentoidentidad", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_documento", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_razonsocial", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_direccion", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_descripcion", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_estado", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));

                cmd.Parameters[0].Value = proveedor.Tipodocumentoindentidad.Idtipodocumentoidentidad;
                cmd.Parameters[1].Value = proveedor.Documento;
                cmd.Parameters[2].Value = proveedor.Razonsocial;
                cmd.Parameters[3].Value = proveedor.Direccion;
                cmd.Parameters[4].Value = proveedor.Descripcion;
                cmd.Parameters[5].Value = proveedor.Estado;
                cmd.Parameters[6].Value = usureg.Idusuario;

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        id = Convert.ToInt32(dr["_id"]);
                    }
                    dr.Close();
                }
                tra.Commit(); tra.Dispose();
                return id;
            }
            catch (MySqlException ex)
            {
                id = -1;
                tra.Rollback();
                return id;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public int actualizar_proveedor(clsProveedor proveedor, clsUsuario usureg)
        {
            int filas_afectadas = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("actualizar_proveedor", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;
                cmd.Parameters.Add(new MySqlParameter("@_idproveedor", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idtipodocumentoidentidad", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_documento", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_razonsocial", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_direccion", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_descripcion", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_estado", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));

                cmd.Parameters[0].Value = proveedor.Idproveedor;
                cmd.Parameters[1].Value = proveedor.Tipodocumentoindentidad.Idtipodocumentoidentidad;
                cmd.Parameters[2].Value = proveedor.Documento;
                cmd.Parameters[3].Value = proveedor.Razonsocial;
                cmd.Parameters[4].Value = proveedor.Direccion;
                cmd.Parameters[5].Value = proveedor.Descripcion;
                cmd.Parameters[6].Value = proveedor.Estado;
                cmd.Parameters[7].Value = usureg.Idusuario;

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        filas_afectadas = Convert.ToInt32(dr["_filas_afectadas"]);
                    }
                    dr.Close();
                }
                tra.Commit(); tra.Dispose();
                return filas_afectadas;
            }
            catch (MySqlException ex)
            {
                filas_afectadas = -1;
                tra.Rollback();
                return filas_afectadas;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable listar_proveedor()
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_proveedor", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }

        public DataTable listar_proveedor_xrazonsocial(clsProveedor proveedor)
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_proveedor_xrazonsocial", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_razonsocial", MySqlDbType.VarChar));
                cmd.Parameters[0].Value = proveedor.Razonsocial;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }
    }
}
