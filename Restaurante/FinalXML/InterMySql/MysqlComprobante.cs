﻿using FinalXML.Conexion;
using FinalXML.Interfaces;
using MySql.Data.MySqlClient;
using System;

using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinalXML.Entidades;
using System.Data;

namespace FinalXML.InterMySql
{
    class MysqlComprobante:IComprobante
    {
        private clsConexionMysql con = new clsConexionMysql();
        private MySqlCommand cmd = null;
        private MySqlDataReader dr = null;
        private MySqlDataAdapter adap = null;
        private MySqlTransaction tra = null;
        private DataTable tabla = null;
        private DataSet data = null;
        private Herramientas her = new Herramientas();
        private string ruta = "";

        public int registrar_comprobante(clsComprobante comprobante, clsUsuario usureg)
        {
            int id = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("registrar_comprobante", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;
                cmd.Parameters.Add(new MySqlParameter("@_idpedido", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idtabnumeracion", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idcliente", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idtipoventa", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idtipooperacion", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_descripcion", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_idmoneda", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_descuento", MySqlDbType.Decimal));
                cmd.Parameters.Add(new MySqlParameter("@_subtotal", MySqlDbType.Decimal));
                cmd.Parameters.Add(new MySqlParameter("@_igv", MySqlDbType.Decimal));
                cmd.Parameters.Add(new MySqlParameter("@_total", MySqlDbType.Decimal));
                cmd.Parameters.Add(new MySqlParameter("@_totalgravada", MySqlDbType.Decimal));
                cmd.Parameters.Add(new MySqlParameter("@_totalexonerada", MySqlDbType.Decimal));
                cmd.Parameters.Add(new MySqlParameter("@_totalinafecta", MySqlDbType.Decimal));
                cmd.Parameters.Add(new MySqlParameter("@_totalgratuita", MySqlDbType.Decimal));
                cmd.Parameters.Add(new MySqlParameter("@_comprobanterelacionadoid", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_comprobanterelacionado", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idusureg", MySqlDbType.Int32));

                cmd.Parameters[0].Value = comprobante.Pedido.Idpedido;
                cmd.Parameters[1].Value = comprobante.Seriefactura.Idserie;
                cmd.Parameters[2].Value = comprobante.Cliente.Idcliente;
                cmd.Parameters[3].Value = comprobante.Tipoventa.Idtipoventa;
                cmd.Parameters[4].Value = comprobante.Tipooperacion.Idtipooeracion;
                cmd.Parameters[5].Value = comprobante.Descripcion;
                cmd.Parameters[6].Value = comprobante.Moneda.IcodMoneda;
                cmd.Parameters[7].Value = comprobante.Descuento;
                cmd.Parameters[8].Value = comprobante.Subtotal;
                cmd.Parameters[9].Value = comprobante.Igv;
                cmd.Parameters[10].Value = comprobante.Total;
                cmd.Parameters[11].Value = comprobante.Gravada;
                cmd.Parameters[12].Value = comprobante.Exonerada;
                cmd.Parameters[13].Value = comprobante.Inafecta;
                cmd.Parameters[14].Value = comprobante.Gratuita;
                cmd.Parameters[15].Value = comprobante.Comprobanterelacionado.Idcomprobante;
                cmd.Parameters[16].Value = comprobante.Comprobanterelacionado.Numero;
                cmd.Parameters[17].Value = comprobante.Usuario.Idusuario;
                cmd.Parameters[18].Value = usureg.Idusuario;

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        comprobante.Idcomprobante = Convert.ToInt32(dr["_id"]);
                    }
                    dr.Close();
                }              

                foreach (clsDetalleComprobante det in comprobante.Lista_detallecomprobante)
                {

                    cmd = new MySqlCommand("registrar_detallecomprobante", con.conector);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new MySqlParameter("@_idcomprobante", MySqlDbType.Int32));
                    cmd.Parameters.Add(new MySqlParameter("@_iddetallepedido", MySqlDbType.Int32));
                    cmd.Parameters.Add(new MySqlParameter("@_idimpuesto", MySqlDbType.Int32));
                    cmd.Parameters.Add(new MySqlParameter("@_subtotal", MySqlDbType.Decimal));
                    cmd.Parameters.Add(new MySqlParameter("@_igv", MySqlDbType.Decimal));
                    cmd.Parameters.Add(new MySqlParameter("@_total", MySqlDbType.Decimal));
                    cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));

                    cmd.Parameters[0].Value = comprobante.Idcomprobante;
                    cmd.Parameters[1].Value = det.Detallepedido.Iddetallepedido;
                    cmd.Parameters[2].Value = det.Tipoimpuesto.Idtipoimpuesto;
                    cmd.Parameters[3].Value = det.Subtotal;
                    cmd.Parameters[4].Value = det.Igv;
                    cmd.Parameters[5].Value = det.Total;
                    cmd.Parameters[6].Value = usureg.Idusuario;

                    dr = cmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            det.Iddetallecomprobante = Convert.ToInt32(dr["_id"]);
                        }
                        dr.Close();
                    }

                }
                tra.Commit(); tra.Dispose();
                id = comprobante.Idcomprobante;
                return id;
            }
            catch (MySqlException ex)
            {
                id = -1;
                tra.Rollback();               
                return id;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public string listar_numerocomprobante_xidcomprobante(clsComprobante comprobante)
        {
            string numero = "";
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("listar_numerocomprobante_xidcomprobante", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_idcomprobante", MySqlDbType.Int32));

                cmd.Parameters[0].Value = comprobante.Idcomprobante;
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    
                    while (dr.Read())
                    {
                        numero = (string)dr["numero"];
                    }

                    dr.Close();
                }
                return numero;
            }
            catch (MySqlException ex)
            {
                return numero;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DateTime listar_fecha_actual()
        {
            DateTime fecha = DateTime.Now;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("listar_fecha_actual", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;                
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        fecha = (DateTime)dr["_fecha_actual"];
                    }

                    dr.Close();
                }
                return fecha;
            }
            catch (MySqlException ex)
            {
                return fecha;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataSet imprimir_comprobante(clsComprobante comprobante)
        {
            try
            {
                con.conectarBD();
                data = new DataSet();
                cmd = new MySqlCommand("imprimir_comprobante", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_idcomprobante", MySqlDbType.Int32));
                cmd.Parameters[0].Value =comprobante.Idcomprobante;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(data, "comprobante_blc_fcc");

                ruta = her.GetResourcesPath4() + "\\blc_fcc.xml";
                data.WriteXml(ruta, XmlWriteMode.WriteSchema);

                return data;
            }
            catch (MySqlException ex)
            {
                return data;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable listar_ventas_xestado_xfecha(DateTime fechaincio, DateTime fechafin, int estado)
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_ventas_xestado_xfecha", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_fechainicio", MySqlDbType.Date));
                cmd.Parameters.Add(new MySqlParameter("@_fechafin", MySqlDbType.Date));
                cmd.Parameters.Add(new MySqlParameter("@_estado", MySqlDbType.Int32));
                cmd.Parameters[0].Value = fechaincio;
                cmd.Parameters[1].Value = fechafin;
                cmd.Parameters[2].Value = estado;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }

        public clsComprobante listar_comprobante_xid(clsComprobante comprobante)
        {
            clsComprobante compro = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("listar_comprobante_xid", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_idcomprobante", MySqlDbType.Int32));
                cmd.Parameters[0].Value = comprobante.Idcomprobante;
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        compro = new clsComprobante()
                        {
                            Idcomprobante = (int)dr["idcomprobante"],
                            Fechaemsion = (DateTime)dr["fechaemision"],
                            Pedido = new clsPedido() {
                                Idpedido = (int)dr["idpedido"]
                            },
                            Seriefactura = new clsSerieFactura() {
                                Idserie = (int)dr["idtabnumeracion"],
                                Nombredocumento = (string)dr["nom_documento"],
                                Tipodocumento = (string)dr["tipo_documento"],
                            },
                            Numero = (string)dr["numero"],
                            Cliente = new clsCliente() {
                                Idcliente = (int)dr["idcliente"],
                                Razonsocial = (string)dr["nombreyapellido"],
                                Documento = (string)dr["documento"],
                                Direccion = (string)dr["direccion"],
                                Tipodocidentidad=new clsTipoDocumentoIndentidad() {
                                    Codsunat = (string)dr["tipodocidentidad"]
                                }
                            },
                            Tipoventa = new clsTipoVenta() {
                                Idtipoventa = (int)dr["idtipoventa"],
                                Descripcion = (string)dr["tipoventa"]
                            },
                            Tipooperacion = new clsTipoOperacion() {
                                Idtipooeracion = (int)dr["idtipooperacion"],
                                Descripcion = (string)dr["descripcion"],
                                Codsunat = (string)dr["codsunatto"]
                            },
                            Descripcion = (string)dr["glosa"],

                            Moneda = new clsMoneda() {
                                IcodMoneda = (int)dr["idmoneda"],
                                SDescripcion = (string)dr["nombremoneda"],
                                CodSunat = (string)dr["codsunatmo"]
                            },
                            Descuento = (decimal)dr["descuento"],
                            Subtotal = (decimal)dr["subtotal"],
                            Igv = (decimal)dr["igv"],
                            Total = (decimal)dr["total"],
                            Gravada = (decimal)dr["totalgravada"],
                            Exonerada=(decimal)dr["totalexonerada"],
                            Inafecta=(decimal)dr["totalinafecta"],
                            Gratuita=(decimal)dr["totalgratuita"],
                            Totalcancelado= (decimal)dr["montocancelado"],
                            Comprobanterelacionado =new clsComprobante() {
                                Idcomprobante=(int)dr["comprobanterelacionadoid"],
                                Numero=(string)dr["comprobanterelacionado"],
                                Seriefactura=new clsSerieFactura() {
                                    Tipodocumento= (string)dr["tipodocrelacionado"]
                                }
                            },
                            Estado=(int)dr["estado"],
                            Usuario=new clsUsuario() {
                                Idusuario=(int)dr["idusuario"],
                                Cuenta=(string)dr["cuenta"]
                            }
                        };

                    }
                    dr.Close();
                }
                return compro;
            }
            catch (MySqlException ex)
            {
                return compro;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable listar_detalle_comprobante_xidcomprobante(clsComprobante comprobante)
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_detalle_comprobante_xidcomprobante", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_idcomprobante", MySqlDbType.Int32));    
                cmd.Parameters[0].Value = comprobante.Idcomprobante;        
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }

        public int anular_comprobante(clsComprobante comprobante,clsUsuario usureg)
        {
            int filas_afectadas = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("anular_comprobante", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;
                cmd.Parameters.Add(new MySqlParameter("@_idcomprobante", MySqlDbType.Int32));  
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));

                cmd.Parameters[0].Value = comprobante.Idcomprobante;  
                cmd.Parameters[1].Value = usureg.Idusuario;

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        filas_afectadas = Convert.ToInt32(dr["_filas_afectadas"]);
                    }
                    dr.Close();
                }
                tra.Commit(); tra.Dispose();
                return filas_afectadas;
            }
            catch (MySqlException ex)
            {
                filas_afectadas = -1;
                tra.Rollback();
                return filas_afectadas;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable listar_notas_xestado_xfecha(DateTime fechaincio, DateTime fechafin, int estado)
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_notas_xestado_xfecha", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_fechainicio", MySqlDbType.Date));
                cmd.Parameters.Add(new MySqlParameter("@_fechafin", MySqlDbType.Date));
                cmd.Parameters.Add(new MySqlParameter("@_estado", MySqlDbType.Int32));
                cmd.Parameters[0].Value = fechaincio;
                cmd.Parameters[1].Value = fechafin;
                cmd.Parameters[2].Value = estado;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }

        public DataTable listar_notas_xestado_xcliente(clsCliente cliente)
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_notas_xestado_xcliente", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_idcliente", MySqlDbType.Int32));
                cmd.Parameters[0].Value = cliente.Idcliente;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }

        public DataTable listar_ventas_mesas_xmozo(DateTime fechaincio, DateTime fechafin)
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_ventas_mesas_xmozo", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_fechaini", MySqlDbType.Date));
                cmd.Parameters.Add(new MySqlParameter("@_fechafin", MySqlDbType.Date));
         
                cmd.Parameters[0].Value = fechaincio;
                cmd.Parameters[1].Value = fechafin;
             
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }

        public DataSet reporte_ventas_mesas_xmozo(DateTime fechaincio, DateTime fechafin)
        {
            try
            {
                con.conectarBD();
                data = new DataSet();
                cmd = new MySqlCommand("reporte_ventas_mesas_xmozo", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_fechaini", MySqlDbType.Date));
                cmd.Parameters.Add(new MySqlParameter("@_fechafin", MySqlDbType.Date)); 
                cmd.Parameters[0].Value = fechaincio;
                cmd.Parameters[1].Value = fechafin;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(data, "reporte_ventas_mesas_xmozo");

                ruta = her.GetResourcesPath4() + "\\reporte_ventas_mesas_xmozo.xml";
                data.WriteXml(ruta, XmlWriteMode.WriteSchema);

                return data;
            }
            catch (MySqlException ex)
            {
                return data;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable listar_ventas_xestado_xfecha_xcomprobante(DateTime fechaincio, DateTime fechafin, int estado, clsComprobante comprobante)
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_ventas_xestado_xfecha_xcomprobante", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_fechainicio", MySqlDbType.Date));
                cmd.Parameters.Add(new MySqlParameter("@_fechafin", MySqlDbType.Date));
                cmd.Parameters.Add(new MySqlParameter("@_estado", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_numero", MySqlDbType.VarChar));
                cmd.Parameters[0].Value = fechaincio;
                cmd.Parameters[1].Value = fechafin;
                cmd.Parameters[2].Value = estado;
                cmd.Parameters[3].Value = comprobante.Numero;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }
    }
}
