﻿using FinalXML.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinalXML.Entidades;
using FinalXML.Conexion;
using MySql.Data.MySqlClient;
using System.Data;

namespace FinalXML.InterMySql
{
    class MysqlTipoUsuario : ITipoUsuario
    {
        private clsConexionMysql con = new clsConexionMysql();
        private MySqlCommand cmd = null;
        private MySqlDataReader dr = null;
        private MySqlDataAdapter adap = null;
        private MySqlTransaction tra = null;
        private DataTable tabla = null;

        public List<clsTipoUsuario> listar_tipousuario()
        {
            List<clsTipoUsuario> lista_tipousuario = null;
            clsTipoUsuario tipousuario = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("listar_tipousuario", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    lista_tipousuario = new List<clsTipoUsuario>();

                    while (dr.Read())
                    {
                        tipousuario = new clsTipoUsuario()
                        {
                            Idtipousuario = (int)dr["idtipousuario"],
                            Descripcion=(string)dr["descripcion"]
                        };

                        lista_tipousuario.Add(tipousuario);
                    }
                    dr.Close();
                }
                return lista_tipousuario;
            }
            catch (MySqlException ex)
            {
                return lista_tipousuario;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }
    }
}
