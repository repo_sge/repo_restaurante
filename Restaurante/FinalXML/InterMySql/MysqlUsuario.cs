﻿using FinalXML.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinalXML.Entidades;
using FinalXML.Conexion;
using MySql.Data.MySqlClient;
using System.Data;

namespace FinalXML.InterMySql
{
    class MysqlUsuario : IUsuario
    {
        private clsConexionMysql con = new clsConexionMysql();
        private MySqlCommand cmd = null;
        private MySqlDataReader dr = null;
        private MySqlDataAdapter adap = null;
        private MySqlTransaction tra = null;
        private DataTable tabla = null;
        public clsUsuario buscar_usuarioxdocumento(clsUsuario usuario)
        {    
            clsUsuario usu = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("buscar_usuarioxdocumento", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_documento", MySqlDbType.VarChar));
                cmd.Parameters[0].Value = usuario.Documentoidentidad;
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {                  
                    while (dr.Read())
                    {
                        usu = new clsUsuario()
                        {
                            Idusuario = (int)dr["idusuario"],
                            Tipousuario = new clsTipoUsuario()
                            {
                                Idtipousuario = (int)dr["idtipousuario"],
                                Descripcion = (string)dr["tipousuario"]
                            },
                            Nombreyapellido = (string)dr["nombreyapellido"],
                            Documentoidentidad = (string)dr["documentoidentidad"],
                            Telefono = (string)dr["telefono"],
                            Cuenta = (string)dr["cuenta"],
                            Clave = (string)dr["clave"]
                        };
                                              
                     }

                    dr.Close();
                }
                return usu;
            }
            catch (MySqlException ex)
            {
                return usu;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public int registrar_usuario(clsUsuario usuario, clsUsuario usureg)
        {
            int id = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("registrar_usuario", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;
                cmd.Parameters.Add(new MySqlParameter("@_idtipousuario", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_nombreyapellido", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_documentoidentidad", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_telefono", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_cuenta", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_clave", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_estado", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));         

                cmd.Parameters[0].Value = usuario.Tipousuario.Idtipousuario;
                cmd.Parameters[1].Value = usuario.Nombreyapellido;
                cmd.Parameters[2].Value = usuario.Documentoidentidad;
                cmd.Parameters[3].Value = usuario.Telefono;
                cmd.Parameters[4].Value = usuario.Cuenta;
                cmd.Parameters[5].Value = usuario.Clave;
                cmd.Parameters[6].Value = usuario.Estado;
                cmd.Parameters[7].Value = usureg.Idusuario;
               

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        id = Convert.ToInt32(dr["_id"]);
                    }
                    dr.Close();
                }
                tra.Commit(); tra.Dispose();
                return id;
            }
            catch (MySqlException ex)
            {
                id = -1;
                tra.Rollback();
                return id;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public int actualizar_usuario(clsUsuario usuario, clsUsuario usureg)
        {
            int filas_afectadas = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("actualizar_usuario", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;
                cmd.Parameters.Add(new MySqlParameter("@_idusuariomod", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idtipousuario", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_nombreyapellido", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_documentoidentidad", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_telefono", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_cuenta", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_clave", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_estado", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));

                cmd.Parameters[0].Value = usuario.Idusuario;
                cmd.Parameters[1].Value = usuario.Tipousuario.Idtipousuario;
                cmd.Parameters[2].Value = usuario.Nombreyapellido;
                cmd.Parameters[3].Value = usuario.Documentoidentidad;
                cmd.Parameters[4].Value = usuario.Telefono;
                cmd.Parameters[5].Value = usuario.Cuenta;
                cmd.Parameters[6].Value = usuario.Clave;
                cmd.Parameters[7].Value = usuario.Estado;
                cmd.Parameters[8].Value = usureg.Idusuario;

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        filas_afectadas = Convert.ToInt32(dr["_filas_afectadas"]);
                    }
                    dr.Close();
                }
                tra.Commit(); tra.Dispose();
                return filas_afectadas;
            }
            catch (MySqlException ex)
            {
                filas_afectadas = -1;
                tra.Rollback();
                return filas_afectadas;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable listar_usuario()
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_usuario", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;    
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }

        public DataTable buscar_usuarioxnombreapellido(clsUsuario usuario)
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("buscar_usuarioxnombreyapellido", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_nombreapellido", MySqlDbType.VarChar));
                cmd.Parameters[0].Value = usuario.Nombreyapellido;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }

        public clsUsuario buscar_usuarioxcuentaclave(clsUsuario usuario)
        {
            clsUsuario usu = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("buscar_usuarioxcuentaclave", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_cuenta", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_clave", MySqlDbType.VarChar));

                cmd.Parameters[0].Value = usuario.Cuenta;
                cmd.Parameters[1].Value = usuario.Clave;
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        usu = new clsUsuario()
                        {
                            Idusuario = (int)dr["idusuario"],
                            Tipousuario = new clsTipoUsuario()
                            {
                                Idtipousuario = (int)dr["idtipousuario"],
                                Descripcion = (string)dr["tipousuario"]
                            },
                            Nombreyapellido = (string)dr["nombreyapellido"],
                            Documentoidentidad = (string)dr["documentoidentidad"],
                            Telefono = (string)dr["telefono"],
                            Cuenta = (string)dr["cuenta"],
                            Clave = (string)dr["clave"]
                        };

                    }
                    dr.Close();
                }
                return usu;
            }
            catch (MySqlException ex)
            {
                return usu;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public clsUsuario buscar_usuarioxcuentaclave_autorizado(clsUsuario usuario)
        {
            clsUsuario usu = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("buscar_usuarioxcuentaclave_autorizado", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_cuenta", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_clave", MySqlDbType.VarChar));

                cmd.Parameters[0].Value = usuario.Cuenta;
                cmd.Parameters[1].Value = usuario.Clave;
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        usu = new clsUsuario()
                        {
                            Idusuario = (int)dr["idusuario"],
                            Tipousuario = new clsTipoUsuario()
                            {
                                Idtipousuario = (int)dr["idtipousuario"],
                                Descripcion = (string)dr["tipousuario"]
                            },
                            Nombreyapellido = (string)dr["nombreyapellido"],
                            Documentoidentidad = (string)dr["documentoidentidad"],
                            Telefono = (string)dr["telefono"],
                            Cuenta = (string)dr["cuenta"],
                            Clave = (string)dr["clave"]
                        };

                    }
                    dr.Close();
                }
                return usu;
            }
            catch (MySqlException ex)
            {
                return usu;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }
    }
}
