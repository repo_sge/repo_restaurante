﻿using FinalXML.Conexion;
using FinalXML.Interfaces;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinalXML.Entidades;

namespace FinalXML.InterMySql
{
    class MysqlTipoCuentaBancaria:ITipoCuentaBancaria
    {
        private clsConexionMysql con = new clsConexionMysql();
        private MySqlCommand cmd = null;
        private MySqlDataReader dr = null;
        private MySqlDataAdapter adap = null;
        private MySqlTransaction tra = null;
        private DataTable tabla = null;

        public int registrar_tipocuenta(clsTipoCuentaBancaria tipocuenta, clsUsuario usureg)
        {
            int id = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("registar_tipo_cuenta_banco", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;
                cmd.Parameters.Add(new MySqlParameter("@_descripcion", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_estado", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));

                cmd.Parameters[0].Value = tipocuenta.Descripcion;
                cmd.Parameters[1].Value = tipocuenta.Estado;
                cmd.Parameters[2].Value = usureg.Idusuario;

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        id = Convert.ToInt32(dr["_id"]);
                    }
                    dr.Close();
                }
                tra.Commit(); tra.Dispose();
                return id;
            }
            catch (MySqlException ex)
            {
                id = -1;
                tra.Rollback();
                return id;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public int actualizar_tipocuenta(clsTipoCuentaBancaria tipocuenta, clsUsuario usureg)
        {
            int filas_afectadas = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("actualizar_tipo_cuenta_banco", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;
                cmd.Parameters.Add(new MySqlParameter("@_tipocuentaid", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_descripcion", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_estado", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));

                cmd.Parameters[0].Value = tipocuenta.Idtipocuentabancaria;
                cmd.Parameters[1].Value = tipocuenta.Descripcion;
                cmd.Parameters[2].Value = tipocuenta.Estado;
                cmd.Parameters[3].Value = usureg.Idusuario;

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        filas_afectadas = Convert.ToInt32(dr["_filas_afectadas"]);
                    }
                    dr.Close();
                }
                tra.Commit(); tra.Dispose();
                return filas_afectadas;
            }
            catch (MySqlException ex)
            {
                filas_afectadas = -1;
                tra.Rollback();
                return filas_afectadas;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable listar_tipocuenta()
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_tipo_cuenta_banco", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }

        public DataTable listar_tipocuenta_xdescripcion(clsTipoCuentaBancaria tipocuenta)
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_tipo_cuenta_banco_xdescripcion", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_descripcion", MySqlDbType.VarChar));
                cmd.Parameters[0].Value = tipocuenta.Descripcion;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }

        public List<clsTipoCuentaBancaria> listar_tipocuenta_xestado()
        {
            List<clsTipoCuentaBancaria> lista_tipocuenta = null;
            clsTipoCuentaBancaria tipocuenta = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("listar_tipo_cuenta_banco_xestado", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;         
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    lista_tipocuenta = new List<clsTipoCuentaBancaria>();

                    while (dr.Read())
                    {
                        tipocuenta = new clsTipoCuentaBancaria
                        {
                            Idtipocuentabancaria = (int)dr["tipocuentaid"],
                            Descripcion = (string)dr["descripcion"],
                            Estado = (int)dr["estado"]
                         };

                        lista_tipocuenta.Add(tipocuenta);
                    }
                    dr.Close();
                }
                return lista_tipocuenta;
            }
            catch (MySqlException ex)
            {
                return lista_tipocuenta;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }
    }
}

