﻿using FinalXML.Conexion;
using FinalXML.Interfaces;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinalXML.Entidades;

namespace FinalXML.InterMySql
{
    class MysqlTipoImpuesto:ITipoImpuesto
    {
        private clsConexionMysql con = new clsConexionMysql();
        private MySqlCommand cmd = null;
        private MySqlDataReader dr = null;
        private MySqlDataAdapter adap = null;
        private MySqlTransaction tra = null;
        private DataTable tabla = null;

        public List<clsTipoImpuesto> listar_tipoimpuesto_xestado()
        {
            List<clsTipoImpuesto> lista_tipoimpuesto = null;
            clsTipoImpuesto tipoimpuesto = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("listar_tipoimpuesto_xestado", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    lista_tipoimpuesto = new List<clsTipoImpuesto>();

                    while (dr.Read())
                    {
                        tipoimpuesto = new clsTipoImpuesto()
                        {
                            Idtipoimpuesto = (int)dr["idtipoimpuesto"],
                            Codsunat = (string)dr["codsunat"],
                            Descripcion = (string)dr["descripcion"],
                            Estado= (int)dr["estado"]

                        };
                        lista_tipoimpuesto.Add(tipoimpuesto);
                    }
                    dr.Close();
                }
                return lista_tipoimpuesto;
            }
            catch (MySqlException ex)
            {
                return lista_tipoimpuesto;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }
    }
}
