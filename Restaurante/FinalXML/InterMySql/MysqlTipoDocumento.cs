﻿using FinalXML.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinalXML.Entidades;
using System.Data;
using FinalXML.Conexion;
using MySql.Data.MySqlClient;

namespace FinalXML.InterMySql
{
    class MysqlTipoDocumento : ITipoDocumento
    {
        clsConexionMysql con = new clsConexionMysql();
        MySqlCommand cmd = null;
        MySqlDataReader dr = null;
        MySqlDataAdapter adap = null;
        DataTable tabla = null;

        public clsTipoDocumento BuscaTipoDocumento(string Sigla)
        {
            throw new NotImplementedException();
        }

        public clsTipoDocumento CargaTipoDocumento(int Codigo)
        {
            clsTipoDocumento doc = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("MuestraTipoDocumento", con.conector);
                cmd.Parameters.AddWithValue("coddoc", Codigo);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        doc = new clsTipoDocumento();
                        doc.CodTipoDocumento = Convert.ToInt32(dr.GetDecimal(0));    
                        doc.Descripcion = dr.GetString(1);
                        doc.Estado = dr.GetBoolean(2);
                        doc.FechaRegistro = dr.GetDateTime(3);// capturo la fecha 
                        doc.Tipodoccodsunat = dr.GetString(4);
                    }
                }
                return doc;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable CargaTipoDocumentos()
        {
            throw new NotImplementedException();
        }

        public DataTable ListaDocumentoNota()
        {
            throw new NotImplementedException();
        }

        public DataTable ListaTipoDocumentos()
        {
            throw new NotImplementedException();
        }

        public DataTable ListaTipoDocumentosDeCaja()
        {
            throw new NotImplementedException();
        }

        public DataTable ListaTipoDocumentosElectronicos()
        {
            throw new NotImplementedException();
        }
    }
}
