﻿using FinalXML.Conexion;
using FinalXML.Interfaces;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinalXML.Entidades;

namespace FinalXML.InterMySql
{
    class MysqlPedido : IPedido
    {
        private clsConexionMysql con = new clsConexionMysql();
        private MySqlCommand cmd = null;
        private MySqlDataReader dr = null;
        private MySqlDataAdapter adap = null;
        private MySqlTransaction tra = null;
        private DataTable tabla = null;
        private DataSet data = null;
        private Herramientas her = new Herramientas();
        private string ruta = "";

        public clsPedido listar_idpedidoxmesa(clsMesa mesa)
        {
            clsPedido pedido = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("listar_idpedidoxmesa", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_idmesa", MySqlDbType.Int32));
                cmd.Parameters[0].Value = mesa.Idmesa;
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        pedido = new clsPedido()
                        {
                            Idpedido = (int)dr["idpedido"]
                        };
                    }
                    dr.Close();
                }
                return pedido;
            }
            catch (MySqlException ex)
            {
                return pedido;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public int registrar_pedido(clsPedido pedido, clsUsuario usureg)
        {
            int id = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("registrar_pedido", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idmesa", MySqlDbType.Int32));           
                cmd.Parameters.Add(new MySqlParameter("@_estado", MySqlDbType.Int32));       

                cmd.Parameters[0].Value = usureg.Idusuario;
                cmd.Parameters[1].Value = pedido.Mesa.Idmesa;
                cmd.Parameters[2].Value = pedido.Estado;            

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        id = Convert.ToInt32(dr["_id"]);
                    }
                    dr.Close();
                }

                pedido.Idpedido = id;

                foreach (clsDetallePedido det in pedido.Lista_detallepedido) {

                    cmd = new MySqlCommand("registrar_detallepedido", con.conector);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new MySqlParameter("@_idpedido", MySqlDbType.Int32));
                    cmd.Parameters.Add(new MySqlParameter("@_idproducto", MySqlDbType.Int32));
                    cmd.Parameters.Add(new MySqlParameter("@_descripcion", MySqlDbType.VarChar));
                    cmd.Parameters.Add(new MySqlParameter("@_cantidad", MySqlDbType.Decimal));
                    cmd.Parameters.Add(new MySqlParameter("@_preciounitario", MySqlDbType.Decimal));
                    cmd.Parameters.Add(new MySqlParameter("@_estado", MySqlDbType.Int32));
                    cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));

                    cmd.Parameters[0].Value = pedido.Idpedido;
                    cmd.Parameters[1].Value = det.Producto.IdProducto;
                    cmd.Parameters[2].Value = det.Descripcion;
                    cmd.Parameters[3].Value = det.Cantidad;
                    cmd.Parameters[4].Value = det.Preciounitario;
                    cmd.Parameters[5].Value = det.Estado;
                    cmd.Parameters[6].Value = usureg.Idusuario;

                    dr = cmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            id = Convert.ToInt32(dr["_id"]);
                        }
                        dr.Close();
                    }

                }
                tra.Commit(); tra.Dispose();
                id = pedido.Idpedido;
                return id;
            }
            catch (MySqlException ex)
            {
                id = -1;
                tra.Rollback();               
                return id;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public clsPedido listar_pedido_xidpedido(clsPedido pedido)
        {
            clsPedido pedi = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("listar_pedido_xidpedido", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_idpedido", MySqlDbType.Int32));        

                cmd.Parameters[0].Value = pedido.Idpedido;         
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    pedi = new clsPedido();
                    pedi.Lista_detallepedido = new List<clsDetallePedido>();

                    while (dr.Read())
                    {
                        pedi.Idpedido = (int)dr["idpedido"];
                        pedi.Estado=(int)dr["estadope"];

                        pedi.Lista_detallepedido.Add( 
                            new clsDetallePedido() {

                                Iddetallepedido=(int)dr["iddetallepedido"],
                                Producto=new clsProducto() {

                                    IdProducto=(int)dr["idproducto"],
                                    Nombre=(string)dr["nombreproducto"],
                                },
                                Cantidad=(decimal)dr["cantidad"],
                                Preciounitario=(decimal)dr["preciounitario"],
                                Estado=(int)dr["estadodet"],
                                Parallevar=(int)dr["parallevar"],
                                Atendido= int.Parse(dr["atendido"].ToString())
                            }
                        );
                    }
                    dr.Close();
                }
                return pedi;
            }
            catch (MySqlException ex)
            {
                return pedi;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public int registrar_detallepedido(clsPedido pedido, clsUsuario usureg)
        {
            int id = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();

                foreach (clsDetallePedido det in pedido.Lista_detallepedido)
                {
                    cmd = new MySqlCommand("registrar_detallepedido", con.conector);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Transaction = tra;
                    cmd.Parameters.Add(new MySqlParameter("@_idpedido", MySqlDbType.Int32));
                    cmd.Parameters.Add(new MySqlParameter("@_idproducto", MySqlDbType.Int32));
                    cmd.Parameters.Add(new MySqlParameter("@_descripcion", MySqlDbType.VarChar));
                    cmd.Parameters.Add(new MySqlParameter("@_cantidad", MySqlDbType.Decimal));
                    cmd.Parameters.Add(new MySqlParameter("@_preciounitario", MySqlDbType.Decimal));
                    cmd.Parameters.Add(new MySqlParameter("@_estado", MySqlDbType.Int32));
                    cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));

                    cmd.Parameters[0].Value = pedido.Idpedido;
                    cmd.Parameters[1].Value = det.Producto.IdProducto;
                    cmd.Parameters[2].Value = det.Descripcion;
                    cmd.Parameters[3].Value = det.Cantidad;
                    cmd.Parameters[4].Value = det.Preciounitario;
                    cmd.Parameters[5].Value = det.Estado;
                    cmd.Parameters[6].Value = usureg.Idusuario;

                    dr = cmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            id = Convert.ToInt32(dr["_id"]);
                        }
                        dr.Close();
                    }

                }
                tra.Commit(); tra.Dispose();               
                return id;
            }
            catch (MySqlException ex)
            {
                id = -1;
                tra.Rollback();
                return id;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public int actualizar_detallepedido(clsPedido pedido, clsUsuario usureg)
        {
            int filas_afectadas = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();

                foreach (clsDetallePedido det in pedido.Lista_detallepedido)
                {
                    cmd = new MySqlCommand("actualizar_detallepedido", con.conector);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Transaction = tra;
                    cmd.Parameters.Add(new MySqlParameter("@_iddetallepedido", MySqlDbType.Int32));
                    cmd.Parameters.Add(new MySqlParameter("@_idpedido", MySqlDbType.Int32));
                    cmd.Parameters.Add(new MySqlParameter("@_cantidad", MySqlDbType.Decimal));                  
                    cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));

                    cmd.Parameters[0].Value = det.Iddetallepedido;
                    cmd.Parameters[1].Value = pedido.Idpedido;
                    cmd.Parameters[2].Value = det.Cantidad;            
                    cmd.Parameters[3].Value = usureg.Idusuario;

                    dr = cmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            filas_afectadas = Convert.ToInt32(dr["_filas_afectadas"]);
                        }
                        dr.Close();
                    }

                }
                tra.Commit(); tra.Dispose();
                return filas_afectadas;
            }
            catch (MySqlException ex)
            {
                filas_afectadas = -1;
                tra.Rollback();               
                return filas_afectadas;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public int eliminar_detallepedido(clsPedido pedido, clsUsuario usureg)
        {
            int filas_afectadas = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();

                foreach (clsDetallePedido det in pedido.Lista_detallepedido)
                {
                    cmd = new MySqlCommand("eliminar_detallepedido", con.conector);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Transaction = tra;                   
                    cmd.Parameters.Add(new MySqlParameter("@_idpedido", MySqlDbType.Int32));
                    cmd.Parameters.Add(new MySqlParameter("@_iddetallepedido", MySqlDbType.Int32));
                    cmd.Parameters.Add(new MySqlParameter("@_estado", MySqlDbType.Int32));
                    cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));
                    

                    cmd.Parameters[0].Value = pedido.Idpedido;
                    cmd.Parameters[1].Value = det.Iddetallepedido;                  
                    cmd.Parameters[2].Value = pedido.Estado;
                    cmd.Parameters[3].Value = usureg.Idusuario;

                    dr = cmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            filas_afectadas = Convert.ToInt32(dr["_filas_afectadas"]);
                        }
                        dr.Close();
                    }

                }
                tra.Commit(); tra.Dispose();
                return filas_afectadas;
            }
            catch (MySqlException ex)
            {
                filas_afectadas = -1;
                tra.Rollback();                
                return filas_afectadas;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public int actualizar_pedidoydetalle_anular(clsPedido pedido, clsUsuario usureg)
        {
            int filas_afectadas = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("actualizar_pedidoydetalle_anular", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;
                cmd.Parameters.Add(new MySqlParameter("@_idpedido", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));

                cmd.Parameters[0].Value = pedido.Idpedido;
                cmd.Parameters[1].Value = usureg.Idusuario;

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        filas_afectadas = Convert.ToInt32(dr["_filas_afectadas"]);
                    }
                    dr.Close();
                }
                tra.Commit(); tra.Dispose();
                return filas_afectadas;
            }
            catch (MySqlException ex)
            {
                filas_afectadas = -1;
                tra.Rollback();
                return filas_afectadas;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }


        /***********************Orden de Cocina*********************************/

        public DataTable listar_idordencocina()
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_idordencocina", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;               
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }

        public DataTable listar_detalle_ordencocina(int idordencocina)
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_detalle_ordencocina", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_idordencocina", MySqlDbType.Int32));
                cmd.Parameters[0].Value = idordencocina;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }

        public int actualizar_detallepedido_parallevar(clsPedido pedido, clsUsuario usureg)
        {
            int filas_afectadas = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();

                foreach (clsDetallePedido det in pedido.Lista_detallepedido)
                {
                    cmd = new MySqlCommand("actualizar_detallepedido_parallevar", con.conector);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Transaction = tra;
                    cmd.Parameters.Add(new MySqlParameter("@_iddetallepedido", MySqlDbType.Int32));
                    cmd.Parameters.Add(new MySqlParameter("@_parallevar", MySqlDbType.Int32));   
                    cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));

                    cmd.Parameters[0].Value = det.Iddetallepedido;
                    cmd.Parameters[1].Value = det.Parallevar;  
                    cmd.Parameters[2].Value = usureg.Idusuario;

                    dr = cmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            filas_afectadas = Convert.ToInt32(dr["_filas_afectadas"]);
                        }
                        dr.Close();
                    }

                }
                tra.Commit(); tra.Dispose();
                return filas_afectadas;
            }
            catch (MySqlException ex)
            {
                filas_afectadas = -1;
                tra.Rollback();               
                return filas_afectadas;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public int actualizar_detalleordencocina_atendido(clsPedido pedido, clsUsuario usureg)
        {
            int filas_afectadas = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();

                foreach (clsDetallePedido det in pedido.Lista_detallepedido)
                {
                    cmd = new MySqlCommand("actualizar_detalleordencocina_atendido", con.conector);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Transaction = tra;
                    cmd.Parameters.Add(new MySqlParameter("@_iddetallepedido", MySqlDbType.Int32));
                    cmd.Parameters.Add(new MySqlParameter("@_atendido", MySqlDbType.Int32));
                    cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));

                    cmd.Parameters[0].Value = det.Iddetallepedido;
                    cmd.Parameters[1].Value = det.Atendido;
                    cmd.Parameters[2].Value = usureg.Idusuario;

                    dr = cmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            filas_afectadas = Convert.ToInt32(dr["_filas_afectadas"]);
                        }
                        dr.Close();
                    }

                }
                tra.Commit(); tra.Dispose();
                return filas_afectadas;
            }
            catch (MySqlException ex)
            {
                filas_afectadas = -1;
                tra.Rollback();               
                return filas_afectadas;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public string listar_descripcion_xiddetallepedido(clsDetallePedido detallepedido)
        {
            string descripcion ="";
            try
            {
                    con.conectarBD();           
                    cmd = new MySqlCommand("listar_descripcion_xiddetallepedido", con.conector);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Transaction = tra;
                    cmd.Parameters.Add(new MySqlParameter("@_iddetallepedido", MySqlDbType.Int32));        
                    cmd.Parameters[0].Value = detallepedido.Iddetallepedido;
                    dr = cmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            descripcion =(string)dr["descripcion"];
                        }
                        dr.Close();
                    }
                
                return descripcion;
            }
            catch (MySqlException ex)
            {           
            
                return descripcion;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public int actualizar_descripcion_xdetallepedidoid(clsDetallePedido detallepedido, clsUsuario usureg)
        {
            int filas_afectadas = -1;
            try
            {
                    con.conectarBD();
                    tra = con.conector.BeginTransaction();

                    cmd = new MySqlCommand("actualizar_descripcion_xdetallepedidoid", con.conector);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Transaction = tra;
                    cmd.Parameters.Add(new MySqlParameter("@_iddetallepedido", MySqlDbType.Int32));
                    cmd.Parameters.Add(new MySqlParameter("@_descripcion", MySqlDbType.VarChar));
                    cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));

                    cmd.Parameters[0].Value = detallepedido.Iddetallepedido;
                    cmd.Parameters[1].Value = detallepedido.Descripcion;
                    cmd.Parameters[2].Value = usureg.Idusuario;

                    dr = cmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            filas_afectadas = Convert.ToInt32(dr["_filas_afectadas"]);
                        }
                        dr.Close();
                    }              
                tra.Commit(); tra.Dispose();
                return filas_afectadas;
            }
            catch (MySqlException ex)
            {
                filas_afectadas = -1;
                tra.Rollback();                
                return filas_afectadas;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public clsPedido listar_pedido_xidpedidoxestado(clsPedido pedido)
        {
            clsPedido pedi = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("listar_pedido_xidpedidoxestado", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_idpedido", MySqlDbType.Int32));

                cmd.Parameters[0].Value = pedido.Idpedido;
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    pedi = new clsPedido();
                    pedi.Lista_detallepedido = new List<clsDetallePedido>();

                    while (dr.Read())
                    {
                        pedi.Idpedido = (int)dr["idpedido"];
                        pedi.Estado = (int)dr["estadope"];

                        pedi.Lista_detallepedido.Add(
                            new clsDetallePedido()
                            {

                                Iddetallepedido = (int)dr["iddetallepedido"],
                                Producto = new clsProducto()
                                {

                                    IdProducto = (int)dr["idproducto"],
                                    Nombre = (string)dr["nombreproducto"],
                                },
                                Cantidad = (decimal)dr["cantidad"],
                                Preciounitario = (decimal)dr["preciounitario"],
                                Estado = (int)dr["estadodet"],
                                Parallevar = (int)dr["parallevar"],
                                Atendido = int.Parse(dr["atendido"].ToString())
                            }
                        );
                    }
                    dr.Close();
                }
                return pedi;
            }
            catch (MySqlException ex)
            {
                return pedi;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataSet imprimir_comanda(clsPedido pedido)
        {
            try
            {
                con.conectarBD();
                data = new DataSet();
                cmd = new MySqlCommand("imprimir_comanda", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_idpedido", MySqlDbType.Int32));
                cmd.Parameters[0].Value = pedido.Idpedido;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(data, "comanda");

                ruta = her.GetResourcesPath4() + "\\comanda.xml";
                data.WriteXml(ruta, XmlWriteMode.WriteSchema);

                return data;
            }
            catch (MySqlException ex)
            {
                return data;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }
    }
}
