﻿using FinalXML.Conexion;
using FinalXML.Interfaces;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinalXML.Entidades;

namespace FinalXML.InterMySql
{
    class MysqlTipoCobro:ITipoCobro
    {
        private clsConexionMysql con = new clsConexionMysql();
        private MySqlCommand cmd = null;
        private MySqlDataReader dr = null;
        private MySqlDataAdapter adap = null;
        private MySqlTransaction tra = null;
        private DataTable tabla = null;

        public int existe_sigla(clsTipoCobro tipocobro)
        {
            int existe = 0;

            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("existe_sigla", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_sigla", MySqlDbType.VarChar));
                cmd.Parameters[0].Value = tipocobro.Sigla;
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {                  

                    while (dr.Read())
                    {
                        existe =Convert.ToInt32(dr["nsigla"]);                       
                    }
                    dr.Close();
                }
                return existe;
            }
            catch (MySqlException ex)
            {
                return existe;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public int registrar_tipo_cobro(clsTipoCobro tipocobro, clsUsuario usureg)
        {
            int id = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("registar_tipocobro", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;

                cmd.Parameters.Add(new MySqlParameter("@_nombre", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_sigla", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_estado", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));

                cmd.Parameters[0].Value = tipocobro.Nombre;
                cmd.Parameters[1].Value = tipocobro.Sigla;
                cmd.Parameters[2].Value = tipocobro.Estado;
                cmd.Parameters[3].Value = usureg.Idusuario;

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        id = Convert.ToInt32(dr["_id"]);
                    }
                    dr.Close();
                }
                tra.Commit(); tra.Dispose();
                return id;
            }
            catch (MySqlException ex)
            {
                id = -1;
                tra.Rollback();
                return id;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public int actualizar_tipo_cobro(clsTipoCobro tipocobro, clsUsuario usureg)
        {
            int filas_afectadas = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("actualizar_tipocobro", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;

                cmd.Parameters.Add(new MySqlParameter("@_tipocobroid", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_nombre", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_sigla", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_estado", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));

                cmd.Parameters[0].Value = tipocobro.Idtipocobro;
                cmd.Parameters[1].Value = tipocobro.Nombre;
                cmd.Parameters[2].Value = tipocobro.Sigla;
                cmd.Parameters[3].Value = tipocobro.Estado;
                cmd.Parameters[4].Value = usureg.Idusuario;

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        filas_afectadas = Convert.ToInt32(dr["_filas_afectadas"]);
                    }
                    dr.Close();
                }
                tra.Commit(); tra.Dispose();
                return filas_afectadas;
            }
            catch (MySqlException ex)
            {
                filas_afectadas = -1;
                tra.Rollback();
                return filas_afectadas;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable listar_tipocobro()
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_tipocobro", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }

        public DataTable listar_tipocobro_xnombre(clsTipoCobro tipocobro)
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_tipocobro_xnombre", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_nombre", MySqlDbType.VarChar));
                cmd.Parameters[0].Value = tipocobro.Nombre;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }

        public List<clsTipoCobro> listar_tipo_cobro()
        {
            List<clsTipoCobro> lista_tipocobro = null;
            clsTipoCobro tipocobro = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("listar_tipocobro", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;           
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    lista_tipocobro = new List<clsTipoCobro>();

                    while (dr.Read())
                    {
                        tipocobro = new clsTipoCobro
                        {
                            Idtipocobro = (int)dr["cod"],
                            Nombre = (string)dr["nombre"],
                            Sigla = (string)dr["sigla"]
                        };

                        lista_tipocobro.Add(tipocobro);
                    }
                    dr.Close();
                }
                return lista_tipocobro;
            }
            catch (MySqlException ex)
            {
                return lista_tipocobro;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public List<clsTipoCobro> listar_tipocobro_xestado()
        {
            List<clsTipoCobro> lista_tipocobro = null;
            clsTipoCobro tipocobro = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("listar_tipocobro_xestado", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    lista_tipocobro = new List<clsTipoCobro>();

                    while (dr.Read())
                    {
                        tipocobro = new clsTipoCobro
                        {
                            Idtipocobro = (int)dr["cod"],
                            Nombre = (string)dr["nombre"],
                            Sigla = (string)dr["sigla"]
                        };

                        lista_tipocobro.Add(tipocobro);
                    }
                    dr.Close();
                }
                return lista_tipocobro;
            }
            catch (MySqlException ex)
            {
                return lista_tipocobro;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }
    }
}
