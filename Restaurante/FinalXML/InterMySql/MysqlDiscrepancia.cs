﻿using FinalXML.Conexion;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinalXML.Entidades;

namespace FinalXML.InterMySql
{
    class MysqlDiscrepancia: IDiscrepancia
    {
        private clsConexionMysql con = new clsConexionMysql();
        private MySqlCommand cmd = null;
        private MySqlDataReader dr = null;
        private MySqlDataAdapter adap = null;
        private MySqlTransaction tra = null;
        private DataTable tabla = null;

        public List<clsDiscrepancia> listar_discrepancia_ncc()
        {
            List<clsDiscrepancia> lista_discrepancia_ncc = null;
            clsDiscrepancia discrepancia = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("listar_discrepancia_notacredito", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;               
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    lista_discrepancia_ncc = new List<clsDiscrepancia>();

                    while (dr.Read())
                    {
                        discrepancia = new clsDiscrepancia
                        {

                            Iddiscrepancia = (int)dr["iddiscrepancianc"],
                            Codsunat = (string)dr["codsunat"],
                            Descripcion = (string)dr["descripcion"],
                            Estado = (int)dr["estado"]
                        };

                        lista_discrepancia_ncc.Add(discrepancia);
                    }
                    dr.Close();
                }
                return lista_discrepancia_ncc;
            }
            catch (MySqlException ex)
            {
                return lista_discrepancia_ncc;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }
    }
}
