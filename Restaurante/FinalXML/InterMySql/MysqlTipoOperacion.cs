﻿using FinalXML.Conexion;
using FinalXML.Interfaces;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinalXML.Entidades;

namespace FinalXML.InterMySql
{
    class MysqlTipoOperacion : ITipoOperacion
    {
        private clsConexionMysql con = new clsConexionMysql();
        private MySqlCommand cmd = null;
        private MySqlDataReader dr = null;
        private MySqlDataAdapter adap = null;
        private MySqlTransaction tra = null;
        private DataTable tabla = null;

        public List<clsTipoOperacion> listar_tipooeracion_xestado()
        {
            List<clsTipoOperacion> lista_tipooperacion= null;
            clsTipoOperacion tipooeracion = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("listar_tipooeracion_xestado", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    lista_tipooperacion = new List<clsTipoOperacion>();

                    while (dr.Read())
                    {
                        tipooeracion = new clsTipoOperacion()
                        {
                            Idtipooeracion = (int)dr["idtipooperacion"],
                            Codsunat=(string)dr["codsunat"],
                            Descripcion=(string)dr["descripcion"]                          

                        };

                        lista_tipooperacion.Add(tipooeracion);
                    }
                    dr.Close();
                }
                return lista_tipooperacion;
            }
            catch (MySqlException ex)
            {
                return lista_tipooperacion;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public clsTipoOperacion listar_tipooeracion_xid(int id)
        {
            clsTipoOperacion operacion = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("lista_operacion_xid", con.conector);
                cmd.Parameters.AddWithValue("_idoperacion", id);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        operacion = new clsTipoOperacion();
                        operacion.Idtipooeracion = Convert.ToInt32(dr.GetDecimal(0));
                        operacion.Codsunat = dr.GetString(1);
                        operacion.Descripcion = dr.GetString(2);
                        operacion.Estado = Convert.ToInt32(dr.GetDecimal(3));
                        
                    }
                }
                return operacion;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }
    }
}
