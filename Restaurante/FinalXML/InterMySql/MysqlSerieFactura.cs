﻿using FinalXML.Conexion;
using FinalXML.Interfaces;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinalXML.Entidades;
using System.Data;

namespace FinalXML.InterMySql
{
    class MysqlSerieFactura :ISerieFactura
    {
        private clsConexionMysql con = new clsConexionMysql();
        private MySqlCommand cmd = null;
        private MySqlDataReader dr = null;
        private MySqlDataAdapter adap = null;
        private MySqlTransaction tra = null;
        private DataTable tabla = null;

        public int actualizar_seriexnumeracion(clsSerieFactura serie, clsUsuario usureg)
        {
            int filas_afectadas = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("actualizar_seriexnumeracion", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;
                cmd.Parameters.Add(new MySqlParameter("@_idserie", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_numero", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_estado", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));
               

                cmd.Parameters[0].Value = serie.Idserie;
                cmd.Parameters[1].Value = serie.Correlativo;
                cmd.Parameters[2].Value = serie.Estado;
                cmd.Parameters[3].Value = usureg.Idusuario;


                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        filas_afectadas = Convert.ToInt32(dr["_filas_afectadas"]);
                    }
                    dr.Close();
                }
                tra.Commit(); tra.Dispose();
                return filas_afectadas;
            }
            catch (MySqlException ex)
            {
                filas_afectadas = -1;
                tra.Rollback();
                return filas_afectadas;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable listar_seriefacturacion()
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_seriefacturacion", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }

        public List<clsSerieFactura> _listar_seriefacturacion()
        {
            List<clsSerieFactura> lista_serie = null;
            clsSerieFactura serie = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("listar_seriefacturacion", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    lista_serie = new List<clsSerieFactura>();

                    while (dr.Read())
                    {
                        serie = new clsSerieFactura()
                        {
                            Idserie = (int)dr["id"],
                            Tipodocumento=(string)dr["tipo_documento"],
                            Nombredocumento=(string)dr["nom_documento"],
                            Correlativo=(int)dr["correlativo"],
                            Serie=(string)dr["serie"]                        
                        };

                        lista_serie.Add(serie);
                    }
                    dr.Close();
                }
                return lista_serie;
            }
            catch (MySqlException ex)
            {
                return lista_serie;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }
    }
}
