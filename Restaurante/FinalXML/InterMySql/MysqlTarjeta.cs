﻿using FinalXML.Conexion;
using FinalXML.Interfaces;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinalXML.Entidades;

namespace FinalXML.InterMySql
{
    class MysqlTarjeta :ITarjeta
    {
        private clsConexionMysql con = new clsConexionMysql();
        private MySqlCommand cmd = null;
        private MySqlDataReader dr = null;
        private MySqlDataAdapter adap = null;
        private MySqlTransaction tra = null;
        private DataTable tabla = null;

        public int registrar_tarjeta_cobro(clsTarjeta tarjetacobro, clsUsuario usureg)
        {
            int id = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("registar_tarjeta_cobro", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;
                cmd.Parameters.Add(new MySqlParameter("@_descripcion", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_estado", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));

                cmd.Parameters[0].Value = tarjetacobro.Descripcion;
                cmd.Parameters[1].Value = tarjetacobro.Estado;   
                cmd.Parameters[2].Value = usureg.Idusuario;

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        id = Convert.ToInt32(dr["_id"]);
                    }
                    dr.Close();
                }
                tra.Commit(); tra.Dispose();
                return id;
            }
            catch (MySqlException ex)
            {
                id = -1;
                tra.Rollback();
                return id;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public int actualizar_tarjeta_cobro(clsTarjeta tarjetacobro, clsUsuario usureg)
        {
            int filas_afectadas = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("actualizar_tarjeta_cobro", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;
                cmd.Parameters.Add(new MySqlParameter("@_tarjetaid", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_descripcion", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_estado", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));

                cmd.Parameters[0].Value = tarjetacobro.Idtarjeta;
                cmd.Parameters[1].Value = tarjetacobro.Descripcion;
                cmd.Parameters[2].Value = tarjetacobro.Estado;
                cmd.Parameters[3].Value = usureg.Idusuario;

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        filas_afectadas = Convert.ToInt32(dr["_filas_afectadas"]);
                    }
                    dr.Close();
                }
                tra.Commit(); tra.Dispose();
                return filas_afectadas;
            }
            catch (MySqlException ex)
            {
                filas_afectadas = -1;
                tra.Rollback();
                return filas_afectadas;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable listar_tarjeta_cobro()
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_tarjeta_cobro", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }

        public List<clsTarjeta> listar_tarjetacobro()
        {
            List<clsTarjeta> lista_tarjeta = null;
            clsTarjeta tarjeta = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("listar_tarjeta_cobro", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;               
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    lista_tarjeta = new List<clsTarjeta>();

                    while (dr.Read())
                    {
                        tarjeta = new clsTarjeta()
                        {
                            Idtarjeta = (int)dr["tarjetaid"],
                            Descripcion = (string)dr["descripcion"]
                        };

                        lista_tarjeta.Add(tarjeta);
                    }
                    dr.Close();
                }
                return lista_tarjeta;
            }
            catch (MySqlException ex)
            {
                return lista_tarjeta;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }
    }
}
