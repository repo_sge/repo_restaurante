﻿using FinalXML.Conexion;
using FinalXML.Interfaces;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinalXML.Entidades;

namespace FinalXML.InterMySql
{
    class MysqlIngrediente:IIngrediente
    {
        private clsConexionMysql con = new clsConexionMysql();
        private MySqlCommand cmd = null;
        private MySqlDataReader dr = null;
        private MySqlDataAdapter adap = null;
        private MySqlTransaction tra = null;
        private DataTable tabla = null;

        public int registrar_ingrediente(clsIngrediente ingrediente, clsUsuario usureg)
        {
            int id = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("registrar_ingrediente", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;    
                cmd.Parameters.Add(new MySqlParameter("@_nombre", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_descripcion", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_preciounitario", MySqlDbType.Decimal));
                cmd.Parameters.Add(new MySqlParameter("@_unidadmedidaid", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_estado", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));

                cmd.Parameters[0].Value = ingrediente.Nombre;
                cmd.Parameters[1].Value = ingrediente.Descripcion;
                cmd.Parameters[2].Value = ingrediente.Preciounitario;
                cmd.Parameters[3].Value = ingrediente.Unidadmedida.Unidadmedidaid;
                cmd.Parameters[4].Value = ingrediente.Estado;           
                cmd.Parameters[5].Value = usureg.Idusuario;

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        id = Convert.ToInt32(dr["_id"]);
                    }
                    dr.Close();
                }
                tra.Commit(); tra.Dispose();
                return id;
            }
            catch (MySqlException ex)
            {
                id = -1;
                tra.Rollback();
                return id;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public int actualizar_ingrediente(clsIngrediente ingrediente, clsUsuario usureg)
        {
            int filas_afectadas = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("actualizar_ingrediente", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;
                cmd.Parameters.Add(new MySqlParameter("@_idingrediente", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_nombre", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_descripcion", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_preciounitario", MySqlDbType.Decimal));
                cmd.Parameters.Add(new MySqlParameter("@_unidadmedidaid", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_estado", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));

                cmd.Parameters[0].Value = ingrediente.Idingrediente;
                cmd.Parameters[1].Value = ingrediente.Nombre;
                cmd.Parameters[2].Value = ingrediente.Descripcion;
                cmd.Parameters[3].Value = ingrediente.Preciounitario;
                cmd.Parameters[4].Value = ingrediente.Unidadmedida.Unidadmedidaid;
                cmd.Parameters[5].Value = ingrediente.Estado;
                cmd.Parameters[6].Value = usureg.Idusuario;

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        filas_afectadas = Convert.ToInt32(dr["_filas_afectadas"]);
                    }
                    dr.Close();
                }
                tra.Commit(); tra.Dispose();
                return filas_afectadas;
            }
            catch (MySqlException ex)
            {
                filas_afectadas = -1;
                tra.Rollback();
                return filas_afectadas;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable listar_ingrediente()
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_ingrediente", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }

        public DataTable listar_ingrediente_xnombre(clsIngrediente ingrediente)
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_ingrediente_xnombre", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_nombre", MySqlDbType.VarChar));
                cmd.Parameters[0].Value = ingrediente.Nombre;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }

        public DataTable listar_ingrediente_xnombre_xestado(clsIngrediente ingrediente)
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_ingrediente_xnombre_xestado", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_nombre", MySqlDbType.VarChar));
                cmd.Parameters[0].Value = ingrediente.Nombre;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }
    }
}
