﻿using FinalXML.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinalXML.Entidades;
using System.Data;
using FinalXML.Conexion;
using MySql.Data.MySqlClient;

namespace FinalXML.InterMySql
{
    class MysqlProducto : IProducto
    {
        private clsConexionMysql con = new clsConexionMysql();
        private MySqlCommand cmd = null;
        private MySqlDataReader dr = null;
        private MySqlDataAdapter adap = null;
        private MySqlTransaction tra = null;
        private DataTable tabla = null;

        public int actualizar_producto(clsProducto producto, clsUsuario usureg)
        {
            int filas_afectadas = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("actualizar_producto", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;
                cmd.Parameters.Add(new MySqlParameter("@_idproducto", MySqlDbType.Int32)); 
                cmd.Parameters.Add(new MySqlParameter("@_idcategoria", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_nombreproducto", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_descripcion", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_preciounitario", MySqlDbType.Decimal));
                cmd.Parameters.Add(new MySqlParameter("@_estado", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_codsunat", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_icbper", MySqlDbType.Bit));

                cmd.Parameters[0].Value = producto.IdProducto;
                cmd.Parameters[1].Value = producto.Categoria.Idcategoria;
                cmd.Parameters[2].Value = producto.Nombre;
                cmd.Parameters[3].Value = producto.Descripcion;
                cmd.Parameters[4].Value = producto.Preciounitario;
                cmd.Parameters[5].Value = producto.Estado;
                cmd.Parameters[6].Value = usureg.Idusuario;
                cmd.Parameters[7].Value = producto.Codsunat;
                cmd.Parameters[8].Value = producto.Icbper;

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        filas_afectadas = Convert.ToInt32(dr["_filas_afectadas"]);
                    }
                    dr.Close();
                }
                tra.Commit(); tra.Dispose();
                return filas_afectadas;
            }
            catch (MySqlException ex)
            {
                filas_afectadas = -1;
                tra.Rollback();
                return filas_afectadas;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable listar_producto()
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_producto", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }

        public DataTable listar_productoxnombre(clsProducto producto)
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_productoxnombre", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_nombreproducto", MySqlDbType.VarChar));
                cmd.Parameters[0].Value = producto.Nombre;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }

        public DataTable listar_productoxnombreestado(clsProducto producto)
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_productoxnombreestado", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_nombreproducto", MySqlDbType.VarChar));
                cmd.Parameters[0].Value = producto.Nombre;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }

        public int registrar_producto(clsProducto producto, clsUsuario usureg)
        {
            int id = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("registar_producto", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;
                cmd.Parameters.Add(new MySqlParameter("@_idcategoria", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_nombreproducto", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_descripcion", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_preciounitario", MySqlDbType.Decimal));
                cmd.Parameters.Add(new MySqlParameter("@_estado", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_codsunat", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_icbper", MySqlDbType.Bit));

                cmd.Parameters[0].Value = producto.Categoria.Idcategoria;
                cmd.Parameters[1].Value = producto.Nombre;
                cmd.Parameters[2].Value = producto.Descripcion;
                cmd.Parameters[3].Value = producto.Preciounitario;
                cmd.Parameters[4].Value = producto.Estado;
                cmd.Parameters[5].Value = usureg.Idusuario;
                cmd.Parameters[6].Value = producto.Codsunat;
                cmd.Parameters[7].Value = producto.Icbper;

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        id = Convert.ToInt32(dr["_id"]);
                    }
                    dr.Close();
                }
                tra.Commit(); tra.Dispose();
                return id;
            }
            catch (MySqlException ex)
            {
                id = -1;
                tra.Rollback();
                return id;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable listar_productoxcategorianombre(clsProducto producto)
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_productoxcategorianombre", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_nombreproducto", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_idcategoria", MySqlDbType.Int32));
                cmd.Parameters[0].Value = producto.Nombre;
                cmd.Parameters[1].Value = producto.Categoria.Idcategoria;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }

        public int producto_paracocina(clsProducto producto)
        {
            int paracocina = -1;

            try
            {
                con.conectarBD();            
                cmd = new MySqlCommand("producto_paracocina", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;         
                cmd.Parameters.Add(new MySqlParameter("@_idproducto", MySqlDbType.Int32));    
                cmd.Parameters[0].Value = producto.IdProducto;
     

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        paracocina = Convert.ToInt32(dr["paracocina"]);
                    }
                    dr.Close();
                }
          
                return paracocina;
            }
            catch (MySqlException ex)
            {              
                return paracocina;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public clsProducto listar_productoxid(int id)
        {
            clsProducto producto = new clsProducto();

            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("listar_productoxid", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_idproducto", MySqlDbType.Int32));
                cmd.Parameters[0].Value = id;


                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        producto.IdProducto = Convert.ToInt32(dr["idproducto"]);
                        producto.Categoria.Idcategoria = Convert.ToInt32(dr["idcategoria"]);
                        producto.Nombre = Convert.ToString(dr["nombreproducto"]);
                        producto.Descripcion = Convert.ToString(dr["descripcion"]);
                        producto.Preciounitario = Convert.ToDecimal(dr["preciounitario"]);
                        producto.Estado = Convert.ToInt32(dr["estado"]);
                        producto.Categoria.Nombre = Convert.ToString(dr["nombrecategoria"]);

                    }
                    dr.Close();
                }

                return producto;
            }
            catch (MySqlException ex)
            {
                return producto;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable listar_codproductosunatxdesc(String desc)
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_codproductosunatxdesc", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("_desc", MySqlDbType.String));
                cmd.Parameters[0].Value = desc;   
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }

        public int validaProductoBolsa()
        {
            int cont = 0;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("validaProductoBolsa", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        cont= Convert.ToInt32(dr[0]);

                    }
                    dr.Close();
                }

                return cont;
            }
            catch (MySqlException ex)
            {
                return cont;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }
    }
}
