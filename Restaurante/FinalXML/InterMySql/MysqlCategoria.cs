﻿using FinalXML.Conexion;
using FinalXML.Interfaces;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinalXML.Entidades;

namespace FinalXML.InterMySql
{
    class MysqlCategoria :ICategoria
    {
        private clsConexionMysql con = new clsConexionMysql();
        private MySqlCommand cmd = null;
        private MySqlDataReader dr = null;
        private MySqlDataAdapter adap = null;
        private MySqlTransaction tra = null;
        private DataTable tabla = null;

        public int registrar_categoria(clsCategoria categoria, clsUsuario usureg)
        {
            int id = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("registrar_categoria", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;
                cmd.Parameters.Add(new MySqlParameter("@_nombrecategoria", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_descripcion", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_paracocina", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_estado", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));

                cmd.Parameters[0].Value = categoria.Nombre;
                cmd.Parameters[1].Value = categoria.Descripcion;
                cmd.Parameters[2].Value = categoria.Paracocina;
                cmd.Parameters[3].Value = categoria.Estado;
                cmd.Parameters[4].Value = usureg.Idusuario;

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        id = Convert.ToInt32(dr["_id"]);
                    }
                    dr.Close();
                }
                tra.Commit(); tra.Dispose();
                return id;
            }
            catch (MySqlException ex)
            {
                id = -1;
                tra.Rollback();
                return id;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public int actualizar_categoria(clsCategoria categoria, clsUsuario usureg)
        {
            int filas_afectadas = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("actualizar_categoria", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;
                cmd.Parameters.Add(new MySqlParameter("@_idcategoria", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_nombrecategoria", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_descripcion", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_paracocina", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_estado", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));

                cmd.Parameters[0].Value = categoria.Idcategoria;
                cmd.Parameters[1].Value = categoria.Nombre;
                cmd.Parameters[2].Value = categoria.Descripcion;
                cmd.Parameters[3].Value = categoria.Paracocina;
                cmd.Parameters[4].Value = categoria.Estado;
                cmd.Parameters[5].Value = usureg.Idusuario;

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        filas_afectadas = Convert.ToInt32(dr["_filas_afectadas"]);
                    }
                    dr.Close();
                }
                tra.Commit(); tra.Dispose();
                return filas_afectadas;
            }
            catch (MySqlException ex)
            {
                filas_afectadas = -1;
                tra.Rollback();
                return filas_afectadas;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public List<clsCategoria> listar_categoria()
        {
            List<clsCategoria> lista_categoria = null;
            clsCategoria categoria = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("listar_categoria", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    lista_categoria = new List<clsCategoria>();

                    while (dr.Read())
                    {
                        categoria = new clsCategoria()
                        {
                            Idcategoria = (int)dr["idcategoria"],
                            Nombre = (string)dr["nombrecategoria"]
                        };

                        lista_categoria.Add(categoria);
                    }
                    dr.Close();
                }
                return lista_categoria;
            }
            catch (MySqlException ex)
            {
                return lista_categoria;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable _listar_categoria()
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_categoria_todo", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;            
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }
    }
}
