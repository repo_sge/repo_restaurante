﻿using FinalXML.Conexion;
using FinalXML.Interfaces;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinalXML.Entidades;

namespace FinalXML.InterMySql
{
    class MysqlCuentaBancaria: ICuentaBancaria
    {
        private clsConexionMysql con = new clsConexionMysql();
        private MySqlCommand cmd = null;
        private MySqlDataReader dr = null;
        private MySqlDataAdapter adap = null;
        private MySqlTransaction tra = null;
        private DataTable tabla = null;

        public int registrar_cuenta(clsCuentaBancaria cuenta, clsUsuario usureg)
        {
            int id = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("registrar_cuenta_banco", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;
                cmd.Parameters.Add(new MySqlParameter("@_bancoid", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_tipocuentaid", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_monedaid", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_numero", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_estado", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));

                cmd.Parameters[0].Value = cuenta.Banco.Idbanco;
                cmd.Parameters[1].Value = cuenta.Tipocuenta.Idtipocuentabancaria;
                cmd.Parameters[2].Value = cuenta.Moneda.IcodMoneda;
                cmd.Parameters[3].Value = cuenta.Numero;
                cmd.Parameters[4].Value = cuenta.Estado;
                cmd.Parameters[5].Value = usureg.Idusuario;

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        id = Convert.ToInt32(dr["_id"]);
                    }
                    dr.Close();
                }
                tra.Commit(); tra.Dispose();
                return id;
            }
            catch (MySqlException ex)
            {
                id = -1;
                tra.Rollback();
                return id;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public int actualizar_cuenta(clsCuentaBancaria cuenta, clsUsuario usureg)
        {
            int filas_afectadas = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("actualizar_cuenta_banco", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;

                cmd.Parameters.Add(new MySqlParameter("@_cuentaid", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_bancoid", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_tipocuentaid", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_monedaid", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_numero", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_estado", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));

                cmd.Parameters[0].Value = cuenta.Idcuentabancaria;
                cmd.Parameters[1].Value = cuenta.Banco.Idbanco;
                cmd.Parameters[2].Value = cuenta.Tipocuenta.Idtipocuentabancaria;
                cmd.Parameters[3].Value = cuenta.Moneda.IcodMoneda;
                cmd.Parameters[4].Value = cuenta.Numero;
                cmd.Parameters[5].Value = cuenta.Estado;
                cmd.Parameters[6].Value = usureg.Idusuario;

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        filas_afectadas = Convert.ToInt32(dr["_filas_afectadas"]);
                    }
                    dr.Close();
                }
                tra.Commit(); tra.Dispose();
                return filas_afectadas;
            }
            catch (MySqlException ex)
            {
                filas_afectadas = -1;
                tra.Rollback();
                return filas_afectadas;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable listar_cuenta()
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_cuenta_banco", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }

        public DataTable listar_cuenta_xbanco(clsBanco banco)
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_cuenta_banco_xbanco", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_bancoid", MySqlDbType.Int32));
                cmd.Parameters[0].Value = banco.Idbanco;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }

        public List<clsCuentaBancaria> listar_cuenta_banco_xestado()
        {
            List<clsCuentaBancaria> lista_cuenta_bancaria = null;
            clsCuentaBancaria cuenta = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("listar_cuenta_banco_xestado", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;           
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    lista_cuenta_bancaria = new List<clsCuentaBancaria>();

                    while (dr.Read())
                    {
                        cuenta = new clsCuentaBancaria
                        {
                            Idcuentabancaria = (int)dr["cod"],
                            Banco = new clsBanco() {
                                Idbanco = (int)dr["bancoid"],
                                Nombre = (string)dr["nombre"]
                            },

                            Tipocuenta = new clsTipoCuentaBancaria() {
                                Idtipocuentabancaria = (int)dr["tipocuentaid"],
                                Descripcion = (string)dr["descripcion"]                               
                            },
                            Numero = (string)dr["numero"],
                            Moneda = new clsMoneda() {
                                IcodMoneda = (int)dr["monedaid"],
                                SDescripcion= (string)dr["sigla"]
                             }
                           
                    };

                        lista_cuenta_bancaria.Add(cuenta);
                    }
                    dr.Close();
                }
                return lista_cuenta_bancaria;
            }
            catch (MySqlException ex)
            {
                return lista_cuenta_bancaria;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public List<clsCuentaBancaria> listar_cuenta_x_banco(clsBanco banco)
        {
            List<clsCuentaBancaria> lista_cuenta_bancaria = null;
            clsCuentaBancaria cuenta = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("listar_cuenta_banco_xbanco", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_bancoid", MySqlDbType.Int32));
                cmd.Parameters[0].Value = banco.Idbanco;
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    lista_cuenta_bancaria = new List<clsCuentaBancaria>();

                    while (dr.Read())
                    {
                        cuenta = new clsCuentaBancaria
                        {
                            Idcuentabancaria = (int)dr["cod"],
                            Banco = new clsBanco()
                            {
                                Idbanco = (int)dr["bancoid"],
                                Nombre = (string)dr["nombre"]
                            },

                            Tipocuenta = new clsTipoCuentaBancaria()
                            {
                                Idtipocuentabancaria = (int)dr["tipocuentaid"],
                                Descripcion = (string)dr["descripcion"]
                            },
                            Numero = (string)dr["numero"],
                            Moneda = new clsMoneda()
                            {
                                IcodMoneda = (int)dr["monedaid"],
                                SDescripcion = (string)dr["sigla"]
                            }

                        };

                        lista_cuenta_bancaria.Add(cuenta);
                    }
                    dr.Close();
                }
                return lista_cuenta_bancaria;
            }
            catch (MySqlException ex)
            {
                return lista_cuenta_bancaria;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }
    }
}
