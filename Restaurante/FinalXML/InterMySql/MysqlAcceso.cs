﻿using FinalXML.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinalXML.Entidades;
using FinalXML.Conexion;
using MySql.Data.MySqlClient;
using System.Data;

namespace FinalXML.InterMySql
{
    class MysqlAcceso : IAcceso
    {
        private clsConexionMysql con = new clsConexionMysql();
        private MySqlCommand cmd = null;
        private MySqlDataReader dr = null;
        private MySqlDataAdapter adap = null;
        private MySqlTransaction tra = null;
        private DataTable tabla = null;
        public int registrar_acceso(clsAcceso acceso)
        {
            int id = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("registrar_acceso", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_pc", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_usuariopc", MySqlDbType.VarChar));     

                cmd.Parameters[0].Value = acceso.Usuario.Idusuario;
                cmd.Parameters[1].Value = acceso.Pc;
                cmd.Parameters[2].Value = acceso.Usuariopc;

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        id = Convert.ToInt32(dr["_id"]);
                    }
                    dr.Close();
                }
                tra.Commit(); tra.Dispose();
                return id;
            }
            catch (MySqlException ex)
            {
                id = -1;
                tra.Rollback();
                return id;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }
    }
}
