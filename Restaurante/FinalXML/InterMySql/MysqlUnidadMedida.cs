﻿using FinalXML.Conexion;
using FinalXML.Interfaces;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinalXML.Entidades;

namespace FinalXML.InterMySql
{
    class MysqlUnidadMedida:IUnidadMedida
    {
        private clsConexionMysql con = new clsConexionMysql();
        private MySqlCommand cmd = null;
        private MySqlDataReader dr = null;
        private MySqlDataAdapter adap = null;
        private MySqlTransaction tra = null;
        private DataTable tabla = null;

        public int registrar_unidadmedida(clsUnidadMedida unidadmedida, clsUsuario usureg)
        {
            int id = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("registrar_unidad_medida", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;
                cmd.Parameters.Add(new MySqlParameter("@_nombre", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_sigla", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_estado", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));

                cmd.Parameters[0].Value = unidadmedida.Nombre;
                cmd.Parameters[1].Value = unidadmedida.Sigla;
                cmd.Parameters[2].Value = unidadmedida.Estado;
                cmd.Parameters[3].Value = usureg.Idusuario;

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        id = Convert.ToInt32(dr["_id"]);
                    }
                    dr.Close();
                }
                tra.Commit(); tra.Dispose();
                return id;
            }
            catch (MySqlException ex)
            {
                id = -1;
                tra.Rollback();
                return id;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public int actualizar_unidadmedida(clsUnidadMedida unidadmedida, clsUsuario usureg)
        {
            int filas_afectadas = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("actualizar_unidad_medida", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;
                cmd.Parameters.Add(new MySqlParameter("@_unidadmedidaid", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_nombre", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_sigla", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_estado", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));

                cmd.Parameters[0].Value = unidadmedida.Unidadmedidaid;
                cmd.Parameters[1].Value = unidadmedida.Nombre;
                cmd.Parameters[2].Value = unidadmedida.Sigla;
                cmd.Parameters[3].Value = unidadmedida.Estado;
                cmd.Parameters[4].Value = usureg.Idusuario;

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        filas_afectadas = Convert.ToInt32(dr["_filas_afectadas"]);
                    }
                    dr.Close();
                }
                tra.Commit(); tra.Dispose();
                return filas_afectadas;
            }
            catch (MySqlException ex)
            {
                filas_afectadas = -1;
                tra.Rollback();
                return filas_afectadas;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable listar_unidadmedida()
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_unidad_medida", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }

        public List<clsUnidadMedida> listar_unidadmedida_xestado()
        {
            List<clsUnidadMedida> lista_unidadmedida = null;
            clsUnidadMedida unidadmedida = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("listar_unidad_medida_xestado", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    lista_unidadmedida = new List<clsUnidadMedida>();

                    while (dr.Read())
                    {
                        unidadmedida = new clsUnidadMedida
                        {
                            Unidadmedidaid = (int)dr["unidadmedidaid"],
                            Nombre = (string)dr["nombre"],
                            Sigla = (string)dr["sigla"],
                            Estado = (int)dr["estado"]
                        };

                        lista_unidadmedida.Add(unidadmedida);
                    }
                    dr.Close();
                }
                return lista_unidadmedida;
            }
            catch (MySqlException ex)
            {
                return lista_unidadmedida;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }
    }
}
