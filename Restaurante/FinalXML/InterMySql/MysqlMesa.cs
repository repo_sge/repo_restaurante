﻿using FinalXML.Conexion;
using FinalXML.Interfaces;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinalXML.Entidades;

namespace FinalXML.InterMySql
{
    class MysqlMesa : IMesa
    {
        private clsConexionMysql con = new clsConexionMysql();
        private MySqlCommand cmd = null;
        private MySqlDataReader dr = null;
        private MySqlDataAdapter adap = null;
        private MySqlTransaction tra = null;
        private DataTable tabla = null;

        public int registrar_mesa(clsMesa mesa, clsUsuario usureg)
        {
            int id = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("registrar_mesa", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;
                cmd.Parameters.Add(new MySqlParameter("@_nombremesa", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_descripcion", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_idpiso", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_estado", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));

                cmd.Parameters[0].Value = mesa.Nombre;
                cmd.Parameters[1].Value = mesa.Descripcion;
                cmd.Parameters[2].Value = mesa.Piso.Idpiso;
                cmd.Parameters[3].Value = mesa.Estado;
                cmd.Parameters[4].Value = usureg.Idusuario;

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        id = Convert.ToInt32(dr["_id"]);
                    }
                    dr.Close();
                }
                tra.Commit(); tra.Dispose();
                return id;
            }
            catch (MySqlException ex)
            {
                id = -1;
                tra.Rollback();
                return id;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public int actualizar_mesa(clsMesa mesa, clsUsuario usureg)
        {
            int filas_afectadas = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("actualizar_mesa", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;
                cmd.Parameters.Add(new MySqlParameter("@_idmesa", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_nombremesa", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_descripcion", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_idpiso", MySqlDbType.Int32));                
                cmd.Parameters.Add(new MySqlParameter("@_estado", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));

                cmd.Parameters[0].Value = mesa.Idmesa;
                cmd.Parameters[1].Value = mesa.Nombre;
                cmd.Parameters[2].Value = mesa.Descripcion;
                cmd.Parameters[3].Value = mesa.Piso.Idpiso;
                cmd.Parameters[4].Value = mesa.Estado;
                cmd.Parameters[5].Value = usureg.Idusuario;

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        filas_afectadas = Convert.ToInt32(dr["_filas_afectadas"]);
                    }
                    dr.Close();
                }
                tra.Commit(); tra.Dispose();
                return filas_afectadas;
            }
            catch (MySqlException ex)
            {
                filas_afectadas = -1;
                tra.Rollback();
                return filas_afectadas;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }
    

        public DataTable listar_mesa()
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_mesa", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }

        public List<clsMesa> listar_mesaxestado(clsPiso piso)
        {
            List<clsMesa> lista_mesa = null;
            clsMesa mesa = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("listar_mesaxestado", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_idpiso", MySqlDbType.Int32));
                cmd.Parameters[0].Value = piso.Idpiso;
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    lista_mesa = new List<clsMesa>();

                    while (dr.Read())
                    {
                        mesa = new clsMesa {

                            Idmesa=(int)dr["idmesa"],
                            Nombre=(string)dr["nombremesa"],
                            Descripcion=(string)dr["descripcion"],
                            Estado=(int)dr["estado"]
                        };

                        lista_mesa.Add(mesa);
                    }
                    dr.Close();
                }
                return lista_mesa;
            }
            catch (MySqlException ex)
            {
                return lista_mesa;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public List<clsMesa> listar_mesa_noenmesagrupo(clsPiso piso)
        {
            List<clsMesa> lista_mesa = null;
            clsMesa mesa = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("listar_mesa_noenmesagrupo", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_idpiso", MySqlDbType.Int32));
                cmd.Parameters[0].Value = piso.Idpiso;
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    lista_mesa = new List<clsMesa>();

                    while (dr.Read())
                    {
                        mesa = new clsMesa
                        {

                            Idmesa = (int)dr["idmesa"],
                            Nombre = (string)dr["nombremesa"],
                            Descripcion = (string)dr["descripcion"],
                            Estado = (int)dr["estado"]
                        };

                        lista_mesa.Add(mesa);
                    }
                    dr.Close();
                }
                return lista_mesa;
            }
            catch (MySqlException ex)
            {
                return lista_mesa;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable listar_mesaxgrupo(clsGrupo grupo,clsPiso piso)
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_mesaxgrupo", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_idgrupo", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idpiso", MySqlDbType.Int32));
                cmd.Parameters[0].Value = grupo.Idgrupo;
                cmd.Parameters[1].Value = piso.Idpiso;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }

        public DataTable listar_mesaxgruposinpedido(clsGrupo grupo, clsPiso piso)
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_mesaxgruposinpedido", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_idgrupo", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idpiso", MySqlDbType.Int32));
                cmd.Parameters[0].Value = grupo.Idgrupo;
                cmd.Parameters[1].Value = piso.Idpiso;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }

        public int cambiar_mesa(clsMesa mesa, clsPedido pedido, clsUsuario usureg)
        {
            int filas_afectadas = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("cambiar_mesa", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;
                cmd.Parameters.Add(new MySqlParameter("@_idpedido", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idmesa", MySqlDbType.Int32));               
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));

                cmd.Parameters[0].Value = pedido.Idpedido;
                cmd.Parameters[1].Value = mesa.Idmesa;
                cmd.Parameters[2].Value = usureg.Idusuario;

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        filas_afectadas = Convert.ToInt32(dr["_filas_afectadas"]);
                    }
                    dr.Close();
                }
                tra.Commit(); tra.Dispose();
                return filas_afectadas;
            }
            catch (MySqlException ex)
            {
                filas_afectadas = -1;
                tra.Rollback();
                return filas_afectadas;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable listar_grupodemesas(clsMesa mesa)
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_grupodemesas", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_idmesa", MySqlDbType.Int32));
                cmd.Parameters[0].Value = mesa.Idmesa;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }
        
    }
}
