﻿using FinalXML.Conexion;
using FinalXML.Interfaces;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinalXML.Entidades;

namespace FinalXML.InterMySql
{
    class MysqlEmpresa:IEmpresa
    {
        private clsConexionMysql con = new clsConexionMysql();
        private MySqlCommand cmd = null;
        private MySqlDataReader dr = null;
        private MySqlDataAdapter adap = null;
        private MySqlTransaction tra = null;
        private DataTable tabla = null;

        public clsEmpresa listar_empresa_xestado()
        {
            clsEmpresa empre = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("listar_empresa_xestado", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;              
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        empre = new clsEmpresa()
                        {
                            Idempresa=(int)dr["idempresa"],
                            Razonsocial=(string)dr["razonsocial"],
                            Tipodocidentidad=new clsTipoDocumentoIndentidad() {
                                Idtipodocumentoidentidad=(int)dr["idtipodocumentoidentidad"],
                                Codsunat= (string)dr["codsunat"]
                            },
                            Ruc =(string)dr["ruc"],
                            Urlenvio = (string)dr["urlenvio"],
                            Ubigeo =(string)dr["ubigeo"],
                            Direccion=(string)dr["direccion"],
                            Departamento=(string)dr["departamento"],
                            Provincia=(string)dr["provincia"],
                            Distrito=(string)dr["dristrito"],
                            Usuariosol=(string)dr["usuariosol"],
                            Clavesol=(string)dr["clavesol"],
                            Nombrecertificado = (string)dr["nombrecerfiticado"],                           
                            Clavecertificado=(string)dr["clavecertificado"],
                            Estado=(int)dr["estado"]
                        };

                    }
                    dr.Close();
                }
                return empre;
            }
            catch (MySqlException ex)
            {
                return empre;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }
    }
}
