﻿using FinalXML.Conexion;
using FinalXML.Interfaces;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinalXML.Entidades;

namespace FinalXML.InterMySql
{
    class MysqlPiso : IPiso
    {
        private clsConexionMysql con = new clsConexionMysql();
        private MySqlCommand cmd = null;
        private MySqlDataReader dr = null;
        private MySqlDataAdapter adap = null;
        private MySqlTransaction tra = null;
        private DataTable tabla = null;

        public int registrar_piso(clsPiso piso, clsUsuario usureg)
        {
            int id = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("registrar_piso", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;
                cmd.Parameters.Add(new MySqlParameter("@_numero", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_nombre", MySqlDbType.VarChar));           
                cmd.Parameters.Add(new MySqlParameter("@_estado", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));

                cmd.Parameters[0].Value = piso.Numero;
                cmd.Parameters[1].Value = piso.Nombre;     
                cmd.Parameters[2].Value = piso.Estado;
                cmd.Parameters[3].Value = usureg.Idusuario;


                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        id = Convert.ToInt32(dr["_id"]);
                    }
                    dr.Close();
                }
                tra.Commit(); tra.Dispose();
                return id;
            }
            catch (MySqlException ex)
            {
                id = -1;
                tra.Rollback();
                return id;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public int actualizar_piso(clsPiso piso, clsUsuario usureg)
        {
            int filas_afectadas = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("actualizar_piso", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;
                cmd.Parameters.Add(new MySqlParameter("@_idpiso", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_numero", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_nombre", MySqlDbType.VarChar));              
                cmd.Parameters.Add(new MySqlParameter("@_estado", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));

                cmd.Parameters[0].Value = piso.Idpiso;
                cmd.Parameters[1].Value = piso.Numero;
                cmd.Parameters[2].Value = piso.Nombre;       
                cmd.Parameters[3].Value = piso.Estado;
                cmd.Parameters[4].Value = usureg.Idusuario;

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        filas_afectadas = Convert.ToInt32(dr["_filas_afectadas"]);
                    }
                    dr.Close();
                }
                tra.Commit(); tra.Dispose();
                return filas_afectadas;
            }
            catch (MySqlException ex)
            {
                filas_afectadas = -1;
                tra.Rollback();
                return filas_afectadas;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable listar_piso()
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_piso", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }

        public List<clsPiso>listar_piso_xestado()
        {
            List<clsPiso> lista_piso = null;
            clsPiso piso = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("listar_piso_xestado", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    lista_piso = new List<clsPiso>();

                    while (dr.Read())
                    {
                        piso = new clsPiso
                        {
                            Idpiso= (int)dr["idpiso"],
                            Numero=(int)dr["numero"],
                            Nombre=(string)dr["nombre"],
                            Estado=(int)dr["estado"]
                        };

                        lista_piso.Add(piso);
                    }
                    dr.Close();
                }
                return lista_piso;
            }
            catch (MySqlException ex)
            {
                return lista_piso;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }
    }
}
