﻿using FinalXML.Conexion;
using FinalXML.Interfaces;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinalXML.Entidades;

namespace FinalXML.InterMySql
{
    class MysqlIgv:IIgv
    {
        private clsConexionMysql con = new clsConexionMysql();
        private MySqlCommand cmd = null;
        private MySqlDataReader dr = null;
        private MySqlDataAdapter adap = null;
        private MySqlTransaction tra = null;
        private DataTable tabla = null;

        public clsIgv listar_igv_anual()
        {
            clsIgv igv = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("listar_igv_anual", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;                
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        igv = new clsIgv()
                        {
                            Idigv=(int)dr["igvid"],
                            Valor=(decimal)dr["valor"],
                            Valorinverso=(decimal)dr["valorinverso"],
                            Anho=(int)dr["anho"],
                            Estado=(int)dr["estado"]                            
                        };
                    }
                    dr.Close();
                }
                return igv;
            }
            catch (MySqlException ex)
            {
                return igv;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }
    }
}
