﻿using FinalXML;

using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace FinalXML
{
    [Serializable]
    public class Perception : IXmlSerializable, IEstructuraXml
    {
        public string UblVersionId { get; set; }

        public string CustomizationId { get; set; }

        public string Id { get; set; }

        public UBLExtensions UblExtensions { get; set; }

        public SignatureCac Signature { get; set; }

        public string IssueDate { get; set; }

        public AgentParty AgentParty { get; set; }

        public AgentParty ReceiverParty { get; set; }

        public string SunatPerceptionSystemCode { get; set; }

        public decimal SunatPerceptionPercent { get; set; }

        public string Note { get; set; }

        public PayableAmount TotalInvoiceAmount { get; set; }

        public PayableAmount TotalPaid { get; set; }

        public List<SUNATRetentionDocumentReference> SunatPerceptionDocumentReference { get; set; }

        public IFormatProvider Formato { get; set; }

        public Perception()
        {
            UblExtensions = new UBLExtensions();
            AgentParty = new AgentParty();
            ReceiverParty = new AgentParty();
            TotalInvoiceAmount = new PayableAmount();
            TotalPaid = new PayableAmount();
            SunatPerceptionDocumentReference = new List<SUNATRetentionDocumentReference>();

            UblVersionId = "2.0";
            CustomizationId = "1.0";
            Formato = new System.Globalization.CultureInfo(Constantes.Cultura);
        }

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            throw new NotImplementedException();
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteAttributeString("xmlns", EspacioNombres.xmlnsPerception);
            writer.WriteAttributeString("xmlns:cac", EspacioNombres.cac);
            writer.WriteAttributeString("xmlns:cbc", EspacioNombres.cbc);
            writer.WriteAttributeString("xmlns:ccts", EspacioNombres.ccts);
            writer.WriteAttributeString("xmlns:ds", EspacioNombres.ds);
            writer.WriteAttributeString("xmlns:ext", EspacioNombres.ext);
            writer.WriteAttributeString("xmlns:qdt", EspacioNombres.qdt);
            writer.WriteAttributeString("xmlns:sac", EspacioNombres.sac);
            writer.WriteAttributeString("xmlns:udt", EspacioNombres.udt);
            writer.WriteAttributeString("xmlns:xsi", EspacioNombres.xsi);

            #region UBLExtensions

            {
                writer.WriteStartElement("ext:UBLExtensions");

                #region UBLExtension

                {
                    writer.WriteStartElement("ext:UBLExtension");

                    #region ExtensionContent

                    {
                        writer.WriteStartElement("ext:ExtensionContent");

                        writer.WriteEndElement();
                    }

                    #endregion ExtensionContent

                    writer.WriteEndElement();
                }

                #endregion UBLExtension

                writer.WriteEndElement();
            }

            #endregion UBLExtensions

            writer.WriteElementString("cbc:UBLVersionID", UblVersionId);
            writer.WriteElementString("cbc:CustomizationID", CustomizationId);

            #region Signature

            writer.WriteStartElement("cac:Signature");
            {
                writer.WriteElementString("cbc:ID", Signature.ID);

                #region SignatoryParty

                writer.WriteStartElement("cac:SignatoryParty");
                {
                    writer.WriteStartElement("cac:PartyIdentification");
                    {
                        writer.WriteElementString("cbc:ID", Signature.SignatoryParty.PartyIdentification.ID.value);
                    }
                    writer.WriteEndElement();

                    #region PartyName

                    writer.WriteStartElement("cac:PartyName");
                    {
                        writer.WriteStartElement("cbc:Name");
                        writer.WriteCData(Signature.SignatoryParty.PartyName.Name);
                        writer.WriteEndElement();
                        //writer.WriteElementString("cbc:Name", Signature.SignatoryParty.PartyName.Name);
                    }
                    writer.WriteEndElement();

                    #endregion PartyName
                }
                writer.WriteEndElement();

                #endregion SignatoryParty

                #region DigitalSignatureAttachment

                writer.WriteStartElement("cac:DigitalSignatureAttachment");
                {
                    writer.WriteStartElement("cac:ExternalReference");
                    {
                        writer.WriteElementString("cbc:URI", Signature.DigitalSignatureAttachment.ExternalReference.URI);
                    }
                    writer.WriteEndElement();

                    #endregion DigitalSignatureAttachment
                }
                writer.WriteEndElement();
            }
            writer.WriteEndElement();

            #endregion Signature

            writer.WriteElementString("cbc:ID", Id);
            writer.WriteElementString("cbc:IssueDate", IssueDate);

            #region AgentParty

            writer.WriteStartElement("cac:AgentParty");

            #region PartyIdentification

            writer.WriteStartElement("cac:PartyIdentification");

            writer.WriteStartElement("cbc:ID");
            writer.WriteAttributeString("schemeID", AgentParty.PartyIdentification.ID.schemeID);
            writer.WriteValue(AgentParty.PartyIdentification.ID.value);
            writer.WriteEndElement();

            writer.WriteEndElement();

            #endregion PartyIdentification

            #region PartyName

            writer.WriteStartElement("cac:PartyName");
            {
                writer.WriteStartElement("cbc:Name");
                {
                    writer.WriteCData(AgentParty.PartyName.Name);
                }
                writer.WriteEndElement();
                //writer.WriteElementString("cbc:Name", AgentParty.PartyName.Name);
            }
            writer.WriteEndElement();

            #endregion PartyName

            #region PostalAddress

            writer.WriteStartElement("cac:PostalAddress");
            writer.WriteElementString("cbc:ID", AgentParty.PostalAddress.ID);
            writer.WriteStartElement("cbc:StreetName");
            {
                writer.WriteCData(AgentParty.PostalAddress.StreetName);
            }
            writer.WriteEndElement();
            //writer.WriteElementString("cbc:StreetName", AgentParty.PostalAddress.StreetName);
            writer.WriteElementString("cbc:CitySubdivisionName", AgentParty.PostalAddress.CitySubdivisionName);
            writer.WriteElementString("cbc:CityName", AgentParty.PostalAddress.CityName);
            writer.WriteElementString("cbc:CountrySubentity", AgentParty.PostalAddress.CountrySubentity);
            writer.WriteElementString("cbc:District", AgentParty.PostalAddress.District);

            #region Country

            writer.WriteStartElement("cac:Country");
            writer.WriteElementString("cbc:IdentificationCode",
                AgentParty.PostalAddress.Country.IdentificationCode);
            writer.WriteEndElement();

            #endregion Country

            writer.WriteEndElement();

            #endregion PostalAddress

            #region PartyLegalEntity

            writer.WriteStartElement("cac:PartyLegalEntity");
            {
                writer.WriteStartElement("cbc:RegistrationName");
                {
                    writer.WriteCData(AgentParty.PartyLegalEntity.RegistrationName);
                }
                writer.WriteEndElement();
                //writer.WriteElementString("cbc:RegistrationName", AgentParty.PartyLegalEntity.RegistrationName);
            }
            writer.WriteEndElement();

            #endregion PartyLegalEntity

            writer.WriteEndElement();

            #endregion AgentParty

            #region ReceiverParty

            writer.WriteStartElement("cac:ReceiverParty");
            {
                #region PartyIdentification

                writer.WriteStartElement("cac:PartyIdentification");
                {
                    writer.WriteStartElement("cbc:ID");
                    {
                        writer.WriteAttributeString("schemeID", ReceiverParty.PartyIdentification.ID.schemeID);
                        writer.WriteValue(ReceiverParty.PartyIdentification.ID.value);
                    }
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();

                #endregion PartyIdentification

                #region PartyName

                writer.WriteStartElement("cac:PartyName");
                {
                    writer.WriteStartElement("cbc:Name");
                    {
                        writer.WriteCData(ReceiverParty.PartyName.Name);
                    }
                    writer.WriteEndElement();
                    //writer.WriteElementString("cbc:Name", ReceiverParty.PartyName.Name);
                }
                writer.WriteEndElement();

                #endregion PartyName

                #region PostalAddress

                writer.WriteStartElement("cac:PostalAddress");
                {
                    if (!string.IsNullOrEmpty(ReceiverParty.PostalAddress.ID))
                        writer.WriteElementString("cbc:ID", ReceiverParty.PostalAddress.ID);
                    //writer.WriteElementString("cbc:StreetName", ReceiverParty.PostalAddress.StreetName);
                    writer.WriteStartElement("cbc:StreetName");
                    {
                        writer.WriteCData(AgentParty.PostalAddress.StreetName);
                    }
                    writer.WriteEndElement();
                    writer.WriteElementString("cbc:CitySubdivisionName", ReceiverParty.PostalAddress.CitySubdivisionName);
                    writer.WriteElementString("cbc:CityName", ReceiverParty.PostalAddress.CityName);
                    writer.WriteElementString("cbc:CountrySubentity", ReceiverParty.PostalAddress.CountrySubentity);
                    writer.WriteElementString("cbc:District", ReceiverParty.PostalAddress.District);

                    #region Country

                    writer.WriteStartElement("cac:Country");
                    {
                        writer.WriteElementString("cbc:IdentificationCode",
                            ReceiverParty.PostalAddress.Country.IdentificationCode);
                        writer.WriteEndElement();
                    }

                    #endregion Country
                }
                writer.WriteEndElement();

                #endregion PostalAddress

                #region PartyLegalEntity

                writer.WriteStartElement("cac:PartyLegalEntity");
                {
                    writer.WriteStartElement("cbc:RegistrationName");
                    {
                        writer.WriteCData(ReceiverParty.PartyLegalEntity.RegistrationName);
                    }
                    writer.WriteEndElement();
                    //writer.WriteElementString("cbc:RegistrationName", ReceiverParty.PartyLegalEntity.RegistrationName);
                }
                writer.WriteEndElement();

                #endregion PartyLegalEntity

                writer.WriteEndElement();
            }

            #endregion ReceiverParty

            writer.WriteElementString("sac:SUNATPerceptionSystemCode", SunatPerceptionSystemCode);
            writer.WriteElementString("sac:SUNATPerceptionPercent", SunatPerceptionPercent.ToString(Constantes.FormatoNumerico, Formato));
            if (!string.IsNullOrEmpty(Note))
                writer.WriteElementString("cbc:Note", Note);

            writer.WriteStartElement("cbc:TotalInvoiceAmount");
            {
                writer.WriteAttributeString("currencyID", TotalInvoiceAmount.currencyID);
                writer.WriteValue(TotalInvoiceAmount.value.ToString(Constantes.FormatoNumerico, Formato));
            }
            writer.WriteEndElement();

            writer.WriteStartElement("sac:SUNATTotalCashed");
            {
                writer.WriteAttributeString("currencyID", TotalPaid.currencyID);
                writer.WriteValue(TotalPaid.value.ToString(Constantes.FormatoNumerico, Formato));
            }
            writer.WriteEndElement();

            #region SUNATPerceptionDocumentReference

            foreach (var info in SunatPerceptionDocumentReference)
            {
                writer.WriteStartElement("sac:SUNATPerceptionDocumentReference");

                #region ID

                writer.WriteStartElement("cbc:ID");
                {
                    writer.WriteAttributeString("schemeID", info.ID.schemeID);
                    writer.WriteValue(info.ID.value);
                }
                writer.WriteEndElement();

                #endregion ID

                writer.WriteElementString("cbc:IssueDate", info.IssueDate);

                #region TotalInvoiceAmount

                writer.WriteStartElement("cbc:TotalInvoiceAmount");
                {
                    writer.WriteAttributeString("currencyID", info.TotalInvoiceAmount.currencyID);
                    writer.WriteValue(info.TotalInvoiceAmount.value.ToString(Constantes.FormatoNumerico, Formato));
                }
                writer.WriteEndElement();

                #endregion TotalInvoiceAmount

                #region Payment

                writer.WriteStartElement("cac:Payment");
                {
                    writer.WriteElementString("cbc:ID", info.Payment.IdPayment.ToString());

                    writer.WriteStartElement("cbc:PaidAmount");
                    {
                        writer.WriteAttributeString("currencyID", info.Payment.PaidAmount.currencyID);
                        writer.WriteValue(info.Payment.PaidAmount.value.ToString(Constantes.FormatoNumerico, Formato));
                        writer.WriteEndElement();
                    }
                    writer.WriteElementString("cbc:PaidDate", info.Payment.PaidDate);
                }
                writer.WriteEndElement();

                #endregion Payment

                #region SUNATPerceptionInformation

                writer.WriteStartElement("sac:SUNATPerceptionInformation");
                {
                    #region SUNATPerceptionAmount

                    writer.WriteStartElement("sac:SUNATPerceptionAmount");
                    {
                        writer.WriteAttributeString("currencyID", info.SUNATRetentionInformation.SunatRetentionAmount.currencyID);
                        writer.WriteValue(info.SUNATRetentionInformation.SunatRetentionAmount.value.ToString(Constantes.FormatoNumerico, Formato));
                    }
                    writer.WriteEndElement();

                    #endregion SUNATPerceptionAmount

                    writer.WriteElementString("sac:SUNATPerceptionDate", info.SUNATRetentionInformation.SUNATRetentionDate);

                    #region SUNATNetTotalCashed

                    writer.WriteStartElement("sac:SUNATNetTotalCashed");
                    {
                        writer.WriteAttributeString("currencyID", info.SUNATRetentionInformation.SUNATNetTotalPaid.currencyID);
                        writer.WriteValue(info.SUNATRetentionInformation.SUNATNetTotalPaid.value.ToString(Constantes.FormatoNumerico, Formato));
                    }
                    writer.WriteEndElement();

                    #endregion SUNATNetTotalCashed

                    #region ExchangeRate

                    writer.WriteStartElement("cac:ExchangeRate");
                    {
                        writer.WriteElementString("cbc:SourceCurrencyCode", info.SUNATRetentionInformation.ExchangeRate.SourceCurrencyCode);
                        writer.WriteElementString("cbc:TargetCurrencyCode", info.SUNATRetentionInformation.ExchangeRate.TargetCurrencyCode);
                        writer.WriteElementString("cbc:CalculationRate", info.SUNATRetentionInformation.ExchangeRate.CalculationRate.ToString(Constantes.FormatoNumerico, Formato));
                        writer.WriteElementString("cbc:Date",
                            !string.IsNullOrEmpty(info.SUNATRetentionInformation.ExchangeRate.Date)
                                ? info.SUNATRetentionInformation.ExchangeRate.Date
                                : info.SUNATRetentionInformation.SUNATRetentionDate);
                    }
                    writer.WriteEndElement();

                    #endregion ExchangeRate
                }
                writer.WriteEndElement();

                #endregion SUNATPerceptionInformation

                writer.WriteEndElement();
            }

            #endregion SUNATPerceptionDocumentReference
        }
    }
}