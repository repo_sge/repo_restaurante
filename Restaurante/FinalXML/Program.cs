﻿using System;
using System.IO;
using System.Windows.Forms;
using FinalXML;
using FinalXML.vista;

namespace FinalXML
{

   public static class Program
    {
        public static string CarpetaXml => "./XML";
        public static string CarpetaDocumentos => "./Documentos";
        public static string CarpetaCdr => "./CDR";
        public static string Certificado => "./Certificado";
        public static string CarpetaBoletas => "./BOLETAS_PDF";
        public static string CarpetaFacturas => "./FACTURAS_PDF";
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Application.EnableVisualStyles();
           // Application.SetCompatibleTextRenderingDefault(false);
           
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            try
            {
                if (!Directory.Exists(CarpetaXml))
                    Directory.CreateDirectory(CarpetaXml);
                if (!Directory.Exists(CarpetaCdr))
                    Directory.CreateDirectory(CarpetaCdr);
                if (!Directory.Exists(CarpetaBoletas))
                    Directory.CreateDirectory(CarpetaBoletas);
                if (!Directory.Exists(CarpetaFacturas))
                    Directory.CreateDirectory(CarpetaFacturas);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName);
            }
            Application.Run(new frmInicio());
        }
    }
}
