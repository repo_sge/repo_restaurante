﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FinalXML.Administradores;
using FinalXML.Entidades;

namespace FinalXML
{
    public partial class FrmDetalleDocumento : PlantillaBase
    {
        private readonly DetalleDocumento _detalle;
        private readonly DocumentoElectronico _documento;
        public FrmDetalleDocumento()
        {
            InitializeComponent();
        }
        public FrmDetalleDocumento(DetalleDocumento detalle, DocumentoElectronico documento)
        {
            InitializeComponent();
            _detalle = detalle;
            _documento = documento;

            detalleDocumentoBindingSource.DataSource = detalle;
            detalleDocumentoBindingSource.ResetBindings(false);

            Load += (s, e) =>
            {
                using (var ctx = new OpenInvoicePeruDb())
                {
                    tipoImpuestoBindingSource.DataSource = ctx.TipoImpuestos.ToList();
                    tipoImpuestoBindingSource.ResetBindings(false);

                    tipoPrecioBindingSource.DataSource = ctx.TipoPrecios.ToList();
                    tipoPrecioBindingSource.ResetBindings(false);
                }
            };
            decimal sumar = 0;
            sumar = documento.Items.Count() + 1;
            idNumericUpDown.Value = (sumar);
        }
        private void FrmDetalleDocumento_Load(object sender, EventArgs e)
        {

         
            decimal sumar = 0;
            idNumericUpDown.Value = 1;
            sumar = _documento.Items.Count() + 1;
            idNumericUpDown.Value = (sumar);

            cargarProductos();

        }

        public void cargarProductos()
        {

            /*try
            {

                clsAdmProducto admProducto = new clsAdmProducto();
                cmbPro.DataSource = admProducto.Listar();
                cmbPro.ValueMember = "codProducto";
                cmbPro.DisplayMember = "Descripcion";

            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }*/
        }

        private void toolCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void toolOk_Click(object sender, EventArgs e)
        {
            tipoPrecioComboBox.Focus();
            _detalle.UnidadMedida = unidadMedidaComboBox.Text;

            // Evaluamos el tipo de Impuesto.
            if (!_detalle.TipoImpuesto.StartsWith("1"))
            {
                _detalle.Suma = _detalle.PrecioUnitario * _detalle.Cantidad;
                _detalle.TotalVenta = _detalle.Suma;
            }
            else
            {
                if (_detalle.OtroImpuesto > 0)
                    _detalle.TotalVenta = _detalle.TotalVenta + _detalle.OtroImpuesto;
            }

            DialogResult = DialogResult.Cancel;
        }

        private void btnCalcIgv_Click(object sender, EventArgs e)
        {
            _detalle.Suma = _detalle.PrecioUnitario * _detalle.Cantidad;
            // _detalle.Impuesto = _detalle.Suma * _documento.CalculoIgv;


            if (_documento.CalculoIgv > 0)
            {
                _detalle.SubTotalVenta = _detalle.Suma;
                _detalle.Impuesto = _detalle.Suma * _documento.CalculoIgv;
                //Válido cuando el precio incluye IGV
                // _detalle.SubTotalVenta = Math.Round((_detalle.Suma / Convert.ToDecimal(1.18)), 2);
                // _detalle.Impuesto = Math.Round(_detalle.Suma - _detalle.SubTotalVenta, 2);
            }
            else
            {
                //_documento.SubTotalVenta = _detalle.Suma;
                _detalle.SubTotalVenta = _detalle.Suma;
                _detalle.Impuesto = 0;
            }


            //_detalle.TotalVenta = _detalle.Suma;
            _detalle.TotalVenta = _detalle.Suma;

            detalleDocumentoBindingSource.ResetBindings(false);
        }

        private void btnCalcIsc_Click(object sender, EventArgs e)
        {
            _detalle.Suma = _detalle.PrecioUnitario * _detalle.Cantidad;
            _detalle.ImpuestoSelectivo = _detalle.Suma * _documento.CalculoIsc;

            detalleDocumentoBindingSource.ResetBindings(false);
        }



        private void cantidadTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            
           if (e.KeyChar == (char)Keys.Enter)
            {
                _detalle.Cantidad = Convert.ToDecimal(cantidadTextBox.Text);
                btnCalcIgv_Click(null,null);
            }
        }


        private void cmbPro_SelectionChangeCommitted(object sender, EventArgs e)
        {
            
           /* try
            {
               
               if (cmbPro.Items.Count > 0 && cmbPro.SelectedValue.ToString() != "System.Data.DataRowView")
               {

                    clsAdmProducto admProducto = new clsAdmProducto();
                    clsProducto oProducto = null;

                    oProducto = admProducto.Buscar(Convert.ToInt32(cmbPro.SelectedValue.ToString()));

                    if (oProducto != null)
                    {
                       codigoItemTextBox.Text = oProducto.IdProducto.ToString();
                       descripcionTextBox.Text = oProducto.Descripcion.ToString();
                       precioUnitarioTextBox.Text = oProducto.Precio.ToString();

                       _detalle.CodigoItem = oProducto.IdProducto.ToString();
                       _detalle.Id = Convert.ToInt32(idNumericUpDown.Value);
                       _detalle.PrecioUnitario = oProducto.Precio;
                       _detalle.Descripcion = descripcionTextBox.Text;
                        
                       cantidadTextBox.Focus();
                    }

                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }*/
        }

        private void descripcionTextBox_TextChanged(object sender, EventArgs e)
        {
            descripcionTextBox.CharacterCasing = CharacterCasing.Upper;
        }
    }
}
