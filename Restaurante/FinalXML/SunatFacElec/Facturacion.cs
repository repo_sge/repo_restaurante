﻿using FinalXML.Administradores;
using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinApp.API;
using WinApp.Comun.Dto.Intercambio;
using WinApp.Comun.Dto.Modelos;
using WinApp.Firmado;
//using WinApp.Comun.Dto.Modelos;

namespace FinalXML.SunatFacElec
{
    public class Facturacion
    {
        #region Variables Privadas

        private WinApp.Comun.Dto.Modelos.DocumentoElectronico _documento;
        private Contribuyente dtsReceptor;
        private Contribuyente dtsEmisor;
        private WinApp.Comun.Dto.Intercambio.FirmadoResponse respuestaFirmado;

        #endregion

        #region Propiedades
        public string RutaArchivo { get; set; }
        public string IdDocumento { get; set; }
        public String RutaAlterna { get; set; }
        public Byte[] LogoEmp { get; set; }
        public Int32 enviado = 0;
        public RespuestaComunConArchivo respuestaEnvio;
        public EnviarDocumentoResponse rpta;
        public Int32 VerificaContribuyente = 0;
        public String datosAdicionales_CDB { get; set; }
        public String CodigoCertificado { get; set; }
        public String firmadig { get; set; }
        public String resumenfirmadig { get; set; }

        /*Clases*/
        clsEmpresa empresa = new clsEmpresa();
        clsProducto productos = new clsProducto();
        clsTipoDocumento tipodocumento = new clsTipoDocumento();
        clsTipoOperacion transacciones = new clsTipoOperacion();
        clsRepositorio repositorio = new clsRepositorio();
        //clsNotasCreditoDebitoVenta ds1 = new clsNotasCreditoDebitoVenta();
        clsComprobante venta = new clsComprobante();
        Conversion conv = new Conversion();
        Discrepancia discrepancia = new Discrepancia();
        DocumentoRelacionado dr = new DocumentoRelacionado();

        /*Listas*/
        List<clsRepositorio> lista_repositorio = null;

        /*Administradores*/
        clsAdmComprobante admfac = new clsAdmComprobante();
        clsAdmComprobante AdmVenta = new clsAdmComprobante();
        clsAdmTipoOperacion admTransacciones = new clsAdmTipoOperacion();
        clsAdmTipoDocumento admTipodocumento = new clsAdmTipoDocumento();
        clsAdmProducto admProductos = new clsAdmProducto();
        clsAdmEmpresa admEmpresa = new clsAdmEmpresa();
        //clsReporteFactura ds = new clsReporteFactura();
        clsAdmRepositorio admRepositorio = new clsAdmRepositorio();
        clsAdmRepositorio clsadmrepo = new clsAdmRepositorio();
        #endregion



        public Int32 CodigoErrorEnvio = 0;// 1) Error en el xml  2) Error de envio a sunat - falla de servidor


        public Facturacion()
        {
            _documento = new WinApp.Comun.Dto.Modelos.DocumentoElectronico();
            respuestaFirmado = new WinApp.Comun.Dto.Intercambio.FirmadoResponse();

        }
        #region Metodos de llendo de datos

        /**
         * Carga datos del comtribuyente
         * @param CosEmpresa @type int
         * @return 1 - 2
         * 1 - Correcto
         * 2 - Incorrecto
         * Mejorar la parte de cargar Departamento - Provincia - Distrito - Ubigeo
         **/
        private int DatosComtribuyente(Int32 CodEmpresa)
        {
            try
            {
                empresa = admEmpresa.listar_empresa_xestado();
                if (empresa == null)
                {
                    var dtsEmisor = new WinApp.Comun.Dto.Modelos.Contribuyente()
                    {
                        NroDocumento = empresa.Ruc,
                        TipoDocumento = "6",
                        Direccion = empresa.Direccion,
                        Departamento = "PIURA",
                        Provincia = "PIURA",
                        Distrito = "PIURA",
                        NombreLegal = empresa.Razonsocial,
                        NombreComercial = "",
                        Ubigeo = "200101",
                        CodDomicilioFiscal = "0000" //Código de cuatro dígitos asignado por SUNAT

                    };
                    _documento.Emisor = dtsEmisor;
                    return 1;
                }
                else
                {
                    return 2;
                }
            }
            catch (Exception a) { MessageBox.Show(a.Message); return 2; }


        }

        #region Set datos externos
        /**
         * Genera XML para ser guardados o enviados a SUNAT
         * @param cliente
         * @param venta
         * @param detalleventa
         * @return void
         * 
         **/
        public async Task GeneraDocumento(clsCliente cliente, clsComprobante venta, List<clsDetalleComprobante> detalleventa)
        {

            try
            {
                empresa = admEmpresa.listar_empresa_xestado();
                Cursor.Current = Cursors.WaitCursor;
              

                /**
                 * Carga TipoDocumento y Transacciones
                 * @param CodTipoDocumento
                 * @return TipoDocumento
                 * 
                 * @param CotTipoTransaccion
                 * @return Transacciones
                 **/
                tipodocumento = admTipodocumento.CargaTipoDocumento(venta.Idcomprobante);
                transacciones = admTransacciones.listar_tipooeracion_xid(venta.Tipooperacion.Idtipooeracion);

                /**
                 *
                 * Objeto que tiene los datos del cliente
                 *
                 **/
                dtsReceptor = new WinApp.Comun.Dto.Modelos.Contribuyente()
                {
                    NroDocumento = cliente.Documento,
                    TipoDocumento = cliente.Tipodocidentidad.Codsunat,
                    NombreLegal = cliente.Razonsocial,
                    NombreComercial = "",
                    Direccion = cliente.Direccion
                };

                dtsEmisor=new WinApp.Comun.Dto.Modelos.Contribuyente()
                {
                    NroDocumento = empresa.Ruc,
                    TipoDocumento = empresa.Tipodocidentidad.Codsunat,
                    NombreLegal = empresa.Razonsocial,
                    NombreComercial = "",
                    Direccion = empresa.Direccion,
                    Departamento=empresa.Departamento,
                    Distrito=empresa.Distrito,
                    Provincia=empresa.Provincia,
                    Ubigeo=empresa.Ubigeo
                };
            

                /**
                 * @val _documento.TipoDocumento
                 *  01 Factura
                 *  03 Boleta
                 *  07 NC
                 *  08 ND
                 *  
                 *  @val _documento.Glosa
                 *  Agregado en la nueva version 2.1
                 *  
                 *  @val  _documento.TipoOperacion
                 *  Agregado en la nueva version 2.1, considerando como venta interna 0101
                 *  Valor de Códigos Catálogo N°51
                 * 
                 **/

                _documento.TipoDocumento = tipodocumento.Tipodoccodsunat;
                _documento.Receptor = dtsReceptor;
                _documento.Emisor = dtsEmisor;
                _documento.FechaEmision = venta.Fechaemsion.ToShortDateString();
                _documento.TipoOperacion = transacciones.Codsunat;//0101 - Venta interna
                _documento.Glosa = venta.Descripcion;
                
                /**
                 * 
                 * Valida el tipo de moneda
                 * 
                 **/

                if (venta.Moneda.IcodMoneda == 1)
                {
                    _documento.Moneda = "PEN";
                }
                else
                {
                    _documento.Moneda = "USD";
                }

                /**
                 * Consulta y valída contribuyente
                 * @param Codempresa
                 * @return 1 - 2
                 * 1 válido 
                 * 2 error
                 **/

                VerificaContribuyente = admEmpresa.listar_empresa_xestado().Idempresa;

                if (VerificaContribuyente == 2)
                {
                    MessageBox.Show("No se puede generar documento\n Falta cargar datos de la empresa");
                    return;
                }

                /**
                 * 
                 *Solo evaluamos Facturas y Boletas debido a que las NC y ND se eejecutan desde otro formulario
                 * y usa otros métodos.
                 * Evaluar alguna solución para ejecutar todo aquí
                 **/

                switch (_documento.TipoDocumento)
                {
                    case "07":

                        break;
                    case "08":

                        break;
                    case "03":

                        _documento.IdDocumento = venta.Numero;
                        DatosFactura(cliente, venta, detalleventa);

                        break;
                    case "01":
                        _documento.IdDocumento = venta.Numero;
                        DatosFactura(cliente, venta, detalleventa);
                        break;
                }

                //_documento.MontoEnLetras = conv.enletras(venta.Total.ToString()); //Monto en letras agregado

                /**
                 * @val  serializador
                 * Serializa todo el objeto _documento y es enviado al método Post
                 * 
                 * @param _documento
                 * @method GenerarFactura
                 * Tener en cuenta que estos métodos son Asyncronos
                 * @return response
                 * 
                 * 
                 **/
                ISerializador serializador = new WinApp.Firmado.Serializador();
                DocumentoResponse response = new DocumentoResponse
                {
                    Exito = false
                };
                response = await new GenerarFactura(serializador).Post(_documento);

                /**
                 * @return response | type bool
                 * True | Guarda el archivo XML en carpeta
                 * false | Muestra Error
                 * RutaArchivo | Todos los documentos sin firmar se guardan en esa ruta
                 **/
                if (!response.Exito)
                    MessageBox.Show(response.MensajeError);

                RutaArchivo = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "Documentos\\",
                    $"{_documento.IdDocumento}.xml");

                File.WriteAllBytes(RutaArchivo, Convert.FromBase64String(response.TramaXmlSinFirma));

                /**
                 *@Method Firmar()                 
                 * Se realiza el firmado del documento
                 * se usa await debido a que llama a un método asincrono, este no devuelve ningún valor
                 * 
                 */

                await Firmar();


                /**
                 * @val RutaAlterna
                 * Ruta donde se guardan los documentos C:\
                 * Evalua las rutas donde se van a guardar los documentos firmados, se guardan en 2 rutas para contrarestar pérdida de los mismos
                 * 
                 */

                switch (_documento.TipoDocumento)
                {

                    case "03":
                        File.WriteAllBytes($"{Program.CarpetaXml}\\{empresa.Ruc + "-" + _documento.TipoDocumento + "-" + _documento.IdDocumento}.xml",
                                  Convert.FromBase64String(respuestaFirmado.TramaXmlFirmado)); 

                        //File.WriteAllBytes(@"C:\DOCUMENTOS-" + empresa.Ruc + "\\DOCUMENTOS ENVIAR\\BOLETAS\\" + _documento.Emisor.NroDocumento + "-" + _documento.TipoDocumento + "-" + _documento.IdDocumento + ".xml",
                        //    Convert.FromBase64String(respuestaFirmado.TramaXmlFirmado));


                        break;
                    case "01":

                        File.WriteAllBytes($"{Program.CarpetaXml}\\{empresa.Ruc + "-" + _documento.TipoDocumento + "-" + _documento.IdDocumento}.xml",
                                   Convert.FromBase64String(respuestaFirmado.TramaXmlFirmado));

                        //File.WriteAllBytes(@"C:\DOCUMENTOS-" + empresa.Ruc + _documento.Emisor.NroDocumento + "-" + _documento.TipoDocumento + "-" + _documento.IdDocumento + ".xml",
                        //    Convert.FromBase64String(respuestaFirmado.TramaXmlFirmado));


                        break;
                }


                /**
                 * @Method GeneraPDF
                 * @Param CodFacturaVenta
                 * @resumenfirmadig Resumen de Firma Digital
                 * @firmadig Valor de Firma Digital 
                 **/

                //resumenfirmadig = respuestaFirmado.ResumenFirma;
                //firmadig = respuestaFirmado.ValorFirma;

                //GeneraPDF(Convert.ToInt32(venta.Idcomprobante));

                ///**
                // * Set's para el repositorio de documentos                 
                // */


                //string mirutadearchivo = "";
                //repositorio.Tipodocumento = venta.CodTipoDocumento;
                //repositorio.Nombredoc = _documento.Emisor.NroDocumento + "-" + _documento.TipoDocumento + "-" + _documento.IdDocumento;
                //repositorio.Fechaemision = venta.FechaPago;
                //repositorio.Serie = venta.Serie;
                //repositorio.Correlativo = venta.NumDoc;
                //repositorio.Monto = Convert.ToDecimal(venta.Total);
                //repositorio.CodEmpresa = frmLogin.iCodEmpresa;
                //repositorio.CodSucursal = frmLogin.iCodSucursal;
                //repositorio.CodAlmacen = frmLogin.iCodAlmacen;
                //repositorio.CodFacturaVenta = Convert.ToInt32(venta.CodFacturaVenta);


                //repositorio.Estadosunat = "-1";
                //repositorio.Mensajesunat = "No enviada";

                //if (repositorio.Tipodoc == 2)//1 es boleta,2 es factura
                //{
                //    mirutadearchivo = @"C:\DOCUMENTOS-" + empresa.Ruc + "\\DOCUMENTOS ENVIAR\\FACTURAS\\" + repositorio.Nombredoc + ".xml";
                //}
                //else
                //{
                //    mirutadearchivo = @"C:\DOCUMENTOS-" + empresa.Ruc + "\\DOCUMENTOS ENVIAR\\BOLETAS\\" + repositorio.Nombredoc + ".xml";
                //}
                //repositorio.Xml = File.ReadAllBytes(mirutadearchivo);
                //repositorio.Pdf = File.ReadAllBytes(mirutadearchivo.Replace(".xml", ".pdf"));
                //repositorio.Usuario = frmLogin.iCodUser;




                //if (!admRepositorio.registra_repositorio(repositorio))
                //{
                //    MessageBox.Show("Documento no se pudo enviar al repositorio");
                //}
         

            }
            catch (Exception a) { MessageBox.Show(a.Message); }
            finally { Cursor.Current = Cursors.Default; }

        }

        #endregion

        /**
        * Llenado de datos para la Factura
        * 
        **/
        private void DatosFactura(clsCliente cliente, clsComprobante venta, List<clsDetalleComprobante> detalleventa)
        {
            try
            {
                _documento.Items.Clear();

                Int32 contadori = 1;
                foreach (clsDetalleComprobante lista in detalleventa)
                {
                    var dtsItems = new WinApp.Comun.Dto.Modelos.DetalleDocumento
                    {
                        Id = contadori,
                        Cantidad = Convert.ToDecimal(lista.Detallepedido.Cantidad),
                        UnidadMedida = lista.Detallepedido.Unidad.Sigla,
                        CodigoItem = lista.Detallepedido.Producto.IdProducto.ToString(),
                        ItemClassificationCode = lista.Detallepedido.Producto.Codsunat,// Este código será obligatorio para el 1-1-2019 - catálogo N° 15 del Anexo N° 8
                        Descripcion = lista.Detallepedido.Producto.Nombre,
                        PrecioUnitario = Convert.ToDecimal(Math.Round(lista.Detallepedido.Preciounitario/1.18m,2)),
                        PrecioReferencial = Convert.ToDecimal(Math.Round(lista.Detallepedido.Preciounitario,2)),
                        TipoPrecio = "01",
                        TipoImpuesto = lista.Tipoimpuesto.Codsunat,
                        OtroImpuesto = 0,
                        Suma = Convert.ToDecimal(Math.Round(lista.Subtotal, 2)),
                        Impuesto = Math.Round(lista.Igv, 2),
                        ImpuestoSelectivo = 0,
                        TotalVenta = Convert.ToDecimal(Math.Round(lista.Subtotal, 2))
                    };

                    /**
                     * @param CodProducto
                     * @param CodAlmacen
                     * @return Object
                     **/
                    productos = admProductos.listar_productoxid(lista.Detallepedido.Producto.IdProducto);
                    /*if (productos.CodTipoArticulo == 2) // 2 - servicios
                    {
                        _documento.MontoDetraccion = Convert.ToDecimal(Convert.ToDouble(_documento.Gravadas) * Convert.ToDouble(productos.Porcentajerentencion));
                    }*/

                    //Agregamos Detalle
                    _documento.Items.Add(dtsItems);
                    contadori++;
                }


                CalcularTotales();
            }
            catch (Exception a) { MessageBox.Show(a.Message); }
        }

        //public async void DatosNCredito(clsCliente cliente, clsNotaCredito nc, List<clsDetalleNotaCredito> detalle_nc)
        //{
        //    try
        //    {
        //        Cursor.Current = Cursors.WaitCursor;



        //        empresa = admEmpresa.CargaEmpresa3(frmLogin.iCodEmpresa);

        //        /**
        //         * Carga TipoDocumento y Transacciones
        //         * @param CodTipoDocumento
        //         * @return TipoDocumento
        //         * 
        //         * @param CotTipoTransaccion
        //         * @return Transacciones
        //         **/
        //        tipodocumento = admTipodocumento.CargaTipoDocumento(nc.CodTipoDocumento);
        //        transacciones = admTransacciones.MuestraTransaccion(nc.CodTipoTransaccion);

        //        /**
        //         * Carga la venta relacionada a la factura
        //         * @Method CargaFacturaVenta
        //         * @param CodReferencia
        //         * 
        //         **/
        //        clsFacturaVenta venta = new clsFacturaVenta();
        //        venta = admfac.CargaFacturaVenta(nc.CodReferencia);

        //        /**
        //        *
        //        * Objeto que tiene los datos del cliente
        //        *
        //        **/
        //        dtsReceptor = new Contribuyente()
        //        {
        //            NroDocumento = cliente.RucDni,
        //            TipoDocumento = cliente.DocumentoIdentidad.CodigoSunat.ToString(),
        //            NombreLegal = cliente.RazonSocial,
        //            NombreComercial = "",
        //            Direccion = cliente.DireccionLegal
        //        };

        //        /**
        //         * @val _documento.TipoDocumento
        //         *  01 Factura
        //         *  03 Boleta
        //         *  07 NC
        //         *  08 ND            
        //         * 
        //         **/

        //        _documento.TipoDocumento = tipodocumento.Tipodoccodsunat.ToString();



        //        _documento.Receptor = dtsReceptor;
        //        _documento.FechaEmision = DateTime.Today.ToShortDateString();
        //        _documento.TipoOperacion = transacciones.Codsunat;//0101 - Venta interna


        //        /**
        //        * 
        //        * Valida el tipo de moneda
        //        * 
        //        **/

        //        if (venta.Moneda == 1)
        //        {
        //            _documento.Moneda = "PEN";
        //        }
        //        else
        //        {
        //            _documento.Moneda = "USD";
        //        }

        //        /**
        //         * Consulta y valída contribuyente
        //         * @param Codempresa
        //         * @return 1 - 2
        //         * 1 válido 
        //         * 2 error
        //         **/

        //        VerificaContribuyente = DatosComtribuyente(venta.CodEmpresa);

        //        if (VerificaContribuyente == 2)
        //        {
        //            MessageBox.Show("No se puede generar documento\n Falta cargar datos de la empresa");
        //            return;
        //        }



        //        Int32 contador = 1;
        //        foreach (clsDetalleNotaCredito lista in detalle_nc)
        //        {


        //            var dtsItems = new DetalleDocumento
        //            {
        //                Id = contador,
        //                Cantidad = Convert.ToDecimal(lista.Cantidad),
        //                UnidadMedida = admProductos.SiglaUnidadBase(lista.UnidadIngresada),
        //                CodigoItem = contador.ToString(),
        //                //ItemClassificationCode = "82141601",
        //                Descripcion = lista.DescripcionNC,
        //                PrecioUnitario = Convert.ToDecimal(lista.ValoReal),
        //                PrecioReferencial = Convert.ToDecimal(lista.PrecioUnitario),
        //                TipoPrecio = "01",
        //                TipoImpuesto = lista.TipoImpuesto,
        //                OtroImpuesto = 0,
        //                Descuento = 0,
        //                Suma = Convert.ToDecimal(lista.ValoReal) * Convert.ToDecimal(lista.Cantidad),
        //                Impuesto = (Convert.ToDecimal(lista.ValoReal) * Convert.ToDecimal(lista.Cantidad)) * _documento.CalculoIgv,
        //                ImpuestoSelectivo = 0,
        //                TotalVenta = (Convert.ToDecimal(lista.ValoReal) * Convert.ToDecimal(lista.Cantidad)) - Convert.ToDecimal(lista.Descuento1)
        //            };


        //            /**
        //              * @param CodProducto
        //              * @param CodAlmacen
        //              * @return Object
        //              **/
        //            productos = admProductos.CargaProducto(lista.CodProducto, frmLogin.iCodAlmacen);

        //            if (productos.CodTipoArticulo == 2) // 2 - servicios
        //            {
        //                _documento.MontoDetraccion = Convert.ToDecimal(Convert.ToDouble(_documento.Gravadas) * Convert.ToDouble(productos.Porcentajerentencion));
        //            }

        //            //Agregamos Detalle
        //            _documento.Items.Add(dtsItems);
        //            contador++;
        //        }


        //        /**
        //         * 
        //         * Verifica si el tipo de documento al que se relaciona
        //         * es Boleta - Factura, y según eso se le asigna la serie
        //         **/

        //        if (venta.CodTipoDocumento == 2)
        //        {
        //            _documento.IdDocumento = "F" + nc.Serie + "-" + nc.NumFac.PadLeft(8, '0');
        //            _documento.TipoDocumento = tipodocumento.Tipodoccodsunat;
        //            _documento.TipoOperacion = transacciones.Codsunat;
        //        }
        //        else if (venta.CodTipoDocumento == 1)
        //        {
        //            _documento.IdDocumento = "B" + nc.Serie + "-" + nc.NumFac.PadLeft(8, '0');
        //            _documento.TipoDocumento = tipodocumento.Tipodoccodsunat;
        //            _documento.TipoOperacion = transacciones.Codsunat;
        //        }


        //        _documento.Receptor = dtsReceptor;

        //        /**
        //         * 
        //         * Calcula Totales
        //         **/
        //        CalcularTotales();



        //        /**
        //         * 
        //         * Verificamos y agregamos documento relaciona a la NC
        //         **/

        //        var dtsDocumentoRelacionado = new DocumentoRelacionado
        //        {
        //            NroDocumento = venta.CodTipoDocumento == 2 ? "F" + venta.Serie + "-" + venta.NumDoc.PadLeft(8, '0') : "B" + venta.Serie + "-" + venta.NumDoc.PadLeft(8, '0'),
        //            TipoDocumento = venta.CodTipoDocumento == 2 ? "01" : "03"
        //        };
        //        _documento.Relacionados.Add(dtsDocumentoRelacionado);

        //        var dtsDiscrepancia = new Discrepancia
        //        {
        //            NroReferencia = venta.CodTipoDocumento == 2 ? "F" + venta.Serie + "-" + venta.NumDoc.PadLeft(8, '0') : "B" + venta.Serie + "-" + venta.NumDoc.PadLeft(8, '0'),
        //            Tipo = venta.CodTipoDocumento == 2 ? "01" : "03",
        //            Descripcion = nc.Comentario.ToString()
        //        };
        //        _documento.Discrepancias.Add(dtsDiscrepancia);





        //        /**
        //         * @val  serializador
        //         * Serializa todo el objeto _documento y es enviado al método Post
        //         * 
        //         * @param _documento
        //         * @method GenerarFactura
        //         * Tener en cuenta que estos métodos son Asyncronos
        //         * @return response
        //         * 
        //         * 
        //         **/
        //        ISerializador serializador = new Serializador();
        //        DocumentoResponse response = new DocumentoResponse
        //        {
        //            Exito = false
        //        };
        //        response = await new GenerarNotaCredito(serializador).Post(_documento);

        //        /**
        //         * @return response | type bool
        //         * True | Guarda el archivo XML en carpeta
        //         * false | Muestra Error
        //         * RutaArchivo | Todos los documentos sin firmar se guardan en esa ruta
        //         **/
        //        if (!response.Exito)
        //            MessageBox.Show(response.MensajeError);

        //        RutaArchivo = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "documentos\\",
        //            $"{_documento.Emisor.NroDocumento + "-" + _documento.TipoDocumento + "-" + _documento.IdDocumento}.xml");

        //        File.WriteAllBytes(RutaArchivo, Convert.FromBase64String(response.TramaXmlSinFirma));

        //        /**
        //         *@Method Firmar()                 
        //         * Se realiza el firmado del documento
        //         * se usa await debido a que llama a un método asincrono, este no devuelve ningún valor
        //         * 
        //         */

        //        await Firmar();


        //        /**
        //         * @val RutaAlterna
        //         * Ruta donde se guardan los documentos C:\
        //         * Evalua las rutas donde se van a guardar los documentos firmados, se guardan en 2 rutas para contrarestar pérdida de los mismos
        //         * 
        //         */


        //        File.WriteAllBytes($"{Program.CarpetaNC}\\{_documento.Emisor.NroDocumento + "-" + _documento.TipoDocumento + "-" + _documento.IdDocumento}.xml",
        //                    Convert.FromBase64String(respuestaFirmado.TramaXmlFirmado));

        //        File.WriteAllBytes(@"C:\DOCUMENTOS-" + empresa.Ruc + "\\DOCUMENTOS ENVIAR\\NOTAS CREDITO\\" + _documento.Emisor.NroDocumento + "-" + _documento.TipoDocumento + "-" + _documento.IdDocumento + ".xml",
        //            Convert.FromBase64String(respuestaFirmado.TramaXmlFirmado));



        //        /**
        //         * @Method GeneraPDF_NC
        //         * @Param CodFacturaVenta
        //         * @resumenfirmadig Resumen de Firma Digital
        //         * @firmadig Valor de Firma Digital 
        //         **/

        //        resumenfirmadig = respuestaFirmado.ResumenFirma;
        //        firmadig = respuestaFirmado.ValorFirma;

        //        GeneraPDF_NC(Convert.ToInt32(nc.CodNotaCreditoNueva));

        //        /**
        //         * Set's para el repositorio de documentos                 
        //         */


        //        string mirutadearchivo = "";
        //        repositorio.Tipodoc = nc.CodTipoDocumento;
        //        repositorio.Nombredoc = _documento.Emisor.NroDocumento + "-" + _documento.TipoDocumento + "-" + _documento.IdDocumento; ;
        //        repositorio.Fechaemision = nc.FechaPago;
        //        repositorio.Serie = nc.Serie;
        //        repositorio.Correlativo = nc.NumFac;
        //        repositorio.Monto = Convert.ToDecimal(nc.Total);
        //        repositorio.Estadosunat = "-1";
        //        repositorio.Mensajesunat = "No enviada";
        //        mirutadearchivo = @"C:\DOCUMENTOS-" + empresa.Ruc + "\\DOCUMENTOS ENVIAR\\NOTAS CREDITO\\" + repositorio.Nombredoc + ".xml";
        //        repositorio.Xml = File.ReadAllBytes(mirutadearchivo);
        //        repositorio.Pdf = File.ReadAllBytes(mirutadearchivo.Replace(".xml", ".pdf"));
        //        repositorio.Usuario = frmLogin.iCodUser;
        //        repositorio.CodEmpresa = frmLogin.iCodEmpresa;
        //        repositorio.CodSucursal = frmLogin.iCodSucursal;
        //        repositorio.CodAlmacen = frmLogin.iCodAlmacen;
        //        repositorio.CodFacturaVenta = Convert.ToInt32(nc.CodNotaCreditoNueva);
        //        repositorio.TipDocRelacion = _documento.IdDocumento;




        //        if (!admRepositorio.registra_repositorio(repositorio))
        //        {
        //            MessageBox.Show("Documento no se pudo enviar al repositorio");
        //        }



        //    }
        //    catch (Exception a) { MessageBox.Show(a.Message); }
        //    finally { Cursor.Current = Cursors.Default; }
        //}

        //public async void DatosNDebito(clsCliente cliente, clsNotaDebito nd, List<clsDetalleNotaDebito> detalle_nc)
        //{
        //    try
        //    {
        //        Cursor.Current = Cursors.WaitCursor;



        //        empresa = admEmpresa.CargaEmpresa3(frmLogin.iCodEmpresa);

        //        /**
        //         * Carga TipoDocumento y Transacciones
        //         * @param CodTipoDocumento
        //         * @return TipoDocumento
        //         * 
        //         * @param CotTipoTransaccion
        //         * @return Transacciones
        //         **/
        //        tipodocumento = admTipodocumento.CargaTipoDocumento(nd.CodTipoDocumento);
        //        transacciones = admTransacciones.MuestraTransaccion(nd.CodTipoTransaccion);

        //        /**
        //         * Carga la venta relacionada a la factura
        //         * @Method CargaFacturaVenta
        //         * @param CodReferencia
        //         * 
        //         **/
        //        clsFacturaVenta venta = new clsFacturaVenta();
        //        venta = admfac.CargaFacturaVenta(nd.CodReferencia);

        //        /**
        //        *
        //        * Objeto que tiene los datos del cliente
        //        *
        //        **/
        //        dtsReceptor = new Contribuyente()
        //        {
        //            NroDocumento = cliente.RucDni,
        //            TipoDocumento = cliente.DocumentoIdentidad.CodigoSunat.ToString(),
        //            NombreLegal = cliente.RazonSocial,
        //            NombreComercial = "",
        //            Direccion = cliente.DireccionLegal
        //        };

        //        /**
        //         * @val _documento.TipoDocumento
        //         *  01 Factura
        //         *  03 Boleta
        //         *  07 NC
        //         *  08 ND            
        //         * 
        //         **/

        //        _documento.TipoDocumento = tipodocumento.Tipodoccodsunat.ToString();



        //        _documento.Receptor = dtsReceptor;
        //        _documento.FechaEmision = DateTime.Today.ToShortDateString();
        //        _documento.TipoOperacion = transacciones.Codsunat;//0101 - Venta interna


        //        /**
        //        * 
        //        * Valida el tipo de moneda
        //        * 
        //        **/

        //        if (venta.Moneda == 1)
        //        {
        //            _documento.Moneda = "PEN";
        //        }
        //        else
        //        {
        //            _documento.Moneda = "USD";
        //        }

        //        /**
        //         * Consulta y valída contribuyente
        //         * @param Codempresa
        //         * @return 1 - 2
        //         * 1 válido 
        //         * 2 error
        //         **/

        //        VerificaContribuyente = DatosComtribuyente(venta.CodEmpresa);

        //        if (VerificaContribuyente == 2)
        //        {
        //            MessageBox.Show("No se puede generar documento\n Falta cargar datos de la empresa");
        //            return;
        //        }



        //        Int32 contador = 1;
        //        foreach (clsDetalleNotaDebito lista in detalle_nc)
        //        {


        //            var dtsItems = new DetalleDocumento
        //            {
        //                Id = contador,
        //                Cantidad = Convert.ToDecimal(lista.Cantidad),
        //                UnidadMedida = admProductos.SiglaUnidadBase(lista.UnidadIngresada),
        //                CodigoItem = contador.ToString(),
        //                //ItemClassificationCode = "82141601",
        //                Descripcion = lista.DescripcionND,
        //                PrecioUnitario = Convert.ToDecimal(lista.ValoReal),
        //                PrecioReferencial = Convert.ToDecimal(lista.PrecioUnitario),
        //                TipoPrecio = "01",
        //                TipoImpuesto = lista.Tipoimpuesto,
        //                OtroImpuesto = 0,
        //                Descuento = 0,
        //                Suma = Convert.ToDecimal(lista.ValoReal) * Convert.ToDecimal(lista.Cantidad),
        //                Impuesto = (Convert.ToDecimal(lista.ValoReal) * Convert.ToDecimal(lista.Cantidad)) * _documento.CalculoIgv,
        //                ImpuestoSelectivo = 0,
        //                TotalVenta = (Convert.ToDecimal(lista.ValoReal) * Convert.ToDecimal(lista.Cantidad)) - Convert.ToDecimal(lista.Descuento1)
        //            };


        //            /**
        //              * @param CodProducto
        //              * @param CodAlmacen
        //              * @return Object
        //              **/
        //            productos = admProductos.CargaProducto(lista.CodProducto, frmLogin.iCodAlmacen);

        //            if (productos.CodTipoArticulo == 2) // 2 - servicios
        //            {
        //                _documento.MontoDetraccion = Convert.ToDecimal(Convert.ToDouble(_documento.Gravadas) * Convert.ToDouble(productos.Porcentajerentencion));
        //            }

        //            //Agregamos Detalle
        //            _documento.Items.Add(dtsItems);
        //            contador++;
        //        }


        //        /**
        //         * 
        //         * Verifica si el tipo de documento al que se relaciona
        //         * es Boleta - Factura, y según eso se le asigna la serie
        //         **/

        //        if (venta.CodTipoDocumento == 2)
        //        {
        //            _documento.IdDocumento = "F" + nd.Serie + "-" + nd.NumFac.PadLeft(8, '0');
        //            _documento.TipoDocumento = tipodocumento.Tipodoccodsunat;
        //            _documento.TipoOperacion = transacciones.Codsunat;
        //        }
        //        else if (venta.CodTipoDocumento == 1)
        //        {
        //            _documento.IdDocumento = "B" + nd.Serie + "-" + nd.NumFac.PadLeft(8, '0');
        //            _documento.TipoDocumento = tipodocumento.Tipodoccodsunat;
        //            _documento.TipoOperacion = transacciones.Codsunat;
        //        }


        //        _documento.Receptor = dtsReceptor;

        //        /**
        //         * 
        //         * Calcula Totales
        //         **/
        //        CalcularTotales();



        //        /**
        //         * 
        //         * Verificamos y agregamos documento relaciona a la NC
        //         **/

        //        var dtsDocumentoRelacionado = new DocumentoRelacionado
        //        {
        //            NroDocumento = venta.CodTipoDocumento == 2 ? "F" + venta.Serie + "-" + venta.NumDoc.PadLeft(8, '0') : "B" + venta.Serie + "-" + venta.NumDoc.PadLeft(8, '0'),
        //            TipoDocumento = venta.CodTipoDocumento == 2 ? "01" : "03"
        //        };
        //        _documento.Relacionados.Add(dtsDocumentoRelacionado);

        //        var dtsDiscrepancia = new Discrepancia
        //        {
        //            NroReferencia = venta.CodTipoDocumento == 2 ? "F" + venta.Serie + "-" + venta.NumDoc.PadLeft(8, '0') : "B" + venta.Serie + "-" + venta.NumDoc.PadLeft(8, '0'),
        //            Tipo = venta.CodTipoDocumento == 2 ? "01" : "03",
        //            Descripcion = nd.Comentario.ToString()
        //        };
        //        _documento.Discrepancias.Add(dtsDiscrepancia);





        //        /**
        //         * @val  serializador
        //         * Serializa todo el objeto _documento y es enviado al método Post
        //         * 
        //         * @param _documento
        //         * @method GenerarFactura
        //         * Tener en cuenta que estos métodos son Asyncronos
        //         * @return response
        //         * 
        //         * 
        //         **/
        //        ISerializador serializador = new Serializador();
        //        DocumentoResponse response = new DocumentoResponse
        //        {
        //            Exito = false
        //        };
        //        response = await new GenerarNotaDedito(serializador).Post(_documento);

        //        /**
        //         * @return response | type bool
        //         * True | Guarda el archivo XML en carpeta
        //         * false | Muestra Error
        //         * RutaArchivo | Todos los documentos sin firmar se guardan en esa ruta
        //         **/
        //        if (!response.Exito)
        //            MessageBox.Show(response.MensajeError);

        //        RutaArchivo = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "documentos\\",
        //            $"{_documento.Emisor.NroDocumento + "-" + _documento.TipoDocumento + "-" + _documento.IdDocumento}.xml");

        //        File.WriteAllBytes(RutaArchivo, Convert.FromBase64String(response.TramaXmlSinFirma));

        //        /**
        //         *@Method Firmar()                 
        //         * Se realiza el firmado del documento
        //         * se usa await debido a que llama a un método asincrono, este no devuelve ningún valor
        //         * 
        //         */

        //        await Firmar();


        //        /**
        //         * @val RutaAlterna
        //         * Ruta donde se guardan los documentos C:\
        //         * Evalua las rutas donde se van a guardar los documentos firmados, se guardan en 2 rutas para contrarestar pérdida de los mismos
        //         * 
        //         */


        //        File.WriteAllBytes($"{Program.CarpetaNC}\\{_documento.Emisor.NroDocumento + "-" + _documento.TipoDocumento + "-" + _documento.IdDocumento}.xml",
        //                    Convert.FromBase64String(respuestaFirmado.TramaXmlFirmado));

        //        File.WriteAllBytes(@"C:\DOCUMENTOS-" + empresa.Ruc + "\\DOCUMENTOS ENVIAR\\NOTAS DEBITO\\" + _documento.Emisor.NroDocumento + "-" + _documento.TipoDocumento + "-" + _documento.IdDocumento + ".xml",
        //            Convert.FromBase64String(respuestaFirmado.TramaXmlFirmado));



        //        /**
        //         * @Method GeneraPDF_NC
        //         * @Param CodFacturaVenta
        //         * @resumenfirmadig Resumen de Firma Digital
        //         * @firmadig Valor de Firma Digital 
        //         **/

        //        resumenfirmadig = respuestaFirmado.ResumenFirma;
        //        firmadig = respuestaFirmado.ValorFirma;

        //        GeneraPDF_ND(Convert.ToInt32(nd.CodNotaDebitoNueva));

        //        /**
        //         * Set's para el repositorio de documentos                 
        //         */


        //        string mirutadearchivo = "";
        //        repositorio.Tipodoc = nd.CodTipoDocumento;
        //        repositorio.Nombredoc = _documento.Emisor.NroDocumento + "-" + _documento.TipoDocumento + "-" + _documento.IdDocumento; ;
        //        repositorio.Fechaemision = nd.FechaPago;
        //        repositorio.Serie = nd.Serie;
        //        repositorio.Correlativo = nd.NumFac;
        //        repositorio.Monto = Convert.ToDecimal(nd.Total);
        //        repositorio.Estadosunat = "-1";
        //        repositorio.Mensajesunat = "No enviada";
        //        mirutadearchivo = @"C:\DOCUMENTOS-" + empresa.Ruc + "\\DOCUMENTOS ENVIAR\\NOTAS DEBITO\\" + repositorio.Nombredoc + ".xml";
        //        repositorio.Xml = File.ReadAllBytes(mirutadearchivo);
        //        repositorio.Pdf = File.ReadAllBytes(mirutadearchivo.Replace(".xml", ".pdf"));
        //        repositorio.Usuario = frmLogin.iCodUser;
        //        repositorio.CodEmpresa = frmLogin.iCodEmpresa;
        //        repositorio.CodSucursal = frmLogin.iCodSucursal;
        //        repositorio.CodAlmacen = frmLogin.iCodAlmacen;
        //        repositorio.CodFacturaVenta = Convert.ToInt32(nd.CodNotaDebitoNueva);
        //        repositorio.TipDocRelacion = _documento.IdDocumento;




        //        if (!admRepositorio.registra_repositorio(repositorio))
        //        {
        //            MessageBox.Show("Documento no se pudo enviar al repositorio");
        //        }



        //    }
        //    catch (Exception a) { MessageBox.Show(a.Message); }
        //    finally { Cursor.Current = Cursors.Default; }
        //}

        private void CalcularTotales()
        {
            // Realizamos los cálculos respectivos.

            _documento.TotalIgv = _documento.Items.Sum(d => d.Impuesto);
            _documento.TotalIsc = _documento.Items.Sum(d => d.ImpuestoSelectivo);
            _documento.TotalOtrosTributos = _documento.Items.Sum(d => d.OtroImpuesto);

            _documento.Gravadas = _documento.Items
                .Where(d => d.TipoImpuesto.StartsWith("1"))
                .Sum(d => d.Suma);

            _documento.Exoneradas = _documento.Items
                .Where(d => d.TipoImpuesto.Contains("20"))
                .Sum(d => d.Suma);

            _documento.Inafectas = _documento.Items
                .Where(d => d.TipoImpuesto.StartsWith("3") || d.TipoImpuesto.Contains("40"))
                .Sum(d => d.Suma);

            _documento.Gratuitas = _documento.Items
                .Where(d => d.TipoImpuesto.Contains("21"))
                .Sum(d => d.Suma);
            _documento.LineCountNumeric = Convert.ToString(_documento.Items.Count());
            // Cuando existe ISC se debe recalcular el IGV.
            if (_documento.TotalIsc > 0)
            {
                _documento.TotalIgv = (_documento.Gravadas + _documento.TotalIsc) * _documento.CalculoIgv;
                // Se recalcula nuevamente el Total de Venta.
            }

            _documento.TotalVenta = _documento.Gravadas + _documento.Exoneradas + _documento.Inafectas +
                                      _documento.TotalIsc + _documento.TotalOtrosTributos + _documento.TotalIgv;

            _documento.MontoEnLetras = conv.enletras(_documento.TotalVenta.ToString());
        }

        async Task Firmar()
        {
            try
            {


                if (string.IsNullOrEmpty(_documento.IdDocumento))
                {
                    MessageBox.Show("La Serie y el Correlativo no pueden estar vacíos");
                    return;
                }

                /**
                 * Lee el XML sin firma en la ruta especificada
                 * 
                 **/

                var tramaXmlSinFirma = Convert.ToBase64String(File.ReadAllBytes(Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "Documentos\\",
                    $"{_documento.IdDocumento}.xml")));

                /**
                 * @val UnSoloNodoExtension 
                 * 
                 * Ya no es necesario evaluar si es True o False
                 * Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "Documentos\\",
                    $"{_documento.Emisor.NroDocumento + "-" + _documento.TipoDocumento + "-" + _documento.IdDocumento}.xml");
                 */
                var firmadoRequest = new WinApp.Comun.Dto.Intercambio.FirmadoRequest
                {
                    TramaXmlSinFirma = tramaXmlSinFirma,
                    CertificadoDigital = Convert.ToBase64String(File.ReadAllBytes(AppDomain.CurrentDomain.BaseDirectory + "Certificado\\" + empresa.Nombrecertificado)),
                    PasswordCertificado = empresa.Clavecertificado,
                    UnSoloNodoExtension = false
                };

                ICertificador certificador = new WinApp.Firmado.Certificador();
                respuestaFirmado = await new WinApp.API.Firmar(certificador).Post(firmadoRequest);

                if (!respuestaFirmado.Exito)
                {

                    MessageBox.Show(respuestaFirmado.MensajeError);
                    return;
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        //public async Task Enviar(clsEmpresa empresa, string IdDocumento, string TipoDocumento, string TramaXmlFirmado)
        //{
        //    try
        //    {
        //        bool todocorrecto = false;


        //        var enviarDocumentoRequest = new EnviarDocumentoRequest
        //        {
        //            Ruc = empresa.Ruc,
        //            UsuarioSol = empresa.UsuarioSunat,
        //            ClaveSol = empresa.ClaveSunat,
        //            EndPointUrl = empresa.Url,
        //            IdDocumento = IdDocumento,
        //            TipoDocumento = TipoDocumento,
        //            TramaXmlFirmado = TramaXmlFirmado
        //        };

        //        ISerializador serializador = new Serializador();
        //        IServicioSunatDocumentos servicioSunatDocumentos = new ServicioSunatDocumentos();


        //        respuestaEnvio = await new EnviarDocumento(serializador, servicioSunatDocumentos).Post(enviarDocumentoRequest);

        //        rpta = (EnviarDocumentoResponse)respuestaEnvio;



        //        try
        //        {


        //            switch (TipoDocumento)
        //            {
        //                case "07":


        //                    File.WriteAllBytes($"{Program.CarpetaNC}\\{empresa.Ruc + "-" + TipoDocumento + "-" + IdDocumento}.xml",
        //                               Convert.FromBase64String(TramaXmlFirmado));

        //                    File.WriteAllBytes(@"C:\DOCUMENTOS-" + empresa.Ruc + "\\DOCUMENTOS ENVIAR\\NOTAS CREDITO\\" + empresa.Ruc + "-" + TipoDocumento + "-" + IdDocumento + ".xml",
        //                  Convert.FromBase64String(TramaXmlFirmado));

        //                    break;
        //                case "08":


        //                    File.WriteAllBytes($"{Program.CarpetaND}\\{empresa.Ruc + "-" + TipoDocumento + "-" + IdDocumento}.xml",
        //                               Convert.FromBase64String(TramaXmlFirmado));

        //                    File.WriteAllBytes(@"C:\DOCUMENTOS-" + empresa.Ruc + "\\DOCUMENTOS ENVIAR\\NOTAS DEBITO\\" + empresa.Ruc + "-" + TipoDocumento + "-" + IdDocumento + ".xml",
        //                  Convert.FromBase64String(TramaXmlFirmado));

        //                    break;
        //                case "03":

        //                    File.WriteAllBytes($"{Program.CarpetaBoletas}\\{empresa.Ruc + "-" + TipoDocumento + "-" + IdDocumento}.xml",
        //                              Convert.FromBase64String(TramaXmlFirmado));

        //                    File.WriteAllBytes(@"C:\DOCUMENTOS-" + empresa.Ruc + "\\DOCUMENTOS ENVIAR\\BOLETAS\\" + empresa.Ruc + "-" + TipoDocumento + "-" + IdDocumento + ".xml",
        //                   Convert.FromBase64String(TramaXmlFirmado));


        //                    break;
        //                case "01":

        //                    File.WriteAllBytes($"{Program.CarpetaFacturas}\\{empresa.Ruc + "-" + TipoDocumento + "-" + IdDocumento}.xml",
        //                             Convert.FromBase64String(TramaXmlFirmado));

        //                    File.WriteAllBytes(@"C:\DOCUMENTOS-" + empresa.Ruc + "\\DOCUMENTOS ENVIAR\\FACTURAS\\" + empresa.Ruc + "-" + TipoDocumento + "-" + IdDocumento + ".xml",
        //                    Convert.FromBase64String(TramaXmlFirmado));

        //                    break;
        //            }




        //        }
        //        catch (Exception ex)
        //        {
        //            MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
        //        }



        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //    }
        //    finally
        //    {
        //        Cursor.Current = Cursors.Default;
        //    }
        //}

        //#endregion

        //#region PDF'S
        ///**
        // * Métodos usados para generar PDF
        // */

        //public void GeneraPDF(Int32 codigo)
        //{
        //    DataSet jes = new DataSet();
        //    DataSet abi = new DataSet();
        //    String RutaArch = "";
        //    String RutaXML = "";
        //    if (_documento.TipoDocumento == "01")
        //    {
        //        RutaArch = @"C:\DOCUMENTOS-" + empresa.Ruc + "\\DOCUMENTOS ENVIAR\\FACTURAS\\" + _documento.Emisor.NroDocumento + "-" + _documento.TipoDocumento + "-" + _documento.IdDocumento + ".xml";
        //        RutaXML = $"{Program.CarpetaFacturas}\\{_documento.Emisor.NroDocumento + "-" + _documento.TipoDocumento + "-" + _documento.IdDocumento}.xml";
        //    }
        //    else
        //    {
        //        RutaArch = @"C:\DOCUMENTOS-" + empresa.Ruc + "\\DOCUMENTOS ENVIAR\\BOLETAS\\" + _documento.Emisor.NroDocumento + "-" + _documento.TipoDocumento + "-" + _documento.IdDocumento + ".xml";
        //        RutaXML = $"{Program.CarpetaBoletas}\\{_documento.Emisor.NroDocumento + "-" + _documento.TipoDocumento + "-" + _documento.IdDocumento}.xml";
        //    }

        //    String[] cad = _documento.IdDocumento.Split('-');
        //    String[] fecha = _documento.FechaEmision.Split('/');

        //    datosAdicionales_CDB = _documento.Emisor.NroDocumento + "|" + _documento.TipoDocumento + "|" + cad[0].ToString() + "|" + cad[1].ToString() + "|"
        //                           + _documento.TotalIgv + "|" + _documento.TotalVenta + "|" + fecha[2] + "-" + fecha[1] + "-" + fecha[0] + "|"
        //                           + _documento.Receptor.TipoDocumento + "|" + _documento.Receptor.NroDocumento;

        //    CodigoCertificado = datosAdicionales_CDB + "|" + resumenfirmadig;

        //    BarcodePDF417 codigobarras = new BarcodePDF417();
        //    codigobarras.Options = BarcodePDF417.PDF417_USE_ASPECT_RATIO;
        //    codigobarras.ErrorLevel = 5;
        //    codigobarras.YHeight = 6f;
        //    codigobarras.SetText(CodigoCertificado);
        //    System.Drawing.Bitmap bm = new System.Drawing.Bitmap(codigobarras.CreateDrawingImage(System.Drawing.Color.Black, System.Drawing.Color.White));
        //    bm.Save(@"C:\DOCUMENTOS-" + empresa.Ruc + "\\CERTIFIK\\QR\\" + _documento.Emisor.NroDocumento + "-" + _documento.TipoDocumento + "-" + _documento.IdDocumento + ".jpeg", System.Drawing.Imaging.ImageFormat.Jpeg);
        //    //bm.Save(@"C:\DOCUMENTOS-" + empresa.Ruc + "\\CERTIFIK\\QR\\" + _documento.Emisor.NroDocumento + "-" + _documento.TipoDocumento + "-" + _documento.IdDocumento + ".jpeg", System.Drawing.Imaging.ImageFormat.Jpeg);


        //    LogoEmp = CargarImagen(@"C:\DOCUMENTOS-" + empresa.Ruc + "\\CERTIFIK\\QR\\" + _documento.Emisor.NroDocumento + "-" + _documento.TipoDocumento + "-" + _documento.IdDocumento + ".jpeg");

        //    frmRptFactura form = new frmRptFactura();
        //    CRReporteFactura rpt = new CRReporteFactura();
        //    rpt.Load("CRReporteFactura.rpt");

        //    jes = ds.ReporteFactura2(Convert.ToInt32(codigo));

        //    foreach (DataTable mel in jes.Tables)
        //    {
        //        foreach (DataRow changesRow in mel.Rows)
        //        {
        //            changesRow["firma"] = LogoEmp;
        //        }
        //        if (mel.HasErrors)
        //        {
        //            foreach (DataRow changesRow in mel.Rows)
        //            {
        //                if ((int)changesRow["Item", DataRowVersion.Current] > 100)
        //                {
        //                    changesRow.RejectChanges();
        //                    changesRow.ClearErrors();
        //                }
        //            }
        //        }
        //    }

        //    rpt.SetDataSource(jes);
        //    form.crvReporteFactura.ReportSource = rpt;
        //    form.ShowDialog();
        //    rpt.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, RutaArch.Replace(".xml", ".pdf"));
        //    rpt.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, RutaXML.Replace(".xml", ".pdf"));



        //    rpt.Close();
        //    rpt.Dispose();
        //}

        //public void GeneraPDF_NC(Int32 codigo)
        //{
        //    DataSet jes = new DataSet();
        //    DataSet abi = new DataSet();
        //    String RutaArch = @"C:\DOCUMENTOS-" + empresa.Ruc + "\\DOCUMENTOS ENVIAR\\NOTAS CREDITO\\" + _documento.Emisor.NroDocumento + "-" + _documento.TipoDocumento + "-" + _documento.IdDocumento + ".xml";
        //    String RutaXML = $"{Program.CarpetaNC}\\{_documento.Emisor.NroDocumento + "-" + _documento.TipoDocumento + "-" + _documento.IdDocumento}.xml";

        //    String[] cad = _documento.IdDocumento.Split('-');
        //    String[] fecha = _documento.FechaEmision.Split('/');

        //    datosAdicionales_CDB = _documento.Emisor.NroDocumento + "|" + _documento.TipoDocumento + "|" + cad[0].ToString() + "|" + cad[1].ToString() + "|"
        //                           + _documento.TotalIgv + "|" + _documento.TotalVenta + "|" + fecha[2] + "-" + fecha[1] + "-" + fecha[0] + "|"
        //                           + _documento.Receptor.TipoDocumento + "|" + _documento.Receptor.NroDocumento;

        //    CodigoCertificado = datosAdicionales_CDB + "|" + resumenfirmadig;




        //    BarcodePDF417 codigobarras = new BarcodePDF417();
        //    codigobarras.Options = BarcodePDF417.PDF417_USE_ASPECT_RATIO;
        //    codigobarras.ErrorLevel = 5;
        //    codigobarras.YHeight = 6f;
        //    codigobarras.SetText(CodigoCertificado);
        //    System.Drawing.Bitmap bm = new System.Drawing.Bitmap(codigobarras.CreateDrawingImage(System.Drawing.Color.Black, System.Drawing.Color.White));
        //    bm.Save(@"C:\DOCUMENTOS-" + empresa.Ruc + "\\CERTIFIK\\QR\\" + _documento.Emisor.NroDocumento + "-" + _documento.TipoDocumento + "-" + _documento.IdDocumento + ".jpeg", System.Drawing.Imaging.ImageFormat.Jpeg);

        //    LogoEmp = CargarImagen(@"C:\DOCUMENTOS-" + empresa.Ruc + "\\CERTIFIK\\QR\\" + _documento.Emisor.NroDocumento + "-" + _documento.TipoDocumento + "-" + _documento.IdDocumento + ".jpeg");

        //    frmRptNotaCredito form = new frmRptNotaCredito();
        //    CRNotaCreditoVenta rpt = new CRNotaCreditoVenta();
        //    rpt.Load("CRNotaCreditoVenta.rpt");

        //    jes = ds1.ReportNotaCreditoVenta(Convert.ToInt32(codigo), frmLogin.iCodAlmacen);

        //    foreach (DataTable mel in jes.Tables)
        //    {
        //        foreach (DataRow changesRow in mel.Rows)
        //        {
        //            changesRow["firma"] = LogoEmp;
        //        }
        //        if (mel.HasErrors)
        //        {
        //            foreach (DataRow changesRow in mel.Rows)
        //            {
        //                if ((int)changesRow["Item", DataRowVersion.Current] > 100)
        //                {
        //                    changesRow.RejectChanges();
        //                    changesRow.ClearErrors();
        //                }
        //            }
        //        }
        //    }
        //    rpt.SetDataSource(jes);
        //    form.crvNotaCredito.ReportSource = rpt;
        //    form.ShowDialog();
        //    rpt.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, RutaArch.Replace(".xml", ".pdf"));
        //    rpt.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, RutaXML.Replace(".xml", ".pdf"));


        //    rpt.Close();
        //    rpt.Dispose();
        //}
        //public void GeneraPDF_ND(Int32 codigo)
        //{
        //    DataSet jes = new DataSet();
        //    DataSet abi = new DataSet();
        //    String RutaArch = @"C:\DOCUMENTOS-" + empresa.Ruc + "\\DOCUMENTOS ENVIAR\\NOTAS DEBITO\\" + _documento.Emisor.NroDocumento + "-" + _documento.TipoDocumento + "-" + _documento.IdDocumento + ".xml";
        //    String RutaXML = $"{Program.CarpetaND}\\{_documento.Emisor.NroDocumento + "-" + _documento.TipoDocumento + "-" + _documento.IdDocumento}.xml";

        //    String[] cad = _documento.IdDocumento.Split('-');
        //    String[] fecha = _documento.FechaEmision.Split('/');

        //    datosAdicionales_CDB = _documento.Emisor.NroDocumento + "|" + _documento.TipoDocumento + "|" + cad[0].ToString() + "|" + cad[1].ToString() + "|"
        //                           + _documento.TotalIgv + "|" + _documento.TotalVenta + "|" + fecha[2] + "-" + fecha[1] + "-" + fecha[0] + "|"
        //                           + _documento.Receptor.TipoDocumento + "|" + _documento.Receptor.NroDocumento;

        //    CodigoCertificado = datosAdicionales_CDB + "|" + resumenfirmadig;




        //    BarcodePDF417 codigobarras = new BarcodePDF417();
        //    codigobarras.Options = BarcodePDF417.PDF417_USE_ASPECT_RATIO;
        //    codigobarras.ErrorLevel = 5;
        //    codigobarras.YHeight = 6f;
        //    codigobarras.SetText(CodigoCertificado);
        //    System.Drawing.Bitmap bm = new System.Drawing.Bitmap(codigobarras.CreateDrawingImage(System.Drawing.Color.Black, System.Drawing.Color.White));
        //    bm.Save(@"C:\DOCUMENTOS-" + empresa.Ruc + "\\CERTIFIK\\QR\\" + _documento.Emisor.NroDocumento + "-" + _documento.TipoDocumento + "-" + _documento.IdDocumento + ".jpeg", System.Drawing.Imaging.ImageFormat.Jpeg);

        //    LogoEmp = CargarImagen(@"C:\DOCUMENTOS-" + empresa.Ruc + "\\CERTIFIK\\QR\\" + _documento.Emisor.NroDocumento + "-" + _documento.TipoDocumento + "-" + _documento.IdDocumento + ".jpeg");

        //    frmRptNotaDebito form = new frmRptNotaDebito();
        //    CRNotaDebitoVenta rpt = new CRNotaDebitoVenta();
        //    rpt.Load("CRNotaDebitoVenta.rpt");
        //    //clsNotasCreditoDebitoVenta ds2 = new clsNotasCreditoDebitoVenta();
        //    jes = ds1.ReportNotaDebitoVenta(Convert.ToInt32(codigo), frmLogin.iCodAlmacen);

        //    foreach (DataTable mel in jes.Tables)
        //    {
        //        foreach (DataRow changesRow in mel.Rows)
        //        {
        //            changesRow["firma"] = LogoEmp;
        //        }
        //        if (mel.HasErrors)
        //        {
        //            foreach (DataRow changesRow in mel.Rows)
        //            {
        //                if ((int)changesRow["Item", DataRowVersion.Current] > 100)
        //                {
        //                    changesRow.RejectChanges();
        //                    changesRow.ClearErrors();
        //                }
        //            }
        //        }
        //    }
        //    rpt.SetDataSource(jes);
        //    form.crvNotaDebito.ReportSource = rpt;
        //    form.ShowDialog();
        //    rpt.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, RutaArch.Replace(".xml", ".pdf"));
        //    rpt.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, RutaXML.Replace(".xml", ".pdf"));


        //    rpt.Close();
        //    rpt.Dispose();
        //}
        //#endregion
        //#region CargaImagen
        //public static Byte[] CargarImagen(string rutaArchivo)
        //{
        //    if (rutaArchivo != "")
        //    {
        //        try
        //        {
        //            FileStream Archivo = new FileStream(rutaArchivo, FileMode.Open);//Creo el archivo
        //            BinaryReader binRead = new BinaryReader(Archivo);//Cargo el Archivo en modo binario
        //            Byte[] imagenEnBytes = new Byte[(Int64)Archivo.Length]; //Creo un Array de Bytes donde guardare la imagen
        //            binRead.Read(imagenEnBytes, 0, (int)Archivo.Length);//Cargo la imagen en el array de Bytes
        //            binRead.Close();
        //            Archivo.Close();
        //            return imagenEnBytes;//Devuelvo la imagen convertida en un array de bytes
        //        }
        //        catch
        //        {
        //            return new Byte[0];
        //        }
        //    }
        //    return new byte[0];
        //}
        #endregion

    }
}
