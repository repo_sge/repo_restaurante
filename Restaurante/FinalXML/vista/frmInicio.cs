﻿using DevComponents.DotNetBar;
using DevComponents.DotNetBar.Controls;
using FinalXML.Administradores;
using FinalXML.Entidades;
using FinalXML.vista.administrador;
using FinalXML.vista.cocinero;
using FinalXML.vista.mozo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalXML.vista
{
    public partial class frmInicio : PlantillaBase
    {
        private clsEmpresa empresa = null;
        private clsAdmEmpresa admempre = new clsAdmEmpresa();
        private clsUsuario usuario = null;
        private clsAdmUsuario admusuario = new clsAdmUsuario();
        private clsAdmAcceso admacceso = new clsAdmAcceso();
        private clsEncriptacion encriptar = new clsEncriptacion();
        private clsAcceso acceso = null;        
        private Herramientas herramientas = new Herramientas();
        private TextBoxX tboxActive = null;
        public frmInicio()
        {
            InitializeComponent();
        }

        private void frmInicio_Load(object sender, EventArgs e)
        {
            try
            {
                if (!listar_empresa_xestado())
                {
                    MessageBox.Show("Certificado no configurado...", "Advertencia");
                    this.Close();
                }
            }
            catch (Exception) { }
        }

        private void btn_cancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txt_usuario_TextChanged(object sender, EventArgs e)
        {
            txt_usuario.CharacterCasing = CharacterCasing.Upper;
        }

        private void btn_ingresar_Click(object sender, EventArgs e)
        {
            if (txt_usuario.Text.Length > 0 && txt_clave.Text.Length > 0)
            {

                buscar_usuarioxcuentaclave();
            }
            else {

                MessageBox.Show("Por favor ingrese usuario y clave...", "Advertencia");
            }
        }

        private void txt_clave_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                btn_ingresar.PerformClick();
            }
        }

        private void btn_Click(object sender, EventArgs e)
        {
            ButtonX btn = sender as ButtonX;
            //TextBoxX tboxActive = this.ActiveControl as TextBoxX;
            if (tboxActive != null)
            {
                tboxActive.Text += btn.Text;
                tboxActive.SelectionStart = tboxActive.Text.Length;
            }
        }

        private void txt_usuario_Leave(object sender, EventArgs e)
        {
            tboxActive = txt_usuario;
        }

        private void txt_clave_Leave(object sender, EventArgs e)
        {
            tboxActive = txt_clave;
        }

        /************Mis Metodos******************/

        public bool listar_empresa_xestado() {

            string rutacertificado = "";
            bool certificadoconfigurado = true;

            empresa = admempre.listar_empresa_xestado();
            if (empresa != null) {

                rutacertificado = herramientas.GetResourcesPath() + "\\" + empresa.Nombrecertificado;


                if (!File.Exists(rutacertificado))
                {

                    certificadoconfigurado = false;
                }
                else {

                    empresa.Rutacertificado = rutacertificado;
                }
            }

            return certificadoconfigurado;
        }
        public void buscar_usuarioxcuentaclave() {

            try
            {
                usuario = new clsUsuario()
                {

                    Cuenta = txt_usuario.Text,
                    Clave = encriptar.encriptar(txt_clave.Text)
                };

                usuario = admusuario.buscar_usuarioxcuentaclave(usuario);

                if (usuario != null)
                {
                    acceso = new clsAcceso()
                    {
                        Usuario = usuario,
                        Pc = SystemInformation.UserDomainName,
                        Usuariopc = SystemInformation.UserName
                    };


                    switch (usuario.Tipousuario.Idtipousuario)
                    {

                        case 1:

                            if (admacceso.registrar_acceso(acceso) > 0)
                            {
                                if (Application.OpenForms["frmPrincipalAdministrador"] != null)
                                {
                                    Application.OpenForms["frmPrincipalAdministrador"].Activate();
                                }
                                else
                                {
                                    this.Visible = false;
                                    frmPrincipalAdministrador frm_prinadmin = new frmPrincipalAdministrador();
                                    frm_prinadmin.empresa = empresa;
                                    frm_prinadmin.usuario = usuario;
                                    frm_prinadmin.ShowDialog();
                                    this.Visible = true;
                                    txt_usuario.Text = string.Empty;
                                    txt_clave.Text = string.Empty;
                                    txt_usuario.Focus();

                                }
                            }
                            else
                            {

                                MessageBox.Show("Problemas de acceso...", "Advertencia");
                            }
                            break;
                        case 2:

                            if (Application.OpenForms["frmPrincipalAdministrador"] != null)
                            {
                                Application.OpenForms["frmPrincipalAdministrador"].Activate();
                            }
                            else
                            {
                                this.Visible = false;
                                frmPrincipalAdministrador frm_prinadmin = new frmPrincipalAdministrador();
                                frm_prinadmin.empresa = empresa;
                                frm_prinadmin.usuario = usuario;
                                frm_prinadmin.btn_item_serie.Visible = false;
                                frm_prinadmin.btn_item_cliente.Visible = false;
                                frm_prinadmin.btn_item_usuario.Visible = false;
                                frm_prinadmin.btn_prod_bebi_pla.Visible = false;
                                frm_prinadmin.btn_item_mesa.Visible = false;
                                frm_prinadmin.btn_item_ordencocina.Visible = false;
                                frm_prinadmin.btn_item_venta.Visible = false;
                                frm_prinadmin.btn_item_piso.Visible = false;
                                frm_prinadmin.btn_item_caja.Visible = false;
                                frm_prinadmin.btn_item_historial_pedido.Visible = false;
                                frm_prinadmin.btn_item_venta.Visible = false;
                                frm_prinadmin.btn_item_pedido.Focus();
                                frm_prinadmin.btn_item_pedido_Click(null, null);
                                frm_prinadmin.ShowDialog();

                                this.Visible = true;
                                txt_usuario.Text = string.Empty;
                                txt_clave.Text = string.Empty;
                                txt_usuario.Focus();
                            }

                            break;


                        case 3:
                            if (Application.OpenForms["frmPrincipalAdministrador"] != null)
                            {
                                Application.OpenForms["frmPrincipalAdministrador"].Activate();
                            }
                            else
                            {
                                this.Visible = false;
                                frmPrincipalAdministrador frm_prinadmin = new frmPrincipalAdministrador();
                                frm_prinadmin.empresa = empresa;
                                frm_prinadmin.usuario = usuario;
                                frm_prinadmin.btn_item_serie.Visible = false;
                                frm_prinadmin.btn_item_usuario.Visible = false;
                                frm_prinadmin.btn_prod_bebi_pla.Visible = false;
                                frm_prinadmin.btn_item_mesa.Visible = false;
                                frm_prinadmin.btn_item_piso.Visible = false;
                                frm_prinadmin.btn_item_ordencocina.Visible = false;
                                frm_prinadmin.btn_item_venta.Focus();
                                frm_prinadmin.btn_item_venta_Click(null, null);
                                frm_prinadmin.ShowDialog();

                                this.Visible = true;
                                txt_usuario.Text = string.Empty;
                                txt_clave.Text = string.Empty;
                                txt_usuario.Focus();
                            }
                            break;
                        case 4:

                            if (Application.OpenForms["frmOrdenCocina"] != null)
                            {
                                Application.OpenForms["frmOrdenCocina"].Activate();
                            }
                            else
                            {
                                this.Visible = false;

                                frmOrdenCocina frm_ordencocina = new frmOrdenCocina();
                                /* frmPrincipalAdministrador frm_prinadmin = new frmPrincipalAdministrador();
                                 frm_prinadmin.empresa = empresa;
                                 frm_prinadmin.usuario = usuario;
                                 frm_prinadmin.btn_item_serie.Visible = false;
                                 frm_prinadmin.btn_item_cliente.Visible = false;
                                 frm_prinadmin.btn_item_usuario.Visible = false;
                                 frm_prinadmin.btn_prod_bebi_pla.Visible = false;
                                 frm_prinadmin.btn_item_mesa.Visible = false;
                                 frm_prinadmin.btn_item_pedido.Visible = false;
                                 frm_prinadmin.btn_item_piso.Visible = false;
                                 frm_prinadmin.btn_item_caja.Visible = false;
                                 frm_prinadmin.btn_item_venta.Visible = false;
                                 frm_prinadmin.btn_item_historial_pedido.Visible = false;
                                 frm_prinadmin.btn_item_ordencocina.Focus();
                                 frm_prinadmin.ShowDialog();*/

                                frm_ordencocina.ShowDialog();
                                this.Visible = true;
                                txt_usuario.Text = string.Empty;
                                txt_clave.Text = string.Empty;
                                txt_usuario.Focus();
                            }


                            break;
                    }
                }
                else
                {

                    txt_usuario.Text = string.Empty;
                    txt_clave.Text = string.Empty;
                    txt_usuario.Focus();
                }
            }
            catch (Exception) { }
           
        }

        private void btn_del_Click(object sender, EventArgs e)
        {
            if (tboxActive != null)
            {
                if (tboxActive.Text.Length > 0)
                {
                    tboxActive.Text = tboxActive.Text.Substring(0, tboxActive.Text.Count() - 1);                 
                }
            }
        }
    }
}
