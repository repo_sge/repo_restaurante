﻿using DevComponents.DotNetBar;
using DevComponents.DotNetBar.Controls;
using FinalXML.Administradores;
using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalXML.vista.cocinero
{
    public partial class frmOrdenCocina : PlantillaBase
    {
        private clsAdmPedido amdpe = new clsAdmPedido();
        private Timer timer = null;
        public clsUsuario usureg { get; set; }
        private DataTable ordenes = null;
        private DataTable detalle_ordenes = null;
        public frmOrdenCocina()
        {
            InitializeComponent();
        }

        private void frmOrdenCocina_Load(object sender, EventArgs e)
        {

            listar_idordencocina();

            timer = new Timer();
            timer.Interval = (15 * 1000); // 10 secs
            timer.Tick += new EventHandler(timer_Tick);
            timer.Start();

            
        }

        /*****************Mis Metodos**********************/

        private void timer_Tick(object sender, EventArgs e)
        {
            if (pn_principal != null)
            {
                if (pn_principal.Controls.Count > 0)
                {
                    pn_principal.Controls.Clear();
                    listar_idordencocina();
                }
            }
        }

        public void listar_idordencocina() {

            int top = 10;
            int left = 100;
            int il = 1;

            LabelX lb_mesa = null;
            LabelX lb_mozo = null;

            PanelEx pn_contenedor = null;
            DataGridViewX dg_detalle = null;
            ordenes = amdpe.listar_idordencocina();

            if (ordenes != null) {

                if (ordenes.Rows.Count > 0)
                {                  
                    foreach (DataRow roworden in ordenes.Rows) {

                        pn_contenedor = new PanelEx();
                        pn_contenedor.Width = 550;
                        pn_contenedor.Height = 210;
                        pn_contenedor.Top = top;
                        pn_contenedor.Left = left;
                        
                        lb_mesa = new LabelX();
                        lb_mesa.Width =200;
                        lb_mesa.Height = 20;
                        lb_mesa.Top = 10;
                        lb_mesa.Left = 25;

                        lb_mesa.Text = "" + roworden["nombremesa"].ToString();
                        lb_mesa.Font = new Font("Segoe UI", 14, FontStyle.Bold);

                        lb_mozo = new LabelX();
                        lb_mozo.Width = 180;
                        lb_mozo.Height = 20;
                        lb_mozo.Top = 10;
                        lb_mozo.Left = 270;

                        lb_mozo.Text = "MOZO: " + roworden["cuenta"].ToString();
                        lb_mozo.Font = new Font("Segoe UI", 14, FontStyle.Bold);

                        detalle_ordenes = amdpe.listar_detalle_ordencocina(int.Parse(roworden["idordencocina"].ToString()));
                        
                        if (detalle_ordenes != null) {

                            if (detalle_ordenes.Rows.Count > 0) {
                                dg_detalle = null;
                                pn_contenedor.Name = "P" + roworden["idordencocina"].ToString();
                                dg_detalle = new DataGridViewX();
                                dg_detalle.RowHeadersVisible = false;
                                dg_detalle.DefaultCellStyle.Font = new Font("Segoe UI", 9);
                                dg_detalle.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                                dg_detalle.Name = roworden["idordencocina"].ToString();
                                dg_detalle.Width = 500;
                                dg_detalle.Top = 40;
                                dg_detalle.Left = 20;
                                dg_detalle.BackgroundColor = Color.White;
                                dg_detalle.AllowUserToAddRows = false;
                                dg_detalle.ReadOnly = true;
                                dg_detalle.DataSource = detalle_ordenes;                                                         
                                pn_contenedor.Controls.Add(lb_mesa);
                                pn_contenedor.Controls.Add(lb_mozo);
                                pn_contenedor.Controls.Add(dg_detalle);

                                if (il == 2)
                                {
                                    il = 1;
                                    left = 100;
                                    top += 200;
                                }
                                else
                                {
                                    il++;
                                    left += 550;
                                }                             

                                pn_principal.Controls.Add(pn_contenedor);

                                if (dg_detalle.ColumnCount > 0)
                                {
                                    dg_detalle.Columns[0].Visible = false;
                                    dg_detalle.Columns[1].Visible = false;
                                    dg_detalle.Columns[2].Visible = false;
                                }

                            }

                        }

                    }
                }
            }
        }

        private void frmOrdenCocina_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (timer != null) {

                timer.Stop();
                timer.Dispose();
            }
        }
    }
}
