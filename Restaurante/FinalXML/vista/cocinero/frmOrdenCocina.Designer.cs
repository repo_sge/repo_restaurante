﻿namespace FinalXML.vista.cocinero
{
    partial class frmOrdenCocina
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmOrdenCocina));
            this.pn_principal = new DevComponents.DotNetBar.PanelEx();
            this.SuspendLayout();
            // 
            // pn_principal
            // 
            this.pn_principal.AutoScroll = true;
            this.pn_principal.CanvasColor = System.Drawing.SystemColors.Control;
            this.pn_principal.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pn_principal.DisabledBackColor = System.Drawing.Color.Empty;
            this.pn_principal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn_principal.Location = new System.Drawing.Point(0, 0);
            this.pn_principal.Name = "pn_principal";
            this.pn_principal.Size = new System.Drawing.Size(1098, 439);
            this.pn_principal.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pn_principal.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pn_principal.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pn_principal.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pn_principal.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pn_principal.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pn_principal.Style.GradientAngle = 90;
            this.pn_principal.TabIndex = 0;
            // 
            // frmOrdenCocina
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1098, 439);
            this.Controls.Add(this.pn_principal);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmOrdenCocina";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Ordenes de Cocina";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmOrdenCocina_FormClosing);
            this.Load += new System.EventHandler(this.frmOrdenCocina_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.PanelEx pn_principal;
    }
}