﻿namespace FinalXML.vista
{
    partial class frmInicio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmInicio));
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txt_usuario = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txt_clave = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.panelEx1 = new DevComponents.DotNetBar.PanelEx();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btn_del = new DevComponents.DotNetBar.ButtonX();
            this.btn_nueve = new DevComponents.DotNetBar.ButtonX();
            this.btn_ocho = new DevComponents.DotNetBar.ButtonX();
            this.btn_siete = new DevComponents.DotNetBar.ButtonX();
            this.btn_seis = new DevComponents.DotNetBar.ButtonX();
            this.btn_cinco = new DevComponents.DotNetBar.ButtonX();
            this.btn_cuatro = new DevComponents.DotNetBar.ButtonX();
            this.btn_tres = new DevComponents.DotNetBar.ButtonX();
            this.btn_dos = new DevComponents.DotNetBar.ButtonX();
            this.btn_uno = new DevComponents.DotNetBar.ButtonX();
            this.btn_cero = new DevComponents.DotNetBar.ButtonX();
            this.btn_cancelar = new DevComponents.DotNetBar.ButtonX();
            this.btn_ingresar = new DevComponents.DotNetBar.ButtonX();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.styleManager1 = new DevComponents.DotNetBar.StyleManager(this.components);
            this.groupPanel1.SuspendLayout();
            this.panelEx1.SuspendLayout();
            this.groupPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupPanel1
            // 
            this.groupPanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.txt_usuario);
            this.groupPanel1.Controls.Add(this.txt_clave);
            this.groupPanel1.Controls.Add(this.labelX2);
            this.groupPanel1.Controls.Add(this.labelX1);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(240, 41);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.Size = new System.Drawing.Size(307, 130);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 0;
            this.groupPanel1.Text = "Acceso a Sistema";
            // 
            // txt_usuario
            // 
            // 
            // 
            // 
            this.txt_usuario.Border.Class = "TextBoxBorder";
            this.txt_usuario.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_usuario.DisabledBackColor = System.Drawing.Color.White;
            this.txt_usuario.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_usuario.Location = new System.Drawing.Point(93, 20);
            this.txt_usuario.Name = "txt_usuario";
            this.txt_usuario.PreventEnterBeep = true;
            this.txt_usuario.Size = new System.Drawing.Size(177, 29);
            this.txt_usuario.TabIndex = 1;
            this.txt_usuario.TextChanged += new System.EventHandler(this.txt_usuario_TextChanged);
            this.txt_usuario.Leave += new System.EventHandler(this.txt_usuario_Leave);
            // 
            // txt_clave
            // 
            // 
            // 
            // 
            this.txt_clave.Border.Class = "TextBoxBorder";
            this.txt_clave.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_clave.DisabledBackColor = System.Drawing.Color.White;
            this.txt_clave.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txt_clave.Location = new System.Drawing.Point(93, 56);
            this.txt_clave.Name = "txt_clave";
            this.txt_clave.PasswordChar = '*';
            this.txt_clave.PreventEnterBeep = true;
            this.txt_clave.Size = new System.Drawing.Size(177, 29);
            this.txt_clave.TabIndex = 2;
            this.txt_clave.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_clave_KeyDown);
            this.txt_clave.Leave += new System.EventHandler(this.txt_clave_Leave);
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX2.Location = new System.Drawing.Point(22, 54);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(58, 23);
            this.labelX2.TabIndex = 2;
            this.labelX2.Text = "Clave :";
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX1.Location = new System.Drawing.Point(17, 20);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(70, 23);
            this.labelX1.TabIndex = 0;
            this.labelX1.Text = "Usuario :";
            // 
            // panelEx1
            // 
            this.panelEx1.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx1.Controls.Add(this.groupPanel2);
            this.panelEx1.Controls.Add(this.btn_cancelar);
            this.panelEx1.Controls.Add(this.btn_ingresar);
            this.panelEx1.Controls.Add(this.pictureBox1);
            this.panelEx1.Controls.Add(this.groupPanel1);
            this.panelEx1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelEx1.DisabledBackColor = System.Drawing.Color.Empty;
            this.panelEx1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEx1.Location = new System.Drawing.Point(0, 0);
            this.panelEx1.Name = "panelEx1";
            this.panelEx1.Size = new System.Drawing.Size(778, 281);
            this.panelEx1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx1.Style.GradientAngle = 90;
            this.panelEx1.TabIndex = 1;
            // 
            // groupPanel2
            // 
            this.groupPanel2.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.btn_del);
            this.groupPanel2.Controls.Add(this.btn_nueve);
            this.groupPanel2.Controls.Add(this.btn_ocho);
            this.groupPanel2.Controls.Add(this.btn_siete);
            this.groupPanel2.Controls.Add(this.btn_seis);
            this.groupPanel2.Controls.Add(this.btn_cinco);
            this.groupPanel2.Controls.Add(this.btn_cuatro);
            this.groupPanel2.Controls.Add(this.btn_tres);
            this.groupPanel2.Controls.Add(this.btn_dos);
            this.groupPanel2.Controls.Add(this.btn_uno);
            this.groupPanel2.Controls.Add(this.btn_cero);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Location = new System.Drawing.Point(554, 14);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.Size = new System.Drawing.Size(209, 244);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 5;
            // 
            // btn_del
            // 
            this.btn_del.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_del.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_del.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_del.Image = global::FinalXML.Properties.Resources.quitar;
            this.btn_del.Location = new System.Drawing.Point(139, 174);
            this.btn_del.Name = "btn_del";
            this.btn_del.Size = new System.Drawing.Size(61, 55);
            this.btn_del.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_del.TabIndex = 10;
            this.btn_del.Click += new System.EventHandler(this.btn_del_Click);
            // 
            // btn_nueve
            // 
            this.btn_nueve.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_nueve.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_nueve.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_nueve.Location = new System.Drawing.Point(5, 3);
            this.btn_nueve.Name = "btn_nueve";
            this.btn_nueve.Size = new System.Drawing.Size(61, 50);
            this.btn_nueve.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_nueve.TabIndex = 9;
            this.btn_nueve.Text = "9";
            this.btn_nueve.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn_ocho
            // 
            this.btn_ocho.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_ocho.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_ocho.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ocho.Location = new System.Drawing.Point(72, 3);
            this.btn_ocho.Name = "btn_ocho";
            this.btn_ocho.Size = new System.Drawing.Size(61, 50);
            this.btn_ocho.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_ocho.TabIndex = 8;
            this.btn_ocho.Text = "8";
            this.btn_ocho.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn_siete
            // 
            this.btn_siete.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_siete.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_siete.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_siete.Location = new System.Drawing.Point(139, 3);
            this.btn_siete.Name = "btn_siete";
            this.btn_siete.Size = new System.Drawing.Size(61, 50);
            this.btn_siete.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_siete.TabIndex = 7;
            this.btn_siete.Text = "7";
            this.btn_siete.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn_seis
            // 
            this.btn_seis.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_seis.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_seis.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_seis.Location = new System.Drawing.Point(5, 59);
            this.btn_seis.Name = "btn_seis";
            this.btn_seis.Size = new System.Drawing.Size(61, 50);
            this.btn_seis.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_seis.TabIndex = 6;
            this.btn_seis.Text = "6";
            this.btn_seis.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn_cinco
            // 
            this.btn_cinco.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_cinco.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_cinco.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_cinco.Location = new System.Drawing.Point(72, 58);
            this.btn_cinco.Name = "btn_cinco";
            this.btn_cinco.Size = new System.Drawing.Size(61, 50);
            this.btn_cinco.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_cinco.TabIndex = 5;
            this.btn_cinco.Text = "5";
            this.btn_cinco.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn_cuatro
            // 
            this.btn_cuatro.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_cuatro.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_cuatro.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_cuatro.Location = new System.Drawing.Point(139, 58);
            this.btn_cuatro.Name = "btn_cuatro";
            this.btn_cuatro.Size = new System.Drawing.Size(61, 49);
            this.btn_cuatro.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_cuatro.TabIndex = 4;
            this.btn_cuatro.Text = "4";
            this.btn_cuatro.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn_tres
            // 
            this.btn_tres.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_tres.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_tres.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_tres.Location = new System.Drawing.Point(5, 114);
            this.btn_tres.Name = "btn_tres";
            this.btn_tres.Size = new System.Drawing.Size(61, 55);
            this.btn_tres.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_tres.TabIndex = 3;
            this.btn_tres.Text = "3";
            this.btn_tres.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn_dos
            // 
            this.btn_dos.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_dos.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_dos.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_dos.Location = new System.Drawing.Point(72, 114);
            this.btn_dos.Name = "btn_dos";
            this.btn_dos.Size = new System.Drawing.Size(61, 55);
            this.btn_dos.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_dos.TabIndex = 2;
            this.btn_dos.Text = "2";
            this.btn_dos.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn_uno
            // 
            this.btn_uno.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_uno.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_uno.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_uno.Location = new System.Drawing.Point(139, 114);
            this.btn_uno.Name = "btn_uno";
            this.btn_uno.Size = new System.Drawing.Size(61, 55);
            this.btn_uno.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_uno.TabIndex = 1;
            this.btn_uno.Text = "1";
            this.btn_uno.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn_cero
            // 
            this.btn_cero.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_cero.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_cero.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_cero.Location = new System.Drawing.Point(5, 174);
            this.btn_cero.Name = "btn_cero";
            this.btn_cero.Size = new System.Drawing.Size(128, 57);
            this.btn_cero.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_cero.TabIndex = 0;
            this.btn_cero.Text = "0";
            this.btn_cero.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn_cancelar
            // 
            this.btn_cancelar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_cancelar.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_cancelar.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_cancelar.Image = global::FinalXML.Properties.Resources.cancelar;
            this.btn_cancelar.Location = new System.Drawing.Point(404, 191);
            this.btn_cancelar.Name = "btn_cancelar";
            this.btn_cancelar.Size = new System.Drawing.Size(109, 56);
            this.btn_cancelar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_cancelar.TabIndex = 4;
            this.btn_cancelar.Text = "Cancelar";
            this.btn_cancelar.Click += new System.EventHandler(this.btn_cancelar_Click);
            // 
            // btn_ingresar
            // 
            this.btn_ingresar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_ingresar.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_ingresar.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ingresar.Image = global::FinalXML.Properties.Resources.ingresar1;
            this.btn_ingresar.Location = new System.Drawing.Point(275, 191);
            this.btn_ingresar.Name = "btn_ingresar";
            this.btn_ingresar.Size = new System.Drawing.Size(109, 56);
            this.btn_ingresar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_ingresar.TabIndex = 3;
            this.btn_ingresar.Text = "Ingresar";
            this.btn_ingresar.Click += new System.EventHandler(this.btn_ingresar_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::FinalXML.Properties.Resources.restaurante01;
            this.pictureBox1.Location = new System.Drawing.Point(12, 18);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(222, 233);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // styleManager1
            // 
            this.styleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Office2010Blue;
            this.styleManager1.MetroColorParameters = new DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters(System.Drawing.Color.White, System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154))))));
            // 
            // frmInicio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(778, 281);
            this.Controls.Add(this.panelEx1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmInicio";
            this.Text = "Inicio";
            this.Load += new System.EventHandler(this.frmInicio_Load);
            this.groupPanel1.ResumeLayout(false);
            this.panelEx1.ResumeLayout(false);
            this.groupPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_clave;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.PanelEx panelEx1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevComponents.DotNetBar.ButtonX btn_cancelar;
        private DevComponents.DotNetBar.ButtonX btn_ingresar;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_usuario;
        private DevComponents.DotNetBar.StyleManager styleManager1;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.ButtonX btn_nueve;
        private DevComponents.DotNetBar.ButtonX btn_ocho;
        private DevComponents.DotNetBar.ButtonX btn_siete;
        private DevComponents.DotNetBar.ButtonX btn_seis;
        private DevComponents.DotNetBar.ButtonX btn_cinco;
        private DevComponents.DotNetBar.ButtonX btn_cuatro;
        private DevComponents.DotNetBar.ButtonX btn_dos;
        private DevComponents.DotNetBar.ButtonX btn_uno;
        private DevComponents.DotNetBar.ButtonX btn_cero;
        private DevComponents.DotNetBar.ButtonX btn_tres;
        private DevComponents.DotNetBar.ButtonX btn_del;
    }
}