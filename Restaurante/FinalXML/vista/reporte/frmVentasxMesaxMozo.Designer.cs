﻿namespace FinalXML.vista.reporte
{
    partial class frmVentasxMesaxMozo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVentasxMesaxMozo));
            this.panelEx1 = new DevComponents.DotNetBar.PanelEx();
            this.crystal_ventasymesasxmozo = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.panelEx1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelEx1
            // 
            this.panelEx1.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx1.Controls.Add(this.crystal_ventasymesasxmozo);
            this.panelEx1.DisabledBackColor = System.Drawing.Color.Empty;
            this.panelEx1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEx1.Location = new System.Drawing.Point(0, 0);
            this.panelEx1.Name = "panelEx1";
            this.panelEx1.Size = new System.Drawing.Size(334, 749);
            this.panelEx1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx1.Style.GradientAngle = 90;
            this.panelEx1.TabIndex = 0;
            // 
            // crystal_ventasymesasxmozo
            // 
            this.crystal_ventasymesasxmozo.ActiveViewIndex = -1;
            this.crystal_ventasymesasxmozo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crystal_ventasymesasxmozo.Cursor = System.Windows.Forms.Cursors.Default;
            this.crystal_ventasymesasxmozo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.crystal_ventasymesasxmozo.Location = new System.Drawing.Point(0, 0);
            this.crystal_ventasymesasxmozo.Name = "crystal_ventasymesasxmozo";
            this.crystal_ventasymesasxmozo.Size = new System.Drawing.Size(334, 749);
            this.crystal_ventasymesasxmozo.TabIndex = 0;
            this.crystal_ventasymesasxmozo.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            // 
            // frmVentasxMesaxMozo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(334, 749);
            this.Controls.Add(this.panelEx1);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmVentasxMesaxMozo";
            this.Text = "Ventas y Mesas por Mozo";
            this.Load += new System.EventHandler(this.frmVentasxMesaxMozo_Load);
            this.panelEx1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.PanelEx panelEx1;
        public CrystalDecisions.Windows.Forms.CrystalReportViewer crystal_ventasymesasxmozo;
    }
}