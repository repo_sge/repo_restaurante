﻿using FinalXML.Administradores;
using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalXML.vista.administrador
{
    public partial class frmUnidadMedida : PlantillaBase
    {
        private clsUnidadMedida unidadmedida = null;
        private clsAdmUnidadMedida admunidad = new clsAdmUnidadMedida();
        private DataTable unidades = null;

        public clsUsuario usureg { get; set; }
        public frmUnidadMedida()
        {
            InitializeComponent();
        }

        private void frmUnidadMedida_Load(object sender, EventArgs e)
        {
            dg_unidades.AutoGenerateColumns = false;
            cb_estado.SelectedIndex = 0;
        }

        private void txt_nombre_TextChanged(object sender, EventArgs e)
        {
            txt_nombre.CharacterCasing = CharacterCasing.Upper;
        }

        private void txt_sigla_TextChanged(object sender, EventArgs e)
        {
            txt_sigla.CharacterCasing = CharacterCasing.Upper;
        }

        private void btn_guardar_Click(object sender, EventArgs e)
        {
            try
            {

                if (txt_cod.Text.Length == 0)
                {
                    if (txt_nombre.Text.Length > 0 && txt_sigla.Text.Length > 0)
                    {
                        DialogResult respuesta = MessageBox.Show("¿Desea registrar unidad de medida?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (respuesta == DialogResult.Yes)
                        {
                            unidadmedida = new clsUnidadMedida();
                            unidadmedida.Nombre = txt_nombre.Text;
                            unidadmedida.Sigla = txt_sigla.Text;

                            if (cb_estado.SelectedItem.ToString() == "ACTIVO") { unidadmedida.Estado = 1; } else { unidadmedida.Estado = 0; }

                            if (admunidad.registrar_unidadmedida(unidadmedida, usureg) > 0)
                            {
                                MessageBox.Show("Registro correcto...", "Información");
                                btn_cargar.PerformClick();

                            }
                            else
                            {

                                MessageBox.Show("Problemas en el registro...", "Error");
                            }

                        }

                    }
                    else
                    {

                        MessageBox.Show("Por favor complete los campos solictados...", "Advertencia");

                    }
                }
                else
                {


                    if (txt_nombre.Text.Length > 0 && txt_sigla.Text.Length > 0)
                    {
                        DialogResult respuesta = MessageBox.Show("¿Desea actualizar unidad de medida?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (respuesta == DialogResult.Yes)
                        {
                            unidadmedida = new clsUnidadMedida();
                            unidadmedida.Unidadmedidaid = int.Parse(txt_cod.Text);
                            unidadmedida.Nombre = txt_nombre.Text;
                            unidadmedida.Sigla = txt_sigla.Text;

                            if (cb_estado.SelectedItem.ToString() == "ACTIVO") { unidadmedida.Estado = 1; } else { unidadmedida.Estado = 0; }

                            if (admunidad.actualizar_unidadmedida(unidadmedida, usureg) > 0)
                            {
                                MessageBox.Show("Actualizar correcta...", "Información");
                                btn_cargar.PerformClick();
                            }
                            else
                            {

                                MessageBox.Show("Problemas en la actualización...", "Error");
                            }
                        }

                    }
                    else
                    {

                        MessageBox.Show("Por favor complete los campos solictados...", "Advertencia");

                    }

                }
            }
            catch (Exception) { }
        }

        private void btn_cargar_Click(object sender, EventArgs e)
        {
            try
            {

                listar_unidadmedida();
            }
            catch (Exception)
            {


            }
        }
        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void dg_unidades_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dg_unidades.Rows.Count > 0)
                {
                    if (e.RowIndex != -1)
                    {
                        txt_cod.Text = dg_unidades.Rows[e.RowIndex].Cells[0].Value.ToString();
                        txt_nombre.Text = dg_unidades.Rows[e.RowIndex].Cells[1].Value.ToString();
                        txt_sigla.Text = dg_unidades.Rows[e.RowIndex].Cells[2].Value.ToString();
                        if (dg_unidades.Rows[e.RowIndex].Cells[3].Value.ToString() == "ACTIVO") { cb_estado.SelectedIndex = 0; } else { cb_estado.SelectedIndex = 1; }
                    }
                }
            }
            catch (Exception) { }
        }
        /***********************Mis Metodos*********************/
        public void listar_unidadmedida()
        {
            try
            {
                unidades = admunidad.listar_unidadmedida();
                if (unidades != null)
                {
                    if (unidades.Rows.Count > 0)
                    {
                        dg_unidades.DataSource = unidades;

                    }
                }
            }
            catch (Exception) { }

        }

        public void limpiar()
        {
            try
            {
                txt_cod.Text = string.Empty;
                txt_nombre.Text = string.Empty;
                txt_sigla.Text = string.Empty;
                cb_estado.SelectedIndex = 0;
            }
            catch (Exception) { }
        }

      
    }
}
