﻿using FinalXML.Administradores;
using FinalXML.Entidades;
using FinalXML.Librerias;
using FinalXML.vista.cajero;
using FinalXML.vista.cocinero;
using FinalXML.vista.mozo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalXML.vista.administrador
{
    public partial class frmPrincipalAdministrador : DevComponents.DotNetBar.Office2007RibbonForm
    {
        public clsUsuario usuario { get; set; }
        public clsEmpresa empresa { get; set; }

        private Double comp;
        private Double vent;
        private DataTable tabla = new DataTable();
        private clsTipoCambioSunat clstipoc = new clsTipoCambioSunat();
        private clsTipoCambio oTipoCambio = new clsTipoCambio();
        public bool EstadoTC_BD = false;
        private clsAdmTipoCambio admTipoCambio = new clsAdmTipoCambio();
        private double tc_hoy = 0;
        private int tcvalida;
        private static clsParametros Configuracion = new clsParametros();
        private bool EstadoTC = false;
        private DateTime dia;
        private clsValidarSGE valida = new clsValidarSGE();

        public frmPrincipalAdministrador()
        {
            InitializeComponent();
        }

        private void frmPrincipalAdministrador_Load(object sender, EventArgs e)
        {
            try
            {
                if (usuario != null)
                {

                    this.Text = "*********** USUARIO  : " + usuario.Nombreyapellido + " - " + DateTime.Now + " **************";
                    CargarTipoCambio();
                }
            }
            catch (Exception) { }
        }

        private void Usuarios_Click(object sender, EventArgs e)
        {
            try
            {
                if (Application.OpenForms["frmUsuario"] != null)
                {
                    Application.OpenForms["frmUsuario"].Activate();
                }
                else
                {
                    frmUsuario frm_usuario = new frmUsuario();
                    frm_usuario.usureg = usuario;
                    frm_usuario.MdiParent = this;
                    frm_usuario.Show();
                }
            }
            catch (Exception) { }
        }

        private void btn_item_categoria_Click(object sender, EventArgs e)
        {
            try
            {
                if (Application.OpenForms["frmCategoria"] != null)
                {
                    Application.OpenForms["frmCategoria"].Activate();
                }
                else
                {
                    frmCategoria frm_catego = new frmCategoria();
                    frm_catego.usureg = usuario;
                    frm_catego.MdiParent = this;
                    frm_catego.Show();
                }
            }
            catch (Exception) { }
        }

        private void btn_prod_bebi_pla_Click(object sender, EventArgs e)
        {
            try
            {
                if (Application.OpenForms["frmProducto"] != null)
                {
                    Application.OpenForms["frmProducto"].Activate();
                }
                else
                {
                    frmProducto frm_product = new frmProducto();
                    frm_product.usureg = usuario;
                    frm_product.MdiParent = this;
                    frm_product.Show();
                }
            }
            catch (Exception) { }
        }

        private void btn_item_mesa_Click(object sender, EventArgs e)
        {
            try
            {
                if (Application.OpenForms["frmMesa"] != null)
                {
                    Application.OpenForms["frmMesa"].Activate();
                }
                else
                {
                    frmMesa frm_mesa = new frmMesa();
                    frm_mesa.usureg = usuario;
                    frm_mesa.MdiParent = this;
                    frm_mesa.Show();
                }
            }
            catch (Exception) { }
        }

        private void btn_item_serie_Click(object sender, EventArgs e)
        {
            try
            {
                if (Application.OpenForms["frmSerieFactura"] != null)
                {
                    Application.OpenForms["frmSerieFactura"].Activate();
                }
                else
                {
                    frmSerieFactura frm_seriefact = new frmSerieFactura();
                    frm_seriefact.usureg = usuario;
                    frm_seriefact.MdiParent = this;
                    frm_seriefact.Show();
                }
            }
            catch (Exception) { }
        }

        private void btn_item_cliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (Application.OpenForms["frmCliente"] != null)
                {
                    Application.OpenForms["frmCliente"].Activate();
                }
                else
                {
                    frmCliente frm_cliente = new frmCliente();
                    frm_cliente.usureg = usuario;
                    frm_cliente.MdiParent = this;
                    frm_cliente.Show();
                }
            }
            catch (Exception) { }
        }

        public void btn_item_pedido_Click(object sender, EventArgs e)
        {
            try
            {
                if (Application.OpenForms["frmPedido"] != null)
                {
                    Application.OpenForms["frmPedido"].Activate();
                }
                else
                {
                    frmPedido frm_pedido = new frmPedido();
                    frm_pedido.usureg = usuario;
                    frm_pedido.MdiParent = this;
                    frm_pedido.Dock = DockStyle.Fill;
                    frm_pedido.Show();
                    //this.ribbonPanel3.Hide();
                    ribbonControl1.Expanded=false;
                    frm_pedido.FormClosed+= new FormClosedEventHandler(frm_pedido_FormClosed); ;
                    
                }
            }
            catch (Exception) { }
        }

        private void frm_pedido_FormClosed(object sender, FormClosedEventArgs e)
        {
            ribbonControl1.Expanded = true;
        }

        private void btn_item_ordencocina_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["frmOrdenCocina"] != null)
            {
                Application.OpenForms["frmOrdenCocina"].Activate();
            }
            else
            {
                frmOrdenCocina frm_ordencocina = new frmOrdenCocina();
                frm_ordencocina.usureg = usuario;
                frm_ordencocina.Dock = DockStyle.Fill;
                frm_ordencocina.WindowState = FormWindowState.Maximized;
                frm_ordencocina.MdiParent = this;
                frm_ordencocina.Show();              
            }
        }

        public void btn_item_venta_Click(object sender, EventArgs e)
        {
            try
            {
                if (Application.OpenForms["frmVenta"] != null)
                {
                    Application.OpenForms["frmVenta"].Activate();
                }
                else
                {
                    frmVenta frm_venta = new frmVenta();
                    frm_venta.empresa = empresa;
                    frm_venta.usureg = usuario;
                    frm_venta.MdiParent = this;
                    frm_venta.Show();
                }
            }
            catch (Exception) { }
        }

        private void btn_item_configuracion_envio_Click(object sender, EventArgs e)
        {
            try
            {
                if (Application.OpenForms["frmConfiguracionEnvio"] != null)
                {
                    Application.OpenForms["frmConfiguracionEnvio"].Activate();
                }
                else
                {
                    frmConfiguracionEnvio frm_config = new frmConfiguracionEnvio();
                    frm_config.usureg = usuario;
                    frm_config.MdiParent = this;
                    frm_config.Show();
                }
            }
            catch (Exception) { }
        }

        private void btn_item_lista_ticket_Click(object sender, EventArgs e)
        {
            try
            {
                if (Application.OpenForms["frmListaTicket"] != null)
                {
                    Application.OpenForms["frmListaTicket"].Activate();
                }
                else
                {
                    frmListaTicket frm_listatick = new frmListaTicket();
                    frm_listatick.usureg = usuario;
                    frm_listatick.MdiParent = this;
                    frm_listatick.Show();
                }
            }
            catch (Exception) { }
        }

        private void btn_item_piso_Click(object sender, EventArgs e)
        {
            try
            {
                if (Application.OpenForms["frmPiso"] != null)
                {
                    Application.OpenForms["frmPiso"].Activate();
                }
                else
                {
                    frmPiso frm_piso = new frmPiso();
                    frm_piso.usureg = usuario;
                    frm_piso.MdiParent = this;
                    frm_piso.Show();
                }
            }
            catch (Exception) { }
        }

        /****************************Mis Metodos***************************/

        public void CargarTipoCambio()
        {
            try
            {

                Configuracion.Autoguardado = true;
                dia = DateTime.Today.Date;

                EstadoTC_BD = admTipoCambio.VerificaTCFecha(dia);

                if (EstadoTC_BD)
                {
                    tcvalida = 1;
                    oTipoCambio = admTipoCambio.CargaTipoCambio(dia, 2);
                    tc_hoy = oTipoCambio.Venta;
                    lb_tipo_cambio.Text = "Fecha TC:  " + oTipoCambio.Fecha.ToShortDateString() + "  Compra: " + oTipoCambio.Compra.ToString() + " - Venta: " + oTipoCambio.Venta.ToString();
                }
                else
                {
                    if (valida.AccesoInternet())
                    {
                        MetodoTipoCambio();
                        //if ("" != "Tipo de Cambio" && Configuracion.Autoguardado == true)
                        if (lb_tipo_cambio.Text != "Tipo de Cambio" && Configuracion.Autoguardado == true)
                        {
                            tcvalida = 1;
                            oTipoCambio = admTipoCambio.CargaTipoCambio(dia, 2);
                            tc_hoy = oTipoCambio.Venta;
                        }
                    }
                    else
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }

        private void MetodoTipoCambio()
        {
            try
            {
                Boolean auto = Configuracion.Autoguardado;

                tabla = clstipoc.ConsultaTCSunat(dia);
                if (tabla != null && tabla.Rows.Count > 0)
                {
                    //var sdiabuscado = TipoCam.Select("Día = '" + Fechabuscada.Date.Day.ToString() + "'");
                    String cadenabusqueda = "[Día] like '*" + dia.Date.Day.ToString() + "*'";
                    DataRow[] foundRows = tabla.Select(cadenabusqueda);
                    //if (sdiabuscado.Length != 0)
                    if (foundRows.Length != 0)
                    {
                        foreach (DataRow r in tabla.Rows)
                        {
                            if (Convert.ToInt32(r[0]) == dia.Date.Day)
                            {
                                lb_tipo_cambio.Text = "Fecha TC:  " + dia.ToShortDateString() + " Compra: " + r[1].ToString() + " Venta: " + r[2].ToString();
                                //Thread.Sleep(1000);
                                comp = Convert.ToDouble(r[1].ToString().Replace(",", "."));
                                vent = Convert.ToDouble(r[2].ToString().Replace(",", "."));
                            }
                        }
                        if (auto)
                        {
                            //if ("" != "Tipo de Cambio")
                            if (lb_tipo_cambio.Text != "Tipo de Cambio")
                            {
                                oTipoCambio.ICodMoneda = 2;
                                oTipoCambio.Compra = comp;
                                oTipoCambio.Venta = vent;
                                oTipoCambio.Fecha = dia;
                                oTipoCambio.CodUser = 1;
                                oTipoCambio.Automatico_manual = 1; // regitro del tipo de cambio via web es decir automatica
                                if (admTipoCambio.insert(oTipoCambio))
                                {
                                    //MetodoTipoCambio();
                                    EstadoTC = true;
                                    dia = DateTime.Now;
                                    ValidaTipoCambio();
                                }
                            }
                        }
                    }
                    else
                    {
                        dia = dia.AddDays(-1);
                        MetodoTipoCambio();
                    }
                }
                else
                {
                    dia = dia.AddDays(-1);
                    MetodoTipoCambio();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problemas de Conexión : " + ex.Message, "Error en Hilo Tipo Cambio", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }

        public void ValidaTipoCambio()
        {
            try
            {
                EstadoTC_BD = admTipoCambio.VerificaTCFecha(dia);
                if (EstadoTC_BD)
                {
                    tcvalida = 1;
                    oTipoCambio = admTipoCambio.CargaTipoCambio(dia, 2);
                    tc_hoy = oTipoCambio.Venta;
                    lb_tipo_cambio.Text = "Fecha TC:  " + oTipoCambio.Fecha.ToShortDateString() + "  Compra: " + oTipoCambio.Compra.ToString() + " - Venta: " + oTipoCambio.Venta.ToString();
                }
                else
                {
                    if (valida.AccesoInternet())
                    {
                        MetodoTipoCambio();
                        if (lb_tipo_cambio.Text != "Tipo de Cambio" && Configuracion.Autoguardado == true)
                        //if ("" != "Tipo de Cambio" && Configuracion.Autoguardado == true)
                        {
                            tcvalida = 1;
                            oTipoCambio = admTipoCambio.CargaTipoCambio(dia, 2);
                            tc_hoy = oTipoCambio.Venta;
                        }
                        else
                        {
                            MessageBox.Show("Ingresa Tipo de Cambio de Hoy");
                            if (Application.OpenForms["frmTipoCambio"] != null)
                            {
                                Application.OpenForms["frmTipoCambio"].Activate();
                            }
                            else
                            {
                                _frmTipoCambio form = new _frmTipoCambio();
                                form.btnNuevo_Click(null, null);
                                form.ShowDialog();
                                ValidaTipoCambio();
                            }
                        }
                    }
                    else
                    {
                        //dia = dia.AddDays(-1);

                        MessageBox.Show("Ingresa Tipo de Cambio de Hoy");
                        if (Application.OpenForms["frmTipoCambio"] != null)
                        {
                            Application.OpenForms["frmTipoCambio"].Activate();
                        }
                        else
                        {
                            _frmTipoCambio form = new _frmTipoCambio();
                            //form.MdiParent = this;
                            form.btnNuevo_Click(new object(), new EventArgs());
                            form.ShowDialog();
                        }

                        ValidaTipoCambio();
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void btn_item_repositorio_Click(object sender, EventArgs e)
        {
            try
            {
                if (Application.OpenForms["FrmRepositorio"] != null)
                {
                    Application.OpenForms["FrmRepositorio"].Activate();
                }
                else
                {
                    FrmRepositorio frm_repositorio = new FrmRepositorio();
                    frm_repositorio.usureg = usuario;
                    frm_repositorio.empresa = empresa;
                    frm_repositorio.MdiParent = this;
                    frm_repositorio.Show();
                }
            }
            catch (Exception) { }
        }

        private void btn_item_lista_venta_Click(object sender, EventArgs e)
        {
            try
            {
                if (Application.OpenForms["frmListaVenta"] != null)
                {
                    Application.OpenForms["frmListaVenta"].Activate();
                }
                else
                {
                    frmListaVenta frm_lventa = new frmListaVenta();
                    frm_lventa.empresa = empresa;
                    frm_lventa.usureg = usuario;
                    frm_lventa.MdiParent = this;
                    frm_lventa.Show();
                }
            }
            catch (Exception) { }
        }

        private void btn_item_banco_Click(object sender, EventArgs e)
        {
            try
            {
                if (Application.OpenForms["frmBanco"] != null)
                {
                    Application.OpenForms["frmBanco"].Activate();
                }
                else
                {
                    frmBanco frm_banco = new frmBanco();
                    frm_banco.usureg = usuario;
                    frm_banco.MdiParent = this;
                    frm_banco.Show();
                }
            }
            catch (Exception) { }
        }

        private void btn_item_tipocuenta_bancaria_Click(object sender, EventArgs e)
        {
            try
            {
                if (Application.OpenForms["frmTipoCuentaBancaria"] != null)
                {
                    Application.OpenForms["frmTipoCuentaBancaria"].Activate();
                }
                else
                {
                    frmTipoCuentaBancaria frm_tipocuenta = new frmTipoCuentaBancaria();
                    frm_tipocuenta.usureg = usuario;
                    frm_tipocuenta.MdiParent = this;
                    frm_tipocuenta.Show();
                }
            }
            catch (Exception) { }
        }

        private void btn_item_cuentabancaria_Click(object sender, EventArgs e)
        {
            try
            {
                if (Application.OpenForms["frmCuentaBancaria"] != null)
                {
                    Application.OpenForms["frmCuentaBancaria"].Activate();
                }
                else
                {
                    frmCuentaBancaria frm_cuenta = new frmCuentaBancaria();
                    frm_cuenta.usureg = usuario;
                    frm_cuenta.MdiParent = this;
                    frm_cuenta.Show();
                }
            }
            catch (Exception) { }
        }

        private void btn_item_tarjeta_Click(object sender, EventArgs e)
        {
            try
            {
                if (Application.OpenForms["frmTarjeta"] != null)
                {
                    Application.OpenForms["frmTarjeta"].Activate();
                }
                else
                {
                    frmTarjeta frm_tarjeta = new frmTarjeta();
                    frm_tarjeta.usureg = usuario;
                    frm_tarjeta.MdiParent = this;
                    frm_tarjeta.Show();
                }
            }
            catch (Exception) { }
        }

        private void btn_item_tipo_cobro_Click(object sender, EventArgs e)
        {
            try
            {
                if (Application.OpenForms["frmTipoCobro"] != null)
                {
                    Application.OpenForms["frmTipoCobro"].Activate();
                }
                else
                {
                    frmTipoCobro frm_tipoco = new frmTipoCobro();
                    frm_tipoco.usureg = usuario;
                    frm_tipoco.MdiParent = this;
                    frm_tipoco.Show();
                }
            }
            catch (Exception) { }
        }

        private void btn_item_apertura_Click(object sender, EventArgs e)
        {
            try
            {
                if (Application.OpenForms["frmAperturaCaja"] != null)
                {
                    Application.OpenForms["frmAperturaCaja"].Activate();
                }
                else
                {
                    frmAperturaCaja frm_apertura = new frmAperturaCaja();
                    frm_apertura.usureg = usuario;
                    frm_apertura.MdiParent = this;
                    frm_apertura.Show();
                }
            }
            catch (Exception) { }
        }

        private void btn_item_caja_abierta_Click(object sender, EventArgs e)
        {
            try
            {
                if (Application.OpenForms["frmVerAperturaCaja"] != null)
                {
                    Application.OpenForms["frmVerAperturaCaja"].Activate();
                }
                else
                {
                    frmVerAperturaCaja frm_cajaabierta = new frmVerAperturaCaja();
                    frm_cajaabierta.usureg = usuario;
                    frm_cajaabierta.MdiParent = this;
                    frm_cajaabierta.Show();
                }
            }
            catch (Exception) { }
        }

        private void btn_item_cajamovimiento_Click(object sender, EventArgs e)
        {
            try
            {
                if (Application.OpenForms["frmCajaMovimiento"] != null)
                {
                    Application.OpenForms["frmCajaMovimiento"].Activate();
                }
                else
                {
                    frmCajaMovimiento frm_cajamovimiento = new frmCajaMovimiento();
                    frm_cajamovimiento.usureg = usuario;
                    frm_cajamovimiento._Cajaid = -1;
                    frm_cajamovimiento.Estado = 1;

                    frm_cajamovimiento.MdiParent = this;
                    frm_cajamovimiento.Show();
                }
            }
            catch (Exception) { }
        }

        private void btn_item_cajahistorica_Click(object sender, EventArgs e)
        {
            try
            {
                if (Application.OpenForms["frmCajaHistorica"] != null)
                {
                    Application.OpenForms["frmCajaHistorica"].Activate();
                }
                else
                {
                    frmCajaHistorica frm_historico = new frmCajaHistorica();
                    frm_historico.usureg = usuario;
                    frm_historico.MdiParent = this;
                    frm_historico.Show();
                }
            }
            catch (Exception) { }
        }

        private void btn_lista_nota_credito_debito_Click(object sender, EventArgs e)
        {
            try
            {
                if (Application.OpenForms["frmListaNotaCreditoDebito"] != null)
                {
                    Application.OpenForms["frmListaNotaCreditoDebito"].Activate();
                }
                else
                {
                    frmListaNotaCreditoDebito frm_lnota = new frmListaNotaCreditoDebito();
                    frm_lnota.empresa = empresa;
                    frm_lnota.usureg = usuario;
                    frm_lnota.MdiParent = this;
                    frm_lnota.Show();
                }
            }
            catch (Exception) { }
        }

        private void btn_item_tipocambio_Click(object sender, EventArgs e)
        {
            try
            {
                if (Application.OpenForms["frmTipoCambio"] != null)
                {
                    Application.OpenForms["frmTipoCambio"].Activate();
                }
                else
                {
                    frmTipoCambio frm_tipocambio = new frmTipoCambio();
                    frm_tipocambio.MdiParent = this;
                    frm_tipocambio.Show();
                }
            }
            catch (Exception) { }
        }

        private void btn_item_lista_ticket_nofacturado_Click(object sender, EventArgs e)
        {
            try
            {
                if (Application.OpenForms["frmListaTicketNoFacturado"] != null)
                {
                    Application.OpenForms["frmListaTicketNoFacturado"].Activate();
                }
                else
                {
                    frmListaTicketNoFacturado frm_listaticknofact = new frmListaTicketNoFacturado();
                    frm_listaticknofact.usureg = usuario;
                    frm_listaticknofact.MdiParent = this;
                    frm_listaticknofact.Show();
                }
            }
            catch (Exception) { }
        }

        private void btn_item_cajas_cerradas_Click(object sender, EventArgs e)
        {
            try
            {
                if (Application.OpenForms["frmVerCierreCaja"] != null)
                {
                    Application.OpenForms["frmVerCierreCaja"].Activate();
                }
                else
                {
                    frmVerCierreCaja frm_cajacerrada = new frmVerCierreCaja();
                    frm_cajacerrada.usureg = usuario;
                    frm_cajacerrada.MdiParent = this;
                    frm_cajacerrada.Show();
                }
            }
            catch (Exception) { }
        }

        private void btn_item_historial_pedido_Click(object sender, EventArgs e)
        {
            try
            {
                if (Application.OpenForms["frmTicketsxMesa"] != null)
                {
                    Application.OpenForms["frmTicketsxMesa"].Activate();
                }
                else
                {
                    frmTicketsxMesa frm_ticketxmesa = new frmTicketsxMesa();
                    frm_ticketxmesa.usureg = usuario;
                    frm_ticketxmesa.MdiParent = this;
                    frm_ticketxmesa.Show();
                }
            }
            catch (Exception) { }
        }

        private void btn_item_historial_ticket_Click(object sender, EventArgs e)
        {
            try
            {
                if (Application.OpenForms["frmHistorialTickets"] != null)
                {
                    Application.OpenForms["frmHistorialTickets"].Activate();
                }
                else
                {
                    frmHistorialTickets frm_historialticket = new frmHistorialTickets();
                    frm_historialticket.MdiParent = this;
                    frm_historialticket.Show();
                }
            }
            catch (Exception) { }
        }

        private void btn_item_ventas_xmesa_mozo_Click(object sender, EventArgs e)
        {
            try
            {
                if (Application.OpenForms["frmHistorialTickets"] != null)
                {
                    Application.OpenForms["frmHistorialTickets"].Activate();
                }
                else
                {
                    frmMesasyVentasxMozo frm_mesasyventasxmozo = new frmMesasyVentasxMozo();
                    frm_mesasyventasxmozo.MdiParent = this;
                    frm_mesasyventasxmozo.Show();
                }
            }
            catch (Exception) { }
        }

        private void btn_item_ingrediente_Click(object sender, EventArgs e)
        {
            try
            {
                if (Application.OpenForms["frmIngrediente"] != null)
                {
                    Application.OpenForms["frmIngrediente"].Activate();
                }
                else
                {
                    frmIngrediente frm_ingrediente = new frmIngrediente();
                    frm_ingrediente.usureg = usuario;
                    frm_ingrediente.MdiParent = this;
                    frm_ingrediente.Show();
                }
            }
            catch (Exception) { }
        }

        private void btn_item_unidad_medida_Click(object sender, EventArgs e)
        {
            try
            {
                if (Application.OpenForms["frmUnidadMedida"] != null)
                {
                    Application.OpenForms["frmUnidadMedida"].Activate();
                }
                else
                {
                    frmUnidadMedida frm_unidadmedida = new frmUnidadMedida();
                    frm_unidadmedida.usureg = usuario;
                    frm_unidadmedida.MdiParent = this;
                    frm_unidadmedida.Show();
                }
            }
            catch (Exception) { }
        }

        private void btn_item_proveedor_Click(object sender, EventArgs e)
        {
            try
            {
                if (Application.OpenForms["frmProveedor"] != null)
                {
                    Application.OpenForms["frmProveedor"].Activate();
                }
                else
                {
                    frmProveedor frm_proveedor = new frmProveedor();
                    frm_proveedor.usureg = usuario;
                    frm_proveedor.MdiParent = this;
                    frm_proveedor.Show();
                }
            }
            catch (Exception) { }
        }

        private void ribbonControl1_Click(object sender, EventArgs e)
        {

        }
    }
}
