﻿namespace FinalXML.vista.administrador
{
    partial class frmCodProductoSunat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvCodproductosunat = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.idproducto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idclase = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descripcionproducto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codsunat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txtCodproductosunat = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtDescprod = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCodproductosunat)).BeginInit();
            this.groupPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvCodproductosunat
            // 
            this.dgvCodproductosunat.AllowUserToAddRows = false;
            this.dgvCodproductosunat.AllowUserToDeleteRows = false;
            this.dgvCodproductosunat.AllowUserToOrderColumns = true;
            this.dgvCodproductosunat.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCodproductosunat.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idproducto,
            this.idclase,
            this.descripcionproducto,
            this.codsunat});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvCodproductosunat.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvCodproductosunat.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvCodproductosunat.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dgvCodproductosunat.Location = new System.Drawing.Point(7, 3);
            this.dgvCodproductosunat.Name = "dgvCodproductosunat";
            this.dgvCodproductosunat.RowHeadersVisible = false;
            this.dgvCodproductosunat.Size = new System.Drawing.Size(432, 169);
            this.dgvCodproductosunat.TabIndex = 4;
            this.dgvCodproductosunat.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCodproductosunat_CellDoubleClick);
            // 
            // idproducto
            // 
            this.idproducto.DataPropertyName = "idproducto";
            this.idproducto.HeaderText = "CODPRODUCTO";
            this.idproducto.Name = "idproducto";
            this.idproducto.Visible = false;
            // 
            // idclase
            // 
            this.idclase.DataPropertyName = "idclase";
            this.idclase.HeaderText = "CODCLASE";
            this.idclase.Name = "idclase";
            this.idclase.Visible = false;
            // 
            // descripcionproducto
            // 
            this.descripcionproducto.DataPropertyName = "descripcionproducto";
            this.descripcionproducto.HeaderText = "DESCRIPCION";
            this.descripcionproducto.Name = "descripcionproducto";
            // 
            // codsunat
            // 
            this.codsunat.DataPropertyName = "codsunat";
            this.codsunat.HeaderText = "CODSUNAT";
            this.codsunat.Name = "codsunat";
            // 
            // groupPanel1
            // 
            this.groupPanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.txtCodproductosunat);
            this.groupPanel1.Controls.Add(this.txtDescprod);
            this.groupPanel1.Controls.Add(this.labelX2);
            this.groupPanel1.Controls.Add(this.buttonX2);
            this.groupPanel1.Controls.Add(this.buttonX1);
            this.groupPanel1.Controls.Add(this.dgvCodproductosunat);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(2, 2);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.Size = new System.Drawing.Size(454, 315);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 5;
            this.groupPanel1.Text = "Lista Codigos";
            // 
            // txtCodproductosunat
            // 
            // 
            // 
            // 
            this.txtCodproductosunat.Border.Class = "TextBoxBorder";
            this.txtCodproductosunat.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtCodproductosunat.Location = new System.Drawing.Point(351, 260);
            this.txtCodproductosunat.Name = "txtCodproductosunat";
            this.txtCodproductosunat.PreventEnterBeep = true;
            this.txtCodproductosunat.Size = new System.Drawing.Size(100, 22);
            this.txtCodproductosunat.TabIndex = 11;
            this.txtCodproductosunat.Visible = false;
            // 
            // txtDescprod
            // 
            // 
            // 
            // 
            this.txtDescprod.Border.Class = "TextBoxBorder";
            this.txtDescprod.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDescprod.Location = new System.Drawing.Point(75, 220);
            this.txtDescprod.Name = "txtDescprod";
            this.txtDescprod.PreventEnterBeep = true;
            this.txtDescprod.Size = new System.Drawing.Size(353, 22);
            this.txtDescprod.TabIndex = 10;
            this.txtDescprod.TextChanged += new System.EventHandler(this.textBoxX2_TextChanged);
            this.txtDescprod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxX2_KeyDown);
            // 
            // labelX2
            // 
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Location = new System.Drawing.Point(3, 220);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(61, 23);
            this.labelX2.TabIndex = 9;
            this.labelX2.Text = "Descripcion";
            // 
            // buttonX2
            // 
            this.buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX2.Location = new System.Drawing.Point(239, 259);
            this.buttonX2.Name = "buttonX2";
            this.buttonX2.Size = new System.Drawing.Size(75, 23);
            this.buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX2.TabIndex = 6;
            this.buttonX2.Text = "Cancelar";
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX1.Location = new System.Drawing.Point(116, 259);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.Size = new System.Drawing.Size(75, 23);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX1.TabIndex = 5;
            this.buttonX1.Text = "Guardar";
            // 
            // frmCodProductoSunat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(459, 319);
            this.Controls.Add(this.groupPanel1);
            this.DoubleBuffered = true;
            this.Name = "frmCodProductoSunat";
            this.Text = "Codigos Producto Sunat";
            this.Load += new System.EventHandler(this.frmCodProductoSunat_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCodproductosunat)).EndInit();
            this.groupPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.Controls.TextBoxX txtDescprod;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.ButtonX buttonX2;
        private DevComponents.DotNetBar.ButtonX buttonX1;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvCodproductosunat;
        private DevComponents.DotNetBar.Controls.TextBoxX txtCodproductosunat;
        private System.Windows.Forms.DataGridViewTextBoxColumn idproducto;
        private System.Windows.Forms.DataGridViewTextBoxColumn idclase;
        private System.Windows.Forms.DataGridViewTextBoxColumn descripcionproducto;
        private System.Windows.Forms.DataGridViewTextBoxColumn codsunat;
    }
}