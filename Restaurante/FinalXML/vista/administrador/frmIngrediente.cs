﻿using FinalXML.Administradores;
using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalXML.vista.administrador
{
    public partial class frmIngrediente : PlantillaBase
    {
        public clsUsuario usureg { set; get; }
        private clsAdmIngrediente admingre = new clsAdmIngrediente();
        private DataTable ingredientes = null;
        private List<clsUnidadMedida> lista_unidadmedida = null;
        private clsAdmUnidadMedida admunidadm = new clsAdmUnidadMedida();
        public frmIngrediente()
        {
            InitializeComponent();
        }

        private void frmIngrediente_Load(object sender, EventArgs e)
        {
            cb_estado.SelectedIndex = 0;
            dg_ingrediente.AutoGenerateColumns = false;
            listar_unidad_medida_xestado();
        }

        private void txt_nombre_TextChanged(object sender, EventArgs e)
        {
            txt_nombre.CharacterCasing = CharacterCasing.Upper;
        }

        private void txt_descripcion_TextChanged(object sender, EventArgs e)
        {
            txt_descripcion.CharacterCasing = CharacterCasing.Upper;
        }

        private void txt_preciounitario_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }


            if (e.KeyChar == '.' && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }

            if (!char.IsControl(e.KeyChar))
            {

                TextBox textBox = (TextBox)sender;

                if (textBox.Text.IndexOf('.') > -1 &&
                         textBox.Text.Substring(textBox.Text.IndexOf('.')).Length >= 4)
                {
                    e.Handled = true;
                }

            }
        }

        private void txt_preciounitario_Leave(object sender, EventArgs e)
        {
            if (txt_preciounitario.Text.Length == 0)
            {
                txt_preciounitario.Text = "0.00";
            }
        }

        private void btn_guardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (lista_unidadmedida == null) {

                    MessageBox.Show("No se han cargado las unidades de medida...", "Advertencia");
                    return;
                }

                if (txt_codigo.Text.Length == 0)
                {
                    if (txt_nombre.Text.Length > 0 && txt_preciounitario.Text.Length > 0)
                    {
                        if (es_numero(txt_preciounitario.Text.Trim()))
                        {
                            if (decimal.Parse(txt_preciounitario.Text.Trim()) >= 0)
                            {

                                DialogResult respuesta = MessageBox.Show("¿Desea registrar ingrediente?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                                if (respuesta == DialogResult.Yes)
                                {
                                    registrar_ingrediente();
                                    btn_buscar.PerformClick();
                                }

                            }                            

                        }
                        else
                        {

                            MessageBox.Show("Valor ingresado no es un número...", "Advertencia");
                        }
                    }
                    else
                    {

                        MessageBox.Show("Por favor complete la información solicitada...", "Advertencia");
                    }
                }
                else
                {
                    if (txt_nombre.Text.Length > 0 && txt_preciounitario.Text.Length > 0)
                    {
                        if (es_numero(txt_preciounitario.Text.Trim()))
                        {
                            if (decimal.Parse(txt_preciounitario.Text.Trim()) >= 0)
                            {

                                DialogResult respuesta = MessageBox.Show("¿Desea actualizar ingrediente?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                                if (respuesta == DialogResult.Yes)
                                {
                                    actualizar_ingrediente();
                                    btn_buscar.PerformClick();
                                }

                            }
                            else
                            {
                                MessageBox.Show("Precio debe ser mayor a 0.00", "Advertencia");
                            }

                        }
                        else
                        {

                            MessageBox.Show("Valor ingresado no es un número...", "Advertencia");
                        }
                    }
                    else
                    {

                        MessageBox.Show("Por favor complete la información solicitada...", "Advertencia");
                    }


                }
            }
            catch (Exception) { }
        }

        private void btn_cargar_Click(object sender, EventArgs e)
        {
            try {

                listar_listar_ingrediente();
            }
            catch (Exception) {

            }
        }

        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            try
            {
                limpiar();
            }
            catch (Exception) { }
        }
        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                listar_ingrediente_xnombre();
            }
            catch (Exception) { }
        }

        /************************Mis Metodos***************************/
        public void listar_unidad_medida_xestado() {

            try
            {
                lista_unidadmedida = admunidadm.listar_unidadmedida_xestado();
                if (lista_unidadmedida != null)
                {

                    if (lista_unidadmedida.Count > 0)
                    {

                        cb_unidadmedida.DataSource = lista_unidadmedida;
                        cb_unidadmedida.DisplayMember = "Sigla";
                        cb_unidadmedida.ValueMember = "Unidadmedidaid";
                    }
                }
            }
            catch (Exception) { }
            
        }
        public bool es_numero(object Expression)
        {
            try
            {
                bool isNum;
                double retNum;
                isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
                return isNum;
            }
            catch (Exception) { return false; }
        }

        public void registrar_ingrediente() {

            int id = -1;
            try {

                id = admingre.registrar_ingrediente(
                    new clsIngrediente()
                    {
                        Nombre = txt_nombre.Text,
                        Descripcion = txt_descripcion.Text,
                        Preciounitario = decimal.Parse(txt_preciounitario.Text),
                        Estado= (cb_estado.Text == "ACTIVO") ? 1 : 0,
                        Unidadmedida=lista_unidadmedida[cb_unidadmedida.SelectedIndex]
                    },
                    usureg);

                if (id > 0)
                {

                    MessageBox.Show("Registro correcto...", "Información");
                }
                else {

                    MessageBox.Show("Problemas para registrar ingrediente...", "Advertencia");
                }
            }
            catch (Exception) {


            }

        }

        public void actualizar_ingrediente()
        {

            int id = -1;
            try
            {

                id = admingre.actualizar_ingrediente(
                    new clsIngrediente()
                    {
                        Idingrediente=int.Parse(txt_codigo.Text),
                        Nombre = txt_nombre.Text,
                        Descripcion = txt_descripcion.Text,
                        Preciounitario = decimal.Parse(txt_preciounitario.Text),
                        Estado = (cb_estado.Text == "ACTIVO") ? 1 : 0,
                        Unidadmedida = lista_unidadmedida[cb_unidadmedida.SelectedIndex]
                    },
                    usureg);

                if (id > 0)
                {

                    MessageBox.Show("Actualización correcta...", "Información");
                }
                else
                {

                    MessageBox.Show("Problemas para actualizar ingrediente...", "Advertencia");
                }
            }
            catch (Exception)
            {


            }

        }

        public void listar_ingrediente_xnombre()
        {
            try
            {
                dg_ingrediente.DataSource = null;
                ingredientes = admingre.listar_ingrediente_xnombre(
                                                                new clsIngrediente()
                                                                {
                                                                    Nombre = txt_nombre.Text
                                                                }
                                                            );

                if (ingredientes != null)
                {
                    if (ingredientes.Rows.Count > 0)
                    {
                        dg_ingrediente.DataSource = ingredientes;
                        dg_ingrediente.Refresh();
                    }
                }
            }
            catch (Exception) { }
        }

        public void listar_listar_ingrediente()
        {

            try
            {
                dg_ingrediente.DataSource = null;
                ingredientes = admingre.listar_ingrediente();

                if (ingredientes != null)
                {
                    if (ingredientes.Rows.Count > 0)
                    {
                        dg_ingrediente.DataSource = ingredientes;
                        dg_ingrediente.Refresh();
                    }
                }
            }
            catch (Exception) { }

        }

        public void limpiar()
        {
            try
            {
                txt_codigo.Text = string.Empty;
                cb_estado.SelectedIndex = 0;            
                txt_nombre.Text = string.Empty;
                txt_descripcion.Text = string.Empty;
                txt_preciounitario.Text = "0.00";
                dg_ingrediente.DataSource = null;
                cb_unidadmedida.SelectedIndex = 0;
            }
            catch (Exception) { }
        }

        private void dg_ingrediente_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dg_ingrediente.Rows.Count > 0)
                {
                    if (e.RowIndex != -1)
                    {
                        txt_codigo.Text = dg_ingrediente.Rows[e.RowIndex].Cells[idingrediente.Index].Value.ToString();
                        txt_nombre.Text = dg_ingrediente.Rows[e.RowIndex].Cells[nombre.Index].Value.ToString();
                        txt_descripcion.Text = dg_ingrediente.Rows[e.RowIndex].Cells[descripcion.Index].Value.ToString();
                        txt_preciounitario.Text= dg_ingrediente.Rows[e.RowIndex].Cells[preciounitario.Index].Value.ToString();
                        if (dg_ingrediente.Rows[e.RowIndex].Cells[estado.Index].Value.ToString() == "ACTIVO") { cb_estado.SelectedIndex = 0; } else { cb_estado.SelectedIndex = 1; }
                        cb_unidadmedida.Text= dg_ingrediente.Rows[e.RowIndex].Cells[sigla.Index].Value.ToString();

                    }
                }
            }
            catch (Exception) { }
        }

        private void btn_unidad_equivalente_Click(object sender, EventArgs e)
        {
            try
            {
                if (txt_codigo.Text.Length > 0)
                {
                    if (dg_ingrediente.Rows.Count > 0)
                    {

                        if (dg_ingrediente.CurrentCell != null)
                        {

                            if (dg_ingrediente.CurrentCell.RowIndex != -1)
                            {

                                cb_unidadmedida.Text = dg_ingrediente.Rows[dg_ingrediente.CurrentCell.RowIndex].Cells[sigla.Index].Value.ToString();

                                if (Application.OpenForms["frmUnidadCompra"] != null)
                                {
                                    Application.OpenForms["frmUnidadCompra"].Activate();
                                }
                                else
                                {
                                    frmUnidadCompra frm_unidadcompra = new frmUnidadCompra();
                                    frm_unidadcompra.usureg = usureg;
                                    frm_unidadcompra.ingrediente = new clsIngrediente()
                                    {

                                        Idingrediente = int.Parse(dg_ingrediente.Rows[dg_ingrediente.CurrentCell.RowIndex].Cells[idingrediente.Index].Value.ToString()),
                                        Nombre = dg_ingrediente.Rows[dg_ingrediente.CurrentCell.RowIndex].Cells[nombre.Index].Value.ToString(),
                                        Unidadmedida = lista_unidadmedida[cb_unidadmedida.SelectedIndex],

                                    };
                                    frm_unidadcompra.ShowDialog();
                                }

                            }

                        }

                    }
                }
                
            }
            catch (Exception) { }
        }
    }
}
