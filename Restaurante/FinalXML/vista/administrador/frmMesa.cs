﻿using FinalXML.Administradores;
using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalXML.vista.administrador
{

    public partial class frmMesa : PlantillaBase
    {
        public clsUsuario usureg { get; set; }
        private clsMesa mesa = null;
        private clsAdmMesa admme = new clsAdmMesa();
        private DataTable mesas = null;
        private List<clsPiso> lista_piso = null;
        private clsAdmPiso admpi = new clsAdmPiso();
        public frmMesa()
        {
            InitializeComponent();
        }

        private void frmMesa_Load(object sender, EventArgs e)
        {
            listar_piso_xestado();
            cb_estado.SelectedIndex = 1;
            dg_mesa.AutoGenerateColumns = false;
        }

        private void txt_nombre_TextChanged(object sender, EventArgs e)
        {
            txt_nombre.CharacterCasing = CharacterCasing.Upper;
        }

        private void txt_descripcion_TextChanged(object sender, EventArgs e)
        {
            txt_descripcion.CharacterCasing = CharacterCasing.Upper;
        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void btn_guardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (txt_cod.Text.Length == 0)
                {

                    if (txt_nombre.Text.Length > 0)
                    {
                        DialogResult respuesta = MessageBox.Show("¿Desea registrar mesa?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (respuesta == DialogResult.Yes)
                        {
                            registrar_mesa();
                            btn_cargar.PerformClick();
                        }
                    }
                    else
                    {

                        MessageBox.Show("Por favor complete la información solicitada...", "Advertencia");
                    }

                }
                else
                {

                    if (txt_nombre.Text.Length > 0)
                    {
                        DialogResult respuesta = MessageBox.Show("¿Desea actualizar mesa?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (respuesta == DialogResult.Yes)
                        {
                            actualizar_mesa();
                            btn_cargar.PerformClick();
                        }
                    }
                    else
                    {

                        MessageBox.Show("Por favor complete la información solicitada...", "Advertencia");
                    }

                }
            }
            catch (Exception) { }
        }

        private void btn_cargar_Click(object sender, EventArgs e)
        {
            try
            {
                listar_mesa();
            }
            catch (Exception) { }
        }

        /********************Mis Metodos******************/
        public void listar_piso_xestado()
        {
            try
            {
                lista_piso = admpi.listar_piso_xestado();

                if (lista_piso != null)
                {

                    if (lista_piso.Count > 0)
                    {

                        cb_piso.DataSource = lista_piso;
                        cb_piso.DisplayMember = "Nombre";
                        cb_piso.ValueMember = "Idpiso";
                    }
                }
            }
            catch (Exception) { }
        }
        public void limpiar() {

            try
            {
                cb_estado.SelectedIndex = 1;
                txt_cod.Text = string.Empty;
                txt_nombre.Text = string.Empty;
                txt_descripcion.Text = string.Empty;
                dg_mesa.DataSource = null;
            }
            catch (Exception) { }
        }

        public void registrar_mesa() {

            int estado = -1;

            try
            {
                switch (cb_estado.SelectedItem.ToString())
                {

                    case "DETERIORO": estado = 0; break;
                    case "DISPONIBLE": estado = 1; break;
                    case "OCUPADA": estado = 2; break;
                }
                mesa = new clsMesa()
                {
                    Nombre = txt_nombre.Text,
                    Descripcion = txt_descripcion.Text,
                    Estado = estado,
                    Piso = lista_piso[cb_piso.SelectedIndex]
                };

                if (admme.registrar_mesa(mesa, usureg) > 0)
                {
                    MessageBox.Show("Registro correcto...", "Informacion");
                }
                else
                {

                    MessageBox.Show("Problemas para registrar mesa...", "Advertencia");
                }
            }
            catch (Exception) { }
        }

        public void actualizar_mesa()
        {
            int filas = -1;
            int estado = -1;

            try
            {
                switch (cb_estado.SelectedItem.ToString())
                {

                    case "DETERIORO": estado = 0; break;
                    case "DISPONIBLE": estado = 1; break;
                    case "OCUPADA": estado = 2; break;
                }
                mesa = new clsMesa()
                {
                    Idmesa = int.Parse(txt_cod.Text),
                    Nombre = txt_nombre.Text,
                    Descripcion = txt_descripcion.Text,
                    Estado = estado,
                    Piso = lista_piso[cb_piso.SelectedIndex]
                };

                filas = admme.actualizar_mesa(mesa, usureg);

                if (filas > 0)
                {
                    MessageBox.Show("Actualización correcta...", "Informacion");
                }
                else
                {
                    if (filas == -2)
                    {
                        MessageBox.Show("Mesa Presenta pedido ó esta registrada en un grupo...", "Advertencia");
                    }
                    else
                    {
                        MessageBox.Show("Problemas para actualizar mesa...", "Advertencia");
                    }
                }
            }
            catch (Exception) { }
        }

        public void listar_mesa() {

            try
            {
                dg_mesa.DataSource = null;
                mesas = admme.listar_mesa();

                if (mesas != null)
                {

                    if (mesas.Rows.Count > 0)
                    {

                        dg_mesa.DataSource = mesas;
                        dg_mesa.Refresh();
                    }
                }
            }
            catch (Exception) { }
        }

        private void dg_mesa_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dg_mesa.Rows.Count > 0)
                {

                    if (e.RowIndex != -1)
                    {

                        txt_cod.Text = dg_mesa.Rows[e.RowIndex].Cells[idmesa.Index].Value.ToString();
                        switch (dg_mesa.Rows[e.RowIndex].Cells[estado.Index].Value.ToString())
                        {
                            case "DETERIORO": cb_estado.SelectedIndex = 0; break;
                            case "DISPONIBLE": cb_estado.SelectedIndex = 1; break;
                            case "OCUPADA": cb_estado.SelectedIndex = 2; break;
                        }

                        txt_nombre.Text = dg_mesa.Rows[e.RowIndex].Cells[nombremesa.Index].Value.ToString();
                        txt_descripcion.Text = dg_mesa.Rows[e.RowIndex].Cells[descripcion.Index].Value.ToString();

                        cb_piso.SelectedValue = ((from x in lista_piso
                                                  where x.Nombre == dg_mesa.Rows[e.RowIndex].Cells[nombre.Index].Value.ToString()
                                                  select x.Idpiso).ToList())[0];
                    }
                }
            }
            catch (Exception) { }
        }
    }
}
