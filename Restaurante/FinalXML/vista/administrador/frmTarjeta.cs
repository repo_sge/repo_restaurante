﻿using FinalXML.Administradores;
using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalXML.vista.administrador
{
    public partial class frmTarjeta : PlantillaBase
    {
        public clsUsuario usureg { get; set; }
        private clsTarjeta tarjetacobro;
        private clsAdmTarjeta admtar = new clsAdmTarjeta();
        private DataTable tarjetas = null;
        public frmTarjeta()
        {
            InitializeComponent();
        }

        private void frmTarjeta_Load(object sender, EventArgs e)
        {
            cb_estado.SelectedIndex = 0;
            dg_resultado.AutoGenerateColumns = false;
        }

        private void txt_decripcion_TextChanged(object sender, EventArgs e)
        {
            txt_decripcion.CharacterCasing = CharacterCasing.Upper;
        }

        private void btn_guardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (txt_cod.Text.Length == 0)
                {

                    if (txt_decripcion.Text.Length > 0)
                    {
                        DialogResult respuesta = MessageBox.Show("¿Desea registrar tarjeta?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (respuesta == DialogResult.Yes)
                        {
                            tarjetacobro = new clsTarjeta()
                            {
                                Descripcion = txt_decripcion.Text
                            };

                            if (cb_estado.SelectedIndex == 0) { tarjetacobro.Estado = 1; } else { tarjetacobro.Estado = 0; }

                            if (admtar.registrar_tarjeta_cobro(tarjetacobro, usureg) > 0)
                            {
                                MessageBox.Show("Registro correcto...", "Información");
                                cargar_tarjeta();
                            }
                            else
                            {

                                MessageBox.Show("Problemas en el registro...", "Advertencia");
                            }
                        }
                    }
                }
                else
                {

                    if (txt_decripcion.Text.Length > 0)
                    {
                        DialogResult respuesta = MessageBox.Show("¿Desea actualizar tarjeta?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (respuesta == DialogResult.Yes)
                        {
                            tarjetacobro = new clsTarjeta()
                            {
                                Idtarjeta = int.Parse(txt_cod.Text),
                                Descripcion = txt_decripcion.Text

                            };

                            if (cb_estado.SelectedIndex == 0) { tarjetacobro.Estado = 1; } else { tarjetacobro.Estado = 0; }

                            if (admtar.actualizar_tarjeta_cobro(tarjetacobro, usureg) > 0)
                            {
                                MessageBox.Show("Actualización correcta...", "Información");
                                cargar_tarjeta();
                            }
                            else
                            {

                                MessageBox.Show("Problemas en la actualización...", "Advertencia");
                            }
                        }
                    }
                }
            }
            catch (Exception) { }
        }
        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            try
            {
                limpiar();
            }
            catch (Exception) { }
        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_cargar_Click(object sender, EventArgs e)
        {
            try
            {
                cargar_tarjeta();
            }
            catch (Exception) { }
        }
        private void dg_resultado_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dg_resultado.Rows.Count > 0)
                {
                    if (e.RowIndex != -1)
                    {
                        txt_cod.Text = dg_resultado.Rows[e.RowIndex].Cells[0].Value.ToString();
                        txt_decripcion.Text = dg_resultado.Rows[e.RowIndex].Cells[1].Value.ToString();
                        if (dg_resultado.Rows[e.RowIndex].Cells[2].Value.ToString() == "ACTIVO") { cb_estado.SelectedIndex = 0; }
                        else { cb_estado.SelectedIndex = 1; }
                    }
                }
            }
            catch (Exception) { }
        }

        /***********************Mis Metodos*****************************/

        public void cargar_tarjeta()
        {
            try
            {
                tarjetas = admtar.listar_tarjeta_cobro();
                dg_resultado.DataSource = null;

                if (tarjetas != null)
                {

                    if (tarjetas.Rows.Count > 0)
                    {

                        dg_resultado.DataSource = tarjetas;
                    }

                }
            }
            catch (Exception) { }

        }

        public void limpiar()
        {
            try
            {
                txt_cod.Text = string.Empty;
                txt_decripcion.Text = string.Empty;
                dg_resultado.DataSource = null;
                cb_estado.SelectedIndex = 0;
            }
            catch (Exception) { }
        }
        
    }
}
