﻿using FinalXML.Administradores;
using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalXML.vista.administrador
{
    public partial class frmProveedor : PlantillaBase
    {
        private List<clsTipoDocumentoIndentidad> lista_tipodoc = null;
        private clsAdmTipoDocumentoIdentidad admtipodocidentidad = new clsAdmTipoDocumentoIdentidad();
        private clsAdmProveedor admprovee = new clsAdmProveedor();
        private DataTable proveedores = null;
        public clsUsuario usureg { get; set; }
        public frmProveedor()
        {
            InitializeComponent();
        }

        private void frmProveedor_Load(object sender, EventArgs e)
        {
            dg_proveedor.AutoGenerateColumns = false;
            cb_estado.SelectedIndex = 0;
            listar_tipodoc_identidad();
        }

        private void txt_razonsocial_TextChanged(object sender, EventArgs e)
        {
            txt_razonsocial.CharacterCasing = CharacterCasing.Upper;
        }

        private void txt_direccion_TextChanged(object sender, EventArgs e)
        {
            txt_direccion.CharacterCasing = CharacterCasing.Upper;
        }

        private void txt_descripcion_TextChanged(object sender, EventArgs e)
        {
            txt_descripcion.CharacterCasing = CharacterCasing.Upper;
        }

        private void txt_doc_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void btn_guardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (txt_cod.Text.Length == 0)
                {
                    if (txt_razonsocial.Text.Length > 0
                      && txt_doc.Text.Length > 0)
                    {
                        DialogResult respuesta = MessageBox.Show("¿Desea registrar Proveedor?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (respuesta == DialogResult.Yes)
                        {
                            registrar_proveedor();
                            listar_proveedor_xrazonsocial();
                        }

                    }
                    else
                    {

                        MessageBox.Show("Por favor complete la información solicitada...", "Advertencia");
                    }
                }
                else
                {

                    if (txt_razonsocial.Text.Length > 0
                     && txt_doc.Text.Length > 0)
                    {

                        DialogResult respuesta = MessageBox.Show("¿Desea actualizar Proveedor?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (respuesta == DialogResult.Yes)
                        {
                            actualizar_proveedor();
                            listar_proveedor_xrazonsocial();
                        }
                    }
                    else
                    {

                        MessageBox.Show("Por favor complete la información solicitada...", "Advertencia");
                    }

                }
            }
            catch (Exception) { }
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            if (txt_razonsocial.Text.Length > 0)
            {
                listar_proveedor_xrazonsocial();
            }
        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_cargar_Click(object sender, EventArgs e)
        {
            listar_proveedor();
        }

        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            limpiar_todo();
        }

        /******************************Mis Metodos*************************************/
        public void listar_tipodoc_identidad()
        {

            try
            {
                lista_tipodoc = admtipodocidentidad.listar_tipo_documento_identidad();

                if (lista_tipodoc != null)
                {

                    if (lista_tipodoc.Count > 0)
                    {

                        cb_tipodoc.DataSource = lista_tipodoc;
                        cb_tipodoc.DisplayMember = "Descripcion";
                        cb_tipodoc.ValueMember = "Idtipodocumentoidentidad";
                        cb_tipodoc.SelectedIndex = 0;
                    }

                }
            }
            catch (Exception) { }
        }

        public void registrar_proveedor() {

            int id = -1;

            try
            {
                if (lista_tipodoc == null) {

                    MessageBox.Show("No se ha podido cargar los tipos de documento...", "Advertencia");
                    return;
                }

                id = admprovee.registrar_proveedor(
                    new clsProveedor()
                    {
                        Tipodocumentoindentidad=lista_tipodoc[cb_tipodoc.SelectedIndex],
                        Documento=txt_doc.Text,
                        Razonsocial=txt_razonsocial.Text,
                        Direccion=txt_direccion.Text,
                        Descripcion=txt_descripcion.Text,
                        Estado = (cb_estado.SelectedItem.ToString() == "ACTIVO") ? 1 : 0
                    }
                    ,usureg);

                    if (id > 0)
                    {
                        MessageBox.Show("Registro correcto...", "Información");
                    }
                    else
                    {
                        MessageBox.Show("Problemas para registrar cliente...", "Advertencia");
                    }

            }
            catch (Exception) { }

        }


        public void actualizar_proveedor()
        {

            int filas_afectadas = -1;

            try
            {
                if (lista_tipodoc == null)
                {

                    MessageBox.Show("No se ha podido cargar los tipos de documento...", "Advertencia");
                    return;
                }

                filas_afectadas = admprovee.actualizar_proveedor(
                    new clsProveedor()
                    {
                        Idproveedor=int.Parse(txt_cod.Text),
                        Tipodocumentoindentidad = lista_tipodoc[cb_tipodoc.SelectedIndex],
                        Documento = txt_doc.Text,
                        Razonsocial = txt_razonsocial.Text,
                        Direccion = txt_direccion.Text,
                        Descripcion = txt_descripcion.Text,
                        Estado = (cb_estado.SelectedItem.ToString() == "ACTIVO") ? 1 : 0
                    }
                    , usureg);

                if (filas_afectadas > 0)
                {
                    MessageBox.Show("Actualización correcto...", "Información");
                }
                else
                {
                    MessageBox.Show("Problemas para actualizar cliente...", "Advertencia");
                }

            }
            catch (Exception) { }

        }

        public void listar_proveedor_xrazonsocial()
        {
            try
            {
                dg_proveedor.DataSource = null;
                proveedores = admprovee.listar_proveedor_xrazonsocial(new clsProveedor()
                {
                    Razonsocial = txt_razonsocial.Text
                });
                if (proveedores != null)
                {
                    if (proveedores.Rows.Count > 0)
                    {

                        dg_proveedor.DataSource = proveedores;
                        dg_proveedor.Refresh();
                    }
                }
            }
            catch (Exception) { }
        }

        public void listar_proveedor()
        {

            try
            {
                dg_proveedor.DataSource = null;
                proveedores = admprovee.listar_proveedor();
                if (proveedores != null)
                {

                    if (proveedores.Rows.Count > 0)
                    {

                        dg_proveedor.DataSource = proveedores;
                        dg_proveedor.Refresh();
                    }
                }
            }
            catch (Exception) { }
        }

        public void limpiar_todo()
        {

            try
            {
                txt_cod.Text = string.Empty;
                cb_estado.SelectedIndex = 0;
                txt_razonsocial.Text = string.Empty;
                cb_tipodoc.SelectedIndex = 0;
                txt_doc.Text = string.Empty;
                txt_direccion.Text = string.Empty;
                txt_descripcion.Text = string.Empty;
                dg_proveedor.DataSource = null;
            }
            catch (Exception) { }
        }

        private void dg_proveedor_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dg_proveedor.Rows.Count > 0)
                {
                    if (e.RowIndex != -1)
                    {
                        txt_cod.Text = dg_proveedor.Rows[e.RowIndex].Cells[idproveedor.Index].Value.ToString();
                        cb_estado.SelectedIndex = (dg_proveedor.Rows[e.RowIndex].Cells[estado.Index].Value.ToString() == "ACTIVO") ? 0 : 1;
                        txt_razonsocial.Text = dg_proveedor.Rows[e.RowIndex].Cells[razonsocial.Index].Value.ToString();
                        cb_tipodoc.Text = dg_proveedor.Rows[e.RowIndex].Cells[tipodoc.Index].Value.ToString();                                               
                        txt_doc.Text = dg_proveedor.Rows[e.RowIndex].Cells[documento.Index].Value.ToString();
                        txt_direccion.Text = dg_proveedor.Rows[e.RowIndex].Cells[direccion.Index].Value.ToString();
                        txt_descripcion.Text= dg_proveedor.Rows[e.RowIndex].Cells[descripcion.Index].Value.ToString();
                    }
                }
            }
            catch (Exception) { }
        }
    }
}
