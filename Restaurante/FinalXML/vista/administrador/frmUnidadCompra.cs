﻿using FinalXML.Administradores;
using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalXML.vista.administrador
{
    public partial class frmUnidadCompra : PlantillaBase
    {
        public clsIngrediente ingrediente { get; set; }
        public clsUsuario usureg { get; set; }
        private List<clsUnidadMedida> lista_unidadmedida = null;
        private DataTable unidadescompra = null;
        private clsAdmUnidadMedida admunidadme = new clsAdmUnidadMedida();
        private clsAdmUnidadCompra admunidadco = new clsAdmUnidadCompra();
        public frmUnidadCompra()
        {
            InitializeComponent();
        }

        private void frmUnidadCompra_Load(object sender, EventArgs e)
        {
            dg_unidades_compra.AutoGenerateColumns = false;
            cb_estado.SelectedIndex = 0;
            listar_unidad_medida_xestado();

            if (ingrediente != null) {

                txt_ingrediente.Text = ingrediente.Nombre;
                txt_unidad_medida.Text = ingrediente.Unidadmedida.Sigla;
                listar_unidad_compra_xingrediente();
            }

        }

        private void txt_ingrediente_TextChanged(object sender, EventArgs e)
        {
            txt_ingrediente.CharacterCasing = CharacterCasing.Upper;
        }

        private void txt_unidad_medida_TextChanged(object sender, EventArgs e)
        {
            txt_unidad_medida.CharacterCasing = CharacterCasing.Upper;
        }

        private void txt_unidad_equivalente_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }


            if (e.KeyChar == '.' && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }

            if (!char.IsControl(e.KeyChar))
            {

                TextBox textBox = (TextBox)sender;

                if (textBox.Text.IndexOf('.') > -1 &&
                         textBox.Text.Substring(textBox.Text.IndexOf('.')).Length >= 4)
                {
                    e.Handled = true;
                }

            }
        }

        private void txt_unidad_equivalente_Leave(object sender, EventArgs e)
        {
            if (txt_unidad_medida.Text.Length == 0)
            {
                txt_unidad_medida.Text = "0.00";
            }
        }

        private void dg_unidades_compra_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dg_unidades_compra.Rows.Count > 0)
                {

                    if (e.RowIndex != -1)
                    {

                        txt_cod.Text = dg_unidades_compra.Rows[e.RowIndex].Cells[idunidadcompra.Index].Value.ToString();
                        txt_unidad_equivalente.Text = dg_unidades_compra.Rows[e.RowIndex].Cells[equivalencia.Index].Value.ToString();
                        if (dg_unidades_compra.Rows[e.RowIndex].Cells[estado.Index].Value.ToString() == "ACTIVO") { cb_estado.SelectedIndex = 0; } else { cb_estado.SelectedIndex = 1; }
                        cb_unidadmedida.Text = dg_unidades_compra.Rows[e.RowIndex].Cells[sigla.Index].Value.ToString();

                    }

                }
            }
            catch (Exception) { }
        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            try
            {
                limpiar();
            }
            catch (Exception) { }
        }

        private void btn_guardar_Click(object sender, EventArgs e)
        {
            try
            {

                if (lista_unidadmedida == null)
                {

                    MessageBox.Show("No se han cargado las unidades de medida...", "Advertencia");
                    return;
                }

                if (txt_cod.Text.Length == 0)
                {

                    if (txt_unidad_equivalente.Text.Length > 0)
                    {
                        if (es_numero(txt_unidad_equivalente.Text.Trim()))
                        {
                            if (decimal.Parse(txt_unidad_equivalente.Text.Trim()) > 0)
                            {
                                DialogResult respuesta = MessageBox.Show("¿Desea registrar unidad de compra?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                                if (respuesta == DialogResult.Yes)
                                {
                                    registrar_unidad_compra();
                                    listar_unidad_compra_xingrediente();
                                }

                            }
                            else
                            {
                                MessageBox.Show("Equivalencia debe ser mayor a 0.00", "Advertencia");
                            }

                        }
                        else
                        {

                            MessageBox.Show("Valor ingresado no es un número...", "Advertencia");
                        }

                    }
                    else
                    {

                        MessageBox.Show("Por favor complete la información solicitada...", "Información");
                    }
                }
                else
                {
                    if (txt_unidad_equivalente.Text.Length > 0)
                    {
                        if (es_numero(txt_unidad_equivalente.Text.Trim()))
                        {
                            if (decimal.Parse(txt_unidad_equivalente.Text.Trim()) > 0)
                            {
                                DialogResult respuesta = MessageBox.Show("¿Desea actualizar unidad de compra?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                                if (respuesta == DialogResult.Yes)
                                {
                                    actualizar_unidad_compra();
                                    listar_unidad_compra_xingrediente();
                                }

                            }
                            else
                            {
                                MessageBox.Show("Equivalencia debe ser mayor a 0.00", "Advertencia");
                            }

                        }
                        else
                        {

                            MessageBox.Show("Valor ingresado no es un número...", "Advertencia");
                        }

                    }
                    else
                    {

                        MessageBox.Show("Por favor complete la información solicitada...", "Información");
                    }
                }
            }
            catch (Exception)
            {

            }
        }
        /************************Mis Metodos***************************/
        public void listar_unidad_medida_xestado()
        {
            try
            {
                lista_unidadmedida = admunidadme.listar_unidadmedida_xestado();

                if (lista_unidadmedida != null)
                {
                    if (lista_unidadmedida.Count > 0)
                    {
                        cb_unidadmedida.DataSource = lista_unidadmedida;
                        cb_unidadmedida.DisplayMember = "Sigla";
                        cb_unidadmedida.ValueMember = "Unidadmedidaid";
                    }
                }
            }
            catch (Exception) { }

        }


        public void listar_unidad_compra_xingrediente() {

            try
            {
                unidadescompra = admunidadco.listar_unidad_compra_xingrediente(ingrediente);

                if (unidadescompra != null) {

                    if (unidadescompra.Rows.Count > 0) {

                        dg_unidades_compra.DataSource = unidadescompra;
                        dg_unidades_compra.Refresh();
                    }
                }

            }
            catch (Exception) { }

        }

        public bool es_numero(object Expression)
        {
            try
            {
                bool isNum;
                double retNum;
                isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
                return isNum;
            }
            catch (Exception) { return false; }
        }

        public void registrar_unidad_compra()
        {

            int id = -1;
            try
            {

                id = admunidadco.registrar_unidad_compra(
                    new clsUnidadCompra() {
                        Ingrediente=ingrediente,
                        Unidadmedida=lista_unidadmedida[cb_unidadmedida.SelectedIndex],
                        Equivalencia=decimal.Parse(txt_unidad_equivalente.Text),
                        Estado = (cb_estado.Text == "ACTIVO") ? 1 : 0
                    },
                    usureg);

                if (id > 0)
                {

                    MessageBox.Show("Registro correcto...", "Información");
                }
                else
                {

                    MessageBox.Show("Problemas para registrar ingrediente...", "Advertencia");
                }
            }
            catch (Exception)
            {


            }

        }

        public void actualizar_unidad_compra()
        {

            int id = -1;
            try
            {

                id = admunidadco.actualizar_unidad_compra(
                    new clsUnidadCompra()
                    {
                        Idunidadcompra = int.Parse(txt_cod.Text),
                        Ingrediente = ingrediente,
                        Unidadmedida = lista_unidadmedida[cb_unidadmedida.SelectedIndex],
                        Equivalencia = decimal.Parse(txt_unidad_equivalente.Text),
                        Estado = (cb_estado.Text == "ACTIVO") ? 1 : 0
                    },
                    usureg);

                if (id > 0)
                {

                    MessageBox.Show("Actualización correcto...", "Información");
                }
                else
                {

                    MessageBox.Show("Problemas para actualizar ingrediente...", "Advertencia");
                }
            }
            catch (Exception)
            {


            }

        }       

        private void limpiar() {

            try {

                txt_cod.Text = string.Empty;
                txt_unidad_equivalente.Text = "0.00";
                cb_estado.SelectedIndex = 0;
                cb_unidadmedida.SelectedIndex = 0;

            }
            catch (Exception) { }
        }
     
    }
}
