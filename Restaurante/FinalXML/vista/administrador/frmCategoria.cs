﻿using FinalXML.Administradores;
using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalXML.vista.administrador
{
    public partial class frmCategoria : PlantillaBase
    {
        private clsCategoria categoria = null;
        private clsAdmCategoria admcate = new clsAdmCategoria();
        private DataTable categorias = null;
        public clsUsuario usureg { get; set; }
        public frmCategoria()
        {
            InitializeComponent();
        }

        private void frmCategoria_Load(object sender, EventArgs e)
        {
            cb_estado.SelectedIndex = 0;
            dg_categoria.AutoGenerateColumns = false;
        }

        private void txt_nombre_TextChanged(object sender, EventArgs e)
        {
            txt_nombre.CharacterCasing = CharacterCasing.Upper;
        }

        private void txt_descripcion_TextChanged(object sender, EventArgs e)
        {
            txt_descripcion.CharacterCasing = CharacterCasing.Upper;
        }

        private void btn_guardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (txt_cod.Text.Length == 0)
                {
                    if (txt_nombre.Text.Length > 0)
                    {
                        DialogResult respuesta = MessageBox.Show("¿Desea registrar categoria?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (respuesta == DialogResult.Yes)
                        {
                            registrar_categoria();
                            btn_cargar.PerformClick();
                        }
                    }
                    else
                    {

                        MessageBox.Show("Por favor ingrrese la información solicitada...", "Advertencia");
                    }
                }
                else
                {

                    if (txt_nombre.Text.Length > 0)
                    {
                        DialogResult respuesta = MessageBox.Show("¿Desea actualizar categoria?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (respuesta == DialogResult.Yes)
                        {
                            actualizar_categoria();
                            btn_cargar.PerformClick();
                        }
                    }
                    else
                    {

                        MessageBox.Show("Por favor ingrrese la información solicitada...", "Advertencia");
                    }

                }
            }
            catch (Exception) { }
        }

        private void btn_cargar_Click(object sender, EventArgs e)
        {
            try
            {
                listar_categoria();
            }
            catch (Exception) { }
        }

        private void dg_categoria_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dg_categoria.RowCount > 0)
                {
                    if (e.RowIndex != -1)
                    {
                        txt_cod.Text = dg_categoria.Rows[e.RowIndex].Cells[idcategoria.Index].Value.ToString();
                        txt_nombre.Text = dg_categoria.Rows[e.RowIndex].Cells[nombrecategoria.Index].Value.ToString();
                        txt_descripcion.Text = dg_categoria.Rows[e.RowIndex].Cells[descripcion.Index].Value.ToString();
                        cb_estado.SelectedIndex = (dg_categoria.Rows[e.RowIndex].Cells[estado.Index].Value.ToString() == "ACTIVO") ? 0 : 1;
                        chb_cocina.Checked = (dg_categoria.Rows[e.RowIndex].Cells[paracocina.Index].Value.ToString() == "1") ? true : false;

                    }
                }
            }
            catch (Exception) { }
        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            try
            {
                limpiar();
            }
            catch (Exception) { }
        }

        /*****************Mis Metodos***************/

        public void registrar_categoria() {

            try
            {
                categoria = new clsCategoria()
                {
                    Nombre = txt_nombre.Text,
                    Descripcion = txt_descripcion.Text,
                    Paracocina = chb_cocina.Checked ? 1 : 0,
                    Estado = (cb_estado.Text == "ACTIVO") ? 1 : 0

                };

                if (admcate.registrar_categoria(categoria, usureg) > 0)
                {
                    MessageBox.Show("Registro correcto", "Información");

                }
                else
                {

                    MessageBox.Show("Problemas para registrar categoria...", "Advertencia");

                }
            }
            catch (Exception) { }
        }

        public void actualizar_categoria()
        {
            try
            {

                categoria = new clsCategoria()
                {
                    Idcategoria = int.Parse(txt_cod.Text),
                    Nombre = txt_nombre.Text,
                    Descripcion = txt_descripcion.Text,
                    Paracocina = chb_cocina.Checked ? 1 : 0,
                    Estado = (cb_estado.Text == "ACTIVO") ? 1 : 0
                };

                if (admcate.actualizar_categoria(categoria, usureg) > 0)
                {
                    MessageBox.Show("Actualización correcta", "Información");

                }
                else
                {

                    MessageBox.Show("Problemas para actualizar categoria...", "Advertencia");

                }
            }
            catch (Exception) { }
        }

        public void listar_categoria() {

            try
            {
                dg_categoria.DataSource = null;
                categorias = admcate._listar_categoria();

                if (categorias != null)
                {

                    if (categorias.Rows.Count > 0)
                    {

                        dg_categoria.DataSource = categorias;
                        dg_categoria.Refresh();
                    }
                }
            }
            catch (Exception) { }
        }

        public void limpiar() {

            try
            {

                txt_cod.Text = string.Empty;
                cb_estado.SelectedIndex = 0;
                txt_nombre.Text = string.Empty;
                txt_descripcion.Text = string.Empty;
                chb_cocina.Checked = false;
                dg_categoria.DataSource = null;
            }
            catch (Exception) { }
        }
        
    }
}
