﻿using FinalXML.Administradores;
using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalXML.vista.administrador
{
    public partial class frmTipoCuentaBancaria : PlantillaBase
    {
        private clsTipoCuentaBancaria tipocuentabancaria = null;
        private clsAdmTipoCuentaBancaria admtipocuenta = new clsAdmTipoCuentaBancaria();
        public clsUsuario usureg { get; set; }
        private DataTable tabla = null;

        public frmTipoCuentaBancaria()
        {
            InitializeComponent();
        }

        private void frmTipoCuentaBancaria_Load(object sender, EventArgs e)
        {
            cb_estado.SelectedIndex = 0;
            dg_resultado.AutoGenerateColumns = false;
        }

        private void txt_descripcion_TextChanged(object sender, EventArgs e)
        {
            txt_descripcion.CharacterCasing = CharacterCasing.Upper;
        }

        private void btn_guardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (txt_cod.Text.Length == 0)
                {
                    if (txt_descripcion.Text.Length > 0)
                    {
                        DialogResult respuesta = MessageBox.Show("¿Desea registrar tipocuenta?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (respuesta == DialogResult.Yes)
                        {
                            tipocuentabancaria = new clsTipoCuentaBancaria();
                            tipocuentabancaria.Descripcion = txt_descripcion.Text;
                            if (cb_estado.SelectedItem.ToString() == "ACTIVO") { tipocuentabancaria.Estado = 1; } else { tipocuentabancaria.Estado = 0; }


                            if (admtipocuenta.registrar_tipocuenta(tipocuentabancaria, usureg) > 0)
                            {
                                MessageBox.Show("Registro correcto...", "Información");
                                listar_tipocuenta_xdescripcion();

                            }
                            else
                            {

                                MessageBox.Show("Problemas en el registro...", "Error");
                            }


                        }
                    }
                    else
                    {

                        MessageBox.Show("Por favor complete los campos solictados...", "Advertencia");

                    }
                }
                else
                {

                    if (txt_descripcion.Text.Length > 0)
                    {
                        DialogResult respuesta = MessageBox.Show("¿Desea actualizar tipocuenta?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (respuesta == DialogResult.Yes)
                        {
                            tipocuentabancaria = new clsTipoCuentaBancaria();
                            tipocuentabancaria.Idtipocuentabancaria = int.Parse(txt_cod.Text);
                            tipocuentabancaria.Descripcion = txt_descripcion.Text;
                            if (cb_estado.SelectedItem.ToString() == "ACTIVO") { tipocuentabancaria.Estado = 1; } else { tipocuentabancaria.Estado = 0; }

                            if (admtipocuenta.actualizar_tipocuenta(tipocuentabancaria, usureg) > 0)
                            {
                                MessageBox.Show("Actualizar correcta...", "Información");
                                listar_tipocuenta_xdescripcion();
                            }
                            else
                            {

                                MessageBox.Show("Problemas en la actualización...", "Error");
                            }
                        }

                    }
                    else
                    {

                        MessageBox.Show("Por favor complete los campos solictados...", "Advertencia");

                    }

                }
            }
            catch (Exception) { }
        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                if (txt_descripcion.Text.Length > 0)
                {
                    listar_tipocuenta_xdescripcion();
                }
            }
            catch (Exception) { }
        }

        private void btn_cargar_Click(object sender, EventArgs e)
        {
            listar_tipocuenta();
        }

        private void dg_resultado_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dg_resultado.Rows.Count > 0)
                {
                    if (e.RowIndex != -1)
                    {
                        txt_cod.Text = dg_resultado.Rows[e.RowIndex].Cells[0].Value.ToString();
                        txt_descripcion.Text = dg_resultado.Rows[e.RowIndex].Cells[1].Value.ToString();
                        if (dg_resultado.Rows[e.RowIndex].Cells[2].Value.ToString() == "ACTIVO") { cb_estado.SelectedIndex = 0; } else { cb_estado.SelectedIndex = 1; }
                    }
                }
            }
            catch (Exception) { }
        }

        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            try
            {
                limpiar();
            }
            catch (Exception) { }
        }

        /***************************Mis Metodos*******************************/

        public void listar_tipocuenta()
        {
            try
            {
                tabla = admtipocuenta.listar_tipocuenta();
                if (tabla != null)
                {
                    if (tabla.Rows.Count > 0)
                    {
                        dg_resultado.DataSource = tabla;

                    }
                }
            }
            catch (Exception) { }

        }
        public void listar_tipocuenta_xdescripcion()
        {
            try
            {
                tipocuentabancaria = new clsTipoCuentaBancaria();
                tipocuentabancaria.Descripcion = txt_descripcion.Text;
                tabla = admtipocuenta.listar_tipocuenta_xdescripcion(tipocuentabancaria);
                if (tabla != null)
                {
                    if (tabla.Rows.Count > 0)
                    {
                        dg_resultado.DataSource = tabla;

                    }
                }
            }
            catch (Exception) { }
        }
        public void limpiar()
        {
            try
            {
                txt_cod.Text = string.Empty;
                txt_descripcion.Text = string.Empty;
                cb_estado.SelectedIndex = 0;
            }
            catch (Exception) { }
        }

       
    }
}
