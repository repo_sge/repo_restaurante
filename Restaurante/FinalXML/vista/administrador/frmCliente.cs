﻿using AForge;
using AForge.Imaging.Filters;
using FinalXML.Administradores;
using FinalXML.Entidades;
using FinalXML.vista.cajero;
using Ñwinsoft_Ruc;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tesseract;

namespace FinalXML.vista.administrador
{
    public partial class frmCliente : PlantillaBase
    {
        private IntRange red = new IntRange(0, 255);
        private IntRange green = new IntRange(0, 255);
        private IntRange blue = new IntRange(0, 255);
        private _Sunat MyInfoSunat = null;
        private Reniec MyInfoReniec = null; //new Reniec();
        private List<clsTipoDocumentoIndentidad> lista_tipodoc = null;
        private clsAdmTipoDocumentoIdentidad admtipodocidentidad = new clsAdmTipoDocumentoIdentidad();
        private DataTable clientes = null;
        private clsCliente cliente = null;
        private clsAdmCliente admcli = new clsAdmCliente();
        public clsUsuario usureg { get; set; }
        public frmVenta frm_venta { get; set; }
        public frmCliente()
        {
            InitializeComponent();
        }

        private void frmCliente_Load(object sender, EventArgs e)
        {
            dg_cliente.AutoGenerateColumns = false;
            cb_estado.SelectedIndex = 0;
            listar_tipodoc_identidad();
            CargarImagenSunat();
            LeerCaptchaSunat();
            //CargarImagenReniec();
            AplicacionFiltros();
            //LeerCaptchaReniec();
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            if (txt_razonsocial.Text.Length > 0) {

                buscar_clientexnombreyapellido();
            }
        }

        private void txt_doc_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (lista_tipodoc != null)
                {
                    switch (lista_tipodoc[cb_tipodoc.SelectedIndex].Codsunat)
                    {
                        case "1":
                            //valida_dni();
                            break;
                        case "6":
                            valida_ruc();
                            break;
                        case "0":
                            break;
                    }


                }
            }

        }

        private void txt_doc_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }
        private void txt_razonsocial_TextChanged(object sender, EventArgs e)
        {
            txt_razonsocial.CharacterCasing = CharacterCasing.Upper;
        }

        private void txt_direccion_TextChanged(object sender, EventArgs e)
        {
            txt_direccion.CharacterCasing = CharacterCasing.Upper;
        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            limpiar_todo();
        }

        private void btn_cargar_Click(object sender, EventArgs e)
        {
            listar_cliente();
        }

        private void btn_guardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (txt_cod.Text.Length == 0)
                {

                    if (txt_razonsocial.Text.Length > 0
                        && txt_doc.Text.Length > 0
                        && txt_direccion.Text.Length > 0)
                    {
                        DialogResult respuesta = MessageBox.Show("¿Desea registrar cliente?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (respuesta == DialogResult.Yes)
                        {
                            registrar_cliente();
                            buscar_clientexnombreyapellido();
                        }

                    }
                    else
                    {

                        MessageBox.Show("Por favor complete la información solicitada...", "Advertencia");
                    }
                }
                else
                {

                    if (txt_razonsocial.Text.Length > 0
                       && txt_doc.Text.Length > 0
                       && txt_direccion.Text.Length > 0)
                    {
                        DialogResult respuesta = MessageBox.Show("¿Desea actualizar cliente?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (respuesta == DialogResult.Yes)
                        {
                            actualizar_cliente();
                            buscar_clientexnombreyapellido();
                        }

                    }
                    else
                    {

                        MessageBox.Show("Por favor complete la información solicitada...", "Advertencia");
                    }
                }
            }
            catch (Exception) { }

        }

        private void dg_cliente_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dg_cliente.Rows.Count > 0)
                {
                    if (e.RowIndex != -1)
                    {
                        txt_cod.Text = dg_cliente.Rows[e.RowIndex].Cells[idcliente.Index].Value.ToString();
                        cb_estado.SelectedIndex = (dg_cliente.Rows[e.RowIndex].Cells[estado.Index].Value.ToString() == "ACTIVO") ? 0 : 1;
                        txt_razonsocial.Text = dg_cliente.Rows[e.RowIndex].Cells[nombreyapellido.Index].Value.ToString();
                        cb_tipodoc.SelectedValue = (from x in lista_tipodoc
                                                    where x.Descripcion == dg_cliente.Rows[e.RowIndex].Cells[descripcion.Index].Value.ToString()
                                                    select x.Idtipodocumentoidentidad).ToList()[0];
                        txt_doc.Text = dg_cliente.Rows[e.RowIndex].Cells[documento.Index].Value.ToString();
                        txt_direccion.Text = dg_cliente.Rows[e.RowIndex].Cells[direccion.Index].Value.ToString();
                    }
                }
            }
            catch (Exception) { }
        }
        /******************Mis Metodos*********************/
        public void valida_dni()
        {
            try
            {
                try
                {
                    if (txt_doc.Text.Length != 8)
                    {
                        MessageBox.Show("Ingreso Dni no Valido", "Error");
                        txt_doc.SelectAll();
                        txt_doc.Focus();
                        return;
                    }

                    MyInfoReniec.GetInfo(txt_doc.Text, this.txtreniec.Text);

                    switch (MyInfoReniec.GetResul)
                    {
                        case Reniec.Resul.Ok:
                            limpiar();
                            txt_doc.Text = MyInfoReniec.Dni;
                            txt_razonsocial.Text = MyInfoReniec.Nombres + " " + MyInfoReniec.ApePaterno + " " + MyInfoReniec.ApeMaterno;
                            txt_direccion.Text = "-";
                            break;
                        case Reniec.Resul.NoResul:
                            limpiar();
                            MessageBox.Show("No Existe DNI", "Error");
                            break;
                        case Reniec.Resul.ErrorCapcha:
                            limpiar();
                            CargarImagenReniec();
                            break;
                        default:
                            MessageBox.Show("Error Desconocido", "Error");
                            break;
                    }
                    CargarImagenReniec();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void limpiar()
        {           
            txt_razonsocial.Text = string.Empty;
            txt_doc.Text = string.Empty;
            txt_direccion.Text = string.Empty;
        }
        public void valida_ruc()
        {

            try
            {
                try
                {

                    if (txt_doc.Text.Length != 11)
                    {
                        MessageBox.Show("Ingreso Ruc no Valido", "Error");
                        txt_doc.SelectAll();
                        txt_doc.Focus();
                        return;
                    }

                    MyInfoSunat.GetInfo(txt_doc.Text, this.txttexto.Text);

                    switch (MyInfoSunat.GetResul)
                    {
                        case _Sunat.Resul.Ok:
                            limpiar();
                            txt_doc.Text = MyInfoSunat.Ruc.Normalize(NormalizationForm.FormD); 
                            txt_direccion.Text = MyInfoSunat.Direcion.Normalize(NormalizationForm.FormD);
                            txt_direccion.Text = txt_direccion.Text.Replace("<TD>", "");
                            txt_direccion.Text = txt_direccion.Text.Replace("</TD>", "");
                            txt_direccion.Text = txt_direccion.Text.Replace("<TR>", "");
                            txt_direccion.Text = txt_direccion.Text.Replace("</TR>", "");
                            txt_direccion.Text = txt_direccion.Text.Replace("TD", "");
                            txt_direccion.Text = txt_direccion.Text.Replace("TR", "");
                            txt_direccion.Text = txt_direccion.Text.Replace("----", "");
                            if (txt_direccion.Text.Contains("PROFESION U OCUPACION NO ESPECIFICADA")) {

                                txt_direccion.Text = "-";
                            }
                            txt_razonsocial.Text = MyInfoSunat.RazonSocial.Normalize(NormalizationForm.FormD);

                            break;
                        case _Sunat.Resul.NoResul:
                            limpiar();
                            MessageBox.Show("No Existe RUC", "Error");
                            break;
                        case _Sunat.Resul.ErrorCapcha:
                            limpiar();
                            MessageBox.Show("Ingrese imagen correctamente", "Error");
                            break;
                        default:
                            MessageBox.Show("Error Desconocido", "Error");
                            break;
                    }
                    CargarImagenSunat();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void CargarImagenReniec()
        {
            try
            {
                if (MyInfoReniec == null)
                    MyInfoReniec = new Reniec();
                this.pictureBoxDni.Image = MyInfoReniec.GetCapcha;
                AplicacionFiltros();
                LeerCaptchaReniec();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void AplicacionFiltros()
        {
            try
            {
                if (pictureBoxDni.Image != null)
                {
                    Bitmap bmp = new Bitmap(pictureBoxDni.Image);
                    FiltroInvertir(bmp);
                    ColorFiltros();
                    Bitmap bmp1 = new Bitmap(pictureBoxDni.Image);
                    FiltroInvertir(bmp1);
                    Bitmap bmp2 = new Bitmap(pictureBoxDni.Image);
                    FiltroSharpen(bmp2);
                }
            }
            catch (Exception) { }
        }

        private void FiltroSharpen(Bitmap bmp2)
        {
            IFilter Filtro = new Sharpen();
            Bitmap XImage = Filtro.Apply(bmp2);
            pictureBoxDni.Image = XImage;
        }

        private void LeerCaptchaReniec()
        {
            var ENGLISH_LANGUAGE = @"eng";

            if (pictureBoxDni.Image != null)
            {
                using (var engine = new TesseractEngine(@".\tessdata", ENGLISH_LANGUAGE, EngineMode.Default))
                {
                    using (var image = new System.Drawing.Bitmap(pictureBoxDni.Image))
                    {
                        using (var pix = PixConverter.ToPix(image))
                        {
                            using (var page = engine.Process(pix))
                            {
                                var Porcentaje = String.Format("{0:P}", page.GetMeanConfidence());
                                string CaptchaTexto = page.GetText();
                                char[] eliminarChars = { '\n', ' ' };
                                CaptchaTexto = CaptchaTexto.TrimEnd(eliminarChars);
                                CaptchaTexto = CaptchaTexto.Replace(" ", string.Empty);
                                CaptchaTexto = Regex.Replace(CaptchaTexto, "[^a-zA-Z0-9]+", string.Empty);
                                if (CaptchaTexto != string.Empty & CaptchaTexto.Length == 4)
                                    txtreniec.Text = CaptchaTexto.ToUpper();
                                else
                                    CargarImagenReniec();
                            }
                        }
                    }

                }
            }
        }

        private void ColorFiltros()
        {
            try
            {
                red.Min = Math.Min(red.Max, byte.Parse("229"));
                red.Max = Math.Max(red.Min, byte.Parse("255"));
                green.Min = Math.Min(green.Max, byte.Parse("0"));
                green.Max = Math.Max(green.Min, byte.Parse("255"));
                blue.Min = Math.Min(blue.Max, byte.Parse("0"));
                blue.Max = Math.Max(blue.Min, byte.Parse("130"));
                ActualizarFiltro();
            }
            catch (Exception) { }
        }

        private void FiltroInvertir(Bitmap bmp)
        {
            try
            {
                IFilter Filtro = new Invert();
                Bitmap XImage = Filtro.Apply(bmp);
                pictureBoxDni.Image = XImage;
            }
            catch (Exception) { }
        }

        private void ActualizarFiltro()
        {
            try
            {
                ColorFiltering FiltroColor = new ColorFiltering();
                FiltroColor.Red = red;
                FiltroColor.Green = green;
                FiltroColor.Blue = blue;
                IFilter Filtro = FiltroColor;
                Bitmap bmp = new Bitmap(pictureBoxDni.Image);
                Bitmap XImage = Filtro.Apply(bmp);
                pictureBoxDni.Image = XImage;
            }
            catch (Exception) { }
        }

        private void CargarImagenSunat()
        {
            try
            {
                if (MyInfoSunat == null)
                    MyInfoSunat = new _Sunat();
                this.pictureCapcha.Image = MyInfoSunat.GetCapcha;
                LeerCaptchaSunat();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void LeerCaptchaSunat()
        {
            var ENGLISH_LANGUAGE = @"eng";

            if (pictureCapcha.Image != null)
            {
                using (var engine = new TesseractEngine(@".\tessdata", ENGLISH_LANGUAGE, EngineMode.Default))
                {
                    using (var image = new System.Drawing.Bitmap(pictureCapcha.Image))
                    {
                        using (var pix = PixConverter.ToPix(image))
                        {
                            using (var page = engine.Process(pix))
                            {
                                var Porcentaje = String.Format("{0:P}", page.GetMeanConfidence());
                                string CaptchaTexto = page.GetText();
                                char[] eliminarChars = { '\n', ' ' };
                                CaptchaTexto = CaptchaTexto.TrimEnd(eliminarChars);
                                CaptchaTexto = CaptchaTexto.Replace(" ", string.Empty);
                                CaptchaTexto = Regex.Replace(CaptchaTexto, "[^a-zA-Z]+", string.Empty);
                                if (CaptchaTexto != string.Empty & CaptchaTexto.Length == 4)
                                    txttexto.Text = CaptchaTexto.ToUpper();
                                else
                                    CargarImagenSunat();
                            }
                        }
                    }
                }
            }
        }

        public void listar_tipodoc_identidad() {

            try
            {
                lista_tipodoc = admtipodocidentidad.listar_tipo_documento_identidad();

                if (lista_tipodoc != null)
                {

                    if (lista_tipodoc.Count > 0)
                    {

                        cb_tipodoc.DataSource = lista_tipodoc;
                        cb_tipodoc.DisplayMember = "Descripcion";
                        cb_tipodoc.ValueMember = "Idtipodocumentoidentidad";
                        cb_tipodoc.SelectedIndex = 0;
                    }

                }
            }
            catch (Exception) { }
        }

        public void limpiar_todo() {

            try
            {
                txt_cod.Text = string.Empty;
                cb_estado.SelectedIndex = 0;
                txt_razonsocial.Text = string.Empty;
                cb_tipodoc.SelectedIndex = 0;
                txt_doc.Text = string.Empty;
                txt_direccion.Text = string.Empty;
                dg_cliente.DataSource = null;
            }
            catch (Exception) { }
        }

        public void listar_cliente() {

            try
            {
                dg_cliente.DataSource = null;
                clientes = admcli.listar_cliente();
                if (clientes != null)
                {

                    if (clientes.Rows.Count > 0)
                    {

                        dg_cliente.DataSource = clientes;
                        dg_cliente.Refresh();
                    }
                }
            }
            catch (Exception) { }
        }

        public void buscar_clientexnombreyapellido()
        {
            try
            {
                dg_cliente.DataSource = null;
                clientes = admcli.buscar_clientexnombreyapellido(new clsCliente()
                {
                    Razonsocial = txt_razonsocial.Text
                });
                if (clientes != null)
                {
                    if (clientes.Rows.Count > 0)
                    {

                        dg_cliente.DataSource = clientes;
                        dg_cliente.Refresh();
                    }
                }
            }
            catch (Exception) { }
        }

        public void registrar_cliente() {

            try
            {
                if (lista_tipodoc == null)
                {
                    MessageBox.Show("No se ha podido cargar los tipos de documento...", "Advertencia");
                    return;
                }

                cliente = new clsCliente()
                {
                    Razonsocial = txt_razonsocial.Text,
                    Tipodocidentidad = lista_tipodoc[cb_tipodoc.SelectedIndex],
                    Documento = txt_doc.Text,
                    Direccion = txt_direccion.Text,
                    Estado = (cb_estado.SelectedItem.ToString() == "ACTIVO") ? 1 : 0
                };

                if (admcli.registrar_cliente(cliente, usureg) > 0)
                {
                    MessageBox.Show("Registro correcto...", "Información");
                }
                else
                {
                    MessageBox.Show("Problemas para registrar cliente...", "Advertencia");
                }
            }
            catch (Exception) { }
        }

        public void actualizar_cliente()
        {
            try
            {
                if (lista_tipodoc == null)
                {
                    MessageBox.Show("No se ha poido cargar los tipos de documento...", "Advertencia");
                    return;
                }

                cliente = new clsCliente()
                {
                    Idcliente = int.Parse(txt_cod.Text),
                    Razonsocial = txt_razonsocial.Text,
                    Tipodocidentidad = lista_tipodoc[cb_tipodoc.SelectedIndex],
                    Documento = txt_doc.Text,
                    Direccion = txt_direccion.Text,
                    Estado = (cb_estado.SelectedItem.ToString() == "ACTIVO") ? 1 : 0
                };

                if (admcli.actualizar_cliente(cliente, usureg) > 0)
                {
                    MessageBox.Show("Actualización correcta...", "Información");
                }
                else
                {
                    MessageBox.Show("Problemas para actulizar cliente...", "Advertencia");
                }
            }
            catch (Exception) { }
        
        }

        private void dg_cliente_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dg_cliente.Rows.Count > 0)
                {

                    if (e.RowIndex != -1)
                    {

                        if (frm_venta != null)
                        {

                            frm_venta.cliente = new clsCliente()
                            {

                                Documento = dg_cliente.Rows[e.RowIndex].Cells[documento.Index].Value.ToString()
                            };
                            this.Close();
                        }
                    }

                }
            }
            catch (Exception) { }
        }
    }
}
