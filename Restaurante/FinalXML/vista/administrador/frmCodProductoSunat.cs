﻿using FinalXML.Administradores;
using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalXML.vista.administrador
{
    public partial class frmCodProductoSunat : PlantillaBase
    {

        clsAdmProducto admprod = new clsAdmProducto();
        public clsUsuario usureg { get; set; }
        public frmProducto frm_producto { get; set; }
        public String nombreprod { get; set; }
        DataTable codigosproducto = null;

        public frmCodProductoSunat()
        {
            InitializeComponent();
           
        }

        private void frmCodProductoSunat_Load(object sender, EventArgs e)
        {
            cargaLista();
            nombreprod = "";
            dgvCodproductosunat.AutoGenerateColumns = false;
        }

        private void cargaLista()
        {

            try
            {
                nombreprod = txtDescprod.Text;
                codigosproducto = null;
                dgvCodproductosunat.DataSource = null;
                
                codigosproducto = admprod.listar_codproductosunatxdesc(nombreprod);
                if (codigosproducto != null)
                {
                    if (codigosproducto.Rows.Count > 0)
                    {
                        dgvCodproductosunat.AutoGenerateColumns = false;
                        dgvCodproductosunat.DataSource = codigosproducto;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }

        }

        private void textBoxX2_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {

                    if (txtDescprod.Text.Length > 0)
                    {
                        nombreprod = txtDescprod.Text;
                        cargaLista();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
            
        }

        private void dgvCodproductosunat_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgvCodproductosunat.Rows.Count > 0)
                {

                    if (e.RowIndex != -1)
                    {

                        if (frm_producto != null)
                        {

                            frm_producto.codsunatproducto = dgvCodproductosunat.Rows[e.RowIndex].Cells[codsunat.Index].Value.ToString();

                        }
                        this.Close();
                    }
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message.ToString());
            }
        }

        private void textBoxX2_TextChanged(object sender, EventArgs e)
        {

            cargaLista();
        }
    }
}
