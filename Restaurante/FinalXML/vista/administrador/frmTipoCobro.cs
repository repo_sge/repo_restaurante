﻿using FinalXML.Administradores;
using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalXML.vista.administrador
{
    public partial class frmTipoCobro : PlantillaBase
    {
        public clsUsuario usureg { get; set; }
        private clsTipoCobro tipocobro = null;
        private clsAdmTipoCobro admtico = new clsAdmTipoCobro();
        private DataTable tabla = null;
        public frmTipoCobro()
        {
            InitializeComponent();
        }

        private void frmTipoCobro_Load(object sender, EventArgs e)
        {
            dg_resultado.AutoGenerateColumns = false;
            cb_estado.SelectedIndex = 0;
        }

        private void txt_sigla_TextChanged(object sender, EventArgs e)
        {
            txt_sigla.CharacterCasing = CharacterCasing.Upper;
        }

        private void btn_guardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (txt_cod.Text.Length == 0)
                {
                    if (txt_nombre.Text.Length > 0 && txt_sigla.Text.Length > 0)
                    {
                        DialogResult respuesta = MessageBox.Show("¿Desea registrar tipo cobro?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (respuesta == DialogResult.Yes)
                        {
                            tipocobro = new clsTipoCobro();
                            tipocobro.Nombre = txt_nombre.Text;
                            tipocobro.Sigla = txt_sigla.Text;
                            if (cb_estado.SelectedItem.ToString() == "ACTIVO") { tipocobro.Estado = 1; } else { tipocobro.Estado = 0; }

                            if (admtico.existe_sigla(tipocobro) == 0)
                            {
                                if (admtico.registrar_tipo_cobro(tipocobro, usureg) > 0)
                                {
                                    MessageBox.Show("Registro correcto...", "Información");
                                    listar_tipocobro_xnombre();

                                }
                                else
                                {

                                    MessageBox.Show("Problemas en el registro...", "Error");
                                }


                            }
                            else
                            {
                                MessageBox.Show("Sigla ya registrada...", "Advertencia");
                            }
                        }
                    }
                    else
                    {

                        MessageBox.Show("Por favor complete los campos solictados...", "Advertencia");

                    }
                }
                else
                {


                    if (txt_nombre.Text.Length > 0 && txt_sigla.Text.Length > 0)
                    {
                        DialogResult respuesta = MessageBox.Show("¿Desea actualizar tipo cobro?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (respuesta == DialogResult.Yes)
                        {
                            tipocobro = new clsTipoCobro();
                            tipocobro.Idtipocobro = int.Parse(txt_cod.Text);
                            tipocobro.Nombre = txt_nombre.Text;
                            tipocobro.Sigla = txt_sigla.Text;
                            if (cb_estado.SelectedItem.ToString() == "ACTIVO") { tipocobro.Estado = 1; } else { tipocobro.Estado = 0; }

                            if (admtico.actualizar_tipo_cobro(tipocobro, usureg) > 0)
                            {
                                MessageBox.Show("Actualizar correcta...", "Información");
                                listar_tipocobro_xnombre();
                            }
                            else
                            {

                                MessageBox.Show("Problemas en la actualización...", "Error");
                            }
                        }
                    }
                    else
                    {

                        MessageBox.Show("Por favor complete los campos solictados...", "Advertencia");

                    }

                }
            }
            catch (Exception) { }
        }
        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            try
            {
                limpiar();
            }
            catch (Exception) { }
        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                if (txt_nombre.Text.Length > 0)
                {

                    listar_tipocobro_xnombre();
                }
            }
            catch (Exception) { }
        }
        private void btn_cargar_Click(object sender, EventArgs e)
        {
            try
            {
                listar_tipocobro();
            }
            catch (Exception) { }
        }

        private void dg_resultado_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dg_resultado.Rows.Count > 0)
                {

                    if (e.RowIndex != -1)
                    {
                        txt_cod.Text = dg_resultado.Rows[e.RowIndex].Cells[0].Value.ToString();
                        txt_nombre.Text = dg_resultado.Rows[e.RowIndex].Cells[1].Value.ToString();
                        txt_sigla.Text = dg_resultado.Rows[e.RowIndex].Cells[2].Value.ToString();
                        if (dg_resultado.Rows[e.RowIndex].Cells[3].Value.ToString() == "ACTIVO") { cb_estado.SelectedIndex = 0; } else { cb_estado.SelectedIndex = 1; }
                    }
                }
            }
            catch (Exception) { }
        }

        private void txt_nombre_TextChanged(object sender, EventArgs e)
        {
            txt_nombre.CharacterCasing = CharacterCasing.Upper;
        }

        /***************************Mis Metodos***************************/
        public void listar_tipocobro_xnombre()
        {
            try
            {
                tipocobro = new clsTipoCobro();
                tipocobro.Nombre = txt_nombre.Text;
                tabla = admtico.listar_tipocobro_xnombre(tipocobro);
                if (tabla != null)
                {
                    if (tabla.Rows.Count > 0)
                    {
                        dg_resultado.DataSource = tabla;

                    }
                }
            }
            catch (Exception) { }

        }
        public void limpiar()
        {
            try
            {
                txt_cod.Text = string.Empty;
                txt_nombre.Text = string.Empty;
                txt_sigla.Text = string.Empty;
                cb_estado.SelectedIndex = 0;
                dg_resultado.DataSource = null;
            }
            catch (Exception) { }

        }

        public void listar_tipocobro()
        {
            try
            {
                tabla = admtico.listar_tipocobro();
                if (tabla != null)
                {
                    if (tabla.Rows.Count > 0)
                    {
                        dg_resultado.DataSource = tabla;
                    }
                }
            }
            catch (Exception) { }
        }
       
    }
}
