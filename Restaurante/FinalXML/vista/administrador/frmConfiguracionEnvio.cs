﻿using FinalXML.Administradores;
using FinalXML.controlador;
using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalXML.vista.administrador
{
    public partial class frmConfiguracionEnvio : PlantillaBase
    {
        public clsUsuario usureg { get; set; }
        private CclsConfiguracionEnvio cconfig = new CclsConfiguracionEnvio();
        private clsConfiguracionEnvio config = null;
        public frmConfiguracionEnvio()
        {
            InitializeComponent();
        }

        private void frmConfiguracionEnvio_Load(object sender, EventArgs e)
        {
            listar_configuracionenvio(); 
        }

        private void switch_envio_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                actualizar_configuracionenvio();

            }
            catch (Exception) { }
        }

        /******************Mis Metodos*******************************/

        public void listar_configuracionenvio()
        {
            try
            {
                int id = -1;
                config = cconfig.listar_configuracionenvio();

                if (config == null)
                {

                    config = new clsConfiguracionEnvio()
                    {
                        Estadoenvio = 0,
                        Pcorigen = SystemInformation.UserDomainName,
                        Usuariopc = SystemInformation.UserName
                    };

                    id = cconfig.registrar_configuracionenvio(config);

                    if (id > 0)
                    {

                        config.Configuracionid = id;
                        switch_envio.Value = false;
                    }
                    else
                    {

                        MessageBox.Show("Problemas para registrar configuración...", "Advertencia");
                    }
                }
                else
                {

                    if (config.Estadoenvio == 0)
                    {
                        switch_envio.Value = false;
                    }
                    else
                    {

                        switch_envio.Value = true;

                    }
                }
            }
            catch (Exception) { }
        }
        public void actualizar_configuracionenvio()
        {
            try
            {
                if (config != null)
                {
                    if (switch_envio.Value)
                    {
                        config.Estadoenvio = 1;
                        config.Pcorigen = SystemInformation.UserDomainName;
                        config.Usuariopc = SystemInformation.UserName;
                    }
                    else
                    {
                        config.Estadoenvio = 0;
                        config.Pcorigen = SystemInformation.UserDomainName;
                        config.Usuariopc = SystemInformation.UserName;
                    }

                    if (cconfig.actualizar_configuracionenvio(config, usureg) < 0)
                    {
                        MessageBox.Show("Problemas para cambiar configuración..");
                    }

                }
            }
            catch (Exception) { }
        }
    }
}
