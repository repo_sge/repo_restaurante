﻿namespace FinalXML.vista.administrador
{
    partial class frmPrincipalAdministrador
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrincipalAdministrador));
            this.itemContainer1 = new DevComponents.DotNetBar.ItemContainer();
            this.itemContainer2 = new DevComponents.DotNetBar.ItemContainer();
            this.itemContainer3 = new DevComponents.DotNetBar.ItemContainer();
            this.buttonItem2 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem3 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem4 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem5 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem6 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem7 = new DevComponents.DotNetBar.ButtonItem();
            this.galleryContainer1 = new DevComponents.DotNetBar.GalleryContainer();
            this.labelItem8 = new DevComponents.DotNetBar.LabelItem();
            this.buttonItem8 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem9 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem10 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem11 = new DevComponents.DotNetBar.ButtonItem();
            this.itemContainer4 = new DevComponents.DotNetBar.ItemContainer();
            this.buttonItem12 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem13 = new DevComponents.DotNetBar.ButtonItem();
            this.applicationButton1 = new DevComponents.DotNetBar.ApplicationButton();
            this.ribbonPanel2 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonControl1 = new DevComponents.DotNetBar.RibbonControl();
            this.ribbonPanel3 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonPanel1 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar1 = new DevComponents.DotNetBar.RibbonBar();
            this.btn_item_serie = new DevComponents.DotNetBar.ButtonItem();
            this.btn_item_configuracion_envio = new DevComponents.DotNetBar.ButtonItem();
            this.btn_item_usuario = new DevComponents.DotNetBar.ButtonItem();
            this.btn_item_cliente = new DevComponents.DotNetBar.ButtonItem();
            this.btn_prod_bebi_pla = new DevComponents.DotNetBar.ButtonItem();
            this.btn_item_categoria = new DevComponents.DotNetBar.ButtonItem();
            this.btn_item_piso = new DevComponents.DotNetBar.ButtonItem();
            this.btn_item_mesa = new DevComponents.DotNetBar.ButtonItem();
            this.btn_item_pedido = new DevComponents.DotNetBar.ButtonItem();
            this.btn_item_lista_ticket = new DevComponents.DotNetBar.ButtonItem();
            this.btn_item_lista_ticket_nofacturado = new DevComponents.DotNetBar.ButtonItem();
            this.btn_item_historial_ticket = new DevComponents.DotNetBar.ButtonItem();
            this.btn_item_historial_pedido = new DevComponents.DotNetBar.ButtonItem();
            this.btn_item_ordencocina = new DevComponents.DotNetBar.ButtonItem();
            this.btn_item_caja = new DevComponents.DotNetBar.ButtonItem();
            this.btn_item_tipocambio = new DevComponents.DotNetBar.ButtonItem();
            this.btn_item_apertura = new DevComponents.DotNetBar.ButtonItem();
            this.btn_item_cajamovimiento = new DevComponents.DotNetBar.ButtonItem();
            this.btn_item_caja_abierta = new DevComponents.DotNetBar.ButtonItem();
            this.btn_item_cajas_cerradas = new DevComponents.DotNetBar.ButtonItem();
            this.btn_item_cajahistorica = new DevComponents.DotNetBar.ButtonItem();
            this.btn_item_cuentaydemas = new DevComponents.DotNetBar.ButtonItem();
            this.btn_item_banco = new DevComponents.DotNetBar.ButtonItem();
            this.btn_item_cuentabancaria = new DevComponents.DotNetBar.ButtonItem();
            this.btn_item_tipocuenta_bancaria = new DevComponents.DotNetBar.ButtonItem();
            this.btn_item_tarjeta = new DevComponents.DotNetBar.ButtonItem();
            this.btn_item_tipo_cobro = new DevComponents.DotNetBar.ButtonItem();
            this.btn_item_venta = new DevComponents.DotNetBar.ButtonItem();
            this.btn_item_lista_venta = new DevComponents.DotNetBar.ButtonItem();
            this.btn_lista_nota_credito_debito = new DevComponents.DotNetBar.ButtonItem();
            this.btn_item_repositorio = new DevComponents.DotNetBar.ButtonItem();
            this.btn_item_ventas_xmesa_mozo = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel4 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar2 = new DevComponents.DotNetBar.RibbonBar();
            this.btn_item_ingrediente = new DevComponents.DotNetBar.ButtonItem();
            this.btn_item_unidad_medida = new DevComponents.DotNetBar.ButtonItem();
            this.btn_item_proveedor = new DevComponents.DotNetBar.ButtonItem();
            this.lb_tipo_cambio = new DevComponents.DotNetBar.LabelItem();
            this.Ventas = new DevComponents.DotNetBar.RibbonTabItem();
            this.ribbonTabItem3 = new DevComponents.DotNetBar.RibbonTabItem();
            this.galleryContainer2 = new DevComponents.DotNetBar.GalleryContainer();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.qatCustomizeItem1 = new DevComponents.DotNetBar.QatCustomizeItem();
            this.ribbonTabItem2 = new DevComponents.DotNetBar.RibbonTabItem();
            this.ribbonTabItem1 = new DevComponents.DotNetBar.RibbonTabItem();
            this.styleManager1 = new DevComponents.DotNetBar.StyleManager(this.components);
            this.ribbonControl1.SuspendLayout();
            this.ribbonPanel3.SuspendLayout();
            this.ribbonPanel1.SuspendLayout();
            this.ribbonPanel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // itemContainer1
            // 
            // 
            // 
            // 
            this.itemContainer1.BackgroundStyle.Class = "RibbonFileMenuContainer";
            this.itemContainer1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer1.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer1.Name = "itemContainer1";
            this.itemContainer1.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer2,
            this.itemContainer4});
            // 
            // 
            // 
            this.itemContainer1.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer2
            // 
            // 
            // 
            // 
            this.itemContainer2.BackgroundStyle.Class = "RibbonFileMenuTwoColumnContainer";
            this.itemContainer2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer2.ItemSpacing = 0;
            this.itemContainer2.Name = "itemContainer2";
            this.itemContainer2.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer3,
            this.galleryContainer1});
            // 
            // 
            // 
            this.itemContainer2.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer3
            // 
            // 
            // 
            // 
            this.itemContainer3.BackgroundStyle.Class = "RibbonFileMenuColumnOneContainer";
            this.itemContainer3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer3.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer3.MinimumSize = new System.Drawing.Size(120, 0);
            this.itemContainer3.Name = "itemContainer3";
            this.itemContainer3.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem2,
            this.buttonItem3,
            this.buttonItem4,
            this.buttonItem5,
            this.buttonItem6,
            this.buttonItem7});
            // 
            // 
            // 
            this.itemContainer3.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem2
            // 
            this.buttonItem2.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem2.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem2.Image")));
            this.buttonItem2.Name = "buttonItem2";
            this.buttonItem2.SubItemsExpandWidth = 24;
            this.buttonItem2.Text = "&New";
            // 
            // buttonItem3
            // 
            this.buttonItem3.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem3.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem3.Image")));
            this.buttonItem3.Name = "buttonItem3";
            this.buttonItem3.SubItemsExpandWidth = 24;
            this.buttonItem3.Text = "&Open...";
            // 
            // buttonItem4
            // 
            this.buttonItem4.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem4.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem4.Image")));
            this.buttonItem4.Name = "buttonItem4";
            this.buttonItem4.SubItemsExpandWidth = 24;
            this.buttonItem4.Text = "&Save...";
            // 
            // buttonItem5
            // 
            this.buttonItem5.BeginGroup = true;
            this.buttonItem5.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem5.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem5.Image")));
            this.buttonItem5.Name = "buttonItem5";
            this.buttonItem5.SubItemsExpandWidth = 24;
            this.buttonItem5.Text = "S&hare...";
            // 
            // buttonItem6
            // 
            this.buttonItem6.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem6.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem6.Image")));
            this.buttonItem6.Name = "buttonItem6";
            this.buttonItem6.SubItemsExpandWidth = 24;
            this.buttonItem6.Text = "&Print...";
            // 
            // buttonItem7
            // 
            this.buttonItem7.BeginGroup = true;
            this.buttonItem7.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem7.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem7.Image")));
            this.buttonItem7.Name = "buttonItem7";
            this.buttonItem7.SubItemsExpandWidth = 24;
            this.buttonItem7.Text = "&Close";
            // 
            // galleryContainer1
            // 
            // 
            // 
            // 
            this.galleryContainer1.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer1.EnableGalleryPopup = false;
            this.galleryContainer1.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer1.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer1.MultiLine = false;
            this.galleryContainer1.Name = "galleryContainer1";
            this.galleryContainer1.PopupUsesStandardScrollbars = false;
            this.galleryContainer1.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem8,
            this.buttonItem8,
            this.buttonItem9,
            this.buttonItem10,
            this.buttonItem11});
            // 
            // 
            // 
            this.galleryContainer1.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // labelItem8
            // 
            this.labelItem8.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom;
            this.labelItem8.BorderType = DevComponents.DotNetBar.eBorderType.Etched;
            this.labelItem8.CanCustomize = false;
            this.labelItem8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelItem8.Name = "labelItem8";
            this.labelItem8.PaddingBottom = 2;
            this.labelItem8.PaddingTop = 2;
            this.labelItem8.Stretch = true;
            this.labelItem8.Text = "Recent Documents";
            // 
            // buttonItem8
            // 
            this.buttonItem8.Name = "buttonItem8";
            this.buttonItem8.Text = "&1. Short News 5-7.rtf";
            // 
            // buttonItem9
            // 
            this.buttonItem9.Name = "buttonItem9";
            this.buttonItem9.Text = "&2. Prospect Email.rtf";
            // 
            // buttonItem10
            // 
            this.buttonItem10.Name = "buttonItem10";
            this.buttonItem10.Text = "&3. Customer Email.rtf";
            // 
            // buttonItem11
            // 
            this.buttonItem11.Name = "buttonItem11";
            this.buttonItem11.Text = "&4. example.rtf";
            // 
            // itemContainer4
            // 
            // 
            // 
            // 
            this.itemContainer4.BackgroundStyle.Class = "RibbonFileMenuBottomContainer";
            this.itemContainer4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer4.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Right;
            this.itemContainer4.Name = "itemContainer4";
            this.itemContainer4.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem12,
            this.buttonItem13});
            // 
            // 
            // 
            this.itemContainer4.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem12
            // 
            this.buttonItem12.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem12.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonItem12.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem12.Image")));
            this.buttonItem12.Name = "buttonItem12";
            this.buttonItem12.SubItemsExpandWidth = 24;
            this.buttonItem12.Text = "Opt&ions";
            // 
            // buttonItem13
            // 
            this.buttonItem13.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem13.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonItem13.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem13.Image")));
            this.buttonItem13.Name = "buttonItem13";
            this.buttonItem13.SubItemsExpandWidth = 24;
            this.buttonItem13.Text = "E&xit";
            // 
            // applicationButton1
            // 
            this.applicationButton1.AutoExpandOnClick = true;
            this.applicationButton1.CanCustomize = false;
            this.applicationButton1.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Image;
            this.applicationButton1.Image = ((System.Drawing.Image)(resources.GetObject("applicationButton1.Image")));
            this.applicationButton1.ImageFixedSize = new System.Drawing.Size(16, 16);
            this.applicationButton1.ImagePaddingHorizontal = 0;
            this.applicationButton1.ImagePaddingVertical = 0;
            this.applicationButton1.Name = "applicationButton1";
            this.applicationButton1.ShowSubItems = false;
            this.applicationButton1.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer1});
            this.applicationButton1.Text = "&File";
            // 
            // ribbonPanel2
            // 
            this.ribbonPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel2.Location = new System.Drawing.Point(0, 0);
            this.ribbonPanel2.Name = "ribbonPanel2";
            this.ribbonPanel2.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel2.Size = new System.Drawing.Size(913, 178);
            // 
            // 
            // 
            this.ribbonPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel2.TabIndex = 2;
            this.ribbonPanel2.Visible = false;
            // 
            // ribbonControl1
            // 
            // 
            // 
            // 
            this.ribbonControl1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonControl1.CaptionVisible = true;
            this.ribbonControl1.Controls.Add(this.ribbonPanel3);
            this.ribbonControl1.Controls.Add(this.ribbonPanel4);
            this.ribbonControl1.Controls.Add(this.ribbonPanel2);
            this.ribbonControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ribbonControl1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.lb_tipo_cambio,
            this.Ventas,
            this.ribbonTabItem3});
            this.ribbonControl1.KeyTipsFont = new System.Drawing.Font("Tahoma", 7F);
            this.ribbonControl1.Location = new System.Drawing.Point(5, 1);
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Padding = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.ribbonControl1.Size = new System.Drawing.Size(913, 181);
            this.ribbonControl1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonControl1.SystemText.MaximizeRibbonText = "&Maximize the Ribbon";
            this.ribbonControl1.SystemText.MinimizeRibbonText = "Mi&nimize the Ribbon";
            this.ribbonControl1.SystemText.QatAddItemText = "&Add to Quick Access Toolbar";
            this.ribbonControl1.SystemText.QatCustomizeMenuLabel = "<b>Customize Quick Access Toolbar</b>";
            this.ribbonControl1.SystemText.QatCustomizeText = "&Customize Quick Access Toolbar...";
            this.ribbonControl1.SystemText.QatDialogAddButton = "&Add >>";
            this.ribbonControl1.SystemText.QatDialogCancelButton = "Cancel";
            this.ribbonControl1.SystemText.QatDialogCaption = "Customize Quick Access Toolbar";
            this.ribbonControl1.SystemText.QatDialogCategoriesLabel = "&Choose commands from:";
            this.ribbonControl1.SystemText.QatDialogOkButton = "OK";
            this.ribbonControl1.SystemText.QatDialogPlacementCheckbox = "&Place Quick Access Toolbar below the Ribbon";
            this.ribbonControl1.SystemText.QatDialogRemoveButton = "&Remove";
            this.ribbonControl1.SystemText.QatPlaceAboveRibbonText = "&Place Quick Access Toolbar above the Ribbon";
            this.ribbonControl1.SystemText.QatPlaceBelowRibbonText = "&Place Quick Access Toolbar below the Ribbon";
            this.ribbonControl1.SystemText.QatRemoveItemText = "&Remove from Quick Access Toolbar";
            this.ribbonControl1.TabGroupHeight = 14;
            this.ribbonControl1.TabIndex = 1;
            this.ribbonControl1.Text = "ribbonControl1";
            this.ribbonControl1.Click += new System.EventHandler(this.ribbonControl1_Click);
            // 
            // ribbonPanel3
            // 
            this.ribbonPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonPanel3.Controls.Add(this.ribbonPanel1);
            this.ribbonPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel3.Location = new System.Drawing.Point(0, 53);
            this.ribbonPanel3.Name = "ribbonPanel3";
            this.ribbonPanel3.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel3.Size = new System.Drawing.Size(913, 125);
            // 
            // 
            // 
            this.ribbonPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel3.TabIndex = 3;
            // 
            // ribbonPanel1
            // 
            this.ribbonPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonPanel1.Controls.Add(this.ribbonBar1);
            this.ribbonPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel1.Location = new System.Drawing.Point(3, 0);
            this.ribbonPanel1.Name = "ribbonPanel1";
            this.ribbonPanel1.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel1.Size = new System.Drawing.Size(907, 122);
            // 
            // 
            // 
            this.ribbonPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel1.TabIndex = 2;
            // 
            // ribbonBar1
            // 
            this.ribbonBar1.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar1.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar1.ContainerControlProcessDialogKey = true;
            this.ribbonBar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar1.DragDropSupport = true;
            this.ribbonBar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btn_item_serie,
            this.btn_item_usuario,
            this.btn_item_cliente,
            this.btn_prod_bebi_pla,
            this.btn_item_piso,
            this.btn_item_mesa,
            this.btn_item_pedido,
            this.btn_item_historial_pedido,
            this.btn_item_ordencocina,
            this.btn_item_caja,
            this.btn_item_venta});
            this.ribbonBar1.Location = new System.Drawing.Point(3, 0);
            this.ribbonBar1.Name = "ribbonBar1";
            this.ribbonBar1.Size = new System.Drawing.Size(907, 119);
            this.ribbonBar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonBar1.TabIndex = 0;
            // 
            // 
            // 
            this.ribbonBar1.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar1.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // btn_item_serie
            // 
            this.btn_item_serie.Image = global::FinalXML.Properties.Resources.serie;
            this.btn_item_serie.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btn_item_serie.Name = "btn_item_serie";
            this.btn_item_serie.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btn_item_configuracion_envio});
            this.btn_item_serie.SubItemsExpandWidth = 14;
            this.btn_item_serie.Text = "Serie de Facturación";
            this.btn_item_serie.Click += new System.EventHandler(this.btn_item_serie_Click);
            // 
            // btn_item_configuracion_envio
            // 
            this.btn_item_configuracion_envio.Name = "btn_item_configuracion_envio";
            this.btn_item_configuracion_envio.Text = "Configuración de Envio";
            this.btn_item_configuracion_envio.Click += new System.EventHandler(this.btn_item_configuracion_envio_Click);
            // 
            // btn_item_usuario
            // 
            this.btn_item_usuario.Image = global::FinalXML.Properties.Resources.usuario;
            this.btn_item_usuario.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btn_item_usuario.Name = "btn_item_usuario";
            this.btn_item_usuario.SubItemsExpandWidth = 14;
            this.btn_item_usuario.Text = "Usuarios";
            this.btn_item_usuario.Click += new System.EventHandler(this.Usuarios_Click);
            // 
            // btn_item_cliente
            // 
            this.btn_item_cliente.Image = global::FinalXML.Properties.Resources.cliente;
            this.btn_item_cliente.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btn_item_cliente.Name = "btn_item_cliente";
            this.btn_item_cliente.SubItemsExpandWidth = 14;
            this.btn_item_cliente.Text = "Clientes";
            this.btn_item_cliente.Click += new System.EventHandler(this.btn_item_cliente_Click);
            // 
            // btn_prod_bebi_pla
            // 
            this.btn_prod_bebi_pla.Image = global::FinalXML.Properties.Resources.productos;
            this.btn_prod_bebi_pla.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btn_prod_bebi_pla.Name = "btn_prod_bebi_pla";
            this.btn_prod_bebi_pla.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btn_item_categoria});
            this.btn_prod_bebi_pla.SubItemsExpandWidth = 14;
            this.btn_prod_bebi_pla.Text = "Productos, Platos y Bebidas";
            this.btn_prod_bebi_pla.Click += new System.EventHandler(this.btn_prod_bebi_pla_Click);
            // 
            // btn_item_categoria
            // 
            this.btn_item_categoria.Name = "btn_item_categoria";
            this.btn_item_categoria.Text = "Categoria";
            this.btn_item_categoria.Click += new System.EventHandler(this.btn_item_categoria_Click);
            // 
            // btn_item_piso
            // 
            this.btn_item_piso.Image = global::FinalXML.Properties.Resources.pisos;
            this.btn_item_piso.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btn_item_piso.Name = "btn_item_piso";
            this.btn_item_piso.SubItemsExpandWidth = 14;
            this.btn_item_piso.Text = "Pisos";
            this.btn_item_piso.Click += new System.EventHandler(this.btn_item_piso_Click);
            // 
            // btn_item_mesa
            // 
            this.btn_item_mesa.Image = global::FinalXML.Properties.Resources.mesa;
            this.btn_item_mesa.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btn_item_mesa.Name = "btn_item_mesa";
            this.btn_item_mesa.SubItemsExpandWidth = 14;
            this.btn_item_mesa.Text = "Mesas";
            this.btn_item_mesa.Click += new System.EventHandler(this.btn_item_mesa_Click);
            // 
            // btn_item_pedido
            // 
            this.btn_item_pedido.Image = global::FinalXML.Properties.Resources.pedido01;
            this.btn_item_pedido.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btn_item_pedido.Name = "btn_item_pedido";
            this.btn_item_pedido.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btn_item_lista_ticket,
            this.btn_item_lista_ticket_nofacturado,
            this.btn_item_historial_ticket});
            this.btn_item_pedido.SubItemsExpandWidth = 14;
            this.btn_item_pedido.Text = "Pedidos";
            this.btn_item_pedido.Click += new System.EventHandler(this.btn_item_pedido_Click);
            // 
            // btn_item_lista_ticket
            // 
            this.btn_item_lista_ticket.Name = "btn_item_lista_ticket";
            this.btn_item_lista_ticket.Text = "Lista Tickets Pendientes";
            this.btn_item_lista_ticket.Click += new System.EventHandler(this.btn_item_lista_ticket_Click);
            // 
            // btn_item_lista_ticket_nofacturado
            // 
            this.btn_item_lista_ticket_nofacturado.Name = "btn_item_lista_ticket_nofacturado";
            this.btn_item_lista_ticket_nofacturado.Text = "Lista Tickets Quitados";
            this.btn_item_lista_ticket_nofacturado.Click += new System.EventHandler(this.btn_item_lista_ticket_nofacturado_Click);
            // 
            // btn_item_historial_ticket
            // 
            this.btn_item_historial_ticket.Name = "btn_item_historial_ticket";
            this.btn_item_historial_ticket.Text = "Lista de Tickets";
            this.btn_item_historial_ticket.Click += new System.EventHandler(this.btn_item_historial_ticket_Click);
            // 
            // btn_item_historial_pedido
            // 
            this.btn_item_historial_pedido.Image = global::FinalXML.Properties.Resources.ticket;
            this.btn_item_historial_pedido.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btn_item_historial_pedido.Name = "btn_item_historial_pedido";
            this.btn_item_historial_pedido.SubItemsExpandWidth = 14;
            this.btn_item_historial_pedido.Text = "Tickets por Mesa";
            this.btn_item_historial_pedido.Click += new System.EventHandler(this.btn_item_historial_pedido_Click);
            // 
            // btn_item_ordencocina
            // 
            this.btn_item_ordencocina.Image = global::FinalXML.Properties.Resources.cocina;
            this.btn_item_ordencocina.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btn_item_ordencocina.Name = "btn_item_ordencocina";
            this.btn_item_ordencocina.SubItemsExpandWidth = 14;
            this.btn_item_ordencocina.Text = "Ordenes de Cocina";
            this.btn_item_ordencocina.Click += new System.EventHandler(this.btn_item_ordencocina_Click);
            // 
            // btn_item_caja
            // 
            this.btn_item_caja.Image = global::FinalXML.Properties.Resources.caja;
            this.btn_item_caja.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btn_item_caja.Name = "btn_item_caja";
            this.btn_item_caja.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btn_item_tipocambio,
            this.btn_item_apertura,
            this.btn_item_cajamovimiento,
            this.btn_item_caja_abierta,
            this.btn_item_cajas_cerradas,
            this.btn_item_cajahistorica,
            this.btn_item_cuentaydemas});
            this.btn_item_caja.SubItemsExpandWidth = 14;
            this.btn_item_caja.Text = "Caja";
            // 
            // btn_item_tipocambio
            // 
            this.btn_item_tipocambio.Name = "btn_item_tipocambio";
            this.btn_item_tipocambio.Text = "Tipo de Cambio";
            this.btn_item_tipocambio.Click += new System.EventHandler(this.btn_item_tipocambio_Click);
            // 
            // btn_item_apertura
            // 
            this.btn_item_apertura.Name = "btn_item_apertura";
            this.btn_item_apertura.Text = "Apertura";
            this.btn_item_apertura.Click += new System.EventHandler(this.btn_item_apertura_Click);
            // 
            // btn_item_cajamovimiento
            // 
            this.btn_item_cajamovimiento.Name = "btn_item_cajamovimiento";
            this.btn_item_cajamovimiento.Text = "Movimiento de Caja";
            this.btn_item_cajamovimiento.Click += new System.EventHandler(this.btn_item_cajamovimiento_Click);
            // 
            // btn_item_caja_abierta
            // 
            this.btn_item_caja_abierta.Name = "btn_item_caja_abierta";
            this.btn_item_caja_abierta.Text = "Cajas Abiertas";
            this.btn_item_caja_abierta.Click += new System.EventHandler(this.btn_item_caja_abierta_Click);
            // 
            // btn_item_cajas_cerradas
            // 
            this.btn_item_cajas_cerradas.Name = "btn_item_cajas_cerradas";
            this.btn_item_cajas_cerradas.Text = "Cajas Cerradas";
            this.btn_item_cajas_cerradas.Click += new System.EventHandler(this.btn_item_cajas_cerradas_Click);
            // 
            // btn_item_cajahistorica
            // 
            this.btn_item_cajahistorica.Name = "btn_item_cajahistorica";
            this.btn_item_cajahistorica.Text = "Caja Historica";
            this.btn_item_cajahistorica.Click += new System.EventHandler(this.btn_item_cajahistorica_Click);
            // 
            // btn_item_cuentaydemas
            // 
            this.btn_item_cuentaydemas.Name = "btn_item_cuentaydemas";
            this.btn_item_cuentaydemas.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btn_item_banco,
            this.btn_item_cuentabancaria,
            this.btn_item_tipocuenta_bancaria,
            this.btn_item_tarjeta,
            this.btn_item_tipo_cobro});
            this.btn_item_cuentaydemas.Text = "Adicionales";
            // 
            // btn_item_banco
            // 
            this.btn_item_banco.Name = "btn_item_banco";
            this.btn_item_banco.Text = "Banco";
            this.btn_item_banco.Click += new System.EventHandler(this.btn_item_banco_Click);
            // 
            // btn_item_cuentabancaria
            // 
            this.btn_item_cuentabancaria.Name = "btn_item_cuentabancaria";
            this.btn_item_cuentabancaria.Text = "Cuenta Bancaria";
            this.btn_item_cuentabancaria.Click += new System.EventHandler(this.btn_item_cuentabancaria_Click);
            // 
            // btn_item_tipocuenta_bancaria
            // 
            this.btn_item_tipocuenta_bancaria.Name = "btn_item_tipocuenta_bancaria";
            this.btn_item_tipocuenta_bancaria.Text = "Tipos de Cuenta";
            this.btn_item_tipocuenta_bancaria.Click += new System.EventHandler(this.btn_item_tipocuenta_bancaria_Click);
            // 
            // btn_item_tarjeta
            // 
            this.btn_item_tarjeta.Name = "btn_item_tarjeta";
            this.btn_item_tarjeta.Text = "Tarjeta";
            this.btn_item_tarjeta.Click += new System.EventHandler(this.btn_item_tarjeta_Click);
            // 
            // btn_item_tipo_cobro
            // 
            this.btn_item_tipo_cobro.Name = "btn_item_tipo_cobro";
            this.btn_item_tipo_cobro.Text = "Forma de Pago";
            this.btn_item_tipo_cobro.Click += new System.EventHandler(this.btn_item_tipo_cobro_Click);
            // 
            // btn_item_venta
            // 
            this.btn_item_venta.Image = global::FinalXML.Properties.Resources.venta;
            this.btn_item_venta.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btn_item_venta.Name = "btn_item_venta";
            this.btn_item_venta.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btn_item_lista_venta,
            this.btn_lista_nota_credito_debito,
            this.btn_item_repositorio,
            this.btn_item_ventas_xmesa_mozo});
            this.btn_item_venta.SubItemsExpandWidth = 14;
            this.btn_item_venta.Text = "Venta";
            this.btn_item_venta.Click += new System.EventHandler(this.btn_item_venta_Click);
            // 
            // btn_item_lista_venta
            // 
            this.btn_item_lista_venta.Name = "btn_item_lista_venta";
            this.btn_item_lista_venta.Text = "Lista de Ventas";
            this.btn_item_lista_venta.Click += new System.EventHandler(this.btn_item_lista_venta_Click);
            // 
            // btn_lista_nota_credito_debito
            // 
            this.btn_lista_nota_credito_debito.Name = "btn_lista_nota_credito_debito";
            this.btn_lista_nota_credito_debito.Text = "Listar Notas de Credito";
            this.btn_lista_nota_credito_debito.Visible = false;
            this.btn_lista_nota_credito_debito.Click += new System.EventHandler(this.btn_lista_nota_credito_debito_Click);
            // 
            // btn_item_repositorio
            // 
            this.btn_item_repositorio.Name = "btn_item_repositorio";
            this.btn_item_repositorio.Text = "Repositorio";
            this.btn_item_repositorio.Click += new System.EventHandler(this.btn_item_repositorio_Click);
            // 
            // btn_item_ventas_xmesa_mozo
            // 
            this.btn_item_ventas_xmesa_mozo.Name = "btn_item_ventas_xmesa_mozo";
            this.btn_item_ventas_xmesa_mozo.Text = "Lista de Ventas y Mesas por Mozo";
            this.btn_item_ventas_xmesa_mozo.Click += new System.EventHandler(this.btn_item_ventas_xmesa_mozo_Click);
            // 
            // ribbonPanel4
            // 
            this.ribbonPanel4.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonPanel4.Controls.Add(this.ribbonBar2);
            this.ribbonPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel4.Location = new System.Drawing.Point(0, 53);
            this.ribbonPanel4.Name = "ribbonPanel4";
            this.ribbonPanel4.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel4.Size = new System.Drawing.Size(913, 125);
            // 
            // 
            // 
            this.ribbonPanel4.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel4.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel4.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel4.TabIndex = 4;
            this.ribbonPanel4.Visible = false;
            // 
            // ribbonBar2
            // 
            this.ribbonBar2.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar2.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar2.ContainerControlProcessDialogKey = true;
            this.ribbonBar2.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar2.DragDropSupport = true;
            this.ribbonBar2.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btn_item_ingrediente,
            this.btn_item_proveedor});
            this.ribbonBar2.Location = new System.Drawing.Point(3, 0);
            this.ribbonBar2.Name = "ribbonBar2";
            this.ribbonBar2.Size = new System.Drawing.Size(181, 122);
            this.ribbonBar2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonBar2.TabIndex = 0;
            // 
            // 
            // 
            this.ribbonBar2.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar2.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // btn_item_ingrediente
            // 
            this.btn_item_ingrediente.Image = global::FinalXML.Properties.Resources.ingrediente;
            this.btn_item_ingrediente.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btn_item_ingrediente.Name = "btn_item_ingrediente";
            this.btn_item_ingrediente.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btn_item_unidad_medida});
            this.btn_item_ingrediente.SubItemsExpandWidth = 14;
            this.btn_item_ingrediente.Text = "Ingrediente";
            this.btn_item_ingrediente.Click += new System.EventHandler(this.btn_item_ingrediente_Click);
            // 
            // btn_item_unidad_medida
            // 
            this.btn_item_unidad_medida.Name = "btn_item_unidad_medida";
            this.btn_item_unidad_medida.Text = "Unidad de Medida";
            this.btn_item_unidad_medida.Click += new System.EventHandler(this.btn_item_unidad_medida_Click);
            // 
            // btn_item_proveedor
            // 
            this.btn_item_proveedor.Image = ((System.Drawing.Image)(resources.GetObject("btn_item_proveedor.Image")));
            this.btn_item_proveedor.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btn_item_proveedor.Name = "btn_item_proveedor";
            this.btn_item_proveedor.SubItemsExpandWidth = 14;
            this.btn_item_proveedor.Text = "Proveedor";
            this.btn_item_proveedor.Click += new System.EventHandler(this.btn_item_proveedor_Click);
            // 
            // lb_tipo_cambio
            // 
            this.lb_tipo_cambio.FontBold = true;
            this.lb_tipo_cambio.ForeColor = System.Drawing.Color.DarkRed;
            this.lb_tipo_cambio.Name = "lb_tipo_cambio";
            this.lb_tipo_cambio.Text = "Tipo de Cambio";
            this.lb_tipo_cambio.Visible = false;
            // 
            // Ventas
            // 
            this.Ventas.Checked = true;
            this.Ventas.Name = "Ventas";
            this.Ventas.Panel = this.ribbonPanel3;
            this.Ventas.Text = "Ventas";
            // 
            // ribbonTabItem3
            // 
            this.ribbonTabItem3.Name = "ribbonTabItem3";
            this.ribbonTabItem3.Panel = this.ribbonPanel4;
            this.ribbonTabItem3.Text = "Compras";
            this.ribbonTabItem3.Visible = false;
            // 
            // galleryContainer2
            // 
            // 
            // 
            // 
            this.galleryContainer2.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer2.EnableGalleryPopup = false;
            this.galleryContainer2.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer2.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer2.MultiLine = false;
            this.galleryContainer2.Name = "galleryContainer2";
            this.galleryContainer2.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer2.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // labelItem1
            // 
            this.labelItem1.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom;
            this.labelItem1.BorderType = DevComponents.DotNetBar.eBorderType.Etched;
            this.labelItem1.CanCustomize = false;
            this.labelItem1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelItem1.Name = "labelItem1";
            // 
            // qatCustomizeItem1
            // 
            this.qatCustomizeItem1.Name = "qatCustomizeItem1";
            // 
            // ribbonTabItem2
            // 
            this.ribbonTabItem2.Name = "ribbonTabItem2";
            this.ribbonTabItem2.Panel = this.ribbonPanel2;
            this.ribbonTabItem2.Text = "ribbonTabItem2";
            // 
            // ribbonTabItem1
            // 
            this.ribbonTabItem1.Checked = true;
            this.ribbonTabItem1.Name = "ribbonTabItem1";
            this.ribbonTabItem1.Text = "ribbonTabItem1";
            // 
            // styleManager1
            // 
            this.styleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Office2010Blue;
            this.styleManager1.MetroColorParameters = new DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters(System.Drawing.Color.White, System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154))))));
            // 
            // frmPrincipalAdministrador
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(923, 301);
            this.Controls.Add(this.ribbonControl1);
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "frmPrincipalAdministrador";
            this.Text = "Administrador";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmPrincipalAdministrador_Load);
            this.ribbonControl1.ResumeLayout(false);
            this.ribbonControl1.PerformLayout();
            this.ribbonPanel3.ResumeLayout(false);
            this.ribbonPanel1.ResumeLayout(false);
            this.ribbonPanel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private DevComponents.DotNetBar.ItemContainer itemContainer1;
        private DevComponents.DotNetBar.ItemContainer itemContainer2;
        private DevComponents.DotNetBar.ItemContainer itemContainer3;
        private DevComponents.DotNetBar.ButtonItem buttonItem2;
        private DevComponents.DotNetBar.ButtonItem buttonItem3;
        private DevComponents.DotNetBar.ButtonItem buttonItem4;
        private DevComponents.DotNetBar.ButtonItem buttonItem5;
        private DevComponents.DotNetBar.ButtonItem buttonItem6;
        private DevComponents.DotNetBar.ButtonItem buttonItem7;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer1;
        private DevComponents.DotNetBar.LabelItem labelItem8;
        private DevComponents.DotNetBar.ButtonItem buttonItem8;
        private DevComponents.DotNetBar.ButtonItem buttonItem9;
        private DevComponents.DotNetBar.ButtonItem buttonItem10;
        private DevComponents.DotNetBar.ButtonItem buttonItem11;
        private DevComponents.DotNetBar.ItemContainer itemContainer4;
        private DevComponents.DotNetBar.ButtonItem buttonItem12;
        private DevComponents.DotNetBar.ButtonItem buttonItem13;
        private DevComponents.DotNetBar.ApplicationButton applicationButton1;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel2;
        private DevComponents.DotNetBar.RibbonControl ribbonControl1;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer2;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.QatCustomizeItem qatCustomizeItem1;
        private DevComponents.DotNetBar.RibbonTabItem ribbonTabItem2;
        private DevComponents.DotNetBar.RibbonTabItem ribbonTabItem1;
        private DevComponents.DotNetBar.LabelItem lb_tipo_cambio;
        private DevComponents.DotNetBar.StyleManager styleManager1;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel3;
        private DevComponents.DotNetBar.RibbonTabItem Ventas;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel1;
        private DevComponents.DotNetBar.RibbonBar ribbonBar1;
        public DevComponents.DotNetBar.ButtonItem btn_item_serie;
        private DevComponents.DotNetBar.ButtonItem btn_item_configuracion_envio;
        public DevComponents.DotNetBar.ButtonItem btn_item_usuario;
        public DevComponents.DotNetBar.ButtonItem btn_item_cliente;
        public DevComponents.DotNetBar.ButtonItem btn_prod_bebi_pla;
        private DevComponents.DotNetBar.ButtonItem btn_item_categoria;
        public DevComponents.DotNetBar.ButtonItem btn_item_piso;
        public DevComponents.DotNetBar.ButtonItem btn_item_mesa;
        public DevComponents.DotNetBar.ButtonItem btn_item_pedido;
        private DevComponents.DotNetBar.ButtonItem btn_item_lista_ticket;
        private DevComponents.DotNetBar.ButtonItem btn_item_lista_ticket_nofacturado;
        private DevComponents.DotNetBar.ButtonItem btn_item_historial_ticket;
        public DevComponents.DotNetBar.ButtonItem btn_item_historial_pedido;
        public DevComponents.DotNetBar.ButtonItem btn_item_ordencocina;
        public DevComponents.DotNetBar.ButtonItem btn_item_caja;
        private DevComponents.DotNetBar.ButtonItem btn_item_tipocambio;
        private DevComponents.DotNetBar.ButtonItem btn_item_apertura;
        private DevComponents.DotNetBar.ButtonItem btn_item_cajamovimiento;
        private DevComponents.DotNetBar.ButtonItem btn_item_caja_abierta;
        private DevComponents.DotNetBar.ButtonItem btn_item_cajas_cerradas;
        private DevComponents.DotNetBar.ButtonItem btn_item_cajahistorica;
        private DevComponents.DotNetBar.ButtonItem btn_item_cuentaydemas;
        private DevComponents.DotNetBar.ButtonItem btn_item_banco;
        private DevComponents.DotNetBar.ButtonItem btn_item_cuentabancaria;
        private DevComponents.DotNetBar.ButtonItem btn_item_tipocuenta_bancaria;
        private DevComponents.DotNetBar.ButtonItem btn_item_tarjeta;
        private DevComponents.DotNetBar.ButtonItem btn_item_tipo_cobro;
        public DevComponents.DotNetBar.ButtonItem btn_item_venta;
        private DevComponents.DotNetBar.ButtonItem btn_item_lista_venta;
        private DevComponents.DotNetBar.ButtonItem btn_lista_nota_credito_debito;
        private DevComponents.DotNetBar.ButtonItem btn_item_repositorio;
        private DevComponents.DotNetBar.ButtonItem btn_item_ventas_xmesa_mozo;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel4;
        private DevComponents.DotNetBar.RibbonBar ribbonBar2;
        private DevComponents.DotNetBar.ButtonItem btn_item_ingrediente;
        private DevComponents.DotNetBar.RibbonTabItem ribbonTabItem3;
        private DevComponents.DotNetBar.ButtonItem btn_item_unidad_medida;
        private DevComponents.DotNetBar.ButtonItem btn_item_proveedor;
    }
}