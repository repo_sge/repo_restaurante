﻿using FinalXML.Administradores;
using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalXML.vista.administrador
{
    public partial class frmCuentaBancaria : PlantillaBase
    {
        private clsCuentaBancaria cuenta = null;
        private List<clsCuentaBancaria> lista_cuenta = null;
        private clsAdmCuentaBancaria admcue = new clsAdmCuentaBancaria();
        private clsMoneda moneda = null;
        private List<clsMoneda> lista_moneda = null;
        private clsAdmMoneda admmo = new clsAdmMoneda();
        private clsBanco banco = null;
        private List<clsBanco> lista_banco = null;
        private clsAdmBanco admban = new clsAdmBanco();
        private clsTipoCuentaBancaria tipocuenta = null;
        private List<clsTipoCuentaBancaria> lista_tipocuenta = null;
        private clsAdmTipoCuentaBancaria admtipocuen = new clsAdmTipoCuentaBancaria();
        public clsUsuario usureg { get; set; }
        private DataTable tabla = null;
        public frmCuentaBancaria()
        {
            InitializeComponent();
        }

        private void frmCuentaBancaria_Load(object sender, EventArgs e)
        {
            dg_resultado.AutoGenerateColumns = false;
            listar_banco();
            listar_tipocuenta();
            listar_moneda();
            cb_estado.SelectedIndex = 0;

        }

        private void txt_numero_TextChanged(object sender, EventArgs e)
        {
            txt_numero.CharacterCasing = CharacterCasing.Upper;
        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_guardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (txt_cod.Text.Length == 0)
                {
                    if (txt_numero.Text.Length > 0)
                    {
                        DialogResult respuesta = MessageBox.Show("¿Desea registrar cuenta bancaria?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (respuesta == DialogResult.Yes)
                        {
                            cuenta = new clsCuentaBancaria();
                            cuenta.Banco = lista_banco[cb_banco.SelectedIndex];
                            cuenta.Numero = txt_numero.Text;
                            cuenta.Tipocuenta = lista_tipocuenta[cb_tipocuenta.SelectedIndex];
                            cuenta.Moneda = lista_moneda[cb_moneda.SelectedIndex];
                            if (cb_estado.SelectedItem.ToString() == "ACTIVO") { cuenta.Estado = 1; } else { cuenta.Estado = 0; }

                            if (admcue.registrar_cuenta(cuenta, usureg) > 0)
                            {
                                MessageBox.Show("Registro correcto...", "Información");
                                listar_cuenta_xbanco();

                            }
                            else
                            {

                                MessageBox.Show("Problemas en el registro...", "Error");
                            }

                        }

                    }
                    else
                    {

                        MessageBox.Show("Por favor complete los campos solictados...", "Advertencia");

                    }
                }
                else
                {


                    if (txt_numero.Text.Length > 0)
                    {
                        DialogResult respuesta = MessageBox.Show("¿Desea actualizar cuenta bancaria?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (respuesta == DialogResult.Yes)
                        {
                            cuenta = new clsCuentaBancaria();
                            cuenta.Idcuentabancaria = int.Parse(txt_cod.Text);
                            cuenta.Banco = lista_banco[cb_banco.SelectedIndex];
                            cuenta.Numero = txt_numero.Text;
                            cuenta.Tipocuenta = lista_tipocuenta[cb_tipocuenta.SelectedIndex];
                            cuenta.Moneda = lista_moneda[cb_moneda.SelectedIndex];
                            if (cb_estado.SelectedItem.ToString() == "ACTIVO") { cuenta.Estado = 1; } else { cuenta.Estado = 0; }

                            if (admcue.actualizar_cuenta(cuenta, usureg) > 0)
                            {
                                MessageBox.Show("Actualizar correcta...", "Información");
                                listar_cuenta_xbanco();
                            }
                            else
                            {

                                MessageBox.Show("Problemas en la actualización...", "Error");
                            }
                        }
                    }
                    else
                    {

                        MessageBox.Show("Por favor complete los campos solictados...", "Advertencia");

                    }

                }
            }
            catch (Exception) { }
        }

        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            try
            {
                limpiar();
            }
            catch (Exception) { }
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                listar_cuenta_xbanco();
            }
            catch (Exception) { }
        }
        private void btn_cargar_Click(object sender, EventArgs e)
        {
            try
            {
                listar_cuenta();
            }
            catch (Exception) { }
        }

        private void dg_resultado_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dg_resultado.Rows.Count > 0)
                {

                    if (e.RowIndex != -1)
                    {
                        txt_cod.Text = dg_resultado.Rows[e.RowIndex].Cells[0].Value.ToString();
                        cb_banco.SelectedItem = dg_resultado.Rows[e.RowIndex].Cells[4].Value.ToString();
                        txt_numero.Text = dg_resultado.Rows[e.RowIndex].Cells[5].Value.ToString();
                        cb_tipocuenta.SelectedItem = dg_resultado.Rows[e.RowIndex].Cells[6].Value.ToString();
                        cb_moneda.SelectedItem = dg_resultado.Rows[e.RowIndex].Cells[7].Value.ToString();
                        if (dg_resultado.Rows[e.RowIndex].Cells[8].Value.ToString() == "ACTIVO") { cb_estado.SelectedIndex = 0; } else { cb_estado.SelectedIndex = 1; }
                    }
                }
            }
            catch (Exception) { }
        }

        /**********************Mis Metodos*************************/
        public void listar_banco()
        {
            try
            {
                cb_banco.Items.Clear();
                lista_banco = admban.listar_banco_xestado();

                if (lista_banco != null)
                {
                    if (lista_banco.Count > 0)
                    {
                        foreach (clsBanco b in lista_banco)
                        {

                            cb_banco.Items.Add(b.Nombre);

                        }

                        cb_banco.SelectedIndex = 0;
                    }
                }
            }
            catch (Exception) { }
        }

        public void listar_tipocuenta()
        {
            try
            {
                cb_tipocuenta.Items.Clear();

                lista_tipocuenta = admtipocuen.listar_tipocuenta_xestado();

                if (lista_tipocuenta != null)
                {

                    foreach (clsTipoCuentaBancaria t in lista_tipocuenta)
                    {

                        cb_tipocuenta.Items.Add(t.Descripcion);
                    }

                    cb_tipocuenta.SelectedIndex = 0;

                }
            }
            catch (Exception) { }
        }

        public void listar_moneda()
        {
            try
            {
                lista_moneda = admmo.listar_moneda_xestado();

                if (lista_moneda != null)
                {

                    if (lista_moneda.Count > 0)
                    {

                        foreach (clsMoneda m in lista_moneda)
                        {

                            cb_moneda.Items.Add(m.SDescripcion);
                        }

                        cb_moneda.SelectedIndex = 0;
                    }

                }
            }
            catch (Exception) { }

        }

        public void listar_cuenta_xbanco()
        {
            try
            {
                tabla = admcue.listar_cuenta_xbanco(lista_banco[cb_banco.SelectedIndex]);

                if (tabla != null)
                {
                    if (tabla.Rows.Count > 0)
                    {
                        dg_resultado.DataSource = tabla;

                    }
                }
            }
            catch (Exception) { }

        }
        public void limpiar()
        {
            try
            {
                txt_cod.Text = string.Empty;
                txt_numero.Text = string.Empty;
                cb_banco.SelectedIndex = 0;
                cb_estado.SelectedIndex = 0;
                cb_moneda.SelectedIndex = 0;
            }
            catch (Exception) { }
        }

        public void listar_cuenta()
        {
            try
            {
                tabla = admcue.listar_cuenta();

                if (tabla != null)
                {
                    if (tabla.Rows.Count > 0)
                    {
                        dg_resultado.DataSource = tabla;

                    }
                }
            }
            catch (Exception) { }
        }        
    }
}
