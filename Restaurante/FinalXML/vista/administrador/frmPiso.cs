﻿using FinalXML.Administradores;
using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalXML.vista.administrador
{
    public partial class frmPiso : PlantillaBase
    {
        public clsUsuario usureg { get; set; }
        private clsPiso piso = null;
        private clsAdmPiso admpiso = new clsAdmPiso();
        private DataTable pisos = null;

        public frmPiso()
        {
            InitializeComponent();
        }

        private void frmPiso_Load(object sender, EventArgs e)
        {
            cb_estado.SelectedIndex = 0;
            dg_piso.AutoGenerateColumns = false;
        }

        private void txt_nombre_TextChanged(object sender, EventArgs e)
        {
            txt_nombre.CharacterCasing = CharacterCasing.Upper;
        }

        private void txt_numero_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }       

        private void btn_guardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (txt_cod.Text.Length == 0)
                {
                    if (txt_nombre.Text.Length > 0 && txt_numero.Text.Length > 0)
                    {
                        DialogResult respuesta = MessageBox.Show("¿Desea registrar piso?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (respuesta == DialogResult.Yes)
                        {
                            registrar_piso();
                            btn_cargar.PerformClick();
                        }
                    }
                    else
                    {

                        MessageBox.Show("Por favor ingrese la iformación solicitada...", "Advertencia");
                    }
                }
                else
                {

                    if (txt_nombre.Text.Length > 0 && txt_numero.Text.Length > 0)
                    {
                        DialogResult respuesta = MessageBox.Show("¿Desea actualizar piso?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (respuesta == DialogResult.Yes)
                        {
                            actualizar_piso();
                            btn_cargar.PerformClick();
                        }
                    }
                    else
                    {

                        MessageBox.Show("Por favor ingrese la iformación solicitada...", "Advertencia");
                    }

                }
            }
            catch (Exception) { }
        }
        private void btn_cargar_Click(object sender, EventArgs e)
        {
            try
            {
                limpiar();
                listar_piso();
            }
            catch (Exception) { }
        }
        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void dg_piso_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dg_piso.Rows.Count > 0)
                {

                    if (e.RowIndex != -1)
                    {

                        txt_cod.Text = dg_piso.Rows[e.RowIndex].Cells[idpiso.Index].Value.ToString();
                        txt_numero.Text = dg_piso.Rows[e.RowIndex].Cells[numero.Index].Value.ToString();
                        txt_nombre.Text = dg_piso.Rows[e.RowIndex].Cells[nombre.Index].Value.ToString();
                        cb_estado.SelectedIndex = dg_piso.Rows[e.RowIndex].Cells[estado.Index].Value.ToString() == "ACTIVO" ? 0 : 1;
                    }
                }
            }
            catch (Exception) { }
        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        /***********************Mis Metodos************************/
        public void registrar_piso() {

            try
            {
                piso = new clsPiso
                {
                    Numero = int.Parse(txt_numero.Text),
                    Nombre = txt_nombre.Text,
                    Estado = cb_estado.SelectedItem.ToString() == "ACTIVO" ? 1 : 0
                };

                if (admpiso.registrar_piso(piso, usureg) > 0)
                {
                    MessageBox.Show("Registro de piso correcto...", "Información");
                }
                else
                {

                    MessageBox.Show("Problemas para registrar piso...", "Advertencia");
                }
            }
            catch (Exception) { }
        }

        public void actualizar_piso()
        {
            try
            {
                piso = new clsPiso
                {
                    Idpiso = int.Parse(txt_cod.Text),
                    Numero = int.Parse(txt_numero.Text),
                    Nombre = txt_nombre.Text,
                    Estado = cb_estado.SelectedItem.ToString() == "ACTIVO" ? 1 : 0
                };

                if (admpiso.actualizar_piso(piso, usureg) > 0)
                {
                    MessageBox.Show("Actualización correcta de piso...", "Información");
                }
                else
                {

                    MessageBox.Show("Problemas para actualizar piso...", "Advertencia");
                }
            }
            catch (Exception) { }
        }

        public void listar_piso() {

            try
            {
                dg_piso.DataSource = null;
                pisos = admpiso.listar_piso();

                if (pisos != null)
                {

                    if (pisos.Rows.Count > 0)
                    {

                        dg_piso.DataSource = pisos;
                        dg_piso.Refresh();
                    }
                }
            }
            catch (Exception) { }
        }

        public void limpiar() {

            try
            {
                dg_piso.DataSource = null;
                txt_cod.Text = string.Empty;
                txt_numero.Text = string.Empty;
                txt_nombre.Text = string.Empty;
                cb_estado.SelectedIndex = 0;
            }
            catch (Exception) { }
        }

        
    }
}
