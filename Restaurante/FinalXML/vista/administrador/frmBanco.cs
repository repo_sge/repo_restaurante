﻿using FinalXML.Administradores;
using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalXML.vista.administrador
{
    public partial class frmBanco : PlantillaBase
    {
        private clsBanco banco = null;
        private clsAdmBanco admban = new clsAdmBanco();
        private DataTable tabla = null;
        public clsUsuario usureg { get; set; }
        public frmBanco()
        {
            InitializeComponent();
        }

        private void frmBanco_Load(object sender, EventArgs e)
        {
            cb_estado.SelectedIndex = 0;
            dg_resultado.AutoGenerateColumns = false;
        }

        private void btn_guardar_Click(object sender, EventArgs e)
        {
            try
            {

                if (txt_cod.Text.Length == 0)
                {
                    if (txt_nombre.Text.Length > 0 && txt_sigla.Text.Length > 0)
                    {
                        DialogResult respuesta = MessageBox.Show("¿Desea registrar banco?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (respuesta == DialogResult.Yes)
                        {
                            banco = new clsBanco();
                            banco.Nombre = txt_nombre.Text;
                            banco.Sigla = txt_sigla.Text;

                            if (cb_estado.SelectedItem.ToString() == "ACTIVO") { banco.Estado = 1; } else { banco.Estado = 0; }

                            if (admban.registrar_banco(banco, usureg) > 0)
                            {
                                MessageBox.Show("Registro correcto...", "Información");
                                listar_banco_xnombre();

                            }
                            else
                            {

                                MessageBox.Show("Problemas en el registro...", "Error");
                            }

                        }

                    }
                    else
                    {

                        MessageBox.Show("Por favor complete los campos solictados...", "Advertencia");

                    }
                }
                else
                {


                    if (txt_nombre.Text.Length > 0 && txt_sigla.Text.Length > 0)
                    {
                        DialogResult respuesta = MessageBox.Show("¿Desea actualizar banco?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (respuesta == DialogResult.Yes)
                        {
                            banco = new clsBanco();
                            banco.Idbanco = int.Parse(txt_cod.Text);
                            banco.Nombre = txt_nombre.Text;
                            banco.Sigla = txt_sigla.Text;
                            if (cb_estado.SelectedItem.ToString() == "ACTIVO") { banco.Estado = 1; } else { banco.Estado = 0; }


                            if (admban.actualizar_banco(banco, usureg) > 0)
                            {
                                MessageBox.Show("Actualizar correcta...", "Información");
                                listar_banco_xnombre();
                            }
                            else
                            {

                                MessageBox.Show("Problemas en la actualización...", "Error");
                            }
                        }

                    }
                    else
                    {

                        MessageBox.Show("Por favor complete los campos solictados...", "Advertencia");

                    }

                }
            }
            catch (Exception) { }
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                if (txt_nombre.Text.Length > 0)
                {

                    listar_banco_xnombre();
                }
            }
            catch (Exception) { }
        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_cargar_Click(object sender, EventArgs e)
        {
            try
            {
                listar_banco();
            }
            catch (Exception) { }
        }
        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void dg_resultado_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dg_resultado.Rows.Count > 0)
                {
                    if (e.RowIndex != -1)
                    {
                        txt_cod.Text = dg_resultado.Rows[e.RowIndex].Cells[0].Value.ToString();
                        txt_nombre.Text = dg_resultado.Rows[e.RowIndex].Cells[1].Value.ToString();
                        txt_sigla.Text = dg_resultado.Rows[e.RowIndex].Cells[2].Value.ToString();
                        if (dg_resultado.Rows[e.RowIndex].Cells[3].Value.ToString() == "ACTIVO") { cb_estado.SelectedIndex = 0; } else { cb_estado.SelectedIndex = 1; }
                    }
                }
            }
            catch (Exception) { }
        }

        private void txt_nombre_TextChanged(object sender, EventArgs e)
        {
            txt_nombre.CharacterCasing = CharacterCasing.Upper;
        }

        private void txt_sigla_TextChanged(object sender, EventArgs e)
        {
            txt_sigla.CharacterCasing = CharacterCasing.Upper;
        }

        /***************************Mis Metodos*******************************/

        public void listar_banco()
        {
            try
            {
                tabla = admban.listar_banco();
                if (tabla != null)
                {
                    if (tabla.Rows.Count > 0)
                    {
                        dg_resultado.DataSource = tabla;

                    }
                }
            }
            catch (Exception) { }

        }
        public void listar_banco_xnombre()
        {
            try
            {
                banco = new clsBanco();
                banco.Nombre = txt_nombre.Text;
                tabla = admban.listar_banco_xnombre(banco);
                if (tabla != null)
                {
                    if (tabla.Rows.Count > 0)
                    {
                        dg_resultado.DataSource = tabla;

                    }
                }
            }
            catch (Exception) { }
        }
        public void limpiar()
        {
            try
            {
                txt_cod.Text = string.Empty;
                txt_nombre.Text = string.Empty;
                txt_sigla.Text = string.Empty;
                cb_estado.SelectedIndex = 0;
            }
            catch (Exception) { }
        }        
    }
}
