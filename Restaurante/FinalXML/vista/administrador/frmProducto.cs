﻿using FinalXML.Administradores;
using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalXML.vista.administrador
{
    public partial class frmProducto : PlantillaBase
    {
        private clsAdmCategoria admcate = new clsAdmCategoria();
        private List<clsCategoria> lista_categoria = null;
        private clsProducto producto = null;
        private clsAdmProducto admpro = new clsAdmProducto();
        private DataTable productos = null;
        public clsUsuario usureg { get; set; }
        public string codsunatproducto { get; set; }

        public frmProducto()
        {
            InitializeComponent();
        }

        private void frmProducto_Load(object sender, EventArgs e)
        {
            dg_producto.AutoGenerateColumns = false;
            cb_estado.SelectedIndex = 0;
            
            listar_categoria();
        }
        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txt_nombre_TextChanged(object sender, EventArgs e)
        {
            txt_nombre.CharacterCasing = CharacterCasing.Upper;
        }

        private void txt_descripcion_TextChanged(object sender, EventArgs e)
        {
            txt_descripcion.CharacterCasing = CharacterCasing.Upper;

        }

        private void txt_precio_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }


            if (e.KeyChar == '.' && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }

            if (!char.IsControl(e.KeyChar))
            {

                TextBox textBox = (TextBox)sender;

                if (textBox.Text.IndexOf('.') > -1 &&
                         textBox.Text.Substring(textBox.Text.IndexOf('.')).Length >= 4)
                {
                    e.Handled = true;
                }

            }
        }

        private void txt_precio_Leave(object sender, EventArgs e)
        {
            if (txt_precio.Text.Length == 0)
            {
                txt_precio.Text = "0.00";
            }
        }
        private void btn_guardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (lista_categoria == null)
                {
                    MessageBox.Show("No se puedo cargar categorias...", "Advertencia");
                    return;
                }

                if (txt_cod.Text.Length == 0)
                {
                    if (txt_nombre.Text.Length > 0 && txt_precio.Text.Length > 0)
                    {
                        if (es_numero(txt_precio.Text.Trim()))
                        {
                            if (decimal.Parse(txt_precio.Text.Trim()) > 0)
                            {
                                DialogResult respuesta = MessageBox.Show("¿Desea registrar producto?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                                if (respuesta == DialogResult.Yes)
                                {
                                    registrar_producto();
                                    btn_buscar.PerformClick();
                                    txt_nombre.Text = "";
                                    txt_nombre.Focus();
                                    txt_precio.Text = "";
                                    txt_descripcion.Text = "";
                                    txtCodsunat.Text = "";
                                }
                            }
                            else
                            {

                                MessageBox.Show("Precio debe ser mayor a 0.00", "Advertencia");
                            }
                        }
                        else
                        {

                            MessageBox.Show("Valor ingresado no es un número...", "Advertencia");
                        }
                    }
                    else
                    {

                        MessageBox.Show("Por favor ingrese la información solicitada...", "Advertencia");
                    }
                }
                else
                {
                    if (txt_nombre.Text.Length > 0 && txt_precio.Text.Length > 0)
                    {
                        if (es_numero(txt_precio.Text.Trim()))
                        {
                            if (decimal.Parse(txt_precio.Text.Trim()) > 0)
                            {
                                DialogResult respuesta = MessageBox.Show("¿Desea actualizar producto?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                                if (respuesta == DialogResult.Yes)
                                {
                                    actualizar_producto();
                                    btn_buscar.PerformClick();
                                }
                            }
                            else
                            {

                                MessageBox.Show("Precio debe ser mayor a 0.00", "Advertencia");
                            }
                        }
                        else
                        {

                            MessageBox.Show("Valor ingresado no es un número...", "Advertencia");
                        }
                    }
                    else
                    {

                        MessageBox.Show("Por favor ingrese la información solicitada...", "Advertencia");
                    }


                }
            }
            catch (Exception) { }
        }
        private void btn_cargar_Click(object sender, EventArgs e)
        {
            try
            {
                listar_producto();
            }
            catch (Exception) { }
        }

        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            try
            {
                limpiar();
            }
            catch (Exception) { }
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                if (txt_nombre.Text.Length > 0)
                {
                    listar_productoxnombre();
                }
            }
            catch (Exception) { }
        }

        /*************Mis Metodos******************/

        public void listar_categoria() {

            try
            {
                lista_categoria = admcate.listar_categoria();

                if (lista_categoria != null)
                {

                    if (lista_categoria.Count > 0)
                    {

                        cb_categoria.DataSource = lista_categoria;
                        cb_categoria.DisplayMember = "Nombre";
                        cb_categoria.ValueMember = "Idcategoria";
                        cb_categoria.SelectedIndex = 0;
                    }
                }
            }
            catch (Exception) { }
        }

        public bool es_numero(object Expression)
        {
            try
            {
                bool isNum;
                double retNum;
                isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
                return isNum;
            }
            catch (Exception) { return false; }
        }


        public void registrar_producto() {

            try
            {
                producto = new clsProducto()
                {
                    Categoria = lista_categoria[cb_categoria.SelectedIndex],
                    Estado = (cb_estado.Text == "ACTIVO") ? 1 : 0,
                    Nombre = txt_nombre.Text,
                    Descripcion = txt_descripcion.Text,
                    Preciounitario = decimal.Parse(txt_precio.Text),
                    Codsunat = txtCodsunat.Text,
                    Icbper = chkIcbper.Checked
                   
                };

                if (admpro.registrar_producto(producto, usureg) > 0)
                {

                    MessageBox.Show("Registro correcto...", "Información");
                }
                else
                {

                    MessageBox.Show("Problemas para registrar producto...", "Advertencia");
                }
            }
            catch (Exception) { }
        }

        public void actualizar_producto()
        {
            try
            {
                producto = new clsProducto()
                {
                    IdProducto = int.Parse(txt_cod.Text),
                    Categoria = lista_categoria[cb_categoria.SelectedIndex],
                    Estado = (cb_estado.Text == "ACTIVO") ? 1 : 0,
                    Nombre = txt_nombre.Text,
                    Descripcion = txt_descripcion.Text,
                    Preciounitario = decimal.Parse(txt_precio.Text),
                    Codsunat = txtCodsunat.Text,
                    Icbper=chkIcbper.Checked
                    
                };

                if (admpro.actualizar_producto(producto, usureg) > 0)
                {
                    MessageBox.Show("Actualización correcta...", "Información");
                }
                else
                {

                    MessageBox.Show("Problemas para actualizar producto...", "Advertencia");
                }
            }
            catch (Exception) { }
        }

        public void listar_producto() {

            try
            {
                dg_producto.DataSource = null;
                productos = admpro.listar_producto();

                if (productos != null)
                {
                    if (productos.Rows.Count > 0)
                    {
                        dg_producto.DataSource = productos;
                        dg_producto.Refresh();
                    }
                }   
            }
            catch (Exception ex) { throw ex; }
        }

        public void limpiar() {
            try
            {
                txt_cod.Text = string.Empty;
                cb_estado.SelectedIndex = 0;
                cb_categoria.SelectedIndex = 0;
                txt_nombre.Text = string.Empty;
                txt_descripcion.Text = string.Empty;
                txt_precio.Text = "0.00";
                dg_producto.DataSource = null;
            }
            catch (Exception) { }
        }

        public void listar_productoxnombre()
        {
            try
            {
                dg_producto.DataSource = null;
                productos = admpro.listar_productoxnombre(
                                                            new clsProducto()
                                                            {
                                                                Nombre = txt_nombre.Text
                                                            }
                                                            );

                if (productos != null)
                {
                    if (productos.Rows.Count > 0)
                    {
                        dg_producto.DataSource = productos;
                        dg_producto.Refresh();
                    }
                }
            }
            catch (Exception) { }
        }
      

        private void dg_producto_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dg_producto.Rows.Count > 0)
                {
                    if (e.RowIndex != -1)
                    {

                        txt_cod.Text = dg_producto.Rows[e.RowIndex].Cells[idproducto.Index].Value.ToString();
                        cb_estado.SelectedIndex = (dg_producto.Rows[e.RowIndex].Cells[estado.Index].Value.ToString() == "ACTIVO") ? 0 : 1;
                        cb_categoria.SelectedValue = ((from x in lista_categoria
                                                       where x.Nombre == dg_producto.Rows[e.RowIndex].Cells[nombrecategoria.Index].Value.ToString()
                                                       select x.Idcategoria).ToList())[0];
                        txt_nombre.Text = dg_producto.Rows[e.RowIndex].Cells[nombreproducto.Index].Value.ToString();
                        txt_descripcion.Text = dg_producto.Rows[e.RowIndex].Cells[descripcion.Index].Value.ToString();
                        txt_precio.Text = dg_producto.Rows[e.RowIndex].Cells[preciounitario.Index].Value.ToString();
                        txtCodsunat.Text = dg_producto.Rows[e.RowIndex].Cells[codsunat.Index].Value.ToString();
                    }
                }
            }
            catch (Exception) { }
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            try
            {
                codsunatproducto = "";
                txtCodsunat.Text = string.Empty;

                if (Application.OpenForms["frmCodProductoSunat"] != null)
                {
                    Application.OpenForms["frmCodProductoSunat"].Activate();
                }
                else
                {
                    frmCodProductoSunat frm_codproducto = new frmCodProductoSunat();
                    frm_codproducto.usureg = usureg;
                    frm_codproducto.frm_producto = this;
                    //frm_codproducto.MdiParent = this;
                    frm_codproducto.ShowDialog();
                }

                if (codsunatproducto != null)
                {
                    if (codsunatproducto != "")
                    {
                        txtCodsunat.Text = codsunatproducto;
                    }
                    else
                    {

                        codsunatproducto = "";
                        txtCodsunat.Text = string.Empty;
                    }

                }
            }
            catch (Exception ex) {

                MessageBox.Show("Hubo un error..." + ex.Message);
            }
        }
    }
}
