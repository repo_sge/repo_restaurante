﻿using FinalXML.Administradores;
using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalXML.vista.administrador
{
    public partial class frmSerieFactura : PlantillaBase
    {
        public  clsUsuario usureg { get; set; }
        private DataTable series = null;
        private clsSerieFactura seriefact = null;
        private clsAdmSerieFactura admser = new clsAdmSerieFactura();
        public frmSerieFactura()
        {
            InitializeComponent();
        }

        private void frmSerieFactura_Load(object sender, EventArgs e)
        {
            cb_estado.SelectedIndex = 0;
            dg_seriefactura.AutoGenerateColumns = false;
        }

        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            limpiar();
        }


        private void txt_correlativo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void btn_cargar_Click(object sender, EventArgs e)
        {
            listar_seriefacturacion();
        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dg_seriefactura_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dg_seriefactura.Rows.Count > 0)
                {

                    if (e.RowIndex != -1)
                    {
                        txt_cod.Text = dg_seriefactura.Rows[e.RowIndex].Cells[id.Index].Value.ToString();
                        txt_tipo.Text = dg_seriefactura.Rows[e.RowIndex].Cells[tipo_documento.Index].Value.ToString();
                        txt_nombre.Text = dg_seriefactura.Rows[e.RowIndex].Cells[nom_documento.Index].Value.ToString();
                        txt_serie.Text = dg_seriefactura.Rows[e.RowIndex].Cells[serie.Index].Value.ToString();
                        txt_correlativo.Text = dg_seriefactura.Rows[e.RowIndex].Cells[correlativo.Index].Value.ToString();
                        cb_estado.SelectedIndex = (dg_seriefactura.Rows[e.RowIndex].Cells[estado.Index].Value.ToString() == "ACTIVO") ? 0 : 1;
                    }
                }
            }
            catch (Exception) { }
        }

        private void btn_guardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (txt_cod.Text.Length > 0)
                {

                    if (txt_correlativo.Text.Length > 0)
                    {
                        if (es_numero(txt_correlativo.Text.Trim()))
                        {
                            if (int.Parse(txt_correlativo.Text.Trim()) > 0)
                            {
                                DialogResult respuesta = MessageBox.Show("¿Desea actualizar serie de facturación?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                                if (respuesta == DialogResult.Yes)
                                {
                                    actualizar_seriexnumeracion();
                                    btn_cargar.PerformClick();
                                }
                            }
                            else
                            {

                                MessageBox.Show("correlativo debe ser mayor a 0", "Advertencia");
                            }
                        }
                        else
                        {

                            MessageBox.Show("Valor ingresado no es un número...", "Advertencia");
                        }
                    }
                    else
                    {

                        MessageBox.Show("Por favor ingrese la información solicitada...", "Advertencia");
                    }
                }
            }
            catch (Exception) { }
        }

        /******************Mis Metodos******************/

        public void listar_seriefacturacion() {

            try
            {
                dg_seriefactura.DataSource = null;
                series = admser.listar_seriefacturacion();

                if (series != null)
                {

                    if (series.Rows.Count > 0)
                    {

                        dg_seriefactura.DataSource = series;
                        dg_seriefactura.Refresh();
                    }
                }
            }
            catch (Exception) { }
        }

        public void limpiar() {

            try
            {
                txt_cod.Text = string.Empty;
                txt_tipo.Text = string.Empty;
                txt_nombre.Text = string.Empty;
                txt_serie.Text = string.Empty;
                txt_correlativo.Text = string.Empty;
                cb_estado.SelectedIndex = 0;
            }
            catch (Exception) { }
        }

        public bool es_numero(object Expression)
        {
            try
            {
                bool isNum;
                int retNum;
                isNum = int.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
                return isNum;
            }
            catch (Exception) { return false; }
        }

        public void actualizar_seriexnumeracion() {

            try
            {
                seriefact = new clsSerieFactura()
                {

                    Idserie = int.Parse(txt_cod.Text),
                    Correlativo = int.Parse(txt_correlativo.Text),
                    Estado = (cb_estado.SelectedItem.ToString() == "ACTIVO") ? 1 : 0
                };

                if (admser.actualizar_seriexnumeracion(seriefact, usureg) > 0)
                {
                    MessageBox.Show("Actualización correcta...", "Información");
                }
                else
                {

                    MessageBox.Show("Problemas para actualizar correlativo", "Advertencia");
                }
            }
            catch (Exception) { }
        }       
    }
}
