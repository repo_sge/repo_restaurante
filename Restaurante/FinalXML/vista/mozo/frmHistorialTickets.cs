﻿using FinalXML.Administradores;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalXML.vista.mozo
{
    public partial class frmHistorialTickets : PlantillaBase
    {
        private clsAdmTicket admtick = new clsAdmTicket();
        private DataTable tickets = null;       
        private DataSet data = null;
        public frmHistorialTickets()
        {
            InitializeComponent();
        }

        private void frmHistorialTickets_Load(object sender, EventArgs e)
        {
            try
            {
                dg_ticket.DefaultCellStyle.Font =new Font("Segoe UI",12);
                dg_ticket.AutoGenerateColumns = false;
                dg_ticket.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

                dt_fechainicio.Value = DateTime.Now;
                dt_fecha_fin.Value = DateTime.Now;
                dg_ticket.AutoGenerateColumns = false;
            }
            catch (Exception) { }
        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dt_fechainicio_Click(object sender, EventArgs e)
        {
            dg_ticket.DataSource = null;
            lb_total.Text = "0.00";
        }

        private void dt_fecha_fin_Click(object sender, EventArgs e)
        {
            dg_ticket.DataSource = null;
            lb_total.Text = "0.00";
        }

        private void btn_cargar_Click(object sender, EventArgs e)
        {
            listar_tickets();
        }

        private void btn_copiar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dg_ticket.Rows.Count > 0)
                {
                    dg_ticket.MultiSelect = true;
                    dg_ticket.SelectAll();
                    dg_ticket.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
                    DataObject dataObj = dg_ticket.GetClipboardContent();
                    if (dataObj != null)
                        Clipboard.SetDataObject(dataObj);

                    dg_ticket.MultiSelect = false;

                    MessageBox.Show("Puede copiarlo a cualquier editor de texto...", "Información");
                }

            }
            catch (Exception ex) { MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Information); }
        }
        /***********************Mis Metodos**********************/
        public void listar_tickets()
        {
            try
            {
                tickets = null;
                dg_ticket.DataSource = null;
                tickets = admtick.listar_tickets(
                        dt_fechainicio.Value,
                        dt_fecha_fin.Value
                    );

                if (tickets != null)
                {
                    if (tickets.Rows.Count > 0)
                    {
                        dg_ticket.DataSource = tickets;
                        dg_ticket.Refresh();
                        totales();
                    }
                    else
                    {
                        lb_pendiente.Text = "0.00";
                        lb_total.Text = "0.00";
                    }
                }
                else
                {
                    lb_pendiente.Text = "0.00";
                    lb_total.Text = "0.00";
                }
            }
            catch (Exception) { }
        }

        public void totales()
        {
            try
            {
                if (dg_ticket.Rows.Count > 0)
                {
                    lb_total.Text = (dg_ticket.Rows.Cast<DataGridViewRow>().Where(
                        x => x.Cells["estado"].Value.ToString() == "FACTURADO")
                        .Sum(x => decimal.Parse(x.Cells["monto"].Value.ToString()))).ToString();

                    lb_pendiente.Text = (dg_ticket.Rows.Cast<DataGridViewRow>().Where(
                       x => x.Cells["estado"].Value.ToString() == "PENDIENTE")
                       .Sum(x => decimal.Parse(x.Cells["monto"].Value.ToString()))).ToString();
                }
                else
                {
                    lb_pendiente.Text = "0.00";
                    lb_total.Text = "0.00";
                }
            }
            catch (Exception) { }
        }

        
    }
}
