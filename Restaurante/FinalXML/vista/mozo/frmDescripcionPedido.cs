﻿using FinalXML.Administradores;
using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalXML.vista.mozo
{  
    public partial class frmDescripcionPedido : PlantillaBase
    {
        public clsDetallePedido detallepedido { get; set; }
        public clsUsuario usureg { get; set; }

        private clsAdmPedido admpedi = new clsAdmPedido();
        public frmDescripcionPedido()
        {
            InitializeComponent();
        }

        private void frmDescripcionPedido_Load(object sender, EventArgs e)
        {
            try
            {
                if (detallepedido != null)
                {

                    txt_descripcion.Text = detallepedido.Descripcion;
                }
            }
            catch (Exception) { }
        }

        private void txt_descripcion_TextChanged(object sender, EventArgs e)
        {
            txt_descripcion.CharacterCasing = CharacterCasing.Upper;
        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_guardar_Click(object sender, EventArgs e)
        {
            try
            {
                //if (txt_descripcion.Text.Length > 0)
               // {
                if (detallepedido != null && usureg != null)
                {

                   /// DialogResult respuesta = MessageBox.Show("¿Desea actualizar descripción al pedido?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                   // if (respuesta == DialogResult.Yes)
                    //{
                        detallepedido.Descripcion = txt_descripcion.Text;

                        if (admpedi.actualizar_descripcion_xdetallepedidoid(detallepedido, usureg) > 0)
                        {
                           // MessageBox.Show("Actualización correcta...", "Información");
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("Problemas para actualizar descripción...", "Advertencia");
                        }
                    //}
                }
                //}
                //else {

                  //  MessageBox.Show("Por favor complete información solicitada...","Advertencia");
               // }
            }
            catch (Exception) { }
        }

    }
}
