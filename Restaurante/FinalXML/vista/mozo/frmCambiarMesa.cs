﻿using DevComponents.DotNetBar;
using FinalXML.Administradores;
using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalXML.vista.mozo
{
    public partial class frmCambiarMesa : PlantillaBase
    {
        public clsUsuario usureg { get; set; }
        private clsAdmMesa admme = new clsAdmMesa();
        private clsAdmPedido admpe = new clsAdmPedido();
        private List<clsMesa> lista_mesa = null;
        public clsMesa mesaactual { get; set; }
        private clsMesa nuevamesa = null;
        private clsPedido pedidoenmesa = null;
        public frmPedido frmpedido { get; set; }
        public clsPiso piso { get; set; }

        public frmCambiarMesa()
        {
            InitializeComponent();
        }

        private void frm_CambiarMesa_Load(object sender, EventArgs e)
        {
            try
            {
                listar_mesa_noenmesagrupo();
                if (mesaactual != null)
                {
                    txt_mesa.Text = mesaactual.Nombre;
                }
            }
            catch (Exception) { }
        }

        private void btn_cambiar_Click(object sender, EventArgs e)
        {
            try
            {
                if (lista_mesa != null)
                {
                    if (lista_mesa.Count > 0)
                    {
                        if (frmpedido != null)
                        {
                            if (mesaactual != null)
                            {
                                cambiar_mesa();
                            }
                        }
                    }
                }
            }
            catch (Exception) { }
        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /********************Mis Metodos***************************/
        public void listar_mesa_noenmesagrupo()
        {
            try
            {
                cb_mesa.DataSource = null;
                lista_mesa = admme.listar_mesa_noenmesagrupo(piso);

                if (lista_mesa != null)
                {

                    if (lista_mesa.Count > 0)
                    {
                        cb_mesa.DataSource = lista_mesa;
                        cb_mesa.DisplayMember = "Nombre";
                        cb_mesa.ValueMember = "Idmesa";
                        cb_mesa.SelectedIndex = 0;
                    }
                }
            }
            catch (Exception) { }
        }

        public void cambiar_mesa() {

            int codcambio = -1;

            try
            {
                pedidoenmesa = admpe.listar_idpedidoxmesa(mesaactual);

                if (pedidoenmesa != null)
                {

                    nuevamesa = lista_mesa[cb_mesa.SelectedIndex];
                    codcambio = admme.cambiar_mesa(nuevamesa, pedidoenmesa, usureg);

                    if (codcambio > 0)
                    {
                        MessageBox.Show("Cambio de mesa correcto...", "Información");
                        frmpedido.btn = frmpedido.panel_mesa.Controls[mesaactual.Idmesa.ToString()] as ButtonX;
                        frmpedido.btn.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
                        frmpedido.btn.Refresh();

                        frmpedido.btn = frmpedido.panel_mesa.Controls[nuevamesa.Idmesa.ToString()] as ButtonX;
                        frmpedido.btn.ColorTable = DevComponents.DotNetBar.eButtonColor.MagentaWithBackground;
                        frmpedido.btn.Refresh();
                        frmpedido.lb_mesa.Text = "#MESA";
                        frmpedido.lb_monto.Text = "0.00";
                        frmpedido.limpiar_pedido();
                    }
                    else
                    {

                        if (codcambio == -1)
                        {

                            MessageBox.Show("Problemas para registrar cambio de mesa...", "Advertencia");
                        }

                        if (codcambio == -2)
                        {
                            MessageBox.Show("La mesa a cambiar se encuentra en un grupo...", "Advertencia");
                        }
                    }
                }
            }
            catch (Exception) { }
        }
        
    }
}
