﻿using DevComponents.DotNetBar;
using DevComponents.DotNetBar.Controls;
using FinalXML.Administradores;
using FinalXML.Entidades;
using FinalXML.vista.cajero;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalXML.vista.mozo
{
    public partial class frmAutorizaAnulacion : PlantillaBase
    {
        private clsUsuario usuautorizado = null;
        private clsEncriptacion encriptar = new clsEncriptacion();
        private clsAdmUsuario admusuario = new clsAdmUsuario();
        private clsAcceso acceso = null;
        private clsAdmAcceso admacceso = new clsAdmAcceso();
        public frmPedido frm_pedido { get; set; }       
        public frmListaTicket frm_listaticket { get; set; } 
        public frmListaVenta frm_listaventa { get; set; }
        public frmListaNotaCreditoDebito frm_listanccndb { get; set; }
        public frmListaTicketNoFacturado frm_listanofacturado { get; set; }
        public frmTicketsxMesa frm_ticketsxmesa { get; set; }
        public frmCajaMovimiento frm_cajamovimiento { get; set; }
        public frmVerCierreCaja frm_vercierrecaja { get; set; }

        private TextBoxX tboxActive = null;
        public frmAutorizaAnulacion()
        {
            InitializeComponent();
        }

        private void frmAutorizaAnulacion_Load(object sender, EventArgs e)
        {
            txt_usuario.Focus();
        }

        private void txt_usuario_TextChanged(object sender, EventArgs e)
        {
            txt_usuario.CharacterCasing = CharacterCasing.Upper;
        }

        private void btn_cancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_ingresar_Click(object sender, EventArgs e)
        {
            try
            {
                if (frm_pedido != null || frm_listaticket != null || frm_listaventa != null || frm_listanccndb != null || frm_listanofacturado != null
                    || frm_ticketsxmesa != null || frm_cajamovimiento != null || frm_vercierrecaja!=null)
                {
                    if (txt_usuario.Text.Length > 0 && txt_clave.Text.Length > 0)
                    {

                        usuautorizado = admusuario.buscar_usuarioxcuentaclave_autorizado(
                                new clsUsuario()
                                {
                                    Cuenta = txt_usuario.Text,
                                    Clave = encriptar.encriptar(txt_clave.Text)
                                }
                            );

                        if (usuautorizado != null)
                        {
                            if (usuautorizado.Tipousuario.Idtipousuario == 5)
                            {
                                acceso = new clsAcceso()
                                {
                                    Usuario = usuautorizado,
                                    Pc = SystemInformation.UserDomainName,
                                    Usuariopc = SystemInformation.UserName
                                };

                                if (admacceso.registrar_acceso(acceso) > 0)
                                {
                                    if (frm_pedido != null)
                                    {
                                        frm_pedido.autorizado = true;
                                    }

                                    if (frm_listaticket != null)
                                    {
                                        frm_listaticket.autorizado = true;
                                    }

                                    if (frm_listaventa != null)
                                    {
                                        frm_listaventa.autorizado = true;
                                    }

                                    if (frm_listanccndb != null)
                                    {
                                        frm_listanccndb.autorizado = true;
                                    }

                                    if (frm_listanofacturado != null)
                                    {
                                        frm_listanofacturado.autorizado = true;
                                    }

                                    if (frm_ticketsxmesa != null)
                                    {
                                        frm_ticketsxmesa.autorizado = true;
                                    }

                                    if (frm_cajamovimiento != null)
                                    {
                                        frm_cajamovimiento.autorizado = true;
                                    }

                                    if (frm_vercierrecaja != null)
                                    {
                                        frm_vercierrecaja.autorizado = true;
                                    }
                                    this.Close();
                                }
                                else
                                {
                                    MessageBox.Show("Problemas para registrar acceso...", "Advertencia");
                                }
                            }
                        }
                        else
                        {
                            txt_usuario.Text = string.Empty;
                            txt_clave.Text = string.Empty;
                            txt_usuario.Focus();
                        }
                    }
                }
            }
            catch (Exception) { }
        }

        private void txt_clave_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {

                    btn_ingresar.PerformClick();
                }
            }
            catch (Exception) { }
        }


        private void btn_Click(object sender, EventArgs e)
        {
            try
            {
                ButtonX btn = sender as ButtonX;
                //TextBoxX tboxActive = this.ActiveControl as TextBoxX;
                if (tboxActive != null)
                {
                    tboxActive.Text += btn.Text;
                    tboxActive.SelectionStart = tboxActive.Text.Length;
                }
            }
            catch (Exception) { }
        }

        private void txt_usuario_Leave(object sender, EventArgs e)
        {
            tboxActive = txt_usuario;
        }

        private void txt_clave_Leave(object sender, EventArgs e)
        {
            tboxActive = txt_clave;
        }

        private void btn_del_Click(object sender, EventArgs e)
        {
            try
            {
                if (tboxActive != null)
                {
                    if (tboxActive.Text.Length > 0)
                    {
                        tboxActive.Text = tboxActive.Text.Substring(0, tboxActive.Text.Count() - 1);
                    }
                }
            }
            catch (Exception) { }
        }

        private void frmAutorizaAnulacion_Shown(object sender, EventArgs e)
        {
            txt_usuario.Focus();
        }
    }

}
