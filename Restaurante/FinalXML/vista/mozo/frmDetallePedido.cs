﻿using DevComponents.DotNetBar;
using DevComponents.DotNetBar.Rendering;
using FinalXML.Administradores;
using FinalXML.Entidades;
using FinalXML.vista.cajero;
using FinalXML.vista.mozo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalXML.vista.mozo
{
    public partial class frmDetallePedido : PlantillaBase
    {
        
        private clsProducto producto = null;
        private DataTable productos = null;
        private clsAdmProducto amdpro = new clsAdmProducto();
        private clsAdmCategoria admcate = new clsAdmCategoria();
        private clsAdmPedido admped = new clsAdmPedido();
        private List<clsCategoria> lista_categoria = null;
        public clsUsuario usureg { get; set; }
        public frmPedido frmpedido { get; set; }
        public clsPedido pedidoenmesa { get; set; }
        public clsMesa mesa { get; set; }
        public frmVenta frmventa{get;set;}
        public frmTicketsxMesa frmticketxmesa {get;set;}
      
        public ButtonX btn { get; set; }
        public clsCategoria categoria { get; set; }

        public frmDetallePedido()
        {
            InitializeComponent();
        }

        private void frm_DetallePedido_Load(object sender, EventArgs e)
        {
            
            try
            {
                dg_producto.DefaultCellStyle.Font =new Font("Segoe UI",12);
                dg_producto.AutoGenerateColumns = false;
                //dg_producto.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;       
               // dg_producto.Columns[nombreproducto.Index].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dg_producto.ScrollBars = ScrollBars.Horizontal;
                dg_producto.ScrollBars = ScrollBars.Vertical;
                listar_categoria(4);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Hubo un error..." + ex.Message.ToString());
            }
        }

        private void txt_nombre_TextChanged(object sender, EventArgs e)
        {
            txt_nombre.CharacterCasing = CharacterCasing.Upper;
        }

        private void txt_nombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (txt_nombre.Text.Length > 0)
                {
                    listar_productoxcategorianombre();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Hubo un error..." + ex.Message.ToString());
            }
        }

        private void dg_producto_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                e.Control.KeyPress -= new KeyPressEventHandler(cantidad_KeyPress);
                e.Control.PreviewKeyDown -= new PreviewKeyDownEventHandler(cantidad_KeyDown);
                if (dg_producto.CurrentCell.ColumnIndex == cantidad.Index)
                {
                    TextBox tb = e.Control as TextBox;

                    if (tb != null)
                    {
                        tb.KeyPress += new KeyPressEventHandler(cantidad_KeyPress);
                        tb.PreviewKeyDown += new PreviewKeyDownEventHandler(cantidad_KeyDown);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Hubo un error..." + ex.Message.ToString());
            }
        }


        private void cantidad_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar)
                && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }

        }

        private void cantidad_KeyDown(object sender, PreviewKeyDownEventArgs e) {

            int codpedido = -1;

            try
            {
                if (e.KeyCode == Keys.Enter)
                {

                    if (dg_producto.Rows.Count > 0)
                    {

                        if (dg_producto.CurrentCell != null)
                        {

                            if (dg_producto.CurrentCell.RowIndex != -1)
                            {

                                dg_producto.Rows[dg_producto.CurrentCell.RowIndex].Cells[cantidad.Index].Value = ((TextBox)sender).Text;

                                if (dg_producto.Rows[dg_producto.CurrentCell.RowIndex].Cells[cantidad.Index].Value.ToString() != "0")
                                {
                                    if (frmpedido != null)
                                    {
                                        if (productos != null)
                                        {
                                            if (productos.Rows.Count > 0)
                                            {
                                                if (frmpedido.pedido != null)
                                                {
                                                    /* var existe = (from x in frmpedido.pedido.AsEnumerable()
                                                                   where x.Field<string>("COD PROD") == productos.Rows[dg_producto.CurrentCell.RowIndex].ItemArray[0].ToString()
                                                                   select x
                                                                   ).Any();*/
                                                    // if (!existe)
                                                    //{
                                                    if (pedidoenmesa != null)
                                                    {
                                                        pedidoenmesa.Lista_detallepedido = new List<clsDetallePedido>()
                                                    {
                                                        new clsDetallePedido() {

                                                            Producto=new clsProducto() {
                                                                IdProducto=int.Parse(productos.Rows[dg_producto.CurrentCell.RowIndex].ItemArray[0].ToString())
                                                            } ,
                                                            Cantidad=decimal.Parse(productos.Rows[dg_producto.CurrentCell.RowIndex].ItemArray[3].ToString()),
                                                            Preciounitario=decimal.Parse(productos.Rows[dg_producto.CurrentCell.RowIndex].ItemArray[2].ToString()),
                                                            Descripcion=txt_descripcion.Text,
                                                            Estado=1

                                                        }
                                                    };

                                                        if (admped.registrar_detallepedido(pedidoenmesa, usureg) > 0)
                                                        {
                                                            pedidoenmesa = admped.listar_pedido_xidpedido(pedidoenmesa);

                                                            if (pedidoenmesa != null)
                                                            {
                                                                frmpedido.pedido = new DataTable();
                                                                frmpedido.pedido.Columns.Add("COD PEDI");
                                                                frmpedido.pedido.Columns.Add("COD DETA");
                                                                frmpedido.pedido.Columns.Add("COD PROD");
                                                                frmpedido.pedido.Columns.Add("PRODUCTO");
                                                                frmpedido.pedido.Columns.Add("PRECIO");
                                                                frmpedido.pedido.Columns.Add("CANTIDAD");
                                                                frmpedido.pedido.Columns.Add("TOTAL");
                                                                frmpedido.pedido.Columns.Add("PARA LLEVAR", typeof(bool));
                                                                frmpedido.pedido.Columns.Add("ATENDIDO", typeof(bool));

                                                                foreach (clsDetallePedido det in pedidoenmesa.Lista_detallepedido)
                                                                {
                                                                    DataRow row = frmpedido.pedido.NewRow();

                                                                    row["COD PEDI"] = pedidoenmesa.Idpedido.ToString();
                                                                    row["COD DETA"] = det.Iddetallepedido.ToString();
                                                                    row["COD PROD"] = det.Producto.IdProducto.ToString();
                                                                    row["PRODUCTO"] = det.Producto.Nombre;
                                                                    row["PRECIO"] = det.Preciounitario.ToString();
                                                                    row["CANTIDAD"] = det.Cantidad.ToString().Replace(
                                                                                            det.Cantidad.ToString().Substring(
                                                                                                det.Cantidad.ToString().IndexOf('.'),
                                                                                                det.Cantidad.ToString().Length - det.Cantidad.ToString().IndexOf('.')
                                                                                                ), "");
                                                                    row["TOTAL"] = decimal.Round(det.Cantidad * det.Preciounitario, 2);
                                                                    row["PARA LLEVAR"] = det.Parallevar;
                                                                    row["ATENDIDO"] = det.Atendido;
                                                                    frmpedido.pedido.Rows.Add(row);
                                                                }

                                                                frmpedido.dg_pedido.DataSource = frmpedido.pedido;
                                                                frmpedido.dg_pedido.Columns[0].Visible = false;
                                                                frmpedido.dg_pedido.Columns[1].Visible = false;
                                                                frmpedido.dg_pedido.Columns[2].Visible = false;
                                                                frmpedido.vScrollBar_pedido.Maximum = frmpedido.dg_pedido.RowCount;
                                                                frmpedido.pedidoenmesa = pedidoenmesa;
                                                                frmpedido.btn.ColorTable = DevComponents.DotNetBar.eButtonColor.MagentaWithBackground;
                                                                frmpedido.btn.Refresh();
                                                                frmpedido.total();
                                                                txt_nombre.Focus();
                                                            }

                                                        }
                                                        else
                                                        {
                                                            MessageBox.Show("Problemas para registrar detalle...", "Advertencia");
                                                        }
                                                    }
                                                    /* }
                                                     else {

                                                         MessageBox.Show("Por favor actualice cantidades en el formulario anterior...","Advertencia");
                                                     }*/
                                                }
                                                else
                                                {

                                                    if (pedidoenmesa == null)
                                                    {
                                                        pedidoenmesa = new clsPedido()
                                                        {
                                                            Mesa = mesa,
                                                            Estado = 1,
                                                            Lista_detallepedido = new List<clsDetallePedido>()
                                                    {
                                                        new clsDetallePedido() {

                                                            Producto=new clsProducto() {
                                                                IdProducto=int.Parse(productos.Rows[dg_producto.CurrentCell.RowIndex].ItemArray[0].ToString())
                                                            } ,
                                                            Cantidad=decimal.Parse(productos.Rows[dg_producto.CurrentCell.RowIndex].ItemArray[3].ToString()),
                                                            Preciounitario=decimal.Parse(productos.Rows[dg_producto.CurrentCell.RowIndex].ItemArray[2].ToString()),
                                                            Descripcion=txt_descripcion.Text,
                                                            Estado=1

                                                        }
                                                    },
                                                        };

                                                        codpedido = admped.registrar_pedido(pedidoenmesa, usureg);

                                                        if (codpedido > 0)
                                                        {
                                                            pedidoenmesa = admped.listar_pedido_xidpedido(new clsPedido()
                                                            {
                                                                Idpedido = codpedido
                                                            });

                                                            if (pedidoenmesa != null)
                                                            {
                                                                frmpedido.pedido = new DataTable();
                                                                frmpedido.pedido.Columns.Add("COD PEDI");
                                                                frmpedido.pedido.Columns.Add("COD DETA");
                                                                frmpedido.pedido.Columns.Add("COD PROD");
                                                                frmpedido.pedido.Columns.Add("PRODUCTO");
                                                                frmpedido.pedido.Columns.Add("PRECIO");
                                                                frmpedido.pedido.Columns.Add("CANTIDAD");
                                                                frmpedido.pedido.Columns.Add("TOTAL");
                                                                frmpedido.pedido.Columns.Add("PARA LLEVAR", typeof(bool));
                                                                frmpedido.pedido.Columns.Add("ATENDIDO", typeof(bool));

                                                                foreach (clsDetallePedido det in pedidoenmesa.Lista_detallepedido)
                                                                {
                                                                    DataRow row = frmpedido.pedido.NewRow();

                                                                    row["COD PEDI"] = pedidoenmesa.Idpedido.ToString();
                                                                    row["COD DETA"] = det.Iddetallepedido.ToString();
                                                                    row["COD PROD"] = det.Producto.IdProducto.ToString();
                                                                    row["PRODUCTO"] = det.Producto.Nombre;
                                                                    row["PRECIO"] = det.Preciounitario.ToString();
                                                                    row["CANTIDAD"] = det.Cantidad.ToString().Replace(
                                                                                            det.Cantidad.ToString().Substring(
                                                                                                det.Cantidad.ToString().IndexOf('.'),
                                                                                                det.Cantidad.ToString().Length - det.Cantidad.ToString().IndexOf('.')
                                                                                                ), "");
                                                                    row["TOTAL"] = decimal.Round(det.Cantidad * det.Preciounitario, 2);
                                                                    row["PARA LLEVAR"] = det.Parallevar;
                                                                    row["ATENDIDO"] = det.Atendido;
                                                                    frmpedido.pedido.Rows.Add(row);
                                                                }

                                                                frmpedido.dg_pedido.DataSource = frmpedido.pedido;
                                                                frmpedido.dg_pedido.Columns[0].Visible = false;
                                                                frmpedido.dg_pedido.Columns[1].Visible = false;
                                                                frmpedido.dg_pedido.Columns[2].Visible = false;
                                                                frmpedido.vScrollBar_pedido.Maximum = frmpedido.dg_pedido.RowCount;
                                                                frmpedido.pedidoenmesa = pedidoenmesa;
                                                                frmpedido.btn.ColorTable = DevComponents.DotNetBar.eButtonColor.MagentaWithBackground;
                                                                frmpedido.btn.Refresh();
                                                                frmpedido.total();
                                                                txt_nombre.Focus();
                                                            }

                                                        }
                                                        else
                                                        {

                                                            MessageBox.Show("Problemas para registrar pedido...", "Advertencia");
                                                        }
                                                    }

                                                }

                                            }
                                        }
                                    }

                                    if (frmticketxmesa != null)
                                    {
                                        pedidoenmesa.Lista_detallepedido = new List<clsDetallePedido>()
                                      {
                                                        new clsDetallePedido() {

                                                            Producto=new clsProducto() {
                                                                IdProducto=int.Parse(productos.Rows[dg_producto.CurrentCell.RowIndex].ItemArray[0].ToString())
                                                            } ,
                                                            Cantidad=decimal.Parse(productos.Rows[dg_producto.CurrentCell.RowIndex].ItemArray[3].ToString()),
                                                            Preciounitario=decimal.Parse(productos.Rows[dg_producto.CurrentCell.RowIndex].ItemArray[2].ToString()),
                                                            Descripcion=txt_descripcion.Text,
                                                            Estado=2

                                                        }
                                      };

                                        if (admped.registrar_detallepedido(pedidoenmesa, usureg) > 0)
                                        {
                                            pedidoenmesa = admped.listar_pedido_xidpedidoxestado(pedidoenmesa);

                                            if (pedidoenmesa != null)
                                            {
                                                frmticketxmesa.pedido = new DataTable();
                                                frmticketxmesa.pedido.Columns.Add("COD PEDI");
                                                frmticketxmesa.pedido.Columns.Add("COD DETA");
                                                frmticketxmesa.pedido.Columns.Add("COD PROD");
                                                frmticketxmesa.pedido.Columns.Add("PRODUCTO");
                                                frmticketxmesa.pedido.Columns.Add("PRECIO");
                                                frmticketxmesa.pedido.Columns.Add("CANTIDAD");
                                                frmticketxmesa.pedido.Columns.Add("TOTAL");
                                                frmticketxmesa.pedido.Columns.Add("PARA LLEVAR", typeof(bool));
                                                frmticketxmesa.pedido.Columns.Add("ATENDIDO", typeof(bool));

                                                foreach (clsDetallePedido det in pedidoenmesa.Lista_detallepedido)
                                                {
                                                    DataRow row = frmticketxmesa.pedido.NewRow();

                                                    row["COD PEDI"] = pedidoenmesa.Idpedido.ToString();
                                                    row["COD DETA"] = det.Iddetallepedido.ToString();
                                                    row["COD PROD"] = det.Producto.IdProducto.ToString();
                                                    row["PRODUCTO"] = det.Producto.Nombre;
                                                    row["PRECIO"] = det.Preciounitario.ToString();
                                                    row["CANTIDAD"] = det.Cantidad.ToString().Replace(
                                                                            det.Cantidad.ToString().Substring(
                                                                                det.Cantidad.ToString().IndexOf('.'),
                                                                                det.Cantidad.ToString().Length - det.Cantidad.ToString().IndexOf('.')
                                                                                ), "");
                                                    row["TOTAL"] = decimal.Round(det.Cantidad * det.Preciounitario, 2);
                                                    row["PARA LLEVAR"] = det.Parallevar;
                                                    row["ATENDIDO"] = det.Atendido;
                                                    frmticketxmesa.pedido.Rows.Add(row);
                                                }

                                                frmticketxmesa.dg_pedido.DataSource = frmticketxmesa.pedido;
                                                frmticketxmesa.dg_pedido.Columns[0].Visible = false;
                                                frmticketxmesa.dg_pedido.Columns[1].Visible = false;
                                                frmticketxmesa.dg_pedido.Columns[2].Visible = false;
                                                frmticketxmesa.pedidoenmesa = pedidoenmesa;
                                                frmticketxmesa.btn_buscar.PerformClick();
                                                frmticketxmesa.dg_ticket.Rows[frmticketxmesa.indexpedido].Cells[2].Selected = true;
                                                frmticketxmesa.dg_ticket_CellClick(sender, new DataGridViewCellEventArgs(2, frmticketxmesa.indexpedido));
                                                /*frmticketxmesa.dg_ticket.CellClick+= new DataGridViewCellEventHandler(frmticketxmesa.dg_ticket_CellClick);*/
                                                frmticketxmesa.totales();
                                                frmticketxmesa.columna_bloqueda();
                                                txt_nombre.Focus();
                                            }

                                        }
                                        else
                                        {
                                            MessageBox.Show("Problemas para registrar detalle...", "Advertencia");
                                        }
                                    }
                                }
                                /*********************Otro tipo de venta**************

                                if (frmventa != null) {

                                    int index = 0;
                                    index =frmventa.dg_item_pedido.Rows.Add();

                                    MessageBox.Show(index.ToString());
                                }*/
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Hubo un error..." + ex.Message.ToString());
            }
        }

        private void cb_categoria_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                txt_nombre.Text = string.Empty;
                dg_producto.DataSource = null;
                //listar_productoxcategorianombre();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Hubo un error..." + ex.Message.ToString());
            }
        }

        /*************Mis Metodos******************/

        public bool es_numero(string cadena)
        {

            int retNum;
            try
            {

                retNum = int.Parse(cadena);
                return true;
            }
            catch (FormatException)
            {

                Console.WriteLine("{0} is not an integer");
                return false;
            }

        }    

        public void listar_productoxcategorianombre()
        {
            try
            {
                dg_producto.DataSource = null;

                if (lista_categoria == null)
                {

                    MessageBox.Show("Problemas para cargar categoria de productos...", "Advertencia");
                    return;
                }

                producto = new clsProducto()
                {

                    Nombre = txt_nombre.Text,
                    Categoria = categoria
                };

                productos = amdpro.listar_productoxcategorianombre(producto);

                if (productos != null)
                {

                    if (productos.Rows.Count > 0)
                    {

                        dg_producto.DataSource = productos;
                        dg_producto.Refresh();
                        dg_producto.Columns[idproducto.Index].ReadOnly = true;
                        dg_producto.Columns[nombreproducto.Index].ReadOnly = true;
                        dg_producto.Columns[preciounitario.Index].ReadOnly = true;
                        //vScrollBar_producto.Maximum = dg_producto.RowCount;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Hubo un error..." + ex.Message.ToString());
            }
        }

        private void btn_agregar_Click(object sender, EventArgs e)
        {
            int nselecionado = 0;
            int codpedido = -1;

            try
            {
                if (dg_producto.Rows.Count > 0)
                {

                    nselecionado = dg_producto.Rows.Cast<DataGridViewRow>().Where(
                                                                x => int.Parse(x.Cells["agregar"].Value.ToString()) == 1
                                                              ).Count();

                    if (nselecionado > 0)
                    {
                        for (int i = 0; i < dg_producto.Rows.Count; i++)
                        {
                            if (int.Parse(dg_producto.Rows[i].Cells["agregar"].Value.ToString()) == 1)
                            {
                                if (frmpedido != null)
                                {
                                    if (productos != null)
                                    {
                                        if (productos.Rows.Count > 0)
                                        {
                                            if (frmpedido.pedido != null)
                                            {
                                                /* var existe = (from x in frmpedido.pedido.AsEnumerable()
                                                               where x.Field<string>("COD PROD") == productos.Rows[dg_producto.CurrentCell.RowIndex].ItemArray[0].ToString()
                                                               select x
                                                               ).Any();*/
                                                // if (!existe)
                                                //{
                                                if (pedidoenmesa != null)
                                                {
                                                    pedidoenmesa.Lista_detallepedido = new List<clsDetallePedido>()
                                                                {
                                                                    new clsDetallePedido() {

                                                                        Producto=new clsProducto() {
                                                                            IdProducto=int.Parse(productos.Rows[i].ItemArray[0].ToString())
                                                                        } ,
                                                                        Cantidad=decimal.Parse(productos.Rows[i].ItemArray[3].ToString())==0?1:decimal.Parse(productos.Rows[i].ItemArray[3].ToString()),
                                                                        Preciounitario=decimal.Parse(productos.Rows[i].ItemArray[2].ToString()),
                                                                        Descripcion=txt_descripcion.Text,
                                                                        Estado=1

                                                                    }
                                                                };

                                                    if (admped.registrar_detallepedido(pedidoenmesa, usureg) > 0)
                                                    {
                                                        pedidoenmesa = admped.listar_pedido_xidpedido(pedidoenmesa);

                                                        if (pedidoenmesa != null)
                                                        {
                                                            frmpedido.pedido = new DataTable();
                                                            frmpedido.pedido.Columns.Add("COD PEDI");
                                                            frmpedido.pedido.Columns.Add("COD DETA");
                                                            frmpedido.pedido.Columns.Add("COD PROD");
                                                            frmpedido.pedido.Columns.Add("PRODUCTO");
                                                            frmpedido.pedido.Columns.Add("PRECIO");
                                                            frmpedido.pedido.Columns.Add("CANTIDAD");
                                                            frmpedido.pedido.Columns.Add("TOTAL");
                                                            frmpedido.pedido.Columns.Add("PARA LLEVAR", typeof(bool));
                                                            frmpedido.pedido.Columns.Add("ATENDIDO", typeof(bool));

                                                            foreach (clsDetallePedido det in pedidoenmesa.Lista_detallepedido)
                                                            {
                                                                DataRow row = frmpedido.pedido.NewRow();

                                                                row["COD PEDI"] = pedidoenmesa.Idpedido.ToString();
                                                                row["COD DETA"] = det.Iddetallepedido.ToString();
                                                                row["COD PROD"] = det.Producto.IdProducto.ToString();
                                                                row["PRODUCTO"] = det.Producto.Nombre;
                                                                row["PRECIO"] = det.Preciounitario.ToString();
                                                                row["CANTIDAD"] = det.Cantidad.ToString().Replace(
                                                                                        det.Cantidad.ToString().Substring(
                                                                                            det.Cantidad.ToString().IndexOf('.'),
                                                                                            det.Cantidad.ToString().Length - det.Cantidad.ToString().IndexOf('.')
                                                                                            ), "");
                                                                row["TOTAL"] = decimal.Round(det.Cantidad * det.Preciounitario, 2);
                                                                row["PARA LLEVAR"] = det.Parallevar;
                                                                row["ATENDIDO"] = det.Atendido;
                                                                frmpedido.pedido.Rows.Add(row);
                                                            }

                                                            frmpedido.dg_pedido.DataSource = frmpedido.pedido;
                                                            frmpedido.dg_pedido.Columns[0].Visible = false;
                                                            frmpedido.dg_pedido.Columns[1].Visible = false;
                                                            frmpedido.dg_pedido.Columns[2].Visible = false;
                                                            frmpedido.vScrollBar_pedido.Maximum = frmpedido.dg_pedido.RowCount;
                                                            frmpedido.pedidoenmesa = pedidoenmesa;
                                                            frmpedido.btn.ColorTable = DevComponents.DotNetBar.eButtonColor.MagentaWithBackground;
                                                            frmpedido.btn.Refresh();
                                                            frmpedido.total();
                                                            txt_nombre.Focus();
                                                        }

                                                    }
                                                    else
                                                    {
                                                        MessageBox.Show("Problemas para registrar detalle...", "Advertencia");
                                                    }
                                                }
                                                /* }
                                                 else {

                                                     MessageBox.Show("Por favor actualice cantidades en el formulario anterior...","Advertencia");
                                                 }*/
                                            }
                                            else
                                            {

                                                if (pedidoenmesa == null)
                                                {
                                                    pedidoenmesa = new clsPedido()
                                                    {
                                                        Mesa = mesa,
                                                        Estado = 1,
                                                        Lista_detallepedido = new List<clsDetallePedido>()
                                                                {
                                                                    new clsDetallePedido() {

                                                                        Producto=new clsProducto() {
                                                                            IdProducto=int.Parse(productos.Rows[i].ItemArray[0].ToString())
                                                                        } ,
                                                                        Cantidad=decimal.Parse(productos.Rows[i].ItemArray[3].ToString())==0?1:decimal.Parse(productos.Rows[i].ItemArray[3].ToString()),
                                                                        Preciounitario=decimal.Parse(productos.Rows[i].ItemArray[2].ToString()),
                                                                        Descripcion=txt_descripcion.Text,
                                                                        Estado=1

                                                                    }
                                                                },
                                                    };

                                                    codpedido = admped.registrar_pedido(pedidoenmesa, usureg);

                                                    if (codpedido > 0)
                                                    {
                                                        pedidoenmesa = admped.listar_pedido_xidpedido(new clsPedido()
                                                        {
                                                            Idpedido = codpedido
                                                        });

                                                        if (pedidoenmesa != null)
                                                        {
                                                            frmpedido.pedido = new DataTable();
                                                            frmpedido.pedido.Columns.Add("COD PEDI");
                                                            frmpedido.pedido.Columns.Add("COD DETA");
                                                            frmpedido.pedido.Columns.Add("COD PROD");
                                                            frmpedido.pedido.Columns.Add("PRODUCTO");
                                                            frmpedido.pedido.Columns.Add("PRECIO");
                                                            frmpedido.pedido.Columns.Add("CANTIDAD");
                                                            frmpedido.pedido.Columns.Add("TOTAL");
                                                            frmpedido.pedido.Columns.Add("PARA LLEVAR", typeof(bool));
                                                            frmpedido.pedido.Columns.Add("ATENDIDO", typeof(bool));

                                                            foreach (clsDetallePedido det in pedidoenmesa.Lista_detallepedido)
                                                            {
                                                                DataRow row = frmpedido.pedido.NewRow();

                                                                row["COD PEDI"] = pedidoenmesa.Idpedido.ToString();
                                                                row["COD DETA"] = det.Iddetallepedido.ToString();
                                                                row["COD PROD"] = det.Producto.IdProducto.ToString();
                                                                row["PRODUCTO"] = det.Producto.Nombre;
                                                                row["PRECIO"] = det.Preciounitario.ToString();
                                                                row["CANTIDAD"] = det.Cantidad.ToString().Replace(
                                                                                        det.Cantidad.ToString().Substring(
                                                                                            det.Cantidad.ToString().IndexOf('.'),
                                                                                            det.Cantidad.ToString().Length - det.Cantidad.ToString().IndexOf('.')
                                                                                            ), "");
                                                                row["TOTAL"] = decimal.Round(det.Cantidad * det.Preciounitario, 2);
                                                                row["PARA LLEVAR"] = det.Parallevar;
                                                                row["ATENDIDO"] = det.Atendido;
                                                                frmpedido.pedido.Rows.Add(row);
                                                            }

                                                            frmpedido.dg_pedido.DataSource = frmpedido.pedido;
                                                            frmpedido.dg_pedido.Columns[0].Visible = false;
                                                            frmpedido.dg_pedido.Columns[1].Visible = false;
                                                            frmpedido.dg_pedido.Columns[2].Visible = false;
                                                            frmpedido.vScrollBar_pedido.Maximum = frmpedido.dg_pedido.RowCount;
                                                            frmpedido.pedidoenmesa = pedidoenmesa;
                                                            frmpedido.btn.ColorTable = DevComponents.DotNetBar.eButtonColor.MagentaWithBackground;
                                                            frmpedido.btn.Refresh();
                                                            frmpedido.total();
                                                            txt_nombre.Focus();
                                                        }

                                                    }
                                                    else
                                                    {

                                                        MessageBox.Show("Problemas para registrar pedido...", "Advertencia");
                                                    }
                                                }

                                            }

                                        }
                                    }
                                }

                                if (frmticketxmesa != null)
                                {
                                    pedidoenmesa.Lista_detallepedido = new List<clsDetallePedido>()
                                                  {
                                                                    new clsDetallePedido() {

                                                                        Producto=new clsProducto() {
                                                                            IdProducto=int.Parse(productos.Rows[i].ItemArray[0].ToString())
                                                                        } ,
                                                                        Cantidad=decimal.Parse(productos.Rows[i].ItemArray[3].ToString())==0?1:decimal.Parse(productos.Rows[i].ItemArray[3].ToString()),
                                                                        Preciounitario=decimal.Parse(productos.Rows[i].ItemArray[2].ToString()),
                                                                        Descripcion=txt_descripcion.Text,
                                                                        Estado=2

                                                                    }
                                                  };

                                    if (admped.registrar_detallepedido(pedidoenmesa, usureg) > 0)
                                    {
                                        pedidoenmesa = admped.listar_pedido_xidpedidoxestado(pedidoenmesa);

                                        if (pedidoenmesa != null)
                                        {
                                            frmticketxmesa.pedido = new DataTable();
                                            frmticketxmesa.pedido.Columns.Add("COD PEDI");
                                            frmticketxmesa.pedido.Columns.Add("COD DETA");
                                            frmticketxmesa.pedido.Columns.Add("COD PROD");
                                            frmticketxmesa.pedido.Columns.Add("PRODUCTO");
                                            frmticketxmesa.pedido.Columns.Add("PRECIO");
                                            frmticketxmesa.pedido.Columns.Add("CANTIDAD");
                                            frmticketxmesa.pedido.Columns.Add("TOTAL");
                                            frmticketxmesa.pedido.Columns.Add("PARA LLEVAR", typeof(bool));
                                            frmticketxmesa.pedido.Columns.Add("ATENDIDO", typeof(bool));

                                            foreach (clsDetallePedido det in pedidoenmesa.Lista_detallepedido)
                                            {
                                                DataRow row = frmticketxmesa.pedido.NewRow();

                                                row["COD PEDI"] = pedidoenmesa.Idpedido.ToString();
                                                row["COD DETA"] = det.Iddetallepedido.ToString();
                                                row["COD PROD"] = det.Producto.IdProducto.ToString();
                                                row["PRODUCTO"] = det.Producto.Nombre;
                                                row["PRECIO"] = det.Preciounitario.ToString();
                                                row["CANTIDAD"] = det.Cantidad.ToString().Replace(
                                                                        det.Cantidad.ToString().Substring(
                                                                            det.Cantidad.ToString().IndexOf('.'),
                                                                            det.Cantidad.ToString().Length - det.Cantidad.ToString().IndexOf('.')
                                                                            ), "");
                                                row["TOTAL"] = decimal.Round(det.Cantidad * det.Preciounitario, 2);
                                                row["PARA LLEVAR"] = det.Parallevar;
                                                row["ATENDIDO"] = det.Atendido;
                                                frmticketxmesa.pedido.Rows.Add(row);
                                            }

                                            frmticketxmesa.dg_pedido.DataSource = frmticketxmesa.pedido;
                                            frmticketxmesa.dg_pedido.Columns[0].Visible = false;
                                            frmticketxmesa.dg_pedido.Columns[1].Visible = false;
                                            frmticketxmesa.dg_pedido.Columns[2].Visible = false;
                                            frmticketxmesa.pedidoenmesa = pedidoenmesa;
                                            frmticketxmesa.btn_buscar.PerformClick();
                                            frmticketxmesa.dg_ticket.Rows[frmticketxmesa.indexpedido].Cells[2].Selected = true;
                                            frmticketxmesa.dg_ticket_CellClick(sender, new DataGridViewCellEventArgs(2, frmticketxmesa.indexpedido));
                                            /*frmticketxmesa.dg_ticket.CellClick+= new DataGridViewCellEventHandler(frmticketxmesa.dg_ticket_CellClick);*/
                                            frmticketxmesa.totales();
                                            frmticketxmesa.columna_bloqueda();
                                            txt_nombre.Focus();
                                        }

                                    }
                                    else
                                    {
                                        MessageBox.Show("Problemas para registrar detalle...", "Advertencia");
                                    }
                                }


                                /*TextBox tb = new TextBox();
                                tb.Text = "1";
                                cantidad_KeyDown(tb, new PreviewKeyDownEventArgs(Keys.Enter));*/

                                dg_producto.Rows[i].Cells["agregar"].Value = 0;
                                txt_nombre.Text = string.Empty;
                                txt_descripcion.Text = string.Empty;

                            }

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Hubo un error..." + ex.Message.ToString());
            }
        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void listar_categoria(int separator)
        {

            int top = 10;
            int left = 10;
            int il = 1;

            try
            {
                pn_contenedor.Controls.Clear();
                lista_categoria = admcate.listar_categoria();

                if (lista_categoria != null)
                {

                    if (lista_categoria.Count > 0)
                    {

                        foreach (clsCategoria c in lista_categoria)
                        {

                            btn = new ButtonX();
                            btn.Name = c.Idcategoria.ToString();
                            btn.Text = c.Nombre;
                            btn.Width = 75;
                            btn.Height = 50;
                            btn.Top = top;
                            btn.Left = left;
                            btn.Click += btn_Click;
                            // btn.Image = Image.FromFile(@"img/mesa1.jpg");
                            btn.Font = new Font(this.Font, FontStyle.Bold);

                            if (il == separator)
                            {
                                
                                il = 1;
                                left = 10;
                                top += 60;

                                
                            }
                            else
                            {

                                il++;
                                left += 80;
                            }
                            pn_contenedor.Controls.Add(btn);
                           
                        }

                        if (pn_contenedor.Controls.Count > 0)
                        {
                            pn_contenedor.Controls.Add(new PanelEx() { Height = 1, Width =300, Top = top + 5, Left = left });
                            ((ButtonX)pn_contenedor.Controls[0]).Focus();
                            btn_Click((ButtonX)pn_contenedor.Controls[0], null);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Hubo un error..." + ex.Message.ToString());
            }
        }

        public void btn_Click(object sender, EventArgs e)
        {
            try
            {
                txt_nombre.Text = string.Empty;
                txt_plato.Text = string.Empty;
                txt_descripcion.Text = string.Empty;
                btn = ((ButtonX)sender);
                categoria = new clsCategoria() { Idcategoria = int.Parse(btn.Name), Nombre = btn.Text };
                listar_productoxcategorianombre();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Hubo un error..." + ex.Message.ToString());
            }
        }

        private void dg_producto_Scroll(object sender, ScrollEventArgs e)
        {
        //    try
        //    {
        //        if (dg_producto.Rows.Count > 0)
        //        {
        //            if (e.NewValue >= 0)
        //            {
        //                vScrollBar_producto.Value = e.NewValue;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show("Hubo un error..." + ex.Message.ToString());
        //    }
        }

        private void vScrollBar_producto_Scroll(object sender, ScrollEventArgs e)
        {
            try
            {
                if (dg_producto.Rows.Count > 0)
                {
                    if (e.NewValue >= 0)
                    {
                        dg_producto.FirstDisplayedScrollingRowIndex = e.NewValue;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Hubo un error..." + ex.Message.ToString());
            }
        }

        private void dg_producto_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dg_producto.Rows.Count > 0)
                {

                    if (e.RowIndex != -1)
                    {

                        if (e.ColumnIndex == dg_producto.Columns["nombreproducto"].Index || e.ColumnIndex==agregar.Index)
                        {

                            dg_producto.Rows[e.RowIndex].Cells["agregar"].Value = (int.Parse(dg_producto.Rows[e.RowIndex].Cells["agregar"].Value.ToString()) == 1 ? 0 : 1);
                            if (int.Parse(dg_producto.Rows[e.RowIndex].Cells["agregar"].Value.ToString()) == 0)
                            {

                                txt_plato.Text = string.Empty;
                            }
                            else
                            {

                                txt_plato.Text = dg_producto.Rows[e.RowIndex].Cells["nombreproducto"].Value.ToString();
                                
                            }

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Hubo un error..." + ex.Message.ToString());
            }
        }

        private void txt_descripcion_TextChanged(object sender, EventArgs e)
        {
            txt_descripcion.CharacterCasing = CharacterCasing.Upper;
        }

        private void frmDetallePedido_Resize(object sender, EventArgs e)
        {
            try
            {
                if (WindowState == FormWindowState.Maximized)
                {
                    listar_categoria(4);
                }
                if (WindowState == FormWindowState.Normal)
                {

                    listar_categoria(4);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Hubo un error..." + ex.Message.ToString());
            }

        }
    }
}
