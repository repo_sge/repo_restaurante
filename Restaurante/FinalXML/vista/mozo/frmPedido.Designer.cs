﻿namespace FinalXML.vista.mozo
{
    partial class frmPedido
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPedido));
            this.btn_item_quitar = new DevComponents.DotNetBar.ButtonItem();
            this.btn_item_anularpediomesa = new DevComponents.DotNetBar.ButtonItem();
            this.btn_item_cambio_mesa = new DevComponents.DotNetBar.ButtonItem();
            this.btn_agrupar_mesa = new DevComponents.DotNetBar.ButtonItem();
            this.panelEx1 = new DevComponents.DotNetBar.PanelEx();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.panel_mesa = new DevComponents.DotNetBar.PanelEx();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.btn_dis = new DevComponents.DotNetBar.ButtonX();
            this.btn_ocu = new DevComponents.DotNetBar.ButtonX();
            this.pn_piso = new DevComponents.DotNetBar.PanelEx();
            this.btn_comanda = new DevComponents.DotNetBar.ButtonX();
            this.btn_salir = new DevComponents.DotNetBar.ButtonX();
            this.btn_agrupar_mesas = new DevComponents.DotNetBar.ButtonX();
            this.btn_cambiar_mesa = new DevComponents.DotNetBar.ButtonX();
            this.btn_anular_pedido = new DevComponents.DotNetBar.ButtonX();
            this.lb_ticket = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.btn_imprimir_ticket = new DevComponents.DotNetBar.ButtonX();
            this.btn_anular_ticket = new DevComponents.DotNetBar.ButtonX();
            this.contex_menu_mesa = new DevComponents.DotNetBar.ContextMenuBar();
            this.btn_menu_mesa = new DevComponents.DotNetBar.ButtonItem();
            this.lb_mesa = new DevComponents.DotNetBar.LabelX();
            this.btn_generar_ticket = new DevComponents.DotNetBar.ButtonX();
            this.lb_monto = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.context_menu = new DevComponents.DotNetBar.ContextMenuBar();
            this.btn_menu = new DevComponents.DotNetBar.ButtonItem();
            this.dg_pedido = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.btn_quitar = new DevComponents.DotNetBar.ButtonX();
            this.btn_agregar = new DevComponents.DotNetBar.ButtonX();
            this.groupPanel3 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.vScrollBar_pedido = new DevComponents.DotNetBar.VScrollBarAdv();
            this.panelEx1.SuspendLayout();
            this.groupPanel2.SuspendLayout();
            this.groupPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.contex_menu_mesa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.context_menu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dg_pedido)).BeginInit();
            this.groupPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_item_quitar
            // 
            this.btn_item_quitar.Name = "btn_item_quitar";
            this.btn_item_quitar.Text = "Quitar Item";
            this.btn_item_quitar.Click += new System.EventHandler(this.btn_item_quitar_Click);
            // 
            // btn_item_anularpediomesa
            // 
            this.btn_item_anularpediomesa.Name = "btn_item_anularpediomesa";
            this.btn_item_anularpediomesa.Text = "Anular Pedido";
            this.btn_item_anularpediomesa.Click += new System.EventHandler(this.btn_item_anularpediomesa_Click);
            // 
            // btn_item_cambio_mesa
            // 
            this.btn_item_cambio_mesa.Name = "btn_item_cambio_mesa";
            this.btn_item_cambio_mesa.Text = "Cambiar Mesa";
            this.btn_item_cambio_mesa.Click += new System.EventHandler(this.btn_item_cambio_mesa_Click);
            // 
            // btn_agrupar_mesa
            // 
            this.btn_agrupar_mesa.Name = "btn_agrupar_mesa";
            this.btn_agrupar_mesa.Text = "Agrupar Mesas";
            this.btn_agrupar_mesa.Click += new System.EventHandler(this.btn_agrupar_mesa_Click);
            // 
            // panelEx1
            // 
            this.panelEx1.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx1.Controls.Add(this.groupPanel2);
            this.panelEx1.Controls.Add(this.groupPanel1);
            this.panelEx1.Controls.Add(this.btn_comanda);
            this.panelEx1.Controls.Add(this.btn_salir);
            this.panelEx1.Controls.Add(this.btn_agrupar_mesas);
            this.panelEx1.Controls.Add(this.btn_cambiar_mesa);
            this.panelEx1.Controls.Add(this.btn_anular_pedido);
            this.panelEx1.Controls.Add(this.lb_ticket);
            this.panelEx1.Controls.Add(this.labelX2);
            this.panelEx1.Controls.Add(this.btn_imprimir_ticket);
            this.panelEx1.Controls.Add(this.btn_anular_ticket);
            this.panelEx1.Controls.Add(this.contex_menu_mesa);
            this.panelEx1.Controls.Add(this.lb_mesa);
            this.panelEx1.Controls.Add(this.btn_generar_ticket);
            this.panelEx1.Controls.Add(this.lb_monto);
            this.panelEx1.Controls.Add(this.labelX1);
            this.panelEx1.Controls.Add(this.context_menu);
            this.panelEx1.Controls.Add(this.btn_quitar);
            this.panelEx1.Controls.Add(this.btn_agregar);
            this.panelEx1.Controls.Add(this.groupPanel3);
            this.panelEx1.DisabledBackColor = System.Drawing.Color.Empty;
            this.panelEx1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEx1.Location = new System.Drawing.Point(0, 0);
            this.panelEx1.Name = "panelEx1";
            this.panelEx1.Size = new System.Drawing.Size(1047, 484);
            this.panelEx1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx1.Style.GradientAngle = 90;
            this.panelEx1.TabIndex = 0;
            // 
            // groupPanel2
            // 
            this.groupPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupPanel2.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.panel_mesa);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Location = new System.Drawing.Point(10, 100);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.Size = new System.Drawing.Size(404, 303);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 29;
            // 
            // panel_mesa
            // 
            this.panel_mesa.AutoScroll = true;
            this.panel_mesa.CanvasColor = System.Drawing.SystemColors.Control;
            this.panel_mesa.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panel_mesa.DisabledBackColor = System.Drawing.Color.Empty;
            this.panel_mesa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_mesa.Location = new System.Drawing.Point(0, 0);
            this.panel_mesa.Name = "panel_mesa";
            this.panel_mesa.Size = new System.Drawing.Size(398, 297);
            this.panel_mesa.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panel_mesa.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panel_mesa.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panel_mesa.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panel_mesa.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panel_mesa.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panel_mesa.Style.GradientAngle = 90;
            this.panel_mesa.TabIndex = 4;
            // 
            // groupPanel1
            // 
            this.groupPanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.labelX3);
            this.groupPanel1.Controls.Add(this.btn_dis);
            this.groupPanel1.Controls.Add(this.btn_ocu);
            this.groupPanel1.Controls.Add(this.pn_piso);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(10, 3);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.Size = new System.Drawing.Size(404, 91);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 28;
            // 
            // labelX3
            // 
            this.labelX3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Font = new System.Drawing.Font("Segoe UI", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX3.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.labelX3.Location = new System.Drawing.Point(32, -3);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(112, 35);
            this.labelX3.TabIndex = 16;
            this.labelX3.Text = "MESAS";
            this.labelX3.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // btn_dis
            // 
            this.btn_dis.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_dis.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_dis.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_dis.Location = new System.Drawing.Point(32, 37);
            this.btn_dis.Name = "btn_dis";
            this.btn_dis.Size = new System.Drawing.Size(120, 19);
            this.btn_dis.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_dis.TabIndex = 0;
            this.btn_dis.Text = "Disponible";
            // 
            // btn_ocu
            // 
            this.btn_ocu.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_ocu.ColorTable = DevComponents.DotNetBar.eButtonColor.MagentaWithBackground;
            this.btn_ocu.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ocu.Location = new System.Drawing.Point(32, 62);
            this.btn_ocu.Name = "btn_ocu";
            this.btn_ocu.Size = new System.Drawing.Size(120, 20);
            this.btn_ocu.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_ocu.TabIndex = 1;
            this.btn_ocu.Text = "Ocupada";
            // 
            // pn_piso
            // 
            this.pn_piso.AutoScroll = true;
            this.pn_piso.CanvasColor = System.Drawing.SystemColors.Control;
            this.pn_piso.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pn_piso.DisabledBackColor = System.Drawing.Color.Empty;
            this.pn_piso.Location = new System.Drawing.Point(167, 0);
            this.pn_piso.Name = "pn_piso";
            this.pn_piso.Size = new System.Drawing.Size(228, 85);
            this.pn_piso.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pn_piso.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pn_piso.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pn_piso.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pn_piso.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pn_piso.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pn_piso.Style.GradientAngle = 90;
            this.pn_piso.TabIndex = 0;
            // 
            // btn_comanda
            // 
            this.btn_comanda.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_comanda.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btn_comanda.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_comanda.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_comanda.Image = global::FinalXML.Properties.Resources.comanda;
            this.btn_comanda.ImagePosition = DevComponents.DotNetBar.eImagePosition.Right;
            this.btn_comanda.Location = new System.Drawing.Point(625, 409);
            this.btn_comanda.Name = "btn_comanda";
            this.btn_comanda.Size = new System.Drawing.Size(108, 68);
            this.btn_comanda.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_comanda.TabIndex = 27;
            this.btn_comanda.Text = "Imprimir Comanda";
            this.btn_comanda.Click += new System.EventHandler(this.btn_comanda_Click);
            // 
            // btn_salir
            // 
            this.btn_salir.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_salir.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btn_salir.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_salir.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_salir.Image = global::FinalXML.Properties.Resources.cancelar;
            this.btn_salir.Location = new System.Drawing.Point(8, 409);
            this.btn_salir.Name = "btn_salir";
            this.btn_salir.Size = new System.Drawing.Size(97, 68);
            this.btn_salir.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_salir.TabIndex = 25;
            this.btn_salir.Text = "Salir";
            this.btn_salir.Click += new System.EventHandler(this.btn_salir_Click);
            // 
            // btn_agrupar_mesas
            // 
            this.btn_agrupar_mesas.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_agrupar_mesas.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btn_agrupar_mesas.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_agrupar_mesas.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_agrupar_mesas.Image = global::FinalXML.Properties.Resources.gruposmesa;
            this.btn_agrupar_mesas.ImagePosition = DevComponents.DotNetBar.eImagePosition.Right;
            this.btn_agrupar_mesas.Location = new System.Drawing.Point(523, 409);
            this.btn_agrupar_mesas.Name = "btn_agrupar_mesas";
            this.btn_agrupar_mesas.Size = new System.Drawing.Size(97, 68);
            this.btn_agrupar_mesas.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_agrupar_mesas.TabIndex = 23;
            this.btn_agrupar_mesas.Text = "Agrupar Mesas";
            this.btn_agrupar_mesas.Click += new System.EventHandler(this.btn_agrupar_mesas_Click);
            // 
            // btn_cambiar_mesa
            // 
            this.btn_cambiar_mesa.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_cambiar_mesa.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btn_cambiar_mesa.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_cambiar_mesa.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_cambiar_mesa.Image = global::FinalXML.Properties.Resources.mesacambio;
            this.btn_cambiar_mesa.ImagePosition = DevComponents.DotNetBar.eImagePosition.Right;
            this.btn_cambiar_mesa.Location = new System.Drawing.Point(420, 409);
            this.btn_cambiar_mesa.Name = "btn_cambiar_mesa";
            this.btn_cambiar_mesa.Size = new System.Drawing.Size(97, 68);
            this.btn_cambiar_mesa.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_cambiar_mesa.TabIndex = 22;
            this.btn_cambiar_mesa.Text = "Cambiar Mesa";
            this.btn_cambiar_mesa.Click += new System.EventHandler(this.btn_cambiar_mesa_Click);
            // 
            // btn_anular_pedido
            // 
            this.btn_anular_pedido.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_anular_pedido.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btn_anular_pedido.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_anular_pedido.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_anular_pedido.Image = global::FinalXML.Properties.Resources.anularpedido;
            this.btn_anular_pedido.ImagePosition = DevComponents.DotNetBar.eImagePosition.Right;
            this.btn_anular_pedido.Location = new System.Drawing.Point(317, 409);
            this.btn_anular_pedido.Name = "btn_anular_pedido";
            this.btn_anular_pedido.Size = new System.Drawing.Size(97, 68);
            this.btn_anular_pedido.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_anular_pedido.TabIndex = 21;
            this.btn_anular_pedido.Text = "Anular Pedido";
            this.btn_anular_pedido.Click += new System.EventHandler(this.btn_anular_pedido_Click);
            // 
            // lb_ticket
            // 
            this.lb_ticket.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lb_ticket.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lb_ticket.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lb_ticket.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold);
            this.lb_ticket.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.lb_ticket.Location = new System.Drawing.Point(857, 9);
            this.lb_ticket.Name = "lb_ticket";
            this.lb_ticket.Size = new System.Drawing.Size(174, 28);
            this.lb_ticket.TabIndex = 15;
            this.lb_ticket.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX2
            // 
            this.labelX2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold);
            this.labelX2.Location = new System.Drawing.Point(772, 12);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(83, 23);
            this.labelX2.TabIndex = 14;
            this.labelX2.Text = "#TICKET :";
            // 
            // btn_imprimir_ticket
            // 
            this.btn_imprimir_ticket.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_imprimir_ticket.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btn_imprimir_ticket.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_imprimir_ticket.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_imprimir_ticket.Image = global::FinalXML.Properties.Resources.imprimir;
            this.btn_imprimir_ticket.ImagePosition = DevComponents.DotNetBar.eImagePosition.Right;
            this.btn_imprimir_ticket.Location = new System.Drawing.Point(942, 409);
            this.btn_imprimir_ticket.Name = "btn_imprimir_ticket";
            this.btn_imprimir_ticket.Size = new System.Drawing.Size(97, 68);
            this.btn_imprimir_ticket.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_imprimir_ticket.TabIndex = 13;
            this.btn_imprimir_ticket.Text = "Imprimir Ticket";
            this.btn_imprimir_ticket.Click += new System.EventHandler(this.btn_imprimir_ticket_Click);
            // 
            // btn_anular_ticket
            // 
            this.btn_anular_ticket.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_anular_ticket.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btn_anular_ticket.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_anular_ticket.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_anular_ticket.Image = global::FinalXML.Properties.Resources.anular;
            this.btn_anular_ticket.ImagePosition = DevComponents.DotNetBar.eImagePosition.Right;
            this.btn_anular_ticket.Location = new System.Drawing.Point(839, 409);
            this.btn_anular_ticket.Name = "btn_anular_ticket";
            this.btn_anular_ticket.Size = new System.Drawing.Size(97, 68);
            this.btn_anular_ticket.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_anular_ticket.TabIndex = 12;
            this.btn_anular_ticket.Text = "Anular Ticket";
            this.btn_anular_ticket.Click += new System.EventHandler(this.btn_anular_ticket_Click);
            // 
            // contex_menu_mesa
            // 
            this.contex_menu_mesa.AntiAlias = true;
            this.contex_menu_mesa.DockSide = DevComponents.DotNetBar.eDockSide.Document;
            this.contex_menu_mesa.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.contex_menu_mesa.IsMaximized = false;
            this.contex_menu_mesa.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btn_menu_mesa});
            this.contex_menu_mesa.Location = new System.Drawing.Point(1280, 47);
            this.contex_menu_mesa.Name = "contex_menu_mesa";
            this.contex_menu_mesa.Size = new System.Drawing.Size(75, 25);
            this.contex_menu_mesa.Stretch = true;
            this.contex_menu_mesa.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.contex_menu_mesa.TabIndex = 11;
            this.contex_menu_mesa.TabStop = false;
            this.contex_menu_mesa.Text = "contextMenuBar1";
            // 
            // btn_menu_mesa
            // 
            this.btn_menu_mesa.AutoExpandOnClick = true;
            this.btn_menu_mesa.Name = "btn_menu_mesa";
            this.btn_menu_mesa.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btn_item_anularpediomesa,
            this.btn_item_cambio_mesa,
            this.btn_agrupar_mesa});
            // 
            // lb_mesa
            // 
            this.lb_mesa.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.lb_mesa.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lb_mesa.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold);
            this.lb_mesa.Location = new System.Drawing.Point(420, 15);
            this.lb_mesa.Name = "lb_mesa";
            this.lb_mesa.Size = new System.Drawing.Size(128, 23);
            this.lb_mesa.TabIndex = 10;
            this.lb_mesa.Text = "#MESA";
            // 
            // btn_generar_ticket
            // 
            this.btn_generar_ticket.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_generar_ticket.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btn_generar_ticket.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_generar_ticket.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_generar_ticket.Image = global::FinalXML.Properties.Resources.generar;
            this.btn_generar_ticket.ImagePosition = DevComponents.DotNetBar.eImagePosition.Right;
            this.btn_generar_ticket.Location = new System.Drawing.Point(736, 409);
            this.btn_generar_ticket.Name = "btn_generar_ticket";
            this.btn_generar_ticket.Size = new System.Drawing.Size(97, 68);
            this.btn_generar_ticket.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_generar_ticket.TabIndex = 9;
            this.btn_generar_ticket.Text = "Generar Ticket";
            this.btn_generar_ticket.Click += new System.EventHandler(this.btn_generar_ticket_Click);
            // 
            // lb_monto
            // 
            this.lb_monto.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.lb_monto.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lb_monto.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold);
            this.lb_monto.Location = new System.Drawing.Point(636, 10);
            this.lb_monto.Name = "lb_monto";
            this.lb_monto.Size = new System.Drawing.Size(74, 28);
            this.lb_monto.TabIndex = 8;
            this.lb_monto.Text = "0.00";
            this.lb_monto.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX1
            // 
            this.labelX1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold);
            this.labelX1.Location = new System.Drawing.Point(554, 14);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(83, 23);
            this.labelX1.TabIndex = 7;
            this.labelX1.Text = "MONTO :";
            // 
            // context_menu
            // 
            this.context_menu.AntiAlias = true;
            this.context_menu.DockSide = DevComponents.DotNetBar.eDockSide.Document;
            this.context_menu.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.context_menu.IsMaximized = false;
            this.context_menu.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btn_menu});
            this.context_menu.Location = new System.Drawing.Point(1280, 16);
            this.context_menu.Name = "context_menu";
            this.context_menu.Size = new System.Drawing.Size(75, 25);
            this.context_menu.Stretch = true;
            this.context_menu.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.context_menu.TabIndex = 6;
            this.context_menu.TabStop = false;
            this.context_menu.Text = "contextMenuBar1";
            // 
            // btn_menu
            // 
            this.btn_menu.AutoExpandOnClick = true;
            this.btn_menu.Name = "btn_menu";
            this.btn_menu.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btn_item_quitar});
            // 
            // dg_pedido
            // 
            this.dg_pedido.AllowUserToAddRows = false;
            this.dg_pedido.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dg_pedido.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dg_pedido.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dg_pedido.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dg_pedido.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.context_menu.SetContextMenuEx(this.dg_pedido, this.btn_menu);
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dg_pedido.DefaultCellStyle = dataGridViewCellStyle2;
            this.dg_pedido.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dg_pedido.EnableHeadersVisualStyles = false;
            this.dg_pedido.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dg_pedido.Location = new System.Drawing.Point(0, 0);
            this.dg_pedido.Margin = new System.Windows.Forms.Padding(5, 3, 3, 3);
            this.dg_pedido.Name = "dg_pedido";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dg_pedido.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dg_pedido.RowHeadersVisible = false;
            this.dg_pedido.RowTemplate.Height = 30;
            this.dg_pedido.Size = new System.Drawing.Size(609, 354);
            this.dg_pedido.TabIndex = 1;
            this.dg_pedido.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dg_pedido_CellClick);
            this.dg_pedido.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dg_pedido_CellDoubleClick);
            this.dg_pedido.CurrentCellChanged += new System.EventHandler(this.dg_pedido_CurrentCellChanged);
            this.dg_pedido.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dg_pedido_EditingControlShowing);
            this.dg_pedido.Scroll += new System.Windows.Forms.ScrollEventHandler(this.dg_pedido_Scroll);
            // 
            // btn_quitar
            // 
            this.btn_quitar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_quitar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btn_quitar.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_quitar.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_quitar.Image = global::FinalXML.Properties.Resources.quitar;
            this.btn_quitar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Right;
            this.btn_quitar.Location = new System.Drawing.Point(214, 409);
            this.btn_quitar.Name = "btn_quitar";
            this.btn_quitar.Size = new System.Drawing.Size(97, 68);
            this.btn_quitar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_quitar.TabIndex = 5;
            this.btn_quitar.Text = "Quitar Plato";
            this.btn_quitar.Click += new System.EventHandler(this.btn_quitar_Click);
            // 
            // btn_agregar
            // 
            this.btn_agregar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_agregar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btn_agregar.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_agregar.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_agregar.Image = global::FinalXML.Properties.Resources.add;
            this.btn_agregar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Right;
            this.btn_agregar.Location = new System.Drawing.Point(111, 409);
            this.btn_agregar.Name = "btn_agregar";
            this.btn_agregar.Size = new System.Drawing.Size(97, 68);
            this.btn_agregar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_agregar.TabIndex = 4;
            this.btn_agregar.Text = "Agregar Plato";
            this.btn_agregar.Click += new System.EventHandler(this.btn_agregar_Click);
            // 
            // groupPanel3
            // 
            this.groupPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupPanel3.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel3.Controls.Add(this.vScrollBar_pedido);
            this.groupPanel3.Controls.Add(this.dg_pedido);
            this.groupPanel3.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel3.Location = new System.Drawing.Point(420, 43);
            this.groupPanel3.Name = "groupPanel3";
            this.groupPanel3.Size = new System.Drawing.Size(615, 360);
            // 
            // 
            // 
            this.groupPanel3.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel3.Style.BackColorGradientAngle = 90;
            this.groupPanel3.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel3.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderBottomWidth = 1;
            this.groupPanel3.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel3.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderLeftWidth = 1;
            this.groupPanel3.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderRightWidth = 1;
            this.groupPanel3.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderTopWidth = 1;
            this.groupPanel3.Style.CornerDiameter = 4;
            this.groupPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel3.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel3.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel3.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel3.TabIndex = 3;
            // 
            // vScrollBar_pedido
            // 
            this.vScrollBar_pedido.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.vScrollBar_pedido.Location = new System.Drawing.Point(572, 1);
            this.vScrollBar_pedido.Name = "vScrollBar_pedido";
            this.vScrollBar_pedido.Size = new System.Drawing.Size(36, 350);
            this.vScrollBar_pedido.TabIndex = 2;
            this.vScrollBar_pedido.Scroll += new System.Windows.Forms.ScrollEventHandler(this.vScrollBar_producto_Scroll);
            // 
            // frmPedido
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1047, 484);
            this.Controls.Add(this.panelEx1);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmPedido";
            this.Text = "Pedidos";
            this.Load += new System.EventHandler(this.frmPedido_Load);
            this.Shown += new System.EventHandler(this.frmPedido_Shown);
            this.panelEx1.ResumeLayout(false);
            this.groupPanel2.ResumeLayout(false);
            this.groupPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.contex_menu_mesa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.context_menu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dg_pedido)).EndInit();
            this.groupPanel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.PanelEx panelEx1;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel3;
        private DevComponents.DotNetBar.ButtonX btn_ocu;
        private DevComponents.DotNetBar.ButtonX btn_dis;
        private DevComponents.DotNetBar.ButtonX btn_agregar;
        public DevComponents.DotNetBar.Controls.DataGridViewX dg_pedido;
        private DevComponents.DotNetBar.ButtonX btn_quitar;
        private DevComponents.DotNetBar.ContextMenuBar context_menu;
        private DevComponents.DotNetBar.ButtonItem btn_menu;
        private DevComponents.DotNetBar.ButtonItem btn_item_quitar;
        private DevComponents.DotNetBar.ButtonX btn_generar_ticket;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.ContextMenuBar contex_menu_mesa;
        private DevComponents.DotNetBar.ButtonItem btn_menu_mesa;
        private DevComponents.DotNetBar.ButtonItem btn_item_anularpediomesa;
        private DevComponents.DotNetBar.ButtonItem btn_agrupar_mesa;
        public DevComponents.DotNetBar.PanelEx panel_mesa;
        private DevComponents.DotNetBar.ButtonItem btn_item_cambio_mesa;
        public DevComponents.DotNetBar.LabelX lb_mesa;
        private DevComponents.DotNetBar.ButtonX btn_anular_ticket;
        private DevComponents.DotNetBar.LabelX lb_ticket;
        private DevComponents.DotNetBar.LabelX labelX2;
        public DevComponents.DotNetBar.LabelX lb_monto;
        private DevComponents.DotNetBar.ButtonX btn_anular_pedido;
        private DevComponents.DotNetBar.ButtonX btn_cambiar_mesa;
        private DevComponents.DotNetBar.ButtonX btn_agrupar_mesas;
        private DevComponents.DotNetBar.ButtonX btn_salir;
        private DevComponents.DotNetBar.ButtonX btn_imprimir_ticket;
        public DevComponents.DotNetBar.VScrollBarAdv vScrollBar_pedido;
        private DevComponents.DotNetBar.PanelEx pn_piso;
        private DevComponents.DotNetBar.ButtonX btn_comanda;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.LabelX labelX3;
    }
}