﻿using DevComponents.DotNetBar;
using FinalXML.Administradores;
using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalXML.vista.mozo
{
    public partial class frmMesaGrupo : PlantillaBase
    {
        private clsAdmGrupo admgru = new clsAdmGrupo();        
        private clsAdmMesa admme = new clsAdmMesa();
        private List<clsMesa> lista_mesa = null;
        private DataTable grupos = null;
        private DataTable mesas = null;
        private clsGrupo grupo = null;
        private clsMesa mesa = null;
        public clsUsuario usureg { get; set; }
        public frmPedido frmpedido { get; set; }
        public clsPiso piso { get; set; }
        public frmMesaGrupo()
        {
            InitializeComponent();
        }

        private void frm_MesaGrupo_Load(object sender, EventArgs e)
        {
            try
            {
                dg_mesa.AutoGenerateColumns = false;
                dg_grupo.AutoGenerateColumns = false;
            }
            catch (Exception) { }
        }

        private void btn_generar_Click(object sender, EventArgs e)
        {
            try
            {
                registrar_grupo();
                listar_grupoxestado();
                listar_mesa_noenmesagrupo();
            }
            catch (Exception ex) {
                MessageBox.Show("hubo un error..." + ex.Message.ToString());
            }
        }

        private void btn_agregar_Click(object sender, EventArgs e)
        {
            try
            {
                if (txt_cod_grupo.Text.Length > 0 && cb_mesa.Items.Count > 0)
                {
                    registrar_mesagrupo();
                }
            }
            catch (Exception ex) {
                MessageBox.Show("hubo un error..." + ex.Message.ToString());
            }
        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            try
            {
                limpiar();
            }
            catch (Exception ex) {
                MessageBox.Show("hubo un error..." + ex.Message.ToString());
            }
        }

        private void btn_cargar_Click(object sender, EventArgs e)
        {
            try
            {
                btn_limpiar.PerformClick();
                listar_grupoxestado();
            }
            catch (Exception ex) {
                MessageBox.Show("hubo un error..." + ex.Message.ToString());
            }

        }
        /******************Mis Metodos******************/
        public void registrar_grupo() {

            int codgrupo = -1;

            try
            {
                codgrupo = admgru.registrar_grupo(usureg);

                if (codgrupo > 0)
                {
                    txt_cod_grupo.Text = codgrupo.ToString();
                }
                else
                {

                    MessageBox.Show("Problemas para registrar grupo...", "Advertencia");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("hubo un error..." + ex.Message.ToString());
            }

        }

        public void listar_mesa_noenmesagrupo() {

            try
            {
                cb_mesa.DataSource = null;
                lista_mesa = admme.listar_mesa_noenmesagrupo(piso);

                if (lista_mesa != null)
                {

                    if (lista_mesa.Count > 0)
                    {
                        cb_mesa.DataSource = lista_mesa;
                        cb_mesa.DisplayMember = "Nombre";
                        cb_mesa.ValueMember = "Idmesa";
                        cb_mesa.SelectedIndex = 0;
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show("hubo un error..." + ex.Message.ToString());   
            }
        }

        public void listar_grupoxestado() {

            try
            {
                dg_grupo.DataSource = null;
                grupos = admgru.listar_grupoxestado();

                if (grupos != null)
                {

                    if (grupos.Rows.Count > 0)
                    {

                        dg_grupo.DataSource = grupos;
                        dg_grupo.Refresh();
                    }
                }
            }
            catch (Exception) { }
        }

        public void registrar_mesagrupo() {

            int i = 0;

            try
            {
                grupo = new clsGrupo() { Idgrupo = int.Parse(txt_cod_grupo.Text) };
                mesa = lista_mesa[cb_mesa.SelectedIndex];

                if (admgru.registrar_mesagrupo(grupo, mesa, usureg) > 0)
                {
                    MessageBox.Show("Registro correcto de mesa y grupo...", "Información");
                    listar_mesa_noenmesagrupo();

                    if (frmpedido != null)
                    {

                        if (frmpedido.panel_mesa.Controls.Count > 0)
                        {

                            DataTable mesasgrupo = admme.listar_mesaxgrupo(grupo, piso);

                            if (mesasgrupo != null)
                            {
                                if (mesasgrupo.Rows.Count > 0)
                                {
                                    foreach (DataRow row in mesasgrupo.Rows)
                                    {

                                        if (i == 0)
                                        {

                                            frmpedido.btn = frmpedido.panel_mesa.Controls[row["idmesa"].ToString()] as ButtonX;
                                            frmpedido.btn.ColorTable = DevComponents.DotNetBar.eButtonColor.MagentaWithBackground;
                                            frmpedido.btn.Refresh();
                                        }
                                        else
                                        {

                                            frmpedido.btn = frmpedido.panel_mesa.Controls[row["idmesa"].ToString()] as ButtonX;
                                            frmpedido.btn.Enabled = false;
                                            frmpedido.btn.Refresh();
                                        }
                                        i++;

                                    }
                                }
                            }

                        }
                    }
                }
                else
                {

                    MessageBox.Show("Problemas para registrar mesa y grupo...", "Advertencia");
                }
            }
            catch (Exception ex) {
                MessageBox.Show("hubo un error..." + ex.Message.ToString());
            }
        }

        public void listar_mesaxgrupo() {

            try
            {
                if (dg_grupo.CurrentCell != null)
                {
                    if (dg_grupo.CurrentCell.RowIndex != -1)
                    {

                        grupo = new clsGrupo()
                        {
                            Idgrupo = int.Parse(dg_grupo.Rows[dg_grupo.CurrentCell.RowIndex].Cells[idgrupo.Index].Value.ToString())
                        };

                        dg_mesa.DataSource = null;
                        mesas = admme.listar_mesaxgrupo(grupo, piso);

                        if (mesas != null)
                        {
                            if (mesas.Rows.Count > 0)
                            {

                                dg_mesa.DataSource = mesas;
                                dg_mesa.Refresh();
                            }
                        }

                    }
                }
            }
            catch (Exception) { }
        }
        public void limpiar() {

            try
            {
                txt_cod_grupo.Text = string.Empty;
                cb_mesa.DataSource = null;
                dg_grupo.DataSource = null;
                dg_mesa.DataSource = null;
            }
            catch (Exception) { }
        }

        private void dg_grupo_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dg_grupo.Rows.Count > 0)
                {

                    if (e.RowIndex != -1)
                    {

                        txt_cod_grupo.Text = dg_grupo.Rows[e.RowIndex].Cells[idgrupo.Index].Value.ToString();
                        listar_mesa_noenmesagrupo();
                        listar_mesaxgrupo();

                    }
                }
            }
            catch (Exception) { }
        }

        private void btn_item_anular_Click(object sender, EventArgs e)
        {
            try
            {
                if (dg_grupo.Rows.Count > 0)
                {

                    if (dg_grupo.CurrentCell != null)
                    {

                        if (dg_grupo.CurrentCell.RowIndex != -1)
                        {

                            DialogResult respuesta = MessageBox.Show("¿Desea anular grupo ?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                            if (respuesta == DialogResult.Yes)
                            {
                                grupo = new clsGrupo()
                                {
                                    Idgrupo = int.Parse(dg_grupo.Rows[dg_grupo.CurrentCell.RowIndex].Cells[idgrupo.Index].Value.ToString())
                                };

                                DataTable mesasgrupo = admme.listar_mesaxgruposinpedido(grupo, piso);

                                if (admgru.eliminar_mesagrupo(grupo, usureg) > 0)
                                {
                                    MessageBox.Show("Grupo anulado correctamente...", "Información");

                                    if (mesasgrupo != null)
                                    {
                                        if (mesasgrupo.Rows.Count > 0)
                                        {
                                            foreach (DataRow row in mesasgrupo.Rows)
                                            {
                                                frmpedido.btn = frmpedido.panel_mesa.Controls[row["idmesa"].ToString()] as ButtonX;
                                                frmpedido.btn.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
                                                frmpedido.btn.Enabled = true;
                                                frmpedido.btn.Refresh();
                                            }
                                        }
                                    }
                                    btn_limpiar.PerformClick();
                                    btn_cargar.PerformClick();
                                }
                                else
                                {

                                    MessageBox.Show("Problemas para anular grupo...", "Advertencia");
                                    btn_limpiar.PerformClick();
                                    btn_cargar.PerformClick();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception) { }
        }
    }
}
