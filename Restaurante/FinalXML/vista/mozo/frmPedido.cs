﻿using DevComponents.DotNetBar;
using FinalXML.Administradores;
using FinalXML.Entidades;
using FinalXML.vista.reporte;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Management;
using System.Windows.Forms;

namespace FinalXML.vista.mozo
{
    public partial class frmPedido : PlantillaBase
    {
        private clsTicket ticket = null;
        private clsMesa mesa = null;
        private List<clsMesa> lista_mesa = null;
        private List<clsPiso> lista_piso = null;
        private clsAdmMesa admmen = new clsAdmMesa();
        private clsAdmPedido admped = new clsAdmPedido();
        private clsAdmGrupo admgru = new clsAdmGrupo();
        private clsAdmTicket admtick = new clsAdmTicket();
        private clsAdmPiso admpi = new clsAdmPiso();
        private DataSet data = null;
        private string cantidad_anterior = "";
        public ButtonX btn { get; set; }
        public ButtonX btn_piso { get; set; }
        public clsUsuario usureg { get; set; }
        public DataTable pedido = null;
        public clsPedido pedidoenmesa { get; set; }
        private clsProducto producto = null;
        private clsAdmProducto admpro = new clsAdmProducto();
        private clsPedido pedidoparallevar = null;
        private clsDetallePedido detallepedido = null;
        private clsAdmPedido admpedido = new clsAdmPedido();
        public bool autorizado { get; set;}
        public frmPedido()
        {
            InitializeComponent();
        }

        private void frmPedido_Load(object sender, EventArgs e)
        {
            try
            {
                if (usureg != null)
                {
                    this.Text = "*********** USUARIO  : " + usureg.Nombreyapellido + " - " + DateTime.Now + " **************";              
                }
                dg_pedido.DefaultCellStyle.Font =new Font("Segoe UI",12);
                listar_piso_xestado();
                dg_pedido.ScrollBars = ScrollBars.Horizontal;
                
            
                //dg_pedido.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            }
            catch (Exception) { }
        }

        private void btn_agregar_Click(object sender, EventArgs e)
        {
            try
            {
                if (mesa != null && lb_ticket.Text.Length == 0)
                {
                    //DialogResult respuesta = MessageBox.Show("¿Desea agregar pedido la Mesa " + mesa.Idmesa + " ?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    //if (respuesta == DialogResult.Yes)
                    //{
                    if (Application.OpenForms["frm_DetallePedido"] != null)
                    {
                        Application.OpenForms["frm_DetallePedido"].Activate();
                    }
                    else
                    {
                        frmDetallePedido frm_detpedido = new frmDetallePedido();
                        frm_detpedido.frmpedido = this;
                        frm_detpedido.usureg = usureg;
                        frm_detpedido.mesa = mesa;
                        frm_detpedido.pedidoenmesa = pedidoenmesa;
                        //frm_detpedido.WindowState = FormWindowState.Normal;
                        //frm_detpedido.FormBorderStyle = FormBorderStyle.None;
                        frm_detpedido.WindowState = FormWindowState.Maximized;
                        frm_detpedido.ShowDialog();
                        if (pedido != null)
                        {
                            columna_bloqueda();
                            btn_agregar.Focus();

                        }
                    }
                    // }
                }
            }
            catch (Exception) { }
        }

        private void dg_pedido_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                if (dg_pedido.Rows.Count > 0)
                {
                    if (dg_pedido.CurrentCell != null)
                    {
                        if (dg_pedido.CurrentCell.RowIndex != -1)
                        {
                            e.Control.KeyPress -= new KeyPressEventHandler(cantidad_KeyPress);
                            e.Control.PreviewKeyDown -= new PreviewKeyDownEventHandler(cantidad_KeyDown);

                            if (dg_pedido.CurrentCell.ColumnIndex == dg_pedido.Columns["CANTIDAD"].Index)
                            {
                                TextBox tb = e.Control as TextBox;
                                if (tb != null)
                                {
                                    tb.KeyPress += new KeyPressEventHandler(cantidad_KeyPress);
                                    tb.PreviewKeyDown += new PreviewKeyDownEventHandler(cantidad_KeyDown);

                                }
                            }
                        }
                    }
                }
            }
            catch (Exception) { }
        }

        private void frmPedido_Shown(object sender, EventArgs e)
        {
           
        }

        /*****************Mis Metodos***********************/       
 
        public void columna_bloqueda() {

            try
            {
                dg_pedido.Columns[0].ReadOnly = true;
                dg_pedido.Columns[1].ReadOnly = true;
                dg_pedido.Columns[2].ReadOnly = true;
                dg_pedido.Columns[3].ReadOnly = true;
                dg_pedido.Columns[4].ReadOnly = true;
                dg_pedido.Columns[6].ReadOnly = true;
            }
            catch (Exception) { }

        }
        public void listar_mesa() {

            int top = 15;
            int left = 13;
            int il = 1;

            try
            {
                lista_mesa = admmen.listar_mesaxestado(new clsPiso() { Idpiso=int.Parse(btn_piso.Name)});

                if (lista_mesa != null)
                {

                    if (lista_mesa.Count > 0)
                    {

                        foreach (clsMesa m in lista_mesa)
                        {

                            btn = new ButtonX();
                            btn.Name = m.Idmesa.ToString();
                            contex_menu_mesa.SetContextMenuEx(btn, btn_menu_mesa);
                            btn.Text = m.Nombre;
                            btn.Width = 80;
                            btn.Height = 80;
                            btn.Top = top;
                            btn.Left = left;
                            btn.Click += btn_Click;
                            btn.Image = Image.FromFile(@"img\mesa3.png");
                            btn.Font = new Font("Segoe UI", 8, FontStyle.Bold);
                            btn.ImagePosition=eImagePosition.Bottom;

                            switch (m.Estado)
                            {

                                case 1:
                                    btn.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
                                    btn.Refresh();
                                    break;
                                case 2:

                                    btn.ColorTable = DevComponents.DotNetBar.eButtonColor.MagentaWithBackground;
                                    btn.Refresh();
                                    break;


                            }


                            if (il == 4)
                            {

                                il = 1;
                                left = 13;
                                top += 90;

                            }
                            else
                            {

                                il++;
                                left += 90;
                            }
                            panel_mesa.Controls.Add(btn);
                        }

                        if (panel_mesa.Controls.Count > 0)
                        {
                            panel_mesa.Controls.Add(new PanelEx() { Height = 80, Width = 100, Top = top + 5, Left = left });
                            ((ButtonX)panel_mesa.Controls[0]).Focus();
                        }

                    }
                }
            }
            catch (Exception) { }
        }

        public void listar_mesaengrupo() {

            int i = 0;

            try
            {
                DataTable grupos = admgru.listar_grupoxestado();
                DataTable mesasengrupo = null;

                if (grupos != null)
                {

                    if (grupos.Rows.Count > 0)
                    {

                        foreach (DataRow rowg in grupos.Rows)
                        {

                            mesasengrupo = admmen.listar_mesaxgrupo(
                                    new clsGrupo()
                                    {
                                        Idgrupo = int.Parse(rowg["idgrupo"].ToString())
                                    },
                                    new clsPiso() { Idpiso=int.Parse(btn_piso.Name)}
                                    );


                            if (mesasengrupo != null)
                            {

                                if (mesasengrupo.Rows.Count > 0)
                                {

                                    foreach (DataRow rowm in mesasengrupo.Rows)
                                    {
                                        if (i == 0)
                                        {
                                            btn = panel_mesa.Controls[rowm["idmesa"].ToString()] as ButtonX;
                                            btn.ColorTable = DevComponents.DotNetBar.eButtonColor.MagentaWithBackground;
                                            btn.Refresh();
                                        }
                                        else
                                        {
                                            btn = panel_mesa.Controls[rowm["idmesa"].ToString()] as ButtonX;
                                            btn.Enabled = false;
                                            btn.Refresh();
                                        }
                                        i++;

                                    }

                                    i = 0;

                                }
                            }
                        }

                    }
                }
            }
            catch (Exception) { }

        }
        public void btn_Click(object sender, EventArgs e) {

            try
            {
                limpiar_pedido();

                btn = ((ButtonX)sender);
                mesa = new clsMesa() { Idmesa = int.Parse(btn.Name), Nombre = btn.Text };
                lb_mesa.Text = "#MESA : " + mesa.Nombre;

                pedidoenmesa = admped.listar_idpedidoxmesa(mesa);

                if (pedidoenmesa != null)
                {
                    pedidoenmesa = admped.listar_pedido_xidpedido(pedidoenmesa);

                    if (pedidoenmesa != null)
                    {
                        pedidoenmesa.Mesa = mesa;
                        pedido = new DataTable();
                        pedido.Columns.Add("COD PEDI");
                        pedido.Columns.Add("COD DETA");
                        pedido.Columns.Add("COD PROD");
                        pedido.Columns.Add("PRODUCTO");
                        pedido.Columns.Add("PRECIO");
                        pedido.Columns.Add("CANTIDAD");
                        pedido.Columns.Add("TOTAL");
                        pedido.Columns.Add("PARA LLEVAR", typeof(bool));
                        pedido.Columns.Add("ATENDIDO", typeof(bool));
                        foreach (clsDetallePedido det in pedidoenmesa.Lista_detallepedido)
                        {
                            DataRow row = pedido.NewRow();

                            row["COD PEDI"] = pedidoenmesa.Idpedido.ToString();
                            row["COD DETA"] = det.Iddetallepedido.ToString();
                            row["COD PROD"] = det.Producto.IdProducto.ToString();
                            row["PRODUCTO"] = det.Producto.Nombre;
                            row["PRECIO"] = det.Preciounitario.ToString();
                            row["CANTIDAD"] = det.Cantidad.ToString().Replace(
                                                    det.Cantidad.ToString().Substring(
                                                        det.Cantidad.ToString().IndexOf('.'),
                                                        det.Cantidad.ToString().Length - det.Cantidad.ToString().IndexOf('.')
                                                        ), "");
                            row["TOTAL"] = decimal.Round(det.Cantidad * det.Preciounitario, 2);
                            row["PARA LLEVAR"] = det.Parallevar;
                            row["ATENDIDO"] = det.Atendido;
                            pedido.Rows.Add(row);
                        }

                        dg_pedido.DataSource = null;
                        dg_pedido.DataSource = pedido;
                        dg_pedido.Columns[0].Visible = false;
                        dg_pedido.Columns[1].Visible = false;
                        dg_pedido.Columns[2].Visible = false;
                        vScrollBar_pedido.Maximum = dg_pedido.RowCount;
                        columna_bloqueda();


                    }
                    else
                    {

                        MessageBox.Show("Problemas para cargar detalle de pedido...", "Advertencia");
                    }

                }
                btn_agregar.Focus();
                total();
            }
            catch (Exception) { }
        }

        public void limpiar_pedido() {

            try
            {
                ticket = null;
                lb_ticket.Text = string.Empty;
                dg_pedido.DataSource = null;
                pedido = null;
                mesa = null;
                pedidoenmesa = null;
            }
            catch (Exception) { }

        }
        public void quitar_producto() {

            try
            {
                if (dg_pedido.Rows.Count > 0 && lb_ticket.Text.Length == 0)
                {
                    if (dg_pedido.CurrentCell != null)
                    {
                        if (dg_pedido.CurrentCell.RowIndex != -1)
                        {
                            if (mesa != null)
                            {

                                if (dg_pedido.Rows.Count == 1)
                                {
                                    btn_item_anularpediomesa_Click(null, new EventArgs());
                                }
                                else
                                {
                                    //DialogResult respuesta = MessageBox.Show("¿Desea quitar pedido la Mesa " + mesa.Idmesa + " ?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                                    // if (respuesta == DialogResult.Yes)
                                    //{
                                    autorizado = false;

                                    if (Application.OpenForms["frmAutorizaAnulacion"] != null)
                                    {
                                        Application.OpenForms["frmAutorizaAnulacion"].Activate();
                                    }
                                    else
                                    {
                                        frmAutorizaAnulacion frm_autoriza = new frmAutorizaAnulacion();
                                        frm_autoriza.frm_pedido = this;
                                        frm_autoriza.ShowDialog();

                                        if (autorizado)
                                        {
                                            pedidoenmesa.Lista_detallepedido = new List<clsDetallePedido>()
                                                                        {
                                                                            new clsDetallePedido() {

                                                                               Iddetallepedido=int.Parse(dg_pedido.Rows[dg_pedido.CurrentCell.RowIndex].Cells[dg_pedido.Columns["COD DETA"].Index].Value.ToString())

                                                                            }
                                                                        };

                                            if (admped.eliminar_detallepedido(pedidoenmesa, usureg) > 0)
                                            {
                                                pedidoenmesa = admped.listar_idpedidoxmesa(mesa);

                                                if (pedidoenmesa != null)
                                                {
                                                    pedidoenmesa = admped.listar_pedido_xidpedido(pedidoenmesa);

                                                    if (pedidoenmesa != null)
                                                    {
                                                        pedidoenmesa.Mesa = mesa;
                                                        pedido = new DataTable();
                                                        pedido.Columns.Add("COD PEDI");
                                                        pedido.Columns.Add("COD DETA");
                                                        pedido.Columns.Add("COD PROD");
                                                        pedido.Columns.Add("PRODUCTO");
                                                        pedido.Columns.Add("PRECIO");
                                                        pedido.Columns.Add("CANTIDAD");
                                                        pedido.Columns.Add("TOTAL");
                                                        pedido.Columns.Add("PARA LLEVAR", typeof(bool));
                                                        pedido.Columns.Add("ATENDIDO", typeof(bool));
                                                        foreach (clsDetallePedido det in pedidoenmesa.Lista_detallepedido)
                                                        {
                                                            DataRow row = pedido.NewRow();

                                                            row["COD PEDI"] = pedidoenmesa.Idpedido.ToString();
                                                            row["COD DETA"] = det.Iddetallepedido.ToString();
                                                            row["COD PROD"] = det.Producto.IdProducto.ToString();
                                                            row["PRODUCTO"] = det.Producto.Nombre;
                                                            row["PRECIO"] = det.Preciounitario.ToString();
                                                            row["CANTIDAD"] = det.Cantidad.ToString().Replace(
                                                                                    det.Cantidad.ToString().Substring(
                                                                                        det.Cantidad.ToString().IndexOf('.'),
                                                                                        det.Cantidad.ToString().Length - det.Cantidad.ToString().IndexOf('.')
                                                                                        ), "");
                                                            row["TOTAL"] = decimal.Round(det.Cantidad * det.Preciounitario, 2);
                                                            row["PARA LLEVAR"] = det.Parallevar;
                                                            row["ATENDIDO"] = det.Atendido;
                                                            pedido.Rows.Add(row);
                                                        }

                                                        dg_pedido.DataSource = null;
                                                        dg_pedido.DataSource = pedido;
                                                        dg_pedido.Columns[0].Visible = false;
                                                        dg_pedido.Columns[1].Visible = false;
                                                        dg_pedido.Columns[2].Visible = false;
                                                        vScrollBar_pedido.Maximum = dg_pedido.RowCount;
                                                        btn_agregar.Focus();

                                                    }
                                                    else
                                                    {

                                                        MessageBox.Show("Problemas para cargar detalle de pedido...", "Advertencia");
                                                    }

                                                }

                                            }
                                            else
                                            {

                                                MessageBox.Show("Problemas al quitar detalle de pedido...", "Advertencia");
                                            }


                                            total();
                                        }
                                    }
                                    //}

                                }

                            }

                        }
                    }
                }
            }
            catch (Exception) { }
        }
        private void btn_quitar_Click(object sender, EventArgs e)
        {
            try
            {
                quitar_producto();
            }
            catch (Exception) { }
        }

        private void btn_item_quitar_Click(object sender, EventArgs e)
        {
            btn_quitar.PerformClick();
        }

        public void total() {

            try
            {
                if (dg_pedido.Rows.Count > 0)
                {
                    lb_monto.Text = (from x in pedido.AsEnumerable()
                                     select decimal.Parse(x.Field<string>("total"))).Sum().ToString();
                }
                else
                {

                    lb_monto.Text = "0.00";
                }
            }
            catch (Exception) { }
        }

       

        private void cantidad_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar)
                && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }

        }

        private void cantidad_KeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (dg_pedido.Rows.Count > 0 && lb_ticket.Text.Length == 0)
                    {
                        if (dg_pedido.CurrentCell != null)
                        {
                            if (dg_pedido.CurrentCell.RowIndex != -1)
                            {
                                dg_pedido.Rows[dg_pedido.CurrentCell.RowIndex].Cells[dg_pedido.CurrentCell.ColumnIndex].Value = ((TextBox)sender).Text;
                                dg_pedido.Rows[dg_pedido.CurrentCell.RowIndex].Cells[dg_pedido.Columns["TOTAL"].Index].Value = decimal.Round(
                                      (
                                       decimal.Parse(dg_pedido.Rows[dg_pedido.CurrentCell.RowIndex].Cells[dg_pedido.Columns["PRECIO"].Index].Value.ToString())
                                       *
                                        decimal.Parse(dg_pedido.Rows[dg_pedido.CurrentCell.RowIndex].Cells[dg_pedido.Columns["CANTIDAD"].Index].Value.ToString())
                                      )
                                    , 2).ToString();

                                if (pedidoenmesa != null)
                                {
                                    pedidoenmesa.Lista_detallepedido = new List<clsDetallePedido>()
                                                                    {
                                                                            new clsDetallePedido() {

                                                                                Iddetallepedido=int.Parse(dg_pedido.Rows[dg_pedido.CurrentCell.RowIndex].Cells[dg_pedido.Columns["COD DETA"].Index].Value.ToString()),
                                                                                Cantidad=decimal.Parse(dg_pedido.Rows[dg_pedido.CurrentCell.RowIndex].Cells[dg_pedido.Columns["CANTIDAD"].Index].Value.ToString()),

                                                                                Estado=1

                                                                            }
                                                                     };

                                    if (admped.actualizar_detallepedido(pedidoenmesa, usureg) > 0)
                                    {
                                        pedidoenmesa = admped.listar_pedido_xidpedido(pedidoenmesa);

                                        if (pedidoenmesa != null)
                                        {
                                            pedido = new DataTable();
                                            pedido.Columns.Add("COD PEDI");
                                            pedido.Columns.Add("COD DETA");
                                            pedido.Columns.Add("COD PROD");
                                            pedido.Columns.Add("PRODUCTO");
                                            pedido.Columns.Add("PRECIO");
                                            pedido.Columns.Add("CANTIDAD");
                                            pedido.Columns.Add("TOTAL");
                                            pedido.Columns.Add("PARA LLEVAR", typeof(bool));
                                            pedido.Columns.Add("ATENDIDO", typeof(bool));
                                            foreach (clsDetallePedido det in pedidoenmesa.Lista_detallepedido)
                                            {
                                                DataRow row = pedido.NewRow();

                                                row["COD PEDI"] = pedidoenmesa.Idpedido.ToString();
                                                row["COD DETA"] = det.Iddetallepedido.ToString();
                                                row["COD PROD"] = det.Producto.IdProducto.ToString();
                                                row["PRODUCTO"] = det.Producto.Nombre;
                                                row["PRECIO"] = det.Preciounitario.ToString();
                                                row["CANTIDAD"] = det.Cantidad.ToString().Replace(
                                                                        det.Cantidad.ToString().Substring(
                                                                            det.Cantidad.ToString().IndexOf('.'),
                                                                            det.Cantidad.ToString().Length - det.Cantidad.ToString().IndexOf('.')
                                                                            ), "");
                                                row["TOTAL"] = decimal.Round(det.Cantidad * det.Preciounitario, 2);
                                                row["PARA LLEVAR"] = det.Parallevar;
                                                row["ATENDIDO"] = det.Atendido;
                                                pedido.Rows.Add(row);
                                            }

                                            dg_pedido.DataSource = pedido;
                                            dg_pedido.Columns[0].Visible = false;
                                            dg_pedido.Columns[1].Visible = false;
                                            dg_pedido.Columns[2].Visible = false;
                                            vScrollBar_pedido.Maximum = dg_pedido.RowCount;
                                            btn.ColorTable = DevComponents.DotNetBar.eButtonColor.MagentaWithBackground;
                                            btn.Refresh();
                                            btn_agregar.Focus();
                                            total();
                                        }

                                    }
                                    else
                                    {
                                        MessageBox.Show("Problemas para actualizar detalle...", "Advertencia");
                                    }
                                }
                            }
                        }
                    }
                    else
                    {

                        MessageBox.Show("El ticket está generado...", "Advertencia");

                        if (cantidad_anterior != "")
                        {

                            ((TextBox)sender).Text = cantidad_anterior;
                            dg_pedido.Rows[dg_pedido.CurrentCell.RowIndex].Cells[dg_pedido.CurrentCell.ColumnIndex].Value = ((TextBox)sender).Text;
                            cantidad_anterior = "";
                        }
                    }
                }
            }
            catch (Exception) { }
        }       

        private void btn_item_anularpediomesa_Click(object sender, EventArgs e)
        {
            try
            {
                if (dg_pedido.Rows.Count > 0 && lb_ticket.Text.Length == 0)
                {
                    if (pedido != null)
                    {
                        if (pedido.Rows.Count > 0)
                        {
                            if (btn != null)
                            {
                                mesa = new clsMesa() { Idmesa = int.Parse(btn.Name) };
                                lb_mesa.Text = "#MESA " + mesa.Idmesa;

                                pedidoenmesa = admped.listar_idpedidoxmesa(mesa);

                                if (pedidoenmesa != null)
                                {
                                    //DialogResult respuesta = MessageBox.Show("¿Desea anular pedido la Mesa " + mesa.Idmesa + " ?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                                    // if (respuesta == DialogResult.Yes)
                                    //{
                                    autorizado = false;

                                    if (Application.OpenForms["frmAutorizaAnulacion"] != null)
                                    {
                                        Application.OpenForms["frmAutorizaAnulacion"].Activate();
                                    }
                                    else
                                    {
                                        frmAutorizaAnulacion frm_autoriza = new frmAutorizaAnulacion();
                                        frm_autoriza.frm_pedido = this;
                                        frm_autoriza.ShowDialog();

                                        if (autorizado)
                                        {

                                            DataTable mesas = admmen.listar_grupodemesas(mesa);

                                            if (admped.actualizar_pedidoydetalle_anular(pedidoenmesa, usureg) > 0)
                                            {
                                                dg_pedido.DataSource = null;
                                                pedido = null;
                                                pedidoenmesa = null;
                                                btn.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
                                                MessageBox.Show("Anulación correcta...", "Información");

                                                if (mesas != null)
                                                {

                                                    if (mesas.Rows.Count > 0)
                                                    {
                                                        foreach (DataRow row in mesas.Rows)
                                                        {
                                                            btn = panel_mesa.Controls[row["idmesa"].ToString()] as ButtonX;
                                                            btn.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
                                                            btn.Enabled = true;
                                                            btn.Refresh();
                                                        }

                                                    }
                                                }
                                            }
                                            else
                                            {

                                                MessageBox.Show("Problemas para anular pedido...", "Advertencia");
                                            }
                                            limpiar_pedido();
                                            total();
                                            btn_agregar.Focus();
                                        }
                                    }
                                    // }

                                }
                            }
                        }
                    }
                }
            }
            catch (Exception) { }
          }
        

        private void btn_agrupar_mesa_Click(object sender, EventArgs e)
        {
            try
            {
                //DialogResult respuesta = MessageBox.Show("¿Desea agrupar Mesas ?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                // if (respuesta == DialogResult.Yes)
                //{

                if (Application.OpenForms["frm_MesaGrupo"] != null)
                {
                    Application.OpenForms["frm_MesaGrupo"].Activate();
                }
                else
                {
                    frmMesaGrupo frm_mesagrupo = new frmMesaGrupo();
                    frm_mesagrupo.frmpedido = this;
                    frm_mesagrupo.piso = new clsPiso() { Idpiso = int.Parse(btn_piso.Name) };
                    frm_mesagrupo.usureg = usureg;
                    frm_mesagrupo.ShowDialog();
                }
                //}
            }
            catch (Exception) { }
        }

        private void btn_item_cambio_mesa_Click(object sender, EventArgs e)
        {
            try
            {
                if (dg_pedido.Rows.Count > 0)
                {
                    if (pedido != null)
                    {
                        if (pedido.Rows.Count > 0)
                        {
                            if (btn != null)
                            {
                                // DialogResult respuesta = MessageBox.Show("¿Desea cambiar la Mesa?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                                /// if (respuesta == DialogResult.Yes)
                                //{
                                if (Application.OpenForms["frm_CambiarMesa"] != null)
                                {
                                    Application.OpenForms["frm_CambiarMesa"].Activate();
                                }
                                else
                                {
                                    frmCambiarMesa frm_cambmesa = new frmCambiarMesa();
                                    frm_cambmesa.frmpedido = this;
                                    frm_cambmesa.usureg = usureg;
                                    frm_cambmesa.piso = new clsPiso() { Idpiso = int.Parse(btn_piso.Name) };
                                    frm_cambmesa.mesaactual = mesa;
                                    frm_cambmesa.ShowDialog();
                                }
                                //}
                            }
                        }
                    }
                }
                else
                {

                    MessageBox.Show("-Debe seleccionar primero la mesa a cambiar. \n\r"
                                    + "-Mesa debe tener pedidos.", "Advertencia");
                }
            }
            catch (Exception) { }
        }

        private void btn_generar_ticket_Click(object sender, EventArgs e)
        {
            int codticket = -1;

            try
            {

                if (dg_pedido.Rows.Count > 0)
                {

                    if (pedidoenmesa != null)
                    {
                        // DialogResult respuesta = MessageBox.Show("¿Desea generar ticket?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        // if (respuesta == DialogResult.Yes)
                        // {
                        DataTable mesas = admmen.listar_grupodemesas(mesa);
                        codticket = admtick.registrar_ticket(pedidoenmesa, usureg);

                        if (codticket > 0)
                        {
                            ticket = new clsTicket() { Idticket = codticket };
                            ticket = admtick.listar_ticket_xid(ticket);

                            if (ticket != null)
                            {

                                btn.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
                                lb_ticket.Text = ticket.Numero;
                                //MessageBox.Show("Ticket Generado...", "Información");

                                if (mesas != null)
                                {

                                    if (mesas.Rows.Count > 0)
                                    {
                                        foreach (DataRow row in mesas.Rows)
                                        {
                                            btn = panel_mesa.Controls[row["idmesa"].ToString()] as ButtonX;
                                            btn.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
                                            btn.Enabled = true;
                                            btn.Refresh();
                                        }

                                    }
                                }

                                //btn_imprimir_ticket.PerformClick();
                            }
                            else
                            {

                                MessageBox.Show("Problemas al cargar el ticket...", "Advertencia");
                            }
                        }
                        else {

                            if (codticket == -2)
                            {
                                MessageBox.Show("Ticket ya generado...", "Advertencia");
                            }
                            else
                            {

                                MessageBox.Show("Problemas para generar el ticket...", "Advertencia");
                            }
                        }
                        //}
                    }
                }
            }
            catch (Exception) { }
        }

        private void btn_anular_ticket_Click(object sender, EventArgs e)
        {
            try
            {
                if (dg_pedido.Rows.Count > 0)
                {

                    if (ticket != null)
                    {

                        if (mesa != null)
                        {

                            // DialogResult respuesta = MessageBox.Show("¿Desea anular ticket?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                            // if (respuesta == DialogResult.Yes)
                            // {
                            autorizado = false;

                            if (Application.OpenForms["frmAutorizaAnulacion"] != null)
                            {
                                Application.OpenForms["frmAutorizaAnulacion"].Activate();
                            }
                            else
                            {
                                frmAutorizaAnulacion frm_autoriza = new frmAutorizaAnulacion();
                                frm_autoriza.frm_pedido = this;
                                frm_autoriza.ShowDialog();

                                if (autorizado)
                                {
                                    if (admtick.anular_ticket(ticket, usureg) > 0)
                                    {
                                        btn.ColorTable = DevComponents.DotNetBar.eButtonColor.MagentaWithBackground;
                                        MessageBox.Show("Ticket anulado correctamente...", "Información");
                                        lb_ticket.Text = string.Empty;
                                    }
                                    else
                                    {

                                        MessageBox.Show("Problemas para anular comprobante...", "Información");
                                    }
                                }
                            }
                            //}
                        }
                    }
                }
            }
            catch (Exception) { }
        }    

        private void cb_piso_SelectionChangeCommitted(object sender, EventArgs e)
        {
            
        }

        public string impresora_xdefecto() {

            string nombreimpresora = "";//Donde guardare el nombre de la impresora por defecto

            try
            {
                //Busco la impresora por defecto
                for (int i = 0; i < PrinterSettings.InstalledPrinters.Count; i++)
                {
                    PrinterSettings a = new PrinterSettings();
                    a.PrinterName = PrinterSettings.InstalledPrinters[i].ToString();
                    if (a.IsDefaultPrinter)
                    {
                        nombreimpresora = PrinterSettings.InstalledPrinters[i].ToString();

                    }
                }

                return nombreimpresora;
            }
            catch (Exception) { return nombreimpresora; }

            
        }

        private void btn_imprimir_ticket_Click(object sender, EventArgs e)
        {
            string nombreimpresora = "";

            try
            {

                if (dg_pedido.Rows.Count > 0)
                {

                    if (ticket != null)
                    {

                        data = admtick.imprimir_ticket(ticket);

                        if (data != null)
                        {
                            if (data.Tables[0].Rows.Count > 0)
                            {
                                nombreimpresora = impresora_xdefecto();
                                if (nombreimpresora != "")
                                {
                                    frmImpresionTicket frm = new frmImpresionTicket();
                                    CRTicket rpt = new CRTicket();
                                    rpt.SetDataSource(data);
                                    CrystalDecisions.CrystalReports.Engine.PrintOptions rptoption = rpt.PrintOptions;
                                    frm.crystal_imprimir_ticket.ReportSource = rpt;
                                    rptoption.ApplyPageMargins(new CrystalDecisions.Shared.PageMargins(0, 0, 0, 20));
                                    rpt.PrintOptions.PrinterName = nombreimpresora;
                                    /*if (estaenlinealaimpresora(nombreimpresora))
                                    { rpt.PrintToPrinter(1, false, 1, 1); }
                                    else { frm.ShowDialog(this); }*/
                                    //frm.ShowDialog(this);
                                    rpt.PrintToPrinter(1, false, 1, 1);
                                    rpt.Close();
                                    rpt.Dispose();
                                }
                                else
                                {

                                    MessageBox.Show("No se ha configurado la ticketera por defecto...", "Advertencia");
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception) {

            }
        }
       

        private void dg_pedido_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int parallevar = 0;
            int atendido = 0;

            try
            {
                if (dg_pedido.Rows.Count > 0)
                {
                    if (e.RowIndex != -1)
                    {
                        if (e.ColumnIndex == 5)
                        {
                            cantidad_anterior = dg_pedido.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
                        }

                        if (e.ColumnIndex == 7)
                        {

                            DataGridViewCheckBoxCell ch = null;
                            ch = (DataGridViewCheckBoxCell)dg_pedido.Rows[e.RowIndex].Cells[e.ColumnIndex];

                            if (ch != null)
                            {
                                producto = new clsProducto()
                                {
                                    IdProducto = int.Parse(dg_pedido.Rows[e.RowIndex].Cells["COD PROD"].Value.ToString())
                                };

                                if ((bool)ch.Value)
                                {
                                    ch.Value = false;
                                    parallevar = 0;
                                }
                                else
                                {
                                    ch.Value = true;
                                    parallevar = 1;
                                }

                                if (admpro.producto_paracocina(producto) == 1)
                                {
                                    pedidoparallevar = new clsPedido()
                                    {
                                        Lista_detallepedido = new List<clsDetallePedido>() {

                                        new clsDetallePedido() {

                                            Iddetallepedido= int.Parse(dg_pedido.Rows[e.RowIndex].Cells["COD DETA"].Value.ToString()),
                                            Parallevar=parallevar
                                        }
                                    }
                                    };

                                    if (admped.actualizar_detallepedido_parallevar(pedidoparallevar, usureg) < 0)
                                    {
                                        MessageBox.Show("Problemas para actualizar pedido...", "Advertencia");
                                    }
                                }

                                btn.PerformClick();
                            }
                        }

                        if (e.ColumnIndex == 8)
                        {
                            DataGridViewCheckBoxCell ch = null;
                            ch = (DataGridViewCheckBoxCell)dg_pedido.Rows[e.RowIndex].Cells[e.ColumnIndex];

                            if (ch != null)
                            {
                                producto = new clsProducto()
                                {
                                    IdProducto = int.Parse(dg_pedido.Rows[e.RowIndex].Cells["COD PROD"].Value.ToString())
                                };

                                if ((bool)ch.Value)
                                {
                                    ch.Value = false;
                                    atendido = 0;
                                }
                                else
                                {
                                    ch.Value = true;
                                    atendido = 1;
                                }

                                if (admpro.producto_paracocina(producto) == 1)
                                {
                                    pedidoparallevar = new clsPedido()
                                    {
                                        Lista_detallepedido = new List<clsDetallePedido>() {

                                        new clsDetallePedido() {

                                            Iddetallepedido= int.Parse(dg_pedido.Rows[e.RowIndex].Cells["COD DETA"].Value.ToString()),
                                            Atendido=atendido
                                        }
                                    }
                                    };

                                    if (admped.actualizar_detalleordencocina_atendido(pedidoparallevar, usureg) < 0)
                                    {
                                        MessageBox.Show("Problemas para actualizar pedido...", "Advertencia");
                                    }
                                }

                                btn.PerformClick();
                            }
                        }
                    }
                }
            }
            catch (Exception) { }
        }

        private void dg_pedido_CurrentCellChanged(object sender, EventArgs e)
        {
            try
            {
                if (dg_pedido.Rows.Count > 0)
                {
                    if (dg_pedido.CurrentCell != null)
                    {
                        if (dg_pedido.CurrentCell.RowIndex != -1)
                        {
                            if (dg_pedido.CurrentCell.ColumnIndex == 5)
                            {

                                cantidad_anterior = dg_pedido.Rows[dg_pedido.CurrentCell.RowIndex].Cells[dg_pedido.CurrentCell.ColumnIndex].Value.ToString();
                            }
                        }
                    }
                }
            }
            catch (Exception) { }
        }

        private void dg_pedido_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dg_pedido.Rows.Count > 0)
                {

                    if (e.RowIndex != -1 && e.ColumnIndex != -1)
                    {

                        if (e.ColumnIndex != 5)
                        {

                            producto = new clsProducto()
                            {
                                IdProducto = int.Parse(dg_pedido.Rows[e.RowIndex].Cells["COD PROD"].Value.ToString())
                            };

                            DataGridViewCheckBoxCell ch = null;
                            ch = (DataGridViewCheckBoxCell)dg_pedido.Rows[e.RowIndex].Cells["ATENDIDO"];

                            if (admpro.producto_paracocina(producto) == 1 && !(bool)ch.Value)
                            {
                                detallepedido = null;
                                detallepedido = new clsDetallePedido()
                                {
                                    Iddetallepedido = int.Parse(dg_pedido.Rows[e.RowIndex].Cells["COD DETA"].Value.ToString()),
                                };
                                detallepedido.Descripcion = admped.listar_descripcion_xiddetallepedido(detallepedido);
                                if (Application.OpenForms["frmDescripcionPedido"] != null)
                                {
                                    Application.OpenForms["frmDescripcionPedido"].Activate();
                                }
                                else
                                {
                                    frmDescripcionPedido frm_descripciondetalle = new frmDescripcionPedido();
                                    frm_descripciondetalle.detallepedido = detallepedido;
                                    frm_descripciondetalle.usureg = usureg;
                                    frm_descripciondetalle.ShowDialog();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception) { }
        }

        private void btn_anular_pedido_Click(object sender, EventArgs e)
        {
            btn_item_anularpediomesa_Click(sender, e);
        }

        private void btn_cambiar_mesa_Click(object sender, EventArgs e)
        {
            btn_item_cambio_mesa_Click(sender, e);
        }

        private void btn_agrupar_mesas_Click(object sender, EventArgs e)
        {
            btn_agrupar_mesa_Click(sender, e);
        }     

        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }     

        private void dg_pedido_Scroll(object sender, ScrollEventArgs e)
        {
            try
            {
                if (dg_pedido.Rows.Count > 0)
                {
                    if (e.NewValue >= 0)
                    {
                        vScrollBar_pedido.Value = e.NewValue;
                    }
                }
            }
            catch (Exception) { }
        }

        private void vScrollBar_producto_Scroll(object sender, ScrollEventArgs e)
        {
            try
            {
                if (dg_pedido.Rows.Count > 0)
                {
                    if (e.NewValue >= 0)
                    {
                        dg_pedido.FirstDisplayedScrollingRowIndex = e.NewValue;
                    }
                }
            }
            catch (Exception) { }
        }

        public void listar_piso_xestado()
        {

            int top = 10;
            int left =35;
  

            try
            {
                lista_piso = admpi.listar_piso_xestado();

                if (lista_piso != null)
                {

                    if (lista_piso.Count > 0)
                    {

                        foreach (clsPiso p in lista_piso)
                        {

                            btn_piso = new ButtonX();
                            btn_piso.Name = p.Idpiso.ToString();
                            btn_piso.Text = p.Nombre;
                            btn_piso.Width = 100;
                            btn_piso.Height = 30;
                            btn_piso.Top = top;
                            btn_piso.Left = left;
                            btn_piso.Click += btn_piso_Click;
                            // btn.Image = Image.FromFile(@"img/mesa1.jpg");
                            btn_piso.Font = new Font("Segoe UI", 8, FontStyle.Bold);
                            top += 40;                         
                            pn_piso.Controls.Add(btn_piso);
                        }

                        if (pn_piso.Controls.Count > 0)
                        {
                            pn_piso.Controls.Add(new PanelEx() { Height = 1, Width = 100, Top = top + 5, Left = left });
                            ((ButtonX)pn_piso.Controls[0]).Focus();
                            btn_piso_Click((ButtonX)pn_piso.Controls[0], null);
                        }

                    }
                }
            }
            catch (Exception) { }
        }

        public void btn_piso_Click(object sender, EventArgs e)
        {
            try
            {
                btn_piso = ((ButtonX)sender);

                if (btn_piso!=null)
                {
                    lb_mesa.Text = "#MESA";
                    lb_monto.Text = "0.00";
                    lb_ticket.Text = string.Empty;

                    limpiar_pedido();
                    panel_mesa.Controls.Clear();
                    listar_mesa();
                    listar_mesaengrupo();
                }
            }
            catch (Exception) { }

        }

        private void btn_comanda_Click(object sender, EventArgs e)
        {
            string nombreimpresora = "";

            try
            {

                if (dg_pedido.Rows.Count > 0)
                {
                    if (pedidoenmesa != null) {

                        data = admpedido.imprimir_comanda(pedidoenmesa);

                        if (data != null)
                        {
                            if (data.Tables[0].Rows.Count > 0)
                            {
                                nombreimpresora = impresora_xdefecto();

                                if (nombreimpresora != "")
                                {
                                   
                                    frmImpresionComanda frm = new frmImpresionComanda();
                                    CRComanda rpt = new CRComanda();
                                    rpt.SetDataSource(data);
                                    CrystalDecisions.CrystalReports.Engine.PrintOptions rptoption = rpt.PrintOptions;
                                    frm.crystal_comanda.ReportSource = rpt;
                                    rptoption.ApplyPageMargins(new CrystalDecisions.Shared.PageMargins(0, 0, 0, 20));
                                    /* rpt.PrintOptions.PrinterName = nombreimpresora;
                                    if (estaenlinealaimpresora(nombreimpresora))
                                     { rpt.PrintToPrinter(1, false, 1, 1); }
                                     else { frm.ShowDialog(this); }*/
                                    //frm.ShowDialog(this);
                                    rpt.PrintToPrinter(1, false, 1, 1);
                                    rpt.Close();
                                    rpt.Dispose();

                                    
                                }
                                else
                                {

                                    MessageBox.Show("No se ha configurado la ticketera por defecto...", "Advertencia");
                                }
                            }
                        }
                    }
                    
                }
            }
            catch (Exception)
            {

            }
        }


        public bool estaenlinealaimpresora(string printerName)
        {
            string str = "";
            bool online = false;

            ManagementScope scope = new ManagementScope(ManagementPath.DefaultPath);

            scope.Connect();

            //Consulta para obtener las impresoras, en la API Win32
            SelectQuery query = new SelectQuery("select * from Win32_Printer");

            ManagementClass m = new ManagementClass("Win32_Printer");

            ManagementObjectSearcher obj = new ManagementObjectSearcher(scope, query);

            //Obtenemos cada instancia del objeto ManagementObjectSearcher
            using (ManagementObjectCollection printers = m.GetInstances())
                foreach (ManagementObject printer in printers)
                {
                    if (printer != null)
                    {
                        //Obtenemos la primera impresora en el bucle
                        str = printer["Name"].ToString().ToLower();

                        if (str.Equals(printerName.ToLower()))
                        { 
                            //Una vez encontrada verificamos el estado de ésta
                            if (printer["WorkOffline"].ToString().ToLower().Equals("true") || printer["PrinterStatus"].Equals(7))
                                //Fuera de línea
                                online = false;
                            else
                                //En línea
                                online = true;
                        }
                    }
                    else
                        throw new Exception("No fueron encontradas impresoras instaladas en el equipo");
                }
            return online;
        }
    }
}
