﻿using FinalXML.Administradores;
using FinalXML.Entidades;
using FinalXML.vista.cajero;
using FinalXML.vista.reporte;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalXML.vista.mozo
{
    public partial class frmListaTicket : PlantillaBase
    {
        public clsUsuario usureg { get; set; }
        private clsAdmTicket admtick = new clsAdmTicket();
        private DataTable tickets = null;
        private clsTicket ticket = null;
        private DataSet data = null;
        public bool autorizado { get; set; }
        public frmVenta frm_venta { get; set; }
        public frmListaTicket()
        {
            InitializeComponent();
        }

        private void frmListaPedido_Load(object sender, EventArgs e)
        {
            try
            {
                dg_ticket.DefaultCellStyle.Font =new Font("Segoe UI",12);
                dg_ticket.AutoGenerateColumns = false;
                dg_ticket.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                lb_total.Text = "0.00";
                dg_ticket.AutoGenerateColumns = false;
                btn_cargar.PerformClick();

                if (dg_ticket.Rows.Count > 0)
                {
                    dg_ticket.Focus();
                    dg_ticket.CurrentCell = dg_ticket.Rows[0].Cells[1];

                }
            }
            catch (Exception) { }
        }
        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_cargar_Click(object sender, EventArgs e)
        {
            try
            {
                listar_ticket_generado();
            }
            catch (Exception) { }
           
        }

        /******************Mis Metodos************************/
        public void listar_ticket_generado() {

            DataTable ticketfacturar = null;
            int filas_afectadas = -1;
            try
            {
                tickets = null;
                dg_ticket.DataSource = null;
                tickets = admtick.listar_ticket_generado();

                if (tickets != null)
                {
                    if (tickets.Rows.Count > 0)
                    {
                        foreach (DataRow row in tickets.Rows)
                        {

                            ticketfacturar = admtick.ticket_afacturar(
                                new clsTicket() {
                                    Idticket = int.Parse(row["idticket"].ToString())
                            });

                            if (ticketfacturar != null) {

                                if (ticketfacturar.Rows.Count == 0) {

                                    filas_afectadas = admtick.actualizar_ticket(new clsTicket()
                                    {
                                        Idticket = int.Parse(row["idticket"].ToString())
                                    },
                                    usureg
                                    );

                                    if (filas_afectadas < 0) { MessageBox.Show("Problemas para actualizar ticket...","Advertencia"); }

                                    filas_afectadas = -1;
                                }
                            }                           
                        }


                        tickets = null;
                        dg_ticket.DataSource = null;
                        tickets = admtick.listar_ticket_generado();

                        if (tickets != null)
                        {
                            if (tickets.Rows.Count > 0)
                            {
                                dg_ticket.DataSource = tickets;
                                dg_ticket.Refresh();
                                totales();
                            }
                            else
                            {

                                lb_total.Text = "0.00";
                            }
                        }
                        else
                        {
                            lb_total.Text = "0.00";
                        }
                    }
                    else
                    {

                        lb_total.Text = "0.00";
                    }
                }
                else
                {

                    lb_total.Text = "0.00";
                }
            }
            catch (Exception) { }
        }

        public void totales() {

            try
            {
                if (dg_ticket.Rows.Count > 0)
                {
                    lb_total.Text = (dg_ticket.Rows.Cast<DataGridViewRow>().Sum(x => decimal.Parse(x.Cells["total"].Value.ToString()))).ToString();
                }
                else
                {

                    lb_total.Text = "0.00";
                }
            }
            catch (Exception) { }
        }

        private void btn_item_anular_ticket_Click(object sender, EventArgs e)
        {
            int filas_afectadas = -1;

            try
            {
                if (dg_ticket.Rows.Count > 0)
                {

                    if (dg_ticket.CurrentCell != null)
                    {

                        if (dg_ticket.CurrentCell.RowIndex != -1)
                        {

                            // DialogResult respuesta = MessageBox.Show("¿Desea anular ticket?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                            //if (respuesta == DialogResult.Yes)
                            //{

                            autorizado = false;

                            if (Application.OpenForms["frmAutorizaAnulacion"] != null)
                            {
                                Application.OpenForms["frmAutorizaAnulacion"].Activate();
                            }
                            else
                            {
                                frmAutorizaAnulacion frm_autoriza = new frmAutorizaAnulacion();
                                frm_autoriza.frm_listaticket = this;
                                frm_autoriza.ShowDialog();
                                if (autorizado)
                                {
                                    ticket = new clsTicket()
                                    {
                                        Idticket = int.Parse(dg_ticket.Rows[dg_ticket.CurrentCell.RowIndex].Cells[idticket.Index].Value.ToString())
                                    };

                                    filas_afectadas = admtick.anular_ticket(ticket, usureg);

                                    if (filas_afectadas > 0)
                                    {
                                        MessageBox.Show("Ticket anulado correctamente...", "Información");
                                        btn_cargar.PerformClick();
                                    }
                                    else
                                    {
                                        if (filas_afectadas == -2)
                                        {
                                            MessageBox.Show("ticket tiene comprobantes generados...", "Advertencia");
                                        }
                                        else if (filas_afectadas == -3)
                                        {

                                            MessageBox.Show("Mesa " + dg_ticket.Rows[dg_ticket.CurrentCell.RowIndex].Cells[nombremesa.Index].Value.ToString() + "tiene pedidos actualmente...", "Advertencia");
                                        }
                                        else
                                        {
                                            MessageBox.Show("Problemas para anular ticket...", "Advertencia");
                                        }
                                    }
                                }
                            }
                            //}
                        }
                    }
                }
            }
            catch (Exception) { }
        }

        public string impresora_xdefecto()
        {

            string nombreimpresora = "";//Donde guardare el nombre de la impresora por defecto

            try
            {
                //Busco la impresora por defecto
                for (int i = 0; i < PrinterSettings.InstalledPrinters.Count; i++)
                {
                    PrinterSettings a = new PrinterSettings();
                    a.PrinterName = PrinterSettings.InstalledPrinters[i].ToString();
                    if (a.IsDefaultPrinter)
                    {
                        nombreimpresora = PrinterSettings.InstalledPrinters[i].ToString();

                    }
                }

                return nombreimpresora;
            }
            catch (Exception) { return nombreimpresora; }
          
        }
        private void btn_imprimir_ticket_Click(object sender, EventArgs e)
        {
            string nombreimpresora = "";

            try
            {
                nombreimpresora = impresora_xdefecto();

                if (dg_ticket.Rows.Count > 0)
                {
                    if (dg_ticket.CurrentCell != null)
                    {

                        if (dg_ticket.CurrentCell.RowIndex != -1)
                        {
                            if (nombreimpresora != "")
                            {
                                ticket = new clsTicket()
                                {
                                    Idticket = int.Parse(dg_ticket.Rows[dg_ticket.CurrentCell.RowIndex].Cells[idticket.Index].Value.ToString())
                                };

                                data = admtick.imprimir_ticket(ticket);

                                if (data != null)
                                {
                                    if (data.Tables[0].Rows.Count > 0)
                                    {
                                        frmImpresionTicket frm = new frmImpresionTicket();
                                        CRTicket rpt = new CRTicket();
                                        rpt.SetDataSource(data);
                                        CrystalDecisions.CrystalReports.Engine.PrintOptions rptoption = rpt.PrintOptions;
                                        frm.crystal_imprimir_ticket.ReportSource = rpt;
                                        rptoption.ApplyPageMargins(new CrystalDecisions.Shared.PageMargins(0, 0, 0, 20));
                                        rpt.PrintOptions.PrinterName = nombreimpresora;
                                        rpt.PrintToPrinter(1, false, 1, 1);
                                         /*if (estaenlinealaimpresora(nombreimpresora))
                                        { rpt.PrintToPrinter(1, false, 1, 1); }
                                        else { frm.ShowDialog(this); }*/
                                         // frm.ShowDialog(this);
                                        rpt.Dispose();
                                        rpt.Close();
                                                                 

                                    }
                                }
                            }
                            else
                            {

                                MessageBox.Show("No se ha configurado la ticketera por defecto...", "Advertencia");
                            }
                        }
                    }
                }
            }
            catch (Exception) { }          
        }

        private void dg_ticket_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dg_ticket.Rows.Count > 0)
                {
                    if (e.RowIndex != -1)
                    {
                        if (frm_venta != null)
                        {

                            frm_venta.ticket = new clsTicket()
                            {
                                Idticket = int.Parse(dg_ticket.Rows[e.RowIndex].Cells[idticket.Index].Value.ToString())
                            };
                            this.Close();
                        }
                    }
                }
            }
            catch (Exception) { }
        }

        private void frmListaTicket_Shown(object sender, EventArgs e)
        {
            
        }

        private void btn_item_nofacturado_Click(object sender, EventArgs e)
        {
            int filas_afectadas = -1;

            try
            {
                if (dg_ticket.Rows.Count > 0)
                {
                    if (dg_ticket.CurrentCell != null)
                    {
                        if (dg_ticket.CurrentCell.RowIndex != -1)
                        {
                            //DialogResult respuesta = MessageBox.Show("¿Desea no facturar ticket?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                            //if (respuesta == DialogResult.Yes)
                            //{
                            autorizado = false;

                            if (Application.OpenForms["frmAutorizaAnulacion"] != null)
                            {
                                Application.OpenForms["frmAutorizaAnulacion"].Activate();
                            }
                            else
                            {
                                frmAutorizaAnulacion frm_autoriza = new frmAutorizaAnulacion();
                                frm_autoriza.frm_listaticket = this;
                                frm_autoriza.ShowDialog();
                                if (autorizado)
                                {
                                    ticket = new clsTicket()
                                    {
                                        Idticket = int.Parse(dg_ticket.Rows[dg_ticket.CurrentCell.RowIndex].Cells[idticket.Index].Value.ToString()),
                                        Estado = 3
                                    };

                                    filas_afectadas = admtick.actualizar_estadoticket_anofacturado(ticket, usureg);

                                    if (filas_afectadas > 0)
                                    {
                                        // MessageBox.Show("Ticket no facturado, se actualizó correctamente...", "Información");
                                        btn_cargar.PerformClick();
                                    }
                                    else
                                    {
                                        if (filas_afectadas == -2)
                                        {
                                            MessageBox.Show("ticket tiene comprobantes generados...", "Advertencia");
                                        }
                                        else
                                        {

                                            MessageBox.Show("Problemas para no facturar ticket...", "Advertencia");
                                        }
                                    }
                                }
                            }
                            //}
                        }
                    }
                }
            }
            catch (Exception) { }
         }

        private void btn_anular_ticket_Click(object sender, EventArgs e)
        {
            try
            {
                btn_item_anular_ticket_Click(sender, e);
            }
            catch (Exception) { }
        }

        private void btn_imprmir_Click(object sender, EventArgs e)
        {
            try
            {
                btn_imprimir_ticket_Click(sender, e);
            }
            catch (Exception) { }
        }

        private void btn_nofacturado_Click(object sender, EventArgs e)
        {
            try
            {
                btn_item_nofacturado_Click(sender, e);
            }
            catch (Exception) { }
        }

        private void dg_ticket_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            
        }

        private void dg_ticket_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                   // e.Handled = true;
                    if (dg_ticket.Rows.Count > 0)
                    {

                        if (dg_ticket.CurrentCell != null)
                        {
                            if (dg_ticket.CurrentCell.RowIndex != -1)
                            {
                                dg_ticket_CellDoubleClick(dg_ticket, new DataGridViewCellEventArgs(dg_ticket.CurrentCell.ColumnIndex, dg_ticket.CurrentRow.Index));
                            }
                        }
                    }
                }
            }
            catch (Exception)
            { }
        }

        public bool estaenlinealaimpresora(string printerName)
        {
            string str = "";
            bool online = false;

            ManagementScope scope = new ManagementScope(ManagementPath.DefaultPath);

            scope.Connect();

            //Consulta para obtener las impresoras, en la API Win32
            SelectQuery query = new SelectQuery("select * from Win32_Printer");

            ManagementClass m = new ManagementClass("Win32_Printer");

            ManagementObjectSearcher obj = new ManagementObjectSearcher(scope, query);

            //Obtenemos cada instancia del objeto ManagementObjectSearcher
            using (ManagementObjectCollection printers = m.GetInstances())
                foreach (ManagementObject printer in printers)
                {
                    if (printer != null)
                    {
                        //Obtenemos la primera impresora en el bucle
                        str = printer["Name"].ToString().ToLower();

                        if (str.Equals(printerName.ToLower()))
                        {
                            //Una vez encontrada verificamos el estado de ésta
                            if (printer["WorkOffline"].ToString().ToLower().Equals("true") || printer["PrinterStatus"].Equals(7))
                                //Fuera de línea
                                online = false;
                            else
                                //En línea
                                online = true;
                        }
                    }
                    else
                        throw new Exception("No fueron encontradas impresoras instaladas en el equipo");
                }
            return online;
        }
    }
}
