﻿using FinalXML.Administradores;
using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalXML.vista.mozo
{
    public partial class frmListaTicketNoFacturado : PlantillaBase
    {
        public clsUsuario usureg { get; set; }
        private clsAdmTicket admtick = new clsAdmTicket();
        private DataTable tickets = null;
        private clsTicket ticket = null;
        private DataSet data = null;
        public bool autorizado { get; set; }

        public frmListaTicketNoFacturado()
        {
            InitializeComponent();
        }

        private void frmListaTicketNoFacturado_Load(object sender, EventArgs e)
        {
            try
            {
                dg_ticket.DefaultCellStyle.Font =new Font("Segoe UI",12);
                dg_ticket.AutoGenerateColumns = false;
                dg_ticket.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                lb_total.Text = "0.00";
                dg_ticket.AutoGenerateColumns = false;
                dt_fecha.Value = DateTime.Now;
                btn_cargar.PerformClick();
            }
            catch (Exception) { }
        }

        private void btn_cargar_Click(object sender, EventArgs e)
        {
            try
            {
                listar_ticket_nofacturado();
            }
            catch (Exception) { }
        }
         private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
            
        /******************Mis Metodos************************/
        public void listar_ticket_nofacturado()
        {
            try
            {
                tickets = null;
                dg_ticket.DataSource = null;
                tickets = admtick.listar_ticket_nofacturado(dt_fecha.Value);

                if (tickets != null)
                {
                    if (tickets.Rows.Count > 0)
                    {
                        dg_ticket.DataSource = tickets;
                        dg_ticket.Refresh();
                        totales();
                    }
                    else
                    {

                        lb_total.Text = "0.00";
                    }
                }
                else
                {

                    lb_total.Text = "0.00";
                }
            }
            catch (Exception) { }
        }

        public void totales()
        {
            try
            {
                if (dg_ticket.Rows.Count > 0)
                {
                    lb_total.Text = (dg_ticket.Rows.Cast<DataGridViewRow>().Sum(x => decimal.Parse(x.Cells["total"].Value.ToString()))).ToString();
                }
                else
                {

                    lb_total.Text = "0.00";
                }
            }
            catch (Exception) { }
        }

        private void btn_item_nofacturado_Click(object sender, EventArgs e)
        {
            int filas_afectadas = -1;

            try
            {
                if (dg_ticket.Rows.Count > 0)
                {
                    if (dg_ticket.CurrentCell != null)
                    {
                        if (dg_ticket.CurrentCell.RowIndex != -1)
                        {
                            //DialogResult respuesta = MessageBox.Show("¿Desea facturar ticket?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                            //if (respuesta == DialogResult.Yes)
                            //{
                            autorizado = false;

                            if (Application.OpenForms["frmAutorizaAnulacion"] != null)
                            {
                                Application.OpenForms["frmAutorizaAnulacion"].Activate();
                            }
                            else
                            {
                                frmAutorizaAnulacion frm_autoriza = new frmAutorizaAnulacion();
                                frm_autoriza.frm_listanofacturado = this;
                                frm_autoriza.ShowDialog();
                                if (autorizado)
                                {
                                    ticket = new clsTicket()
                                    {
                                        Idticket = int.Parse(dg_ticket.Rows[dg_ticket.CurrentCell.RowIndex].Cells[idticket.Index].Value.ToString()),
                                        Estado = 1
                                    };

                                    filas_afectadas = admtick.actualizar_estadoticket_anofacturado(ticket, usureg);

                                    if (filas_afectadas > 0)
                                    {
                                        //MessageBox.Show("Ticket para facturar, se actualizó correctamente...", "Información");
                                        btn_cargar.PerformClick();
                                    }
                                    else
                                    {
                                        if (filas_afectadas == -2)
                                        {
                                            MessageBox.Show("ticket tiene comprobantes generados...", "Advertencia");
                                        }
                                        else
                                        {

                                            MessageBox.Show("Problemas para habilitar facturación de ticket...", "Advertencia");
                                        }
                                    }
                                }
                            }
                            // }
                        }
                    }
                }
            }
            catch (Exception) { }
        }

        private void btn_nofacturado_Click(object sender, EventArgs e)
        {
            try
            {
                btn_item_nofacturado_Click(sender, e);
            }
            catch (Exception) { }
        }

        private void btn_quitar_Click(object sender, EventArgs e)
        {
            int filas_afectadas = -1;
            int nticketeliminado = 0;
            try
            {
                if (dg_ticket.Rows.Count > 0)
                {
                       //DialogResult respuesta = MessageBox.Show("¿Desea facturar ticket?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        //if (respuesta == DialogResult.Yes)
                       //{
                            autorizado = false;

                            if (Application.OpenForms["frmAutorizaAnulacion"] != null)
                            {
                                Application.OpenForms["frmAutorizaAnulacion"].Activate();
                            }
                            else
                            {
                                    frmAutorizaAnulacion frm_autoriza = new frmAutorizaAnulacion();
                                    frm_autoriza.frm_listanofacturado = this;
                                    frm_autoriza.ShowDialog();

                                    if (autorizado)
                                    {
                                        foreach (DataGridViewRow row in dg_ticket.Rows)
                                        {
                                            ticket = new clsTicket()
                                            {
                                                Idticket = int.Parse(row.Cells[idticket.Index].Value.ToString())                                 
                                            };

                                            filas_afectadas = admtick.eliminar_ticket_nofacturado(ticket, usureg);

                                            if (filas_afectadas > 0)
                                            {
                                                //MessageBox.Show("Ticket para facturar, se actualizó correctamente...", "Información");
                                                nticketeliminado++;
                                            }                                        
                                        }

                                        if (dg_ticket.Rows.Count == nticketeliminado) {

                                           // MessageBox.Show("Ticket´s quitados correctamente...", "Información");
                                            btn_cargar.PerformClick();
                                        }
                                        else
                                        {
                                            MessageBox.Show("Problemas para quitar ticket´s...", "Advertencia");
                                            btn_cargar.PerformClick();
                                        }
                                    }       
                               }
                       // }                      
                    
                    }
            }
            catch (Exception) { }
        }
    }
}
