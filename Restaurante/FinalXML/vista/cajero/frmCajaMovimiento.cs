﻿using FinalXML.Administradores;
using FinalXML.Entidades;
using FinalXML.vista.mozo;
using FinalXML.vista.reporte;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalXML.vista.cajero
{
    public partial class frmCajaMovimiento : PlantillaBase
    {
        public clsUsuario usureg { get; set; }
        public int Estado { get; set; }
        public int _Cajaid { get; set; }
        public bool autorizado { get; set; }

        private clsAdmCaja admca = new clsAdmCaja();
        private clsCaja caja = null;
        private DataTable cajamovimientos = null;
        private DataSet data = null;
        private clsAdmCobro admcobro = new clsAdmCobro();
        public frmCajaMovimiento()
        {
            InitializeComponent();
        }

        private void frmCajaMovimiento_Load(object sender, EventArgs e)
        {
            try
            {
               //// dg_resultado.DefaultCellStyle.Font =new Font("Segoe UI",12);
                dg_resultado.AutoGenerateColumns = false;
                dg_resultado.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

                if (Estado == 2)
                {
                    btn_cerrar_caja.Enabled = false;
                    caja = new clsCaja() { Idcaja = _Cajaid, Estado = Estado };
                    cargar_caja_movimiento();
                    total_caja();
                }
                else
                {
                    if (_Cajaid == -1)
                    {                     
                        _Cajaid = admca.buscar_caja_abierta(usureg);
                    }

                    if (_Cajaid > 0)
                    {

                        caja = new clsCaja() { Idcaja = _Cajaid, Estado = Estado };
                        cargar_caja_movimiento();
                        total_caja();
                    }
                }
            }
            catch (Exception) { }
        }

        private void btn_copiar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dg_resultado.Rows.Count > 0)
                {
                    dg_resultado.MultiSelect = true;
                    dg_resultado.SelectAll();
                    dg_resultado.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
                    DataObject dataObj = dg_resultado.GetClipboardContent();
                    if (dataObj != null)
                        Clipboard.SetDataObject(dataObj);

                    dg_resultado.MultiSelect = false;

                    MessageBox.Show("Puede copiarlo a cualquier editor de texto...", "Información");
                }

            }
            catch (Exception ex) { MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Information); }
        }

        private void btn_cerrar_caja_Click(object sender, EventArgs e)
        {
            try
            {
                if (dg_resultado.Rows.Count > 0)
                {
                    caja = new clsCaja() { Idcaja = _Cajaid, Estado = Estado };

                    if (caja != null)
                    {
                        DialogResult respuesta = MessageBox.Show("¿Desea cerra caja?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (respuesta == DialogResult.Yes)
                        {
                            if (admca.cerrar_caja(caja, usureg) > 0)
                            {
                                MessageBox.Show("Caja cerrada correctamente...", "Información");
                                btn_cerrar_caja.Enabled = false;

                                data = admca.reporte_caja_movimiento(caja);

                                if (data != null)
                                {
                                    frmCierreCaja _frm = new frmCierreCaja();
                                    CRCierreCaja _rpt = new CRCierreCaja();
                                    _rpt.SetDataSource(data);
                                    _frm.crystal_cierrecaja.ReportSource = _rpt;
                                    _frm.ShowDialog(this);
                                    _rpt.Close();
                                    _rpt.Dispose();

                                }
                            }
                            else
                            {
                                MessageBox.Show("Problemas para cerrar caja...", "Advertencia");
                            }
                        }
                    }
                }
            }
            catch (Exception) { }

        }

        private void btn_detalle_Click(object sender, EventArgs e)
        {
            try
            {

                caja = new clsCaja() { Idcaja = _Cajaid, Estado = Estado };

                if (caja != null)
                {
                    data = admca.reporte_caja_movimiento(caja);
                }

                if (dg_resultado.Rows.Count > 0 && data != null)
                {
                    frmCierreCaja _frm = new frmCierreCaja();
                    CRCierreCaja _rpt = new CRCierreCaja();
                    _rpt.SetDataSource(data);
                    _frm.crystal_cierrecaja.ReportSource = _rpt;
                    _frm.ShowDialog(this);
                    _rpt.Close();
                    _rpt.Dispose();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Close();
        } 

        private void txt_comprobante_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txt_comprobante.CharacterCasing = CharacterCasing.Upper;

                if (txt_comprobante.Text.Length > 0)
                {

                    if (_Cajaid != 0)
                    {
                        listar_movimiento_caja_xcomprobante();
                    }
                }
            }
            catch (Exception) { }
        }
        /**********************Mis Metodos*********************/
        public void cargar_caja_movimiento()
        {
            try
            {

                dg_resultado.DataSource = null;
                cajamovimientos = admca.listar_caja_movimiento(caja);

                if (cajamovimientos != null)
                {

                    if (cajamovimientos.Rows.Count > 0)
                    {

                        dg_resultado.DataSource = cajamovimientos;
                    }

                }
            }
            catch (Exception) { }
        }

        public void total_caja()
        {
            try
            {
                caja = admca.total_caja(caja);

                if (caja != null)
                {

                    txt_apertura.Text = caja.Montoapertura.ToString();
                    txt_t_efectivo.Text = caja.Totalefectivo.ToString();
                    txt_t_deposito.Text = caja.Totaldeposito.ToString();
                    txt_t_transferencia.Text = caja.Totaltransferencia.ToString();
                    txt_t_tarjeta.Text = caja.Totaltarjeta.ToString();
                    txt_cierre.Text = caja.Montocierre.ToString();
                    txt_total_nota.Text = caja.Totalnota.ToString();
                    txt_saldo.Text = caja.Totaldisponible.ToString();

                }
            }
            catch (Exception) { }
        }

        public void listar_movimiento_caja_xcomprobante() {

            try
            {
                dg_resultado.DataSource = null;
                cajamovimientos = admca.listar_movimiento_caja_xcomprobante(
                                                                                new clsCaja() { Idcaja = _Cajaid, Estado = Estado },
                                                                                new clsComprobante()
                                                                                {
                                                                                    Numero = txt_comprobante.Text
                                                                                }

                                                                             );

                if (cajamovimientos != null)
                {
                    if (cajamovimientos.Rows.Count > 1)
                    {
                        dg_resultado.DataSource = cajamovimientos;
                    }
                    else
                    {

                        if (Estado == 2)
                        {
                            btn_cerrar_caja.Enabled = false;
                            caja = new clsCaja() { Idcaja = _Cajaid, Estado = Estado };
                            cargar_caja_movimiento();
                            total_caja();
                        }
                        else
                        {
                            _Cajaid = -1;
                            _Cajaid = admca.buscar_caja_abierta(usureg);

                            if (_Cajaid > 0)
                            {

                                caja = new clsCaja() { Idcaja = _Cajaid, Estado = Estado };
                                cargar_caja_movimiento();
                                total_caja();
                            }
                        }

                    }

                }
            }
            catch (Exception) { }
        }

        private void btn_anular_tipo_cobro_Click(object sender, EventArgs e)
        {
            int filas_afectadas = -1;

            try
            {
                if (dg_resultado.Rows.Count > 0)
                {
                    if (dg_resultado.CurrentCell != null)
                    {
                        if (dg_resultado.CurrentCell.RowIndex != -1)
                        {

                            if (int.Parse(dg_resultado.Rows[dg_resultado.CurrentCell.RowIndex].Cells[cobroid.Index].Value.ToString()) > 0)
                            {
                                autorizado = false;

                                if (Application.OpenForms["frmAutorizaAnulacion"] != null)
                                {
                                    Application.OpenForms["frmAutorizaAnulacion"].Activate();
                                }
                                else
                                {
                                    frmAutorizaAnulacion frm_autoriza = new frmAutorizaAnulacion();
                                    frm_autoriza.frm_cajamovimiento = this;
                                    frm_autoriza.ShowDialog();

                                    if (autorizado)
                                    {
                                        filas_afectadas = admcobro.anular_cobro(
                                                        new clsCobro()
                                                        {
                                                            Idcobro = int.Parse(dg_resultado.Rows[dg_resultado.CurrentCell.RowIndex].Cells[cobroid.Index].Value.ToString())
                                                        },
                                                        usureg
                                                    );
                                        if (filas_afectadas > 0)
                                        {
                                            if (Estado == 2)
                                            {
                                                btn_cerrar_caja.Enabled = false;
                                                caja = new clsCaja() { Idcaja = _Cajaid, Estado = Estado };
                                                cargar_caja_movimiento();
                                                total_caja();
                                            }
                                            else
                                            {
                                                _Cajaid = -1;
                                                _Cajaid = admca.buscar_caja_abierta(usureg);

                                                if (_Cajaid > 0)
                                                {

                                                    caja = new clsCaja() { Idcaja = _Cajaid, Estado = Estado };
                                                    cargar_caja_movimiento();
                                                    total_caja();
                                                }
                                            }

                                        }
                                        else
                                        {

                                            MessageBox.Show("Problemas para anular el cobro...", "Advertencia");
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception) { }

          }

        }
    }

