﻿using FinalXML.Administradores;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalXML.vista.cajero
{
    public partial class frmTipoCambio : PlantillaBase
    {
        private clsAdmComprobante admcom = new clsAdmComprobante();
        private clsAdmTipoCambio admtipo = new clsAdmTipoCambio();
        private DataTable tiposcambio = null;
        public frmTipoCambio()
        {
            InitializeComponent();
        }

        private void frmTipoCambio_Load(object sender, EventArgs e)
        {
                //dg_tipocambio.AutoGenerateColumns = false;
                dt_fecha.Value = admcom.listar_fecha_actual();
        }

        private void btn_cargar_Click(object sender, EventArgs e)
        {
            try
            {
                listar_tipocambio_xfecha();
            }
            catch (Exception) { }
        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        /****************Mis Metodos************/

        public void listar_tipocambio_xfecha() {

            try
            {
                dg_tipocambio.DataSource = null;
                tiposcambio = admtipo.listar_tipocambio_xfecha(dt_fecha.Value);

                if (tiposcambio != null)
                {

                    if (tiposcambio.Rows.Count > 0)
                    {

                        dg_tipocambio.DataSource = tiposcambio;
                        dg_tipocambio.Refresh();

                    }
                }
            }
            catch (Exception) { }
        }        
    }
}
