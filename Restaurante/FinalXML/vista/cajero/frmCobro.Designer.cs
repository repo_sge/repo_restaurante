﻿namespace FinalXML.vista.cajero
{
    partial class frmCobro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCobro));
            this.panelEx1 = new DevComponents.DotNetBar.PanelEx();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.btn_guardar = new DevComponents.DotNetBar.ButtonX();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.cb_tarjeta = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX12 = new DevComponents.DotNetBar.LabelX();
            this.dtpk_fecha = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.txt_observacion = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.txt_parcial = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txt_monto = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.txt_ncc = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.txt_operacion = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.cb_cuenta = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.cb_banco = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.cb_tipo_pago = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txt_monto_pendiente = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.txt_comprobante = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.panelEx1.SuspendLayout();
            this.groupPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpk_fecha)).BeginInit();
            this.groupPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelEx1
            // 
            this.panelEx1.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx1.Controls.Add(this.buttonX1);
            this.panelEx1.Controls.Add(this.btn_guardar);
            this.panelEx1.Controls.Add(this.groupPanel2);
            this.panelEx1.Controls.Add(this.groupPanel1);
            this.panelEx1.DisabledBackColor = System.Drawing.Color.Empty;
            this.panelEx1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEx1.Location = new System.Drawing.Point(0, 0);
            this.panelEx1.Name = "panelEx1";
            this.panelEx1.Size = new System.Drawing.Size(322, 484);
            this.panelEx1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx1.Style.GradientAngle = 90;
            this.panelEx1.TabIndex = 4;
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX1.Image = global::FinalXML.Properties.Resources.cancelar;
            this.buttonX1.Location = new System.Drawing.Point(201, 436);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.Size = new System.Drawing.Size(91, 36);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX1.TabIndex = 25;
            this.buttonX1.Text = "Salir";
            this.buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // btn_guardar
            // 
            this.btn_guardar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_guardar.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_guardar.Image = global::FinalXML.Properties.Resources.guardar;
            this.btn_guardar.Location = new System.Drawing.Point(21, 436);
            this.btn_guardar.Name = "btn_guardar";
            this.btn_guardar.Size = new System.Drawing.Size(91, 36);
            this.btn_guardar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_guardar.TabIndex = 24;
            this.btn_guardar.Text = "Guardar";
            this.btn_guardar.Click += new System.EventHandler(this.btn_guardar_Click);
            // 
            // groupPanel2
            // 
            this.groupPanel2.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.cb_tarjeta);
            this.groupPanel2.Controls.Add(this.labelX12);
            this.groupPanel2.Controls.Add(this.dtpk_fecha);
            this.groupPanel2.Controls.Add(this.labelX4);
            this.groupPanel2.Controls.Add(this.txt_observacion);
            this.groupPanel2.Controls.Add(this.labelX11);
            this.groupPanel2.Controls.Add(this.txt_parcial);
            this.groupPanel2.Controls.Add(this.txt_monto);
            this.groupPanel2.Controls.Add(this.labelX9);
            this.groupPanel2.Controls.Add(this.txt_ncc);
            this.groupPanel2.Controls.Add(this.labelX8);
            this.groupPanel2.Controls.Add(this.txt_operacion);
            this.groupPanel2.Controls.Add(this.labelX7);
            this.groupPanel2.Controls.Add(this.cb_cuenta);
            this.groupPanel2.Controls.Add(this.labelX6);
            this.groupPanel2.Controls.Add(this.cb_banco);
            this.groupPanel2.Controls.Add(this.labelX5);
            this.groupPanel2.Controls.Add(this.cb_tipo_pago);
            this.groupPanel2.Controls.Add(this.labelX3);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Location = new System.Drawing.Point(12, 116);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.Size = new System.Drawing.Size(297, 314);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 1;
            this.groupPanel2.Text = "Datos de Pago";
            // 
            // cb_tarjeta
            // 
            this.cb_tarjeta.DisplayMember = "Text";
            this.cb_tarjeta.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cb_tarjeta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_tarjeta.Enabled = false;
            this.cb_tarjeta.FormattingEnabled = true;
            this.cb_tarjeta.ItemHeight = 16;
            this.cb_tarjeta.Location = new System.Drawing.Point(94, 68);
            this.cb_tarjeta.Name = "cb_tarjeta";
            this.cb_tarjeta.Size = new System.Drawing.Size(183, 22);
            this.cb_tarjeta.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cb_tarjeta.TabIndex = 43;
            // 
            // labelX12
            // 
            // 
            // 
            // 
            this.labelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX12.Location = new System.Drawing.Point(20, 65);
            this.labelX12.Name = "labelX12";
            this.labelX12.Size = new System.Drawing.Size(46, 23);
            this.labelX12.TabIndex = 42;
            this.labelX12.Text = "Tarjeta :";
            // 
            // dtpk_fecha
            // 
            // 
            // 
            // 
            this.dtpk_fecha.BackgroundStyle.Class = "DateTimeInputBackground";
            this.dtpk_fecha.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpk_fecha.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.dtpk_fecha.ButtonDropDown.Visible = true;
            this.dtpk_fecha.IsPopupCalendarOpen = false;
            this.dtpk_fecha.Location = new System.Drawing.Point(94, 38);
            // 
            // 
            // 
            // 
            // 
            // 
            this.dtpk_fecha.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpk_fecha.MonthCalendar.CalendarDimensions = new System.Drawing.Size(1, 1);
            this.dtpk_fecha.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.dtpk_fecha.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.dtpk_fecha.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.dtpk_fecha.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.dtpk_fecha.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.dtpk_fecha.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.dtpk_fecha.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.dtpk_fecha.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpk_fecha.MonthCalendar.DisplayMonth = new System.DateTime(2017, 11, 1, 0, 0, 0, 0);
            // 
            // 
            // 
            this.dtpk_fecha.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.dtpk_fecha.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.dtpk_fecha.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.dtpk_fecha.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpk_fecha.MonthCalendar.TodayButtonVisible = true;
            this.dtpk_fecha.Name = "dtpk_fecha";
            this.dtpk_fecha.Size = new System.Drawing.Size(82, 22);
            this.dtpk_fecha.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.dtpk_fecha.TabIndex = 41;
            this.dtpk_fecha.Value = new System.DateTime(2017, 11, 4, 11, 24, 34, 0);
            this.dtpk_fecha.WatermarkColor = System.Drawing.Color.White;
            // 
            // labelX4
            // 
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Location = new System.Drawing.Point(23, 35);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(43, 23);
            this.labelX4.TabIndex = 40;
            this.labelX4.Text = "Fecha :";
            // 
            // txt_observacion
            // 
            this.txt_observacion.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_observacion.Border.Class = "TextBoxBorder";
            this.txt_observacion.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_observacion.Location = new System.Drawing.Point(94, 227);
            this.txt_observacion.Multiline = true;
            this.txt_observacion.Name = "txt_observacion";
            this.txt_observacion.PreventEnterBeep = true;
            this.txt_observacion.Size = new System.Drawing.Size(183, 55);
            this.txt_observacion.TabIndex = 39;
            this.txt_observacion.TextChanged += new System.EventHandler(this.txt_observacion_TextChanged);
            // 
            // labelX11
            // 
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.Location = new System.Drawing.Point(6, 227);
            this.labelX11.Name = "labelX11";
            this.labelX11.Size = new System.Drawing.Size(74, 23);
            this.labelX11.TabIndex = 38;
            this.labelX11.Text = "Observación :";
            // 
            // txt_parcial
            // 
            this.txt_parcial.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_parcial.Border.Class = "TextBoxBorder";
            this.txt_parcial.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_parcial.Location = new System.Drawing.Point(181, 201);
            this.txt_parcial.Name = "txt_parcial";
            this.txt_parcial.PreventEnterBeep = true;
            this.txt_parcial.ReadOnly = true;
            this.txt_parcial.Size = new System.Drawing.Size(94, 22);
            this.txt_parcial.TabIndex = 35;
            this.txt_parcial.Text = "0.00";
            this.txt_parcial.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt_monto
            // 
            this.txt_monto.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_monto.Border.Class = "TextBoxBorder";
            this.txt_monto.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_monto.Location = new System.Drawing.Point(94, 201);
            this.txt_monto.Name = "txt_monto";
            this.txt_monto.PreventEnterBeep = true;
            this.txt_monto.Size = new System.Drawing.Size(83, 22);
            this.txt_monto.TabIndex = 34;
            this.txt_monto.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txt_monto.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_monto_KeyDown);
            this.txt_monto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_monto_KeyPress);
            this.txt_monto.Leave += new System.EventHandler(this.txt_monto_Leave);
            // 
            // labelX9
            // 
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.Location = new System.Drawing.Point(21, 198);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(43, 23);
            this.labelX9.TabIndex = 33;
            this.labelX9.Text = "Monto:";
            // 
            // txt_ncc
            // 
            this.txt_ncc.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_ncc.Border.Class = "TextBoxBorder";
            this.txt_ncc.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_ncc.Location = new System.Drawing.Point(94, 175);
            this.txt_ncc.Name = "txt_ncc";
            this.txt_ncc.PreventEnterBeep = true;
            this.txt_ncc.ReadOnly = true;
            this.txt_ncc.Size = new System.Drawing.Size(183, 22);
            this.txt_ncc.TabIndex = 32;
            // 
            // labelX8
            // 
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.Location = new System.Drawing.Point(21, 172);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(32, 23);
            this.labelX8.TabIndex = 31;
            this.labelX8.Text = "NCC :";
            // 
            // txt_operacion
            // 
            this.txt_operacion.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_operacion.Border.Class = "TextBoxBorder";
            this.txt_operacion.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_operacion.Location = new System.Drawing.Point(94, 149);
            this.txt_operacion.Name = "txt_operacion";
            this.txt_operacion.PreventEnterBeep = true;
            this.txt_operacion.ReadOnly = true;
            this.txt_operacion.Size = new System.Drawing.Size(183, 22);
            this.txt_operacion.TabIndex = 30;
            this.txt_operacion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_operacion_KeyPress);
            // 
            // labelX7
            // 
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.Location = new System.Drawing.Point(6, 146);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(80, 23);
            this.labelX7.TabIndex = 29;
            this.labelX7.Text = "N° Operación:";
            // 
            // cb_cuenta
            // 
            this.cb_cuenta.DisplayMember = "Text";
            this.cb_cuenta.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cb_cuenta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_cuenta.Enabled = false;
            this.cb_cuenta.FormattingEnabled = true;
            this.cb_cuenta.ItemHeight = 16;
            this.cb_cuenta.Location = new System.Drawing.Point(94, 123);
            this.cb_cuenta.Name = "cb_cuenta";
            this.cb_cuenta.Size = new System.Drawing.Size(183, 22);
            this.cb_cuenta.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cb_cuenta.TabIndex = 28;
            // 
            // labelX6
            // 
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.Location = new System.Drawing.Point(15, 120);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(60, 23);
            this.labelX6.TabIndex = 27;
            this.labelX6.Text = "N° Cuenta:";
            // 
            // cb_banco
            // 
            this.cb_banco.DisplayMember = "Text";
            this.cb_banco.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cb_banco.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_banco.Enabled = false;
            this.cb_banco.FormattingEnabled = true;
            this.cb_banco.ItemHeight = 16;
            this.cb_banco.Location = new System.Drawing.Point(94, 96);
            this.cb_banco.Name = "cb_banco";
            this.cb_banco.Size = new System.Drawing.Size(183, 22);
            this.cb_banco.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cb_banco.TabIndex = 26;
            this.cb_banco.SelectedIndexChanged += new System.EventHandler(this.cb_banco_SelectedIndexChanged);
            // 
            // labelX5
            // 
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Location = new System.Drawing.Point(20, 93);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(46, 23);
            this.labelX5.TabIndex = 25;
            this.labelX5.Text = "Banco:";
            // 
            // cb_tipo_pago
            // 
            this.cb_tipo_pago.DisplayMember = "Text";
            this.cb_tipo_pago.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cb_tipo_pago.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_tipo_pago.FormattingEnabled = true;
            this.cb_tipo_pago.ItemHeight = 16;
            this.cb_tipo_pago.Location = new System.Drawing.Point(94, 11);
            this.cb_tipo_pago.Name = "cb_tipo_pago";
            this.cb_tipo_pago.Size = new System.Drawing.Size(183, 22);
            this.cb_tipo_pago.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cb_tipo_pago.TabIndex = 22;
            this.cb_tipo_pago.SelectedIndexChanged += new System.EventHandler(this.cb_tipo_pago_SelectedIndexChanged);
            // 
            // labelX3
            // 
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Location = new System.Drawing.Point(20, 8);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(65, 23);
            this.labelX3.TabIndex = 21;
            this.labelX3.Text = "TipoPago:";
            // 
            // groupPanel1
            // 
            this.groupPanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.txt_monto_pendiente);
            this.groupPanel1.Controls.Add(this.labelX2);
            this.groupPanel1.Controls.Add(this.txt_comprobante);
            this.groupPanel1.Controls.Add(this.labelX1);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(12, 5);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.Size = new System.Drawing.Size(297, 105);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 0;
            this.groupPanel1.Text = "Datos del Documento";
            // 
            // txt_monto_pendiente
            // 
            this.txt_monto_pendiente.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_monto_pendiente.Border.Class = "TextBoxBorder";
            this.txt_monto_pendiente.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_monto_pendiente.Location = new System.Drawing.Point(143, 43);
            this.txt_monto_pendiente.Name = "txt_monto_pendiente";
            this.txt_monto_pendiente.PreventEnterBeep = true;
            this.txt_monto_pendiente.ReadOnly = true;
            this.txt_monto_pendiente.Size = new System.Drawing.Size(100, 22);
            this.txt_monto_pendiente.TabIndex = 23;
            this.txt_monto_pendiente.Text = "0.00";
            // 
            // labelX2
            // 
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Location = new System.Drawing.Point(42, 43);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(96, 23);
            this.labelX2.TabIndex = 22;
            this.labelX2.Text = "Monto Pendiente:";
            // 
            // txt_comprobante
            // 
            this.txt_comprobante.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_comprobante.Border.Class = "TextBoxBorder";
            this.txt_comprobante.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_comprobante.Location = new System.Drawing.Point(143, 17);
            this.txt_comprobante.Name = "txt_comprobante";
            this.txt_comprobante.PreventEnterBeep = true;
            this.txt_comprobante.ReadOnly = true;
            this.txt_comprobante.Size = new System.Drawing.Size(100, 22);
            this.txt_comprobante.TabIndex = 21;
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(57, 14);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(80, 23);
            this.labelX1.TabIndex = 20;
            this.labelX1.Text = "Comprobante:";
            // 
            // frmCobro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(322, 484);
            this.Controls.Add(this.panelEx1);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmCobro";
            this.Text = "Cobro";
            this.Load += new System.EventHandler(this.frmCobro_Load);
            this.Shown += new System.EventHandler(this.frmCobro_Shown);
            this.panelEx1.ResumeLayout(false);
            this.groupPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtpk_fecha)).EndInit();
            this.groupPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.PanelEx panelEx1;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cb_tarjeta;
        private DevComponents.DotNetBar.LabelX labelX12;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput dtpk_fecha;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_observacion;
        private DevComponents.DotNetBar.LabelX labelX11;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_parcial;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_monto;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_ncc;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_operacion;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cb_cuenta;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cb_banco;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cb_tipo_pago;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_monto_pendiente;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_comprobante;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.ButtonX buttonX1;
        private DevComponents.DotNetBar.ButtonX btn_guardar;
    }
}