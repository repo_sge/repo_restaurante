﻿using FinalXML.Administradores;
using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalXML.vista.cajero
{
    public partial class frmCajaHistorica : PlantillaBase
    {
        public clsUsuario usureg { get; set; }
        private clsAdmCaja admca = new clsAdmCaja();
        private clsCaja caja = null;
        public frmCajaHistorica()
        {
            InitializeComponent();
        }

        private void frmCajaHistorica_Load(object sender, EventArgs e)
        {

        }

        private void txt_numero_caja_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txt_numero_caja_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {

                    if (txt_numero_caja.Text.Length > 0)
                    {
                        if (int.Parse(txt_numero_caja.Text) > 0)
                        {

                            caja = new clsCaja() { Idcaja = int.Parse(txt_numero_caja.Text) };
                            caja = admca.buscar_caja_cerrada(caja);

                            if (caja != null)
                            {
                                if (caja.Idcaja > 0)
                                {
                                    if (Application.OpenForms["frmCajaMovimiento"] != null)
                                    {
                                        Application.OpenForms["frmCajaMovimiento"].Activate();
                                    }
                                    else
                                    {
                                        frmCajaMovimiento frm_cajamovimiento = new frmCajaMovimiento();
                                        frm_cajamovimiento.usureg = usureg;
                                        frm_cajamovimiento._Cajaid = caja.Idcaja;
                                        frm_cajamovimiento.Estado = 2;
                                        frm_cajamovimiento.ShowDialog();
                                        this.Close();

                                    }

                                }
                            }
                            else
                            {

                                MessageBox.Show("Caja no registrada / Caja se encuentra abierta...", "Advertencia...");
                            }
                        }
                        else
                        {

                            MessageBox.Show("Ingrese Números...", "Advertencia...");
                        }
                    }
                    else
                    {

                        MessageBox.Show("Ingrese Número de Caja...", "Advertencia...");
                    }
                }
            }
            catch (Exception) { }
        }
    }
}
