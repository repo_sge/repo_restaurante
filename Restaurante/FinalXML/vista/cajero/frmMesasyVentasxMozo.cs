﻿using FinalXML.Administradores;
using FinalXML.vista.reporte;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalXML.vista.cajero
{
    public partial class frmMesasyVentasxMozo : PlantillaBase
    {
        private DataTable ventas = null;
        private clsAdmComprobante admcompro = new clsAdmComprobante();
        private DataSet data = null;
        public frmMesasyVentasxMozo()
        {
            InitializeComponent();
        }

        private void frmMesasyVentasxMozo_Load(object sender, EventArgs e)
        {
            try
            {
                dt_fechainicio.Value = DateTime.Now;
                dt_fecha_fin.Value = DateTime.Now;
                dg_venta.DefaultCellStyle.Font =new Font("Segoe UI",12);
                dg_venta.AutoGenerateColumns = false;
                dg_venta.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            }
            catch (Exception) { }


        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_copiar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dg_venta.Rows.Count > 0)
                {
                    dg_venta.MultiSelect = true;
                    dg_venta.SelectAll();
                    dg_venta.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
                    DataObject dataObj = dg_venta.GetClipboardContent();
                    if (dataObj != null)
                        Clipboard.SetDataObject(dataObj);

                    dg_venta.MultiSelect = false;

                    MessageBox.Show("Puede copiarlo a cualquier editor de texto...", "Información");
                }

            }
            catch (Exception ex) { MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Information); }
        }

        private void btn_cargar_Click(object sender, EventArgs e)
        {
            try
            {
                listar_ventas_mesas_xmozo();
            }
            catch (Exception) { }
        }

        private void dt_fechainicio_Click(object sender, EventArgs e)
        {
            dg_venta.DataSource = null;
            lb_total.Text = "0.00";
        }

        /*************************Mis Metodos*************************/

        public void listar_ventas_mesas_xmozo()
        {
            try
            {
                ventas = null;
                dg_venta.DataSource = null;
                ventas = admcompro.listar_ventas_mesas_xmozo(
                        dt_fechainicio.Value,
                        dt_fecha_fin.Value
                    );

                if (ventas != null)
                {
                    if (ventas.Rows.Count > 0)
                    {
                        dg_venta.DataSource = ventas;
                        dg_venta.Refresh();
                        totales();
                    }
                    else
                    {

                        lb_total.Text = "0.00";
                    }
                }
                else
                {

                    lb_total.Text = "0.00";
                }
            }
            catch (Exception) { }
        }

        public void totales()
        {
            try
            {
                if (dg_venta.Rows.Count > 0)
                {
                    lb_total.Text = (dg_venta.Rows.Cast<DataGridViewRow>().Sum(x => decimal.Parse(x.Cells["total"].Value.ToString()))).ToString();
                }
                else
                {

                    lb_total.Text = "0.00";
                }
            }
            catch (Exception) { }
        }

        public string impresora_xdefecto()
        {

            string nombreimpresora = "";//Donde guardare el nombre de la impresora por defecto

            try
            {
                //Busco la impresora por defecto
                for (int i = 0; i < PrinterSettings.InstalledPrinters.Count; i++)
                {
                    PrinterSettings a = new PrinterSettings();
                    a.PrinterName = PrinterSettings.InstalledPrinters[i].ToString();
                    if (a.IsDefaultPrinter)
                    {
                        nombreimpresora = PrinterSettings.InstalledPrinters[i].ToString();

                    }
                }

                return nombreimpresora;
            }
            catch (Exception) { return nombreimpresora; }


        }
        private void btn_imprimir_comprobante_Click(object sender, EventArgs e)
        {
            string nombreimpresora = "";

            try
            {
                               
                if (dg_venta.Rows.Count > 0) 
                {
                    data = admcompro.reporte_ventas_mesas_xmozo(
                             dt_fechainicio.Value,
                             dt_fecha_fin.Value
                        );

                    if (data != null)
                    {
                        nombreimpresora = impresora_xdefecto();

                        if (nombreimpresora != "")
                        {
                            frmVentasxMesaxMozo frm = new frmVentasxMesaxMozo();
                            CRVentaxMesaxMozo rpt = new CRVentaxMesaxMozo();

                            rpt.SetDataSource(data);
                            CrystalDecisions.CrystalReports.Engine.PrintOptions rptoption = rpt.PrintOptions;
                            frm.crystal_ventasymesasxmozo.ReportSource = rpt;
                            rptoption.ApplyPageMargins(new CrystalDecisions.Shared.PageMargins(0, 0, 0, 20));
                            rpt.PrintOptions.PrinterName = nombreimpresora;
                            rpt.PrintToPrinter(1, false, 1, 1);
                            rpt.Close();
                            rpt.Dispose();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
