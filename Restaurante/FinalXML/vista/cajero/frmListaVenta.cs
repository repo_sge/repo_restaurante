﻿using FinalXML.Administradores;
using FinalXML.Entidades;
using FinalXML.vista.mozo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalXML.vista.cajero
{
    public partial class frmListaVenta : PlantillaBase
    {
        public clsUsuario usureg { get; set; }
        public clsEmpresa empresa { get; set; }
        private DataTable ventas = null;
        private clsAdmComprobante admcom = new clsAdmComprobante();
        private clsComprobante comprobante = null;
        public bool autorizado { get; set; }
        public frmListaVenta()
        {
            InitializeComponent();
        }

        private void frmListaVenta_Load(object sender, EventArgs e)
        {
            try
            {
               // dg_venta.DefaultCellStyle.Font =new Font("Segoe UI",12);
                dg_venta.AutoGenerateColumns = false;
                dg_venta.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

                dt_fechainicio.Value = admcom.listar_fecha_actual();
                dt_fecha_fin.Value = admcom.listar_fecha_actual();
                cb_estado.SelectedIndex = 0;
            }
            catch (Exception) { }
        }

        private void btn_copiar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dg_venta.Rows.Count > 0)
                {
                    dg_venta.MultiSelect = true;
                    dg_venta.SelectAll();
                    dg_venta.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
                    DataObject dataObj = dg_venta.GetClipboardContent();
                    if (dataObj != null)
                        Clipboard.SetDataObject(dataObj);

                    dg_venta.MultiSelect = false;

                    MessageBox.Show("Puede copiarlo a cualquier editor de texto...", "Información");
                }

            }
            catch (Exception ex) { MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Information); }
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                listar_ventas_xestado_xfecha();
            }
            catch (Exception) { }
        }

        private void cb_estado_SelectedIndexChanged(object sender, EventArgs e)
        {
            lb_total_soles.Text = "0.00";
            lb_total_dolares.Text = "0.00";
            dg_venta.DataSource = null;
            ventas = null;
        }
        private void btn_ver_Click(object sender, EventArgs e)
        {
            try
            {
                if (dg_venta.Rows.Count > 0)
                {
                    if (dg_venta.CurrentCell != null)
                    {
                        if (dg_venta.CurrentCell.RowIndex != -1)
                        {
                            comprobante = new clsComprobante()
                            {
                                Idcomprobante = int.Parse(dg_venta.Rows[dg_venta.CurrentCell.RowIndex].Cells[idcomprobante.Index].Value.ToString())
                            };

                            if (Application.OpenForms["frmVenta"] != null)
                            {
                                Application.OpenForms["frmVenta"].Activate();
                            }
                            else
                            {
                                frmVenta frm_venta = new frmVenta();
                                frm_venta.empresa = empresa;
                                frm_venta.comprobante = comprobante;
                                frm_venta.usureg = usureg;
                                frm_venta.Show();
                            }
                        }
                    }
                }
            }
            catch (Exception) { }
        }

        private void btn_item_ver_Click(object sender, EventArgs e)
        {
            btn_ver.PerformClick();
        }

        private void btn_item_anular_comprobante_Click(object sender, EventArgs e)
        {
            btn_anular.PerformClick();
        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txt_comprobante_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void txt_comprobante_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {

                    if (txt_comprobante.Text.Length > 0)
                    {

                        listar_ventas_xestado_xfecha_xcomprobante();
                    }
                }
            }
            catch (Exception) { }
        }

        /****************************Mis Metodos***************************/

        public void listar_ventas_xestado_xfecha() {

            int estado = 0;

            try
            {
                switch (cb_estado.SelectedItem.ToString())
                {
                    case "REGISTRADO": estado = 1; break;
                    case "ANULADO": estado = 0; break;
                    case "TODO": estado = 2; break;
                }
               

                dg_venta.DataSource = null;
                ventas = admcom.listar_ventas_xestado_xfecha(
                        dt_fechainicio.Value,
                        dt_fecha_fin.Value,
                        estado
                    );

                if (ventas != null)
                {

                    if (ventas.Rows.Count > 0)
                    {

                        dg_venta.DataSource = ventas;
                        dg_venta.Refresh();
                        total_venta();
                    }
                }
            }
            catch (Exception) { }
        }

        public void total_venta() {

            try
            {
                if (dg_venta.Rows.Count > 0)
                {

                    lb_total_soles.Text = (dg_venta.Rows.Cast<DataGridViewRow>().Where(
                                                                        x => int.Parse(x.Cells["idmoneda"].Value.ToString()) == 1

                                                                      ).Select(x => decimal.Parse(x.Cells["total"].Value.ToString())).Sum()).ToString();
                    lb_total_dolares.Text = (dg_venta.Rows.Cast<DataGridViewRow>().Where(
                                                                        x => int.Parse(x.Cells["idmoneda"].Value.ToString()) == 2

                                                                      ).Select(x => decimal.Parse(x.Cells["total"].Value.ToString())).Sum()).ToString();

                }
                else
                {

                    lb_total_soles.Text = "0.00";
                    lb_total_dolares.Text = "0.00";
                }
            }
            catch (Exception) { }
        }

       

        private void btn_anular_Click(object sender, EventArgs e)
        {
            string fechaactual = "";
            int _filas_afectadas = -1;

            try
            {
                if (dg_venta.Rows.Count > 0)
                {

                    if (dg_venta.CurrentCell != null)
                    {

                        if (dg_venta.CurrentCell.RowIndex != -1)
                        {

                            //DialogResult respuesta = MessageBox.Show("¿Desea anular comprobante?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                            // if (respuesta == DialogResult.Yes)
                            // {
                            autorizado = false;

                            if (Application.OpenForms["frmAutorizaAnulacion"] != null)
                            {
                                Application.OpenForms["frmAutorizaAnulacion"].Activate();
                            }
                            else
                            {
                                frmAutorizaAnulacion frm_autoriza = new frmAutorizaAnulacion();
                                frm_autoriza.frm_listaventa = this;
                                frm_autoriza.ShowDialog();

                                if (autorizado)
                                {
                                    fechaactual = admcom.listar_fecha_actual().ToShortDateString();

                                    if (DateTime.Parse(dg_venta.Rows[dg_venta.CurrentCell.RowIndex].Cells[fechaemision.Index].Value.ToString()).ToShortDateString() == fechaactual)
                                    {
                                        comprobante = new clsComprobante()
                                        {

                                            Idcomprobante = int.Parse(dg_venta.Rows[dg_venta.CurrentCell.RowIndex].Cells[idcomprobante.Index].Value.ToString())
                                        };

                                        _filas_afectadas = admcom.anular_comprobante(comprobante, usureg);

                                        if (_filas_afectadas > 0)
                                        {
                                            MessageBox.Show("Comprobante anulado correctamente", "Información");
                                            btn_buscar.PerformClick();
                                        }
                                        else
                                        {
                                            if (_filas_afectadas == -2)
                                            {
                                                MessageBox.Show("El comprobante se encuentra registrado en sunat...", "Información");
                                            }
                                            else if (_filas_afectadas == -3)
                                            {
                                                MessageBox.Show("Caja que contiene el documento esta cerrada...", "Información");
                                            }
                                            else
                                            {
                                                MessageBox.Show("Problemas para anular comprobante", "Advertencia");
                                            }
                                        }
                                    }
                                    else
                                    {

                                        MessageBox.Show("No se puede anular comprobantes de fechas anteriores...", "Advertencia");
                                    }
                                }
                            }
                            //}
                        }
                    }
                }
            }
            catch (Exception) { }
        }

        public void listar_ventas_xestado_xfecha_xcomprobante() {

            int estado = 0;

            try
            {
                switch (cb_estado.SelectedItem.ToString())
                {
                    case "REGISTRADO": estado = 1; break;
                    case "ANULADO": estado = 0; break;
                    case "TODO": estado = 2; break;
                }


                dg_venta.DataSource = null;
                ventas = admcom.listar_ventas_xestado_xfecha_xcomprobante(
                        dt_fechainicio.Value,
                        dt_fecha_fin.Value,
                        estado,
                        new clsComprobante() { Numero=txt_comprobante.Text}
                    );

                if (ventas != null)
                {

                    if (ventas.Rows.Count > 0)
                    {

                        dg_venta.DataSource = ventas;
                        dg_venta.Refresh();
                        total_venta();
                    }
                }
            }
            catch (Exception) { }
        }

        public void limpiar_todo() {

            try
            {
                dg_venta.DataSource = null;
                txt_comprobante.Text = string.Empty;
                cb_estado.SelectedIndex = 0;
                dt_fecha_fin.Value = DateTime.Now;
                dt_fechainicio.Value = DateTime.Now;
            }
            catch (Exception) { }
        }
        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            limpiar_todo();
        }
    }
}
