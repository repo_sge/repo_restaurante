﻿using FinalXML.Administradores;
using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalXML.vista.cajero
{
    public partial class frmNotaCreditoPendiente : PlantillaBase
    {
        public clsCliente cliente { get; set; }
        private clsAdmComprobante admcom = new clsAdmComprobante(); 
        private DataTable nota_credito = null;
        public frmCobro frm_cobro { get; set; }
        public frmCambiarTipoCobro frm_cambiotipocobro { get; set; }
        public frmNotaCreditoPendiente()
        {
            InitializeComponent();
        }

        private void frmNotaCreditoPendiente_Load(object sender, EventArgs e)
        {
            try
            {
                dg_nota.AutoGenerateColumns = false;
                listar_notas_xestado_xcliente();
            }
            catch (Exception) { }
        }

        /***************Mis Metodos***************/
        public void listar_notas_xestado_xcliente() {

            try
            {
                dg_nota.DataSource = null;

                nota_credito = admcom.listar_notas_xestado_xcliente(cliente);

                if (nota_credito != null)
                {

                    if (nota_credito.Rows.Count > 0)
                    {

                        dg_nota.DataSource = nota_credito;
                        dg_nota.Refresh();
                    }

                }
            }
            catch (Exception) { }
        }

        private void dg_nota_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dg_nota.Rows.Count > 0)
                {

                    if (frm_cobro != null)
                    {

                        frm_cobro.notacredito = new clsComprobante()
                        {
                            Idcomprobante = int.Parse(dg_nota.Rows[e.RowIndex].Cells[0].Value.ToString()),
                            Numero = dg_nota.Rows[e.RowIndex].Cells[2].Value.ToString(),
                            Total = decimal.Parse(dg_nota.Rows[e.RowIndex].Cells[3].Value.ToString())
                        };

                        this.Close();
                    }

                    if (frm_cambiotipocobro != null)
                    {

                        frm_cambiotipocobro.notacredito = new clsComprobante()
                        {
                            Idcomprobante = int.Parse(dg_nota.Rows[e.RowIndex].Cells[0].Value.ToString()),
                            Numero = dg_nota.Rows[e.RowIndex].Cells[2].Value.ToString(),
                            Total = decimal.Parse(dg_nota.Rows[e.RowIndex].Cells[3].Value.ToString())
                        };

                        this.Close();
                    }
                }
            }
            catch (Exception) { }
        }

    }
}
