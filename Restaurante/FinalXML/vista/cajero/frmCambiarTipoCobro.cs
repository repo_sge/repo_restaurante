﻿using FinalXML.Administradores;
using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalXML.vista.cajero
{
    public partial class frmCambiarTipoCobro : PlantillaBase
    {
        public clsComprobante comprobante { get; set; }
        private List<clsTarjeta> lista_tarjeta_cobro = null;
        private clsAdmTarjeta admtar = new clsAdmTarjeta();
        private List<clsBanco> lista_banco = null;
        private clsAdmBanco admba = new clsAdmBanco();
        private List<clsCuentaBancaria> lista_cuenta = null;
        private clsAdmCuentaBancaria admcuen = new clsAdmCuentaBancaria();
        private List<clsTipoCobro> lista_tipo_cobro = null;
        private clsAdmTipoCobro admtipoco = new clsAdmTipoCobro();
        public clsCobro cobro { get; set; }
        public clsUsuario usureg { get; set; }
        public clsComprobante notacredito { get; set; }
        public frmCambiarTipoCobro()
        {
            InitializeComponent();
        }

        private void frmCambiarTipoCobro_Load(object sender, EventArgs e)
        {
            cargar_tipo_cobro();
        }

        /***************Mis Metodos*******************/
        public void cargar_tipo_cobro()
        {
            lista_tipo_cobro = admtipoco.listar_tipocobro_xestado();

            if (lista_tipo_cobro != null)
            {
                if (lista_tipo_cobro.Count > 0)
                {
                    foreach (clsTipoCobro t in lista_tipo_cobro)
                    {
                        cb_tipo_pago.Items.Add(t.Nombre);
                    }

                    cb_tipo_pago.SelectedIndex = 0;
                }

            }
        }

        private void cb_tipo_pago_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (cb_tipo_pago.Items.Count > 0)
            {

                txt_ncc.Text = string.Empty;
                txt_operacion.ReadOnly = true;
                txt_operacion.Text = string.Empty;
                txt_ncc.Text = string.Empty;
                txt_monto.ReadOnly = false;

                cb_tarjeta.Items.Clear();
                cb_banco.Items.Clear();
                cb_tarjeta.Items.Clear();
                cb_cuenta.Items.Clear();

                cb_tarjeta.Enabled = false;
                cb_banco.Enabled = false;
                cb_cuenta.Enabled = false;

                if (cb_tipo_pago.SelectedItem.ToString().Contains("EFECTIVO"))
                {
                    txt_operacion.Text = lista_tipo_cobro[cb_tipo_pago.SelectedIndex].Sigla + comprobante.Cliente.Documento;
                    txt_monto.Focus();
                }

                if (cb_tipo_pago.SelectedItem.ToString().Contains("TARJETA"))
                {
                    cargar_tarjeta_cobro();
                    cb_tarjeta.Enabled = true;
                    txt_operacion.Text = "99999";
                    txt_operacion.ReadOnly = false;
                    txt_monto.Focus();

                }

                if (cb_tipo_pago.SelectedItem.ToString().Contains("DEPOSITO") || cb_tipo_pago.SelectedItem.ToString().Contains("TRANSFERENCIA"))
                {
                    cargar_banco();
                    cb_banco.Enabled = true;
                    txt_operacion.ReadOnly = false;
                }

                if (cb_tipo_pago.SelectedItem.ToString().Contains("NOTA DE CREDITO"))
                {

                    txt_monto.ReadOnly = true;

                    if (Application.OpenForms["frmNotaCreditoPendiente"] != null)
                    {
                        Application.OpenForms["frmNotaCreditoPendiente"].Activate();
                    }
                    else
                    {
                        frmNotaCreditoPendiente frm_notacredito = new frmNotaCreditoPendiente();
                        frm_notacredito.frm_cambiotipocobro = this;
                        frm_notacredito.cliente = comprobante.Cliente;
                        frm_notacredito.ShowDialog();
                        if (notacredito != null)
                        {
                            txt_ncc.Text = notacredito.Numero;
                            txt_monto.Text = notacredito.Total.ToString();
                        }
                    }
                }
            }
        }


        public void cargar_tarjeta_cobro()
        {
            lista_tarjeta_cobro = admtar.listar_tarjetacobro();

            if (lista_tarjeta_cobro != null)
            {
                if (lista_tarjeta_cobro.Count > 0)
                {
                    foreach (clsTarjeta tar in lista_tarjeta_cobro)
                    {
                        cb_tarjeta.Items.Add(tar.Descripcion);
                    }

                    cb_tarjeta.SelectedIndex = 0;
                }
            }
        }

        public void cargar_banco()
        {
            lista_banco = admba.listar_banco_xestado();

            if (lista_banco != null)
            {
                if (lista_banco.Count > 0)
                {
                    foreach (clsBanco b in lista_banco)
                    {
                        cb_banco.Items.Add(b.Nombre);

                    }
                    cb_banco.SelectedIndex = 0;
                }
            }
        }

        public void cargar_cuenta_xbanco()
        {
            lista_cuenta = admcuen.listar_cuenta_x_banco(lista_banco[cb_banco.SelectedIndex]);

            if (lista_cuenta != null)
            {
                if (lista_cuenta.Count > 0)
                {
                    foreach (clsCuentaBancaria cu in lista_cuenta)
                    {
                        cb_cuenta.Items.Add(cu.Numero);
                    }
                    cb_cuenta.SelectedIndex = 0;
                    cb_cuenta.Enabled = true;
                }

            }
        }
    }
}
