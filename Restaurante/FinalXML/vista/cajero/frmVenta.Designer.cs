﻿namespace FinalXML.vista.cajero
{
    partial class frmVenta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVenta));
            this.panelEx1 = new DevComponents.DotNetBar.PanelEx();
            this.superTabControl1 = new DevComponents.DotNetBar.SuperTabControl();
            this.superTabControlPanel1 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.panelEx2 = new DevComponents.DotNetBar.PanelEx();
            this.dg_item_pedido = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.idpedido = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iddetallepedido = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idimpuesto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codsunat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idproducto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombreproducto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.preciounitario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.importe = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valorventa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._igv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.precioventa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unidadmedida = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codprodsunat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sp_tab_item_blc_fcc = new DevComponents.DotNetBar.SuperTabItem();
            this.btn_quitar = new DevComponents.DotNetBar.ButtonX();
            this.btn_agregar = new DevComponents.DotNetBar.ButtonX();
            this.btn_salir = new DevComponents.DotNetBar.ButtonX();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btn_cliente = new DevComponents.DotNetBar.ButtonX();
            this.checkBoxX1 = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.ch_notacredito = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.btn_cobro = new DevComponents.DotNetBar.ButtonX();
            this.btn_imprimir_comprobante = new DevComponents.DotNetBar.ButtonX();
            this.btn_limpiar = new DevComponents.DotNetBar.ButtonX();
            this.txt_descripcion = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.cb_tipoventa = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.btn_guardar = new DevComponents.DotNetBar.ButtonX();
            this.groupPanel3 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.labelX20 = new DevComponents.DotNetBar.LabelX();
            this.txt_descuento = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX13 = new DevComponents.DotNetBar.LabelX();
            this.labelX12 = new DevComponents.DotNetBar.LabelX();
            this.txt_subtotal = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txt_total = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txt_igv_total = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX14 = new DevComponents.DotNetBar.LabelX();
            this.txt_igv = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.txt_direccion = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.txt_razonsocial = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.txt_ruc_dni = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.cb_moneda = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.txt_seriecorrelativo = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.cb_tipooperacion = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.dt_fechaemision = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.cb_tipocomprobante = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.textBoxX1 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX16 = new DevComponents.DotNetBar.LabelX();
            this.chkBolsa = new System.Windows.Forms.CheckBox();
            this.cantBolsas = new System.Windows.Forms.NumericUpDown();
            this.panelEx1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl1)).BeginInit();
            this.superTabControl1.SuspendLayout();
            this.superTabControlPanel1.SuspendLayout();
            this.panelEx2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_item_pedido)).BeginInit();
            this.groupPanel1.SuspendLayout();
            this.groupPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dt_fechaemision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cantBolsas)).BeginInit();
            this.SuspendLayout();
            // 
            // panelEx1
            // 
            this.panelEx1.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx1.Controls.Add(this.superTabControl1);
            this.panelEx1.Controls.Add(this.btn_quitar);
            this.panelEx1.Controls.Add(this.btn_agregar);
            this.panelEx1.Controls.Add(this.btn_salir);
            this.panelEx1.Controls.Add(this.groupPanel1);
            this.panelEx1.DisabledBackColor = System.Drawing.Color.Empty;
            this.panelEx1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEx1.Location = new System.Drawing.Point(0, 0);
            this.panelEx1.Name = "panelEx1";
            this.panelEx1.Size = new System.Drawing.Size(975, 510);
            this.panelEx1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx1.Style.GradientAngle = 90;
            this.panelEx1.TabIndex = 0;
            // 
            // superTabControl1
            // 
            this.superTabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            // 
            // 
            // 
            this.superTabControl1.ControlBox.CloseBox.Name = "";
            // 
            // 
            // 
            this.superTabControl1.ControlBox.MenuBox.Name = "";
            this.superTabControl1.ControlBox.Name = "";
            this.superTabControl1.ControlBox.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.superTabControl1.ControlBox.MenuBox,
            this.superTabControl1.ControlBox.CloseBox});
            this.superTabControl1.Controls.Add(this.superTabControlPanel1);
            this.superTabControl1.Location = new System.Drawing.Point(12, 270);
            this.superTabControl1.Name = "superTabControl1";
            this.superTabControl1.ReorderTabsEnabled = true;
            this.superTabControl1.SelectedTabFont = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.superTabControl1.SelectedTabIndex = 0;
            this.superTabControl1.Size = new System.Drawing.Size(844, 225);
            this.superTabControl1.TabFont = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.superTabControl1.TabIndex = 0;
            this.superTabControl1.Tabs.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.sp_tab_item_blc_fcc});
            this.superTabControl1.Text = "superTabControl1";
            // 
            // superTabControlPanel1
            // 
            this.superTabControlPanel1.Controls.Add(this.panelEx2);
            this.superTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel1.Location = new System.Drawing.Point(0, 27);
            this.superTabControlPanel1.Name = "superTabControlPanel1";
            this.superTabControlPanel1.Size = new System.Drawing.Size(844, 198);
            this.superTabControlPanel1.TabIndex = 1;
            this.superTabControlPanel1.TabItem = this.sp_tab_item_blc_fcc;
            // 
            // panelEx2
            // 
            this.panelEx2.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx2.Controls.Add(this.dg_item_pedido);
            this.panelEx2.DisabledBackColor = System.Drawing.Color.Empty;
            this.panelEx2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEx2.Location = new System.Drawing.Point(0, 0);
            this.panelEx2.Name = "panelEx2";
            this.panelEx2.Size = new System.Drawing.Size(844, 198);
            this.panelEx2.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx2.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx2.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx2.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx2.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx2.Style.GradientAngle = 90;
            this.panelEx2.TabIndex = 0;
            this.panelEx2.Text = "panelEx2";
            // 
            // dg_item_pedido
            // 
            this.dg_item_pedido.AllowUserToAddRows = false;
            this.dg_item_pedido.AllowUserToDeleteRows = false;
            this.dg_item_pedido.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dg_item_pedido.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dg_item_pedido.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dg_item_pedido.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_item_pedido.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idpedido,
            this.iddetallepedido,
            this.idimpuesto,
            this.codsunat,
            this.idproducto,
            this.nombreproducto,
            this.cantidad,
            this.preciounitario,
            this.importe,
            this.valorventa,
            this._igv,
            this.precioventa,
            this.unidadmedida,
            this.codprodsunat});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(57)))), ((int)(((byte)(91)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dg_item_pedido.DefaultCellStyle = dataGridViewCellStyle2;
            this.dg_item_pedido.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dg_item_pedido.EnableHeadersVisualStyles = false;
            this.dg_item_pedido.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dg_item_pedido.Location = new System.Drawing.Point(0, 0);
            this.dg_item_pedido.Name = "dg_item_pedido";
            this.dg_item_pedido.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dg_item_pedido.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dg_item_pedido.RowHeadersVisible = false;
            this.dg_item_pedido.RowTemplate.Height = 30;
            this.dg_item_pedido.Size = new System.Drawing.Size(844, 198);
            this.dg_item_pedido.TabIndex = 0;
            // 
            // idpedido
            // 
            this.idpedido.DataPropertyName = "idpedido";
            this.idpedido.HeaderText = "COD PEDIDO";
            this.idpedido.Name = "idpedido";
            this.idpedido.ReadOnly = true;
            this.idpedido.Visible = false;
            this.idpedido.Width = 80;
            // 
            // iddetallepedido
            // 
            this.iddetallepedido.DataPropertyName = "iddetallepedido";
            this.iddetallepedido.HeaderText = "COD DETALLE";
            this.iddetallepedido.Name = "iddetallepedido";
            this.iddetallepedido.ReadOnly = true;
            this.iddetallepedido.Visible = false;
            this.iddetallepedido.Width = 81;
            // 
            // idimpuesto
            // 
            this.idimpuesto.DataPropertyName = "idimpuesto";
            this.idimpuesto.HeaderText = "COD IMPUESTO";
            this.idimpuesto.Name = "idimpuesto";
            this.idimpuesto.ReadOnly = true;
            this.idimpuesto.Visible = false;
            this.idimpuesto.Width = 92;
            // 
            // codsunat
            // 
            this.codsunat.DataPropertyName = "codsunat";
            this.codsunat.HeaderText = "COD SUNAT";
            this.codsunat.Name = "codsunat";
            this.codsunat.ReadOnly = true;
            this.codsunat.Visible = false;
            this.codsunat.Width = 73;
            // 
            // idproducto
            // 
            this.idproducto.DataPropertyName = "idproducto";
            this.idproducto.HeaderText = "COD PROD";
            this.idproducto.Name = "idproducto";
            this.idproducto.ReadOnly = true;
            this.idproducto.Visible = false;
            this.idproducto.Width = 70;
            // 
            // nombreproducto
            // 
            this.nombreproducto.DataPropertyName = "nombreproducto";
            this.nombreproducto.HeaderText = "PROD";
            this.nombreproducto.Name = "nombreproducto";
            this.nombreproducto.ReadOnly = true;
            this.nombreproducto.Width = 62;
            // 
            // cantidad
            // 
            this.cantidad.DataPropertyName = "cantidad";
            this.cantidad.HeaderText = "CANT";
            this.cantidad.Name = "cantidad";
            this.cantidad.ReadOnly = true;
            this.cantidad.Width = 59;
            // 
            // preciounitario
            // 
            this.preciounitario.DataPropertyName = "preciounitario";
            this.preciounitario.HeaderText = "P.UNIT";
            this.preciounitario.Name = "preciounitario";
            this.preciounitario.ReadOnly = true;
            this.preciounitario.Width = 65;
            // 
            // importe
            // 
            this.importe.DataPropertyName = "importe";
            this.importe.HeaderText = "IMPORTE";
            this.importe.Name = "importe";
            this.importe.ReadOnly = true;
            this.importe.Width = 78;
            // 
            // valorventa
            // 
            this.valorventa.DataPropertyName = "valorventa";
            this.valorventa.HeaderText = "V.VENTA";
            this.valorventa.Name = "valorventa";
            this.valorventa.ReadOnly = true;
            this.valorventa.Width = 74;
            // 
            // _igv
            // 
            this._igv.DataPropertyName = "igv";
            this._igv.HeaderText = "IGV";
            this._igv.Name = "_igv";
            this._igv.ReadOnly = true;
            this._igv.Width = 50;
            // 
            // precioventa
            // 
            this.precioventa.DataPropertyName = "precioventa";
            this.precioventa.HeaderText = "P.VENTA";
            this.precioventa.Name = "precioventa";
            this.precioventa.ReadOnly = true;
            this.precioventa.Width = 73;
            // 
            // unidadmedida
            // 
            this.unidadmedida.DataPropertyName = "unidadmedida";
            this.unidadmedida.HeaderText = "UNI";
            this.unidadmedida.Name = "unidadmedida";
            this.unidadmedida.ReadOnly = true;
            this.unidadmedida.Visible = false;
            this.unidadmedida.Width = 51;
            // 
            // codprodsunat
            // 
            this.codprodsunat.DataPropertyName = "codprodsunat";
            this.codprodsunat.HeaderText = "CODPRODSUNAT";
            this.codprodsunat.Name = "codprodsunat";
            this.codprodsunat.ReadOnly = true;
            this.codprodsunat.Visible = false;
            this.codprodsunat.Width = 119;
            // 
            // sp_tab_item_blc_fcc
            // 
            this.sp_tab_item_blc_fcc.AttachedControl = this.superTabControlPanel1;
            this.sp_tab_item_blc_fcc.GlobalItem = false;
            this.sp_tab_item_blc_fcc.Name = "sp_tab_item_blc_fcc";
            this.sp_tab_item_blc_fcc.Text = "Detalle del Comprobante";
            // 
            // btn_quitar
            // 
            this.btn_quitar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_quitar.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btn_quitar.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_quitar.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_quitar.Image = global::FinalXML.Properties.Resources.quitar;
            this.btn_quitar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Right;
            this.btn_quitar.Location = new System.Drawing.Point(862, 352);
            this.btn_quitar.Name = "btn_quitar";
            this.btn_quitar.Size = new System.Drawing.Size(97, 37);
            this.btn_quitar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_quitar.TabIndex = 7;
            this.btn_quitar.Text = "Quitar";
            this.btn_quitar.Click += new System.EventHandler(this.btn_quitar_Click);
            // 
            // btn_agregar
            // 
            this.btn_agregar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_agregar.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btn_agregar.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_agregar.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_agregar.Image = global::FinalXML.Properties.Resources.add;
            this.btn_agregar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Right;
            this.btn_agregar.Location = new System.Drawing.Point(862, 309);
            this.btn_agregar.Name = "btn_agregar";
            this.btn_agregar.Size = new System.Drawing.Size(97, 37);
            this.btn_agregar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_agregar.TabIndex = 6;
            this.btn_agregar.Text = "Agregar";
            this.btn_agregar.Click += new System.EventHandler(this.btn_agregar_Click);
            // 
            // btn_salir
            // 
            this.btn_salir.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_salir.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btn_salir.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_salir.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_salir.Image = global::FinalXML.Properties.Resources.cancelar;
            this.btn_salir.Location = new System.Drawing.Point(862, 421);
            this.btn_salir.Name = "btn_salir";
            this.btn_salir.Size = new System.Drawing.Size(97, 36);
            this.btn_salir.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_salir.TabIndex = 280;
            this.btn_salir.Text = "Salir";
            this.btn_salir.Click += new System.EventHandler(this.btn_salir_Click);
            // 
            // groupPanel1
            // 
            this.groupPanel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.groupPanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.btn_cliente);
            this.groupPanel1.Controls.Add(this.checkBoxX1);
            this.groupPanel1.Controls.Add(this.ch_notacredito);
            this.groupPanel1.Controls.Add(this.btn_cobro);
            this.groupPanel1.Controls.Add(this.btn_imprimir_comprobante);
            this.groupPanel1.Controls.Add(this.btn_limpiar);
            this.groupPanel1.Controls.Add(this.txt_descripcion);
            this.groupPanel1.Controls.Add(this.labelX11);
            this.groupPanel1.Controls.Add(this.cb_tipoventa);
            this.groupPanel1.Controls.Add(this.labelX10);
            this.groupPanel1.Controls.Add(this.btn_guardar);
            this.groupPanel1.Controls.Add(this.groupPanel3);
            this.groupPanel1.Controls.Add(this.txt_igv);
            this.groupPanel1.Controls.Add(this.labelX9);
            this.groupPanel1.Controls.Add(this.txt_direccion);
            this.groupPanel1.Controls.Add(this.labelX8);
            this.groupPanel1.Controls.Add(this.txt_razonsocial);
            this.groupPanel1.Controls.Add(this.labelX7);
            this.groupPanel1.Controls.Add(this.txt_ruc_dni);
            this.groupPanel1.Controls.Add(this.labelX6);
            this.groupPanel1.Controls.Add(this.labelX5);
            this.groupPanel1.Controls.Add(this.cb_moneda);
            this.groupPanel1.Controls.Add(this.labelX4);
            this.groupPanel1.Controls.Add(this.txt_seriecorrelativo);
            this.groupPanel1.Controls.Add(this.labelX3);
            this.groupPanel1.Controls.Add(this.cb_tipooperacion);
            this.groupPanel1.Controls.Add(this.dt_fechaemision);
            this.groupPanel1.Controls.Add(this.labelX2);
            this.groupPanel1.Controls.Add(this.cb_tipocomprobante);
            this.groupPanel1.Controls.Add(this.labelX1);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(27, 7);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.Size = new System.Drawing.Size(912, 257);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 0;
            this.groupPanel1.Text = "Datos del  Comprobante";
            // 
            // btn_cliente
            // 
            this.btn_cliente.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_cliente.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_cliente.Image = global::FinalXML.Properties.Resources.clientefre;
            this.btn_cliente.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btn_cliente.Location = new System.Drawing.Point(673, 53);
            this.btn_cliente.Name = "btn_cliente";
            this.btn_cliente.Size = new System.Drawing.Size(36, 34);
            this.btn_cliente.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_cliente.TabIndex = 290;
            this.btn_cliente.Click += new System.EventHandler(this.btn_cliente_Click);
            // 
            // checkBoxX1
            // 
            // 
            // 
            // 
            this.checkBoxX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.checkBoxX1.Font = new System.Drawing.Font("Segoe UI Historic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxX1.Location = new System.Drawing.Point(273, 2);
            this.checkBoxX1.Name = "checkBoxX1";
            this.checkBoxX1.Size = new System.Drawing.Size(98, 23);
            this.checkBoxX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.checkBoxX1.TabIndex = 289;
            this.checkBoxX1.Text = "Nota de debito7";
            this.checkBoxX1.Visible = false;
            // 
            // ch_notacredito
            // 
            // 
            // 
            // 
            this.ch_notacredito.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ch_notacredito.Font = new System.Drawing.Font("Segoe UI Historic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ch_notacredito.Location = new System.Drawing.Point(55, 1);
            this.ch_notacredito.Name = "ch_notacredito";
            this.ch_notacredito.Size = new System.Drawing.Size(100, 23);
            this.ch_notacredito.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ch_notacredito.TabIndex = 288;
            this.ch_notacredito.Text = "Nota de credito";
            this.ch_notacredito.Visible = false;
            // 
            // btn_cobro
            // 
            this.btn_cobro.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_cobro.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_cobro.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_cobro.Image = global::FinalXML.Properties.Resources.cobro;
            this.btn_cobro.Location = new System.Drawing.Point(251, 186);
            this.btn_cobro.Name = "btn_cobro";
            this.btn_cobro.Size = new System.Drawing.Size(97, 37);
            this.btn_cobro.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_cobro.TabIndex = 8;
            this.btn_cobro.Text = "Cobro";
            this.btn_cobro.Click += new System.EventHandler(this.btn_cobro_Click);
            // 
            // btn_imprimir_comprobante
            // 
            this.btn_imprimir_comprobante.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_imprimir_comprobante.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_imprimir_comprobante.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_imprimir_comprobante.Image = global::FinalXML.Properties.Resources.imprimir;
            this.btn_imprimir_comprobante.Location = new System.Drawing.Point(144, 186);
            this.btn_imprimir_comprobante.Name = "btn_imprimir_comprobante";
            this.btn_imprimir_comprobante.Size = new System.Drawing.Size(97, 37);
            this.btn_imprimir_comprobante.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_imprimir_comprobante.TabIndex = 287;
            this.btn_imprimir_comprobante.Text = "Imprimir";
            this.btn_imprimir_comprobante.Click += new System.EventHandler(this.btn_imprimir_comprobante_Click);
            // 
            // btn_limpiar
            // 
            this.btn_limpiar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_limpiar.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_limpiar.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_limpiar.Image = global::FinalXML.Properties.Resources.limpiar;
            this.btn_limpiar.Location = new System.Drawing.Point(359, 186);
            this.btn_limpiar.Name = "btn_limpiar";
            this.btn_limpiar.Size = new System.Drawing.Size(91, 37);
            this.btn_limpiar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_limpiar.TabIndex = 285;
            this.btn_limpiar.Text = "Limpiar";
            this.btn_limpiar.Click += new System.EventHandler(this.btn_limpiar_Click);
            // 
            // txt_descripcion
            // 
            this.txt_descripcion.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_descripcion.Border.Class = "TextBoxBorder";
            this.txt_descripcion.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_descripcion.Location = new System.Drawing.Point(142, 158);
            this.txt_descripcion.MaxLength = 1000;
            this.txt_descripcion.Name = "txt_descripcion";
            this.txt_descripcion.PreventEnterBeep = true;
            this.txt_descripcion.Size = new System.Drawing.Size(229, 22);
            this.txt_descripcion.TabIndex = 284;
            this.txt_descripcion.TextChanged += new System.EventHandler(this.txt_descripcion_TextChanged);
            // 
            // labelX11
            // 
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX11.Location = new System.Drawing.Point(51, 157);
            this.labelX11.Name = "labelX11";
            this.labelX11.Size = new System.Drawing.Size(69, 23);
            this.labelX11.TabIndex = 283;
            this.labelX11.Text = "Descripción : ";
            // 
            // cb_tipoventa
            // 
            this.cb_tipoventa.DisplayMember = "Text";
            this.cb_tipoventa.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cb_tipoventa.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_tipoventa.FormattingEnabled = true;
            this.cb_tipoventa.ItemHeight = 16;
            this.cb_tipoventa.Location = new System.Drawing.Point(251, 126);
            this.cb_tipoventa.Name = "cb_tipoventa";
            this.cb_tipoventa.Size = new System.Drawing.Size(120, 22);
            this.cb_tipoventa.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cb_tipoventa.TabIndex = 282;
            // 
            // labelX10
            // 
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.Location = new System.Drawing.Point(208, 126);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(52, 23);
            this.labelX10.TabIndex = 281;
            this.labelX10.Text = "Venta :";
            // 
            // btn_guardar
            // 
            this.btn_guardar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_guardar.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_guardar.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_guardar.Image = global::FinalXML.Properties.Resources.guardar;
            this.btn_guardar.Location = new System.Drawing.Point(40, 187);
            this.btn_guardar.Name = "btn_guardar";
            this.btn_guardar.Size = new System.Drawing.Size(94, 36);
            this.btn_guardar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_guardar.TabIndex = 278;
            this.btn_guardar.Text = "Guardar";
            this.btn_guardar.Click += new System.EventHandler(this.btn_guardar_Click);
            // 
            // groupPanel3
            // 
            this.groupPanel3.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel3.Controls.Add(this.cantBolsas);
            this.groupPanel3.Controls.Add(this.chkBolsa);
            this.groupPanel3.Controls.Add(this.textBoxX1);
            this.groupPanel3.Controls.Add(this.labelX16);
            this.groupPanel3.Controls.Add(this.labelX20);
            this.groupPanel3.Controls.Add(this.txt_descuento);
            this.groupPanel3.Controls.Add(this.labelX13);
            this.groupPanel3.Controls.Add(this.labelX12);
            this.groupPanel3.Controls.Add(this.txt_subtotal);
            this.groupPanel3.Controls.Add(this.txt_total);
            this.groupPanel3.Controls.Add(this.txt_igv_total);
            this.groupPanel3.Controls.Add(this.labelX14);
            this.groupPanel3.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel3.Location = new System.Drawing.Point(464, 119);
            this.groupPanel3.Name = "groupPanel3";
            this.groupPanel3.Size = new System.Drawing.Size(439, 106);
            // 
            // 
            // 
            this.groupPanel3.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel3.Style.BackColorGradientAngle = 90;
            this.groupPanel3.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel3.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderBottomWidth = 1;
            this.groupPanel3.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel3.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderLeftWidth = 1;
            this.groupPanel3.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderRightWidth = 1;
            this.groupPanel3.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderTopWidth = 1;
            this.groupPanel3.Style.CornerDiameter = 4;
            this.groupPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel3.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel3.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel3.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel3.TabIndex = 277;
            this.groupPanel3.Text = "Descuentos e impuesto";
            this.groupPanel3.Click += new System.EventHandler(this.groupPanel3_Click);
            // 
            // labelX20
            // 
            // 
            // 
            // 
            this.labelX20.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX20.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX20.Location = new System.Drawing.Point(9, 3);
            this.labelX20.Name = "labelX20";
            this.labelX20.Size = new System.Drawing.Size(67, 23);
            this.labelX20.TabIndex = 93;
            this.labelX20.Text = "Descuento :";
            // 
            // txt_descuento
            // 
            this.txt_descuento.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_descuento.Border.Class = "TextBoxBorder";
            this.txt_descuento.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_descuento.Location = new System.Drawing.Point(75, 6);
            this.txt_descuento.Name = "txt_descuento";
            this.txt_descuento.PreventEnterBeep = true;
            this.txt_descuento.Size = new System.Drawing.Size(89, 22);
            this.txt_descuento.TabIndex = 92;
            this.txt_descuento.Text = "0.00";
            this.txt_descuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txt_descuento.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_descuento_KeyDown);
            this.txt_descuento.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_descuento_KeyPress);
            this.txt_descuento.Leave += new System.EventHandler(this.txt_descuento_Leave);
            // 
            // labelX13
            // 
            // 
            // 
            // 
            this.labelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX13.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX13.Location = new System.Drawing.Point(9, 35);
            this.labelX13.Name = "labelX13";
            this.labelX13.Size = new System.Drawing.Size(53, 23);
            this.labelX13.TabIndex = 89;
            this.labelX13.Text = "Total Igv :";
            // 
            // labelX12
            // 
            // 
            // 
            // 
            this.labelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX12.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX12.Location = new System.Drawing.Point(176, 5);
            this.labelX12.Name = "labelX12";
            this.labelX12.Size = new System.Drawing.Size(61, 23);
            this.labelX12.TabIndex = 87;
            this.labelX12.Text = "Sub Total :";
            // 
            // txt_subtotal
            // 
            this.txt_subtotal.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_subtotal.Border.Class = "TextBoxBorder";
            this.txt_subtotal.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_subtotal.Location = new System.Drawing.Point(240, 6);
            this.txt_subtotal.Name = "txt_subtotal";
            this.txt_subtotal.PreventEnterBeep = true;
            this.txt_subtotal.ReadOnly = true;
            this.txt_subtotal.Size = new System.Drawing.Size(89, 22);
            this.txt_subtotal.TabIndex = 13;
            this.txt_subtotal.Text = "0.00";
            this.txt_subtotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt_total
            // 
            this.txt_total.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_total.Border.Class = "TextBoxBorder";
            this.txt_total.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_total.Location = new System.Drawing.Point(240, 34);
            this.txt_total.Name = "txt_total";
            this.txt_total.PreventEnterBeep = true;
            this.txt_total.ReadOnly = true;
            this.txt_total.Size = new System.Drawing.Size(89, 22);
            this.txt_total.TabIndex = 15;
            this.txt_total.Text = "0.00";
            this.txt_total.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt_igv_total
            // 
            this.txt_igv_total.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_igv_total.Border.Class = "TextBoxBorder";
            this.txt_igv_total.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_igv_total.Location = new System.Drawing.Point(75, 34);
            this.txt_igv_total.Name = "txt_igv_total";
            this.txt_igv_total.PreventEnterBeep = true;
            this.txt_igv_total.ReadOnly = true;
            this.txt_igv_total.Size = new System.Drawing.Size(89, 22);
            this.txt_igv_total.TabIndex = 14;
            this.txt_igv_total.Text = "0.00";
            this.txt_igv_total.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelX14
            // 
            // 
            // 
            // 
            this.labelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX14.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX14.Location = new System.Drawing.Point(175, 34);
            this.labelX14.Name = "labelX14";
            this.labelX14.Size = new System.Drawing.Size(37, 23);
            this.labelX14.TabIndex = 91;
            this.labelX14.Text = "Total :";
            // 
            // txt_igv
            // 
            this.txt_igv.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_igv.Border.Class = "TextBoxBorder";
            this.txt_igv.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_igv.Location = new System.Drawing.Point(142, 126);
            this.txt_igv.Name = "txt_igv";
            this.txt_igv.PreventEnterBeep = true;
            this.txt_igv.ReadOnly = true;
            this.txt_igv.Size = new System.Drawing.Size(60, 22);
            this.txt_igv.TabIndex = 276;
            // 
            // labelX9
            // 
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX9.Location = new System.Drawing.Point(55, 123);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(65, 23);
            this.labelX9.TabIndex = 275;
            this.labelX9.Text = "Valor Igv :";
            // 
            // txt_direccion
            // 
            this.txt_direccion.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_direccion.Border.Class = "TextBoxBorder";
            this.txt_direccion.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_direccion.Location = new System.Drawing.Point(464, 91);
            this.txt_direccion.Name = "txt_direccion";
            this.txt_direccion.PreventEnterBeep = true;
            this.txt_direccion.ReadOnly = true;
            this.txt_direccion.Size = new System.Drawing.Size(394, 22);
            this.txt_direccion.TabIndex = 274;
            // 
            // labelX8
            // 
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX8.Location = new System.Drawing.Point(396, 91);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(63, 23);
            this.labelX8.TabIndex = 273;
            this.labelX8.Text = "Dirección  :";
            // 
            // txt_razonsocial
            // 
            this.txt_razonsocial.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_razonsocial.Border.Class = "TextBoxBorder";
            this.txt_razonsocial.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_razonsocial.Location = new System.Drawing.Point(142, 92);
            this.txt_razonsocial.Name = "txt_razonsocial";
            this.txt_razonsocial.PreventEnterBeep = true;
            this.txt_razonsocial.ReadOnly = true;
            this.txt_razonsocial.Size = new System.Drawing.Size(231, 22);
            this.txt_razonsocial.TabIndex = 272;
            // 
            // labelX7
            // 
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX7.Location = new System.Drawing.Point(55, 92);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(78, 23);
            this.labelX7.TabIndex = 271;
            this.labelX7.Text = "Razón Social :";
            // 
            // txt_ruc_dni
            // 
            // 
            // 
            // 
            this.txt_ruc_dni.Border.Class = "TextBoxBorder";
            this.txt_ruc_dni.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_ruc_dni.Location = new System.Drawing.Point(717, 59);
            this.txt_ruc_dni.Name = "txt_ruc_dni";
            this.txt_ruc_dni.PreventEnterBeep = true;
            this.txt_ruc_dni.Size = new System.Drawing.Size(141, 22);
            this.txt_ruc_dni.TabIndex = 270;
            this.txt_ruc_dni.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_ruc_dni_KeyDown);
            this.txt_ruc_dni.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_ruc_dni_KeyPress);
            // 
            // labelX6
            // 
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX6.Location = new System.Drawing.Point(612, 60);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(55, 23);
            this.labelX6.TabIndex = 13;
            this.labelX6.Text = "Dni/Ruc :";
            // 
            // labelX5
            // 
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX5.Location = new System.Drawing.Point(612, 29);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(99, 23);
            this.labelX5.TabIndex = 12;
            this.labelX5.Text = "Tipo Operación :";
            // 
            // cb_moneda
            // 
            this.cb_moneda.DisplayMember = "Text";
            this.cb_moneda.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cb_moneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_moneda.FormattingEnabled = true;
            this.cb_moneda.ItemHeight = 16;
            this.cb_moneda.Location = new System.Drawing.Point(463, 59);
            this.cb_moneda.Name = "cb_moneda";
            this.cb_moneda.Size = new System.Drawing.Size(141, 22);
            this.cb_moneda.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cb_moneda.TabIndex = 11;
            // 
            // labelX4
            // 
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX4.Location = new System.Drawing.Point(404, 59);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(50, 23);
            this.labelX4.TabIndex = 10;
            this.labelX4.Text = "Moneda :";
            // 
            // txt_seriecorrelativo
            // 
            this.txt_seriecorrelativo.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_seriecorrelativo.Border.Class = "TextBoxBorder";
            this.txt_seriecorrelativo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_seriecorrelativo.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_seriecorrelativo.ForeColor = System.Drawing.Color.Red;
            this.txt_seriecorrelativo.Location = new System.Drawing.Point(142, 61);
            this.txt_seriecorrelativo.Name = "txt_seriecorrelativo";
            this.txt_seriecorrelativo.PreventEnterBeep = true;
            this.txt_seriecorrelativo.ReadOnly = true;
            this.txt_seriecorrelativo.Size = new System.Drawing.Size(231, 23);
            this.txt_seriecorrelativo.TabIndex = 9;
            this.txt_seriecorrelativo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelX3
            // 
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX3.Location = new System.Drawing.Point(40, 61);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(95, 23);
            this.labelX3.TabIndex = 8;
            this.labelX3.Text = "Serie-Correlativo :";
            // 
            // cb_tipooperacion
            // 
            this.cb_tipooperacion.DisplayMember = "Text";
            this.cb_tipooperacion.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cb_tipooperacion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_tipooperacion.FormattingEnabled = true;
            this.cb_tipooperacion.ItemHeight = 16;
            this.cb_tipooperacion.Location = new System.Drawing.Point(717, 29);
            this.cb_tipooperacion.Name = "cb_tipooperacion";
            this.cb_tipooperacion.Size = new System.Drawing.Size(141, 22);
            this.cb_tipooperacion.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cb_tipooperacion.TabIndex = 6;
            // 
            // dt_fechaemision
            // 
            // 
            // 
            // 
            this.dt_fechaemision.BackgroundStyle.Class = "DateTimeInputBackground";
            this.dt_fechaemision.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dt_fechaemision.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.dt_fechaemision.ButtonDropDown.Visible = true;
            this.dt_fechaemision.IsPopupCalendarOpen = false;
            this.dt_fechaemision.Location = new System.Drawing.Point(464, 30);
            // 
            // 
            // 
            // 
            // 
            // 
            this.dt_fechaemision.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dt_fechaemision.MonthCalendar.CalendarDimensions = new System.Drawing.Size(1, 1);
            this.dt_fechaemision.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.dt_fechaemision.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.dt_fechaemision.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.dt_fechaemision.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.dt_fechaemision.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.dt_fechaemision.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.dt_fechaemision.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.dt_fechaemision.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dt_fechaemision.MonthCalendar.DisplayMonth = new System.DateTime(2018, 5, 1, 0, 0, 0, 0);
            this.dt_fechaemision.MonthCalendar.FirstDayOfWeek = System.DayOfWeek.Monday;
            // 
            // 
            // 
            this.dt_fechaemision.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.dt_fechaemision.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.dt_fechaemision.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.dt_fechaemision.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dt_fechaemision.MonthCalendar.TodayButtonVisible = true;
            this.dt_fechaemision.Name = "dt_fechaemision";
            this.dt_fechaemision.Size = new System.Drawing.Size(140, 22);
            this.dt_fechaemision.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.dt_fechaemision.TabIndex = 3;
            // 
            // labelX2
            // 
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX2.Location = new System.Drawing.Point(406, 29);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(49, 23);
            this.labelX2.TabIndex = 2;
            this.labelX2.Text = "Fecha :";
            // 
            // cb_tipocomprobante
            // 
            this.cb_tipocomprobante.DisplayMember = "Text";
            this.cb_tipocomprobante.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cb_tipocomprobante.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_tipocomprobante.FormattingEnabled = true;
            this.cb_tipocomprobante.ItemHeight = 16;
            this.cb_tipocomprobante.Location = new System.Drawing.Point(142, 31);
            this.cb_tipocomprobante.Name = "cb_tipocomprobante";
            this.cb_tipocomprobante.Size = new System.Drawing.Size(231, 22);
            this.cb_tipocomprobante.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cb_tipocomprobante.TabIndex = 1;
            this.cb_tipocomprobante.SelectedIndexChanged += new System.EventHandler(this.cb_tipocomprobante_SelectedIndexChanged);
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX1.Location = new System.Drawing.Point(55, 30);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(61, 23);
            this.labelX1.TabIndex = 0;
            this.labelX1.Text = "Tipo Doc :";
            // 
            // textBoxX1
            // 
            this.textBoxX1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.textBoxX1.Border.Class = "TextBoxBorder";
            this.textBoxX1.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX1.Location = new System.Drawing.Point(186, 58);
            this.textBoxX1.Name = "textBoxX1";
            this.textBoxX1.PreventEnterBeep = true;
            this.textBoxX1.ReadOnly = true;
            this.textBoxX1.Size = new System.Drawing.Size(89, 22);
            this.textBoxX1.TabIndex = 95;
            this.textBoxX1.Text = "0.00";
            this.textBoxX1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelX16
            // 
            // 
            // 
            // 
            this.labelX16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX16.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX16.Location = new System.Drawing.Point(129, 58);
            this.labelX16.Name = "labelX16";
            this.labelX16.Size = new System.Drawing.Size(49, 23);
            this.labelX16.TabIndex = 96;
            this.labelX16.Text = "ICBPER:";
            // 
            // chkBolsa
            // 
            this.chkBolsa.AutoSize = true;
            this.chkBolsa.Location = new System.Drawing.Point(353, 11);
            this.chkBolsa.Name = "chkBolsa";
            this.chkBolsa.Size = new System.Drawing.Size(64, 17);
            this.chkBolsa.TabIndex = 97;
            this.chkBolsa.Text = "Bolsas?";
            this.chkBolsa.UseVisualStyleBackColor = true;
            this.chkBolsa.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // cantBolsas
            // 
            this.cantBolsas.Enabled = false;
            this.cantBolsas.Location = new System.Drawing.Point(353, 34);
            this.cantBolsas.Name = "cantBolsas";
            this.cantBolsas.Size = new System.Drawing.Size(54, 22);
            this.cantBolsas.TabIndex = 98;
            // 
            // frmVenta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(975, 510);
            this.Controls.Add(this.panelEx1);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmVenta";
            this.Text = "Venta";
            this.Load += new System.EventHandler(this.frmVenta_Load);
            this.panelEx1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl1)).EndInit();
            this.superTabControl1.ResumeLayout(false);
            this.superTabControlPanel1.ResumeLayout(false);
            this.panelEx2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dg_item_pedido)).EndInit();
            this.groupPanel1.ResumeLayout(false);
            this.groupPanel3.ResumeLayout(false);
            this.groupPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dt_fechaemision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cantBolsas)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.PanelEx panelEx1;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cb_tipocomprobante;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput dt_fechaemision;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cb_tipooperacion;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_seriecorrelativo;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cb_moneda;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_ruc_dni;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_direccion;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_razonsocial;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_igv;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel3;
        private DevComponents.DotNetBar.LabelX labelX20;
        public DevComponents.DotNetBar.Controls.TextBoxX txt_descuento;
        private DevComponents.DotNetBar.LabelX labelX13;
        private DevComponents.DotNetBar.LabelX labelX12;
        public DevComponents.DotNetBar.Controls.TextBoxX txt_subtotal;
        public DevComponents.DotNetBar.Controls.TextBoxX txt_total;
        public DevComponents.DotNetBar.Controls.TextBoxX txt_igv_total;
        private DevComponents.DotNetBar.LabelX labelX14;
        private DevComponents.DotNetBar.ButtonX btn_salir;
        private DevComponents.DotNetBar.ButtonX btn_guardar;
        private DevComponents.DotNetBar.ButtonX btn_quitar;
        private DevComponents.DotNetBar.SuperTabControl superTabControl1;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel1;
        private DevComponents.DotNetBar.PanelEx panelEx2;
        private DevComponents.DotNetBar.SuperTabItem sp_tab_item_blc_fcc;
        private DevComponents.DotNetBar.ButtonX btn_agregar;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cb_tipoventa;
        private DevComponents.DotNetBar.LabelX labelX10;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_descripcion;
        private DevComponents.DotNetBar.LabelX labelX11;
        private DevComponents.DotNetBar.ButtonX btn_limpiar;
        private DevComponents.DotNetBar.ButtonX btn_imprimir_comprobante;
        private DevComponents.DotNetBar.ButtonX btn_cobro;
        public DevComponents.DotNetBar.Controls.DataGridViewX dg_item_pedido;
        private DevComponents.DotNetBar.Controls.CheckBoxX ch_notacredito;
        private DevComponents.DotNetBar.Controls.CheckBoxX checkBoxX1;
        private DevComponents.DotNetBar.ButtonX btn_cliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn idpedido;
        private System.Windows.Forms.DataGridViewTextBoxColumn iddetallepedido;
        private System.Windows.Forms.DataGridViewTextBoxColumn idimpuesto;
        private System.Windows.Forms.DataGridViewTextBoxColumn codsunat;
        private System.Windows.Forms.DataGridViewTextBoxColumn idproducto;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombreproducto;
        private System.Windows.Forms.DataGridViewTextBoxColumn cantidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn preciounitario;
        private System.Windows.Forms.DataGridViewTextBoxColumn importe;
        private System.Windows.Forms.DataGridViewTextBoxColumn valorventa;
        private System.Windows.Forms.DataGridViewTextBoxColumn _igv;
        private System.Windows.Forms.DataGridViewTextBoxColumn precioventa;
        private System.Windows.Forms.DataGridViewTextBoxColumn unidadmedida;
        private System.Windows.Forms.DataGridViewTextBoxColumn codprodsunat;
        private System.Windows.Forms.NumericUpDown cantBolsas;
        private System.Windows.Forms.CheckBox chkBolsa;
        public DevComponents.DotNetBar.Controls.TextBoxX textBoxX1;
        private DevComponents.DotNetBar.LabelX labelX16;
    }
}