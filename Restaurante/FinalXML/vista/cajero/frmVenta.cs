﻿using FinalXML.Administradores;
using FinalXML.controlador;
using FinalXML.Entidades;
using FinalXML.SunatFacElec;
using FinalXML.vista.administrador;
using FinalXML.vista.mozo;
using FinalXML.vista.reporte;
using iTextSharp.text.pdf;
using QRCoder;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinApp.Comun.Dto.Modelos;

namespace FinalXML.vista.cajero
{
    public partial class frmVenta : PlantillaBase
    {
        public clsUsuario usureg { get; set; }
        public clsEmpresa empresa { get; set; }
        private clsCaja caja = null;
        private clsAdmCaja admca = new clsAdmCaja();
        private clsIgv igv = null;
        private clsAdmMoneda admmo = new clsAdmMoneda();
        private clsAdmTipoOperacion admtipoo = new clsAdmTipoOperacion();
        private clsAdmSerieFactura admsefac = new clsAdmSerieFactura();
        private clsAdmTipoImpuesto admtipoim = new clsAdmTipoImpuesto();
        private clsAdmCliente admcli = new clsAdmCliente();
        private clsAdmIgv admigv = new clsAdmIgv();
        private clsAdmTipoVenta admtipv = new clsAdmTipoVenta();
        private clsAdmTicket admtick = new clsAdmTicket();
        private clsAdmComprobante admcom = new clsAdmComprobante();
        private clsAdmTipoCambio admtipoc = new clsAdmTipoCambio();
        public clsCliente cliente { get; set; }
        public clsTicket ticket = null;

        private clsTipoCambio tipocambio = null;
        private List<clsMoneda> lista_moneda = null;
        private List<clsTipoOperacion> lista_tipooperacion = null;
        private List<clsSerieFactura> lista_seriefactura = null;
        private List<clsTipoImpuesto> lista_tipoimpuesto = null;
        private List<clsTipoVenta> lista_tipoventa = null;
        private DataTable ticketfacturar = null;
        public clsComprobante comprobante { get; set; }
        private clsComprobante notacredito = null;
        private clsDetalleComprobante detallecomprobante = null;
        private List<clsDetalleComprobante> lista_detallecomprobate = null;
        private CclsConfiguracionEnvio cconfig = new CclsConfiguracionEnvio();
        private clsConfiguracionEnvio config = null;
        private Herramientas herra = new Herramientas();
        private DataSet data = null;
        private clsArchivo archivo = null;
        private clsRepositorio repositorio = null;
        private clsAdmRepositorio admrepo = new clsAdmRepositorio();
        private string mensajesunat = "";
        private int estadosunat = -1;
        private bool lectura = false;
        private clsAdmDiscrepancia admdis = new clsAdmDiscrepancia();
        private List<clsDiscrepancia> lista_discrepancia_ncc = null;
        private clsAdmProducto admproducto = new clsAdmProducto();

        /****************Facturacion Electrónica******************************/
        private DocumentoElectronico documento_electronico = null;
        private DetalleDocumento detalle_documento_electronico = null;
        private Conversion conversion = new Conversion();
        private string firmadig = "";
        private string resumenfirmadig = "";

        public string RutaArchivo { get; set; }
        public string TramaXmlSinFirma { get; set; }

        public frmVenta()
        {
            InitializeComponent();
        }

        private void frmVenta_Load(object sender, EventArgs e)
        {
            try
            {
                dg_item_pedido.DefaultCellStyle.Font =new Font("Segoe UI",12);
                dg_item_pedido.AutoGenerateColumns = false;
               // dg_item_pedido.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

                igv = admigv.listar_igv_anual();
                dt_fechaemision.Value = admcom.listar_fecha_actual();
                _listar_seriefacturacion();
                listar_tipooeracion_xestado();
                listar_moneda_xestado();
                listar_tipoventa();
                cliente = new clsCliente { Documento = "00000000" };
                buscar_clientexnumerodocumento();
                txt_igv.Text = igv.Valor.ToString();
                tipocambio = admtipoc.CargaTipoCambio(admcom.listar_fecha_actual(), 2);
                listar_configuracionenvio();
                ch_notacredito.Visible = false;
                btn_agregar.Focus();

                if (comprobante != null)
                {

                    listar_discrepancia_ncc();
                    lectura = true;
                    listar_detalle_comprobante_xidcomprobante();
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }

        }

        private void txt_ruc_dni_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txt_ruc_dni_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {

                    if (txt_ruc_dni.Text.Length > 0)
                    {

                        cliente = new clsCliente()
                        {
                            Documento = txt_ruc_dni.Text
                        };

                        buscar_clientexnumerodocumento();
                    }
                }


                if (e.KeyCode == Keys.F1)
                {

                    if (Application.OpenForms["frmCliente"] != null)
                    {
                        Application.OpenForms["frmCliente"].Activate();
                    }
                    else
                    {
                        cliente = null;
                        frmCliente frm_cliente = new frmCliente();
                        frm_cliente.frm_venta = this;
                        frm_cliente.usureg = usureg;
                        frm_cliente.ShowDialog();
                        if (cliente == null) { cliente = new clsCliente { Documento = "00000000" }; }
                        buscar_clientexnumerodocumento();
                    }
                }
            }
            catch (Exception) { }
        }

        private void txt_descuento_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }


            if (e.KeyChar == '.' && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }

            if (!char.IsControl(e.KeyChar))
            {

                TextBox textBox = (TextBox)sender;

                if (textBox.Text.IndexOf('.') > -1 &&
                         textBox.Text.Substring(textBox.Text.IndexOf('.')).Length >= 4)
                {
                    e.Handled = true;
                }

            }
        }

        private void txt_descripcion_TextChanged(object sender, EventArgs e)
        {
            txt_descripcion.CharacterCasing = CharacterCasing.Upper;
        }

        private void txt_descuento_Leave(object sender, EventArgs e)
        {
            try
            {
                if (txt_descuento.Text.Length == 0)
                {
                    txt_descuento.Text = "0.00";
                    recalcular_total();
                }
                else
                {

                    recalcular_total();
                }
            }
            catch (Exception) { }
        }

        private void btn_agregar_Click(object sender, EventArgs e)
        {
            try
            {
                if (cb_tipoventa.Items.Count > 0 && txt_seriecorrelativo.Text.Length == 0 && !lectura)
                {

                    // DialogResult respuesta = MessageBox.Show("¿Desea agregar ticket a facturar...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    /*if (respuesta == DialogResult.Yes)
                    {*/
                    if (Application.OpenForms["frmListaTicket"] != null)
                    {
                        Application.OpenForms["frmListaTicket"].Activate();
                    }
                    else
                    {
                        ticket = null;
                        frmListaTicket frm_listatick = new frmListaTicket();
                        frm_listatick.usureg = usureg;
                        frm_listatick.frm_venta = this;
                        frm_listatick.ShowDialog();
                        if (ticket != null)
                        {
                            ticket_afacturar();
                        }
                    }
                    //}
                }
            }
            catch (Exception) { }
        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!lectura)
                {
                    limpiar();
                }
            }
            catch (Exception) { }
        }

        private void btn_quitar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dg_item_pedido.Rows.Count > 0 && txt_seriecorrelativo.Text.Length == 0 && !lectura)
                {

                    if (dg_item_pedido.CurrentCell != null)
                    {

                        if (dg_item_pedido.CurrentCell.RowIndex != -1)
                        {

                            //DialogResult respuesta = MessageBox.Show("¿Desea quitar item a facturar...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                            /*if (respuesta == DialogResult.Yes)
                            {*/
                            if (dg_item_pedido.Rows.Count == 1)
                            {
                                limpiar();
                            }
                            else
                            {

                                dg_item_pedido.Rows.RemoveAt(dg_item_pedido.CurrentCell.RowIndex);
                                recalcular_total();
                            }

                            //}
                        }
                    }
                }
            }
            catch (Exception) { }
        }

        private void btn_guardar_Click(object sender, EventArgs e)
        {
            int filas_afectadas = -1;

            try
            {
                if (dg_item_pedido.Rows.Count > 0 && txt_seriecorrelativo.Text.Length == 0 && !lectura)
                {
                    if (cliente != null)
                    {
                        //DialogResult respuesta = MessageBox.Show("¿Desea registar comprobante?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        // if (respuesta == DialogResult.Yes)
                        //{
                       
                        switch (lista_seriefactura[cb_tipocomprobante.SelectedIndex].Idserie)
                        {

                            case 1:
                                if (cliente.Tipodocidentidad.Idtipodocumentoidentidad == 2)
                                {
                                    registrar_comprobante_blc_fcc();


                                    if (comprobante != null)
                                    {
                                        if (comprobante.Idcomprobante > 0)
                                        {
                                            filas_afectadas = admtick.actualizar_ticket_xidcomprobante(comprobante, usureg);
                                            if (filas_afectadas == -1) { MessageBox.Show("Problemas para actualizar ticket...", "Advertencia"); }
                                        }
                                    }
                                    btn_cobro.Focus();
                                }
                                else
                                {

                                    MessageBox.Show("Tipo de documento de identidad del cliente, no corresponde al tipo de comprobante a emitir...", "Advertencia");
                                }
                                break;
                            case 2:
                                if (cliente.Tipodocidentidad.Idtipodocumentoidentidad == 1)
                                {
                                    registrar_comprobante_blc_fcc();
                                    if (comprobante != null)
                                    {
                                        if (comprobante.Idcomprobante > 0)
                                        {
                                            filas_afectadas = admtick.actualizar_ticket_xidcomprobante(comprobante, usureg);
                                            if (filas_afectadas == -1) { MessageBox.Show("Problemas para actualizar ticket...", "Advertencia"); }
                                        }
                                    }
                                    btn_cobro.Focus();
                                }
                                else
                                {

                                    MessageBox.Show("Tipo de documento de identidad del cliente, no corresponde al tipo de comprobante a emitir...", "Advertencia");
                                }
                                break;

                        }
                        //}
                    }
                }
                if (dg_item_pedido.Rows.Count > 0 && txt_seriecorrelativo.Text.Length > 0 && lectura)
                {

                    if (ch_notacredito.Checked)
                    {

                        if (comprobante != null)
                        {
                            registrar_comprobante_ncc();
                        }
                    }

                }
            }
            catch (Exception) { }
        }

        private void txt_descuento_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {

                    if (txt_descuento.Text.Length > 0 && txt_seriecorrelativo.Text.Length == 0)
                    {
                        if (decimal.Parse(txt_descuento.Text) >= 0)
                        {
                            recalcular_total();
                        }

                    }
                    else
                    {
                        txt_descuento.Text = "0.00";
                    }
                }
            }
            catch (Exception) { }
        }

        /*********************Mis Metodos*************************/

        public void listar_discrepancia_ncc()
        {
            try
            {
                lista_discrepancia_ncc = admdis.listar_discrepancia_ncc();
            }
            catch (Exception) { }
        }
        public void listar_detalle_comprobante_xidcomprobante()
        {

            int item = 1;

            try
            {
                ticketfacturar = admcom.listar_detalle_comprobante_xidcomprobante(comprobante);
                comprobante = admcom.listar_comprobante_xid(comprobante);


                if (comprobante != null && ticketfacturar != null)
                {
                    if (comprobante.Seriefactura.Idserie == 1 || comprobante.Seriefactura.Idserie == 2)
                    {
                        // ch_notacredito.Visible = true;
                    }

                    if (ticketfacturar.Rows.Count > 0)
                    {
                        cb_tipocomprobante.SelectedValue = comprobante.Seriefactura.Tipodocumento;
                        dt_fechaemision.Value = comprobante.Fechaemsion;
                        cb_tipooperacion.SelectedValue = comprobante.Tipooperacion.Codsunat;
                        txt_seriecorrelativo.Text = comprobante.Numero;
                        cb_moneda.SelectedValue = comprobante.Moneda.CodSunat;
                        txt_ruc_dni.Text = comprobante.Cliente.Documento;
                        txt_razonsocial.Text = comprobante.Cliente.Razonsocial;
                        txt_direccion.Text = comprobante.Cliente.Direccion;
                        cb_tipoventa.SelectedValue = comprobante.Tipooperacion.Idtipooeracion;
                        txt_descripcion.Text = comprobante.Descripcion;
                        txt_descuento.Text = comprobante.Descuento.ToString();
                        txt_subtotal.Text = comprobante.Subtotal.ToString();
                        txt_igv_total.Text = comprobante.Igv.ToString();
                        txt_total.Text = comprobante.Total.ToString();
                        dg_item_pedido.DataSource = ticketfacturar;

                        lista_detallecomprobate = new List<clsDetalleComprobante>();

                        foreach (DataGridViewRow row in dg_item_pedido.Rows)
                        {

                            detallecomprobante = new clsDetalleComprobante()
                            {

                                Detallepedido = new clsDetallePedido()
                                {
                                    Iddetallepedido = int.Parse(row.Cells["iddetallepedido"].Value.ToString()),
                                    Producto = new clsProducto
                                    {
                                        IdProducto = int.Parse(row.Cells["idproducto"].Value.ToString()),
                                        Nombre = row.Cells["nombreproducto"].Value.ToString()
                                    },
                                    Unidad = new clsUnidadMedida()
                                    {

                                        Sigla = row.Cells["unidadmedida"].Value.ToString()
                                    },
                                    Cantidad = decimal.Parse(row.Cells["cantidad"].Value.ToString()),
                                    Preciounitario = decimal.Parse(row.Cells["preciounitario"].Value.ToString())

                                },

                                Tipoimpuesto = new clsTipoImpuesto()
                                {

                                    Idtipoimpuesto = int.Parse(row.Cells["idimpuesto"].Value.ToString()),
                                    Codsunat = row.Cells["codsunat"].Value.ToString()
                                },

                                Subtotal = decimal.Parse(row.Cells["valorventa"].Value.ToString()),
                                Igv = decimal.Parse(row.Cells["_igv"].Value.ToString()),
                                Total = decimal.Parse(row.Cells["precioventa"].Value.ToString())
                            };

                            lista_detallecomprobate.Add(detallecomprobante);
                        }

                        comprobante.Lista_detallecomprobante = lista_detallecomprobate;

                        documento_electronico = new DocumentoElectronico()
                        {
                            FechaEmision = comprobante.Fechaemsion.ToShortDateString(),
                            IdDocumento = comprobante.Numero,
                            TipoDocumento = lista_seriefactura[cb_tipocomprobante.SelectedIndex].Tipodocumento,
                            TipoOperacion = lista_tipooperacion[cb_tipooperacion.SelectedIndex].Codsunat,
                            Moneda = lista_moneda[cb_moneda.SelectedIndex].CodSunat,
                            DescuentoGlobal = comprobante.Descuento,
                            Emisor = new Contribuyente()
                            {

                                NroDocumento = empresa.Ruc,
                                TipoDocumento = empresa.Tipodocidentidad.Codsunat,
                                Direccion = empresa.Direccion,
                                Departamento = empresa.Departamento,
                                Provincia = empresa.Provincia,
                                Distrito = empresa.Distrito,
                                NombreLegal = empresa.Razonsocial,
                                NombreComercial = "",
                                Ubigeo = empresa.Ubigeo
                            },
                            Receptor = new Contribuyente()
                            {

                                NroDocumento = comprobante.Cliente.Documento,
                                TipoDocumento = comprobante.Cliente.Tipodocidentidad.Codsunat,
                                NombreLegal = comprobante.Cliente.Razonsocial,
                                NombreComercial = "",
                                Direccion = comprobante.Cliente.Direccion
                            },

                            TotalVenta = comprobante.Total
                        };

                        if (comprobante.Seriefactura.Idserie == 3 || comprobante.Seriefactura.Idserie == 8)
                        {

                            documento_electronico.Discrepancias = new List<Discrepancia>() {
                                new Discrepancia() {
                                    Descripcion=lista_discrepancia_ncc[0].Descripcion,
                                    NroReferencia=comprobante.Comprobanterelacionado.Numero,
                                    Tipo=lista_discrepancia_ncc[0].Codsunat
                                }
                            };
                            documento_electronico.Relacionados = new List<DocumentoRelacionado>() {
                                new DocumentoRelacionado() {
                                     NroDocumento= comprobante.Comprobanterelacionado.Numero,
                                     TipoDocumento=comprobante.Comprobanterelacionado.Seriefactura.Tipodocumento
                                 }

                            };
                        }

                        documento_electronico.MontoEnLetras = conversion.enletras(comprobante.Total.ToString());
                        documento_electronico.Gratuitas = comprobante.Gratuita;
                        documento_electronico.Gravadas = comprobante.Gravada;
                        documento_electronico.Exoneradas = comprobante.Exonerada;
                        documento_electronico.Inafectas = comprobante.Inafecta;
                        documento_electronico.TotalIgv = comprobante.Igv;
                        documento_electronico.DescuentoGlobal = comprobante.Descuento;

                        foreach (clsDetalleComprobante det in comprobante.Lista_detallecomprobante)
                        {

                            detalle_documento_electronico = new DetalleDocumento()
                            {

                                Id = item,
                                Cantidad = det.Detallepedido.Cantidad,
                                Descripcion = det.Detallepedido.Producto.Nombre,
                                CodigoItem = item.ToString(),
                                PrecioUnitario = det.Detallepedido.Preciounitario,
                                PrecioReferencial = decimal.Round((det.Detallepedido.Preciounitario / igv.Valorinverso), 2),
                                Impuesto = det.Igv,
                                TotalVenta = det.Subtotal,
                                TipoImpuesto = det.Tipoimpuesto.Codsunat,
                                UnidadMedida = det.Detallepedido.Unidad.Sigla
                            };

                            documento_electronico.Items.Add(detalle_documento_electronico);
                            item++;
                        }



                        //if (comprobante.Seriefactura.Idserie == 3 || comprobante.Seriefactura.Idserie == 8)
                        //{

                        //    var invoice = GeneradorXML.GenerarCreditNote(documento_electronico);
                        //    var serializador = new Serializador();
                        //    TramaXmlSinFirma = serializador.GenerarXml(invoice);

                        //}
                        //else
                        //{


                        //    var invoice = GeneradorXML.GenerarInvoice(documento_electronico);
                        //    var serializador = new Serializador();
                        //    TramaXmlSinFirma = serializador.GenerarXml(invoice);
                        //}


                        RutaArchivo = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Documentos\\" +
                        $"{documento_electronico.IdDocumento}.xml");
                        //File.WriteAllBytes(RutaArchivo, Convert.FromBase64String(TramaXmlSinFirma));

                        if (File.Exists(RutaArchivo))
                        {
                            var tramaXmlSinFirma = Convert.ToBase64String(File.ReadAllBytes(RutaArchivo));

                            var firmadoRequest = new FirmadoRequest
                            {
                                TramaXmlSinFirma = tramaXmlSinFirma,
                                CertificadoDigital = Convert.ToBase64String(
                                                                                 File.ReadAllBytes(herra.GetResourcesPath() + "\\" + empresa.Nombrecertificado)
                                                                            ),
                                PasswordCertificado = empresa.Clavecertificado,
                                UnSoloNodoExtension = false

                            };

                            FirmarController enviar = new FirmarController();
                            var respuestaFirmado = enviar.FirmadoResponse(firmadoRequest);

                            if (!respuestaFirmado.Exito)
                                throw new ApplicationException(respuestaFirmado.MensajeError);

                            resumenfirmadig = respuestaFirmado.ResumenFirma;
                            firmadig = respuestaFirmado.ValorFirma;
                        }
                        else
                        {
                            String Ruta = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Documentos");
                            clsRepositorio repo = admrepo.listar_archivo_xrepositorio_xcomprobante(comprobante);
                            File.WriteAllBytes(RutaArchivo,repo.Archivo.Xml);

                            var tramaXmlSinFirma = Convert.ToBase64String(File.ReadAllBytes(RutaArchivo));
                            

                            var firmadoRequest = new FirmadoRequest
                            {
                                TramaXmlSinFirma = tramaXmlSinFirma,
                                CertificadoDigital = Convert.ToBase64String(
                                                                                 File.ReadAllBytes(herra.GetResourcesPath() + "\\" + empresa.Nombrecertificado)
                                                                            ),
                                PasswordCertificado = empresa.Clavecertificado,
                                UnSoloNodoExtension = false

                            };

                            FirmarController enviar = new FirmarController();
                            var respuestaFirmado = enviar.FirmadoResponse(firmadoRequest);

                            if (!respuestaFirmado.Exito)
                                throw new ApplicationException(respuestaFirmado.MensajeError);

                            resumenfirmadig = respuestaFirmado.ResumenFirma;
                            firmadig = respuestaFirmado.ValorFirma;
                        }

                        
                    }

                }
            }
            catch (FormatException ex) {

                MessageBox.Show(ex.Message);
            }

        }
        public void listar_configuracionenvio()
        {
            try
            {
                config = cconfig.listar_configuracionenvio();
            }
            catch (Exception) { }
        }

        public void buscar_clientexnumerodocumento()
        {
            try
            {
                cliente = admcli.buscar_clientexnumerodocumento(cliente);
                txt_ruc_dni.Text = string.Empty;
                txt_razonsocial.Text = string.Empty;
                txt_direccion.Text = string.Empty;

                if (cliente != null)
                {
                    txt_ruc_dni.Text = cliente.Documento;
                    txt_razonsocial.Text = cliente.Razonsocial;
                    txt_direccion.Text = cliente.Direccion;

                }
                else
                {

                    MessageBox.Show("Presione F1 para registrar");
                }
            }
            catch (Exception) { }
        }

        public void _listar_seriefacturacion()
        {
            try
            {
                lista_seriefactura = admsefac._listar_seriefacturacion();

                if (lista_seriefactura != null)
                {

                    if (lista_seriefactura.Count > 0)
                    {

                        cb_tipocomprobante.DataSource = lista_seriefactura;
                        cb_tipocomprobante.DisplayMember = "Nombredocumento";
                        cb_tipocomprobante.ValueMember = "Tipodocumento";
                        cb_tipocomprobante.SelectedIndex = 1;
                        btn_agregar.Focus();
                    }
                }
            }
            catch (Exception) { }
        }

        public void listar_tipooeracion_xestado()
        {
            try
            {
                lista_tipooperacion = admtipoo.listar_tipooeracion_xestado();

                if (lista_tipooperacion != null)
                {

                    if (lista_tipooperacion.Count > 0)
                    {

                        cb_tipooperacion.DataSource = lista_tipooperacion;
                        cb_tipooperacion.DisplayMember = "Descripcion";
                        cb_tipooperacion.ValueMember = "Codsunat";
                    }
                }
            }
            catch (Exception) { }
        }

        public void listar_moneda_xestado()
        {
            try
            {
                lista_moneda = admmo.listar_moneda_xestado();

                if (lista_moneda != null)
                {
                    if (lista_moneda.Count > 0)
                    {
                        cb_moneda.DataSource = lista_moneda;
                        cb_moneda.DisplayMember = "SDescripcion";
                        cb_moneda.ValueMember = "CodSunat";
                    }
                }
            }
            catch (Exception) { }
        }

        public void listar_tipoventa()
        {
            try
            {
                lista_tipoventa = admtipv.listar_tipoventa();

                if (lista_tipoventa != null)
                {
                    if (lista_tipoventa.Count > 0)
                    {
                        cb_tipoventa.DataSource = lista_tipoventa;
                        cb_tipoventa.DisplayMember = "Descripcion";
                        cb_tipoventa.ValueMember = "Idtipoventa";
                    }
                }
            }
            catch (Exception) { }
        }

        public void ticket_afacturar()
        {
            try
            {
                if (ticket != null)
                {

                    dg_item_pedido.DataSource = null;
                    ticketfacturar = admtick.ticket_afacturar(ticket);

                    if (ticketfacturar != null)
                    {

                        if (ticketfacturar.Rows.Count > 0)
                        {

                            dg_item_pedido.DataSource = ticketfacturar;
                            dg_item_pedido.Refresh();
                            total();
                            btn_guardar.Focus();

                        }
                    }



                }
            }
            catch (Exception) { }
        }

        public void total()
        {
            try
            {
                if (dg_item_pedido.Rows.Count > 0)
                {
                    txt_subtotal.Text = (dg_item_pedido.Rows.Cast<DataGridViewRow>().Sum(x => decimal.Parse(x.Cells["valorventa"].Value.ToString()))).ToString();
                    txt_subtotal.Text =decimal.Round( decimal.Parse(txt_subtotal.Text),4).ToString();
                    txt_igv_total.Text = (dg_item_pedido.Rows.Cast<DataGridViewRow>().Sum(x => decimal.Parse(x.Cells["_igv"].Value.ToString()))).ToString();
                    txt_igv_total.Text = decimal.Round(decimal.Parse(txt_igv_total.Text), 4).ToString();
                    txt_total.Text = (dg_item_pedido.Rows.Cast<DataGridViewRow>().Sum(x => decimal.Parse(x.Cells["precioventa"].Value.ToString()))).ToString();
                    txt_total.Text = decimal.Round(decimal.Parse(txt_total.Text), 4).ToString();
                }
                else
                {

                    txt_subtotal.Text = "0.00";
                    txt_igv.Text = "0.00";
                    txt_total.Text = "0.00";
                }
            }
            catch (Exception) { }
        }

        public void recalcular_total()
        {

            decimal descuento = 0;
            decimal _total = 0;
            decimal _igv = 0;
            decimal subtotal = 0;

            try
            {

                if (dg_item_pedido.Rows.Count > 0)
                {
                    descuento = decimal.Round(decimal.Parse(txt_descuento.Text), 4);
                    _total = (dg_item_pedido.Rows.Cast<DataGridViewRow>().Sum(x => decimal.Parse(x.Cells["precioventa"].Value.ToString())));

                    if (descuento > 0 && descuento < _total)
                    {
                        _total = _total - descuento;
                        subtotal =(_total / igv.Valorinverso);
                        _igv = (subtotal * igv.Valor);
                        _total = subtotal + _igv;

                        txt_subtotal.Text =decimal.Round(subtotal,4).ToString();
                        txt_igv_total.Text = decimal.Round(_igv,4).ToString();
                        txt_total.Text = decimal.Round(_total,4).ToString();
                    }
                    else
                    {
                        if (descuento == 0)
                        {
                            total();
                        }
                        else
                        {

                            MessageBox.Show("Descuento no debe ser mayor al total de la venta...", "Advertencia");
                            txt_descuento.Text = "0.00";
                            recalcular_total();
                        }
                    }
                }
            }
            catch (Exception) { }

        }

        public void limpiar()
        {
            try
            {
                ch_notacredito.Visible = false;
                ticketfacturar = null;
                comprobante = null;
                detallecomprobante = null;
                lista_detallecomprobate = null;
                dg_item_pedido.DataSource = null;
                cb_tipocomprobante.SelectedIndex = 1;
                dt_fechaemision.Value = DateTime.Now;
                cb_tipooperacion.SelectedIndex = 0;
                txt_seriecorrelativo.Text = string.Empty;
                cb_moneda.SelectedIndex = 0;
                cliente = new clsCliente { Documento = "00000000" };
                buscar_clientexnumerodocumento();
                cb_tipoventa.SelectedIndex = 0;
                txt_descripcion.Text = string.Empty;
                txt_descuento.Text = "0.00";
                txt_subtotal.Text = "0.00";
                txt_igv_total.Text = "0.00";
                txt_total.Text = "0.00";
                btn_agregar.Focus();
            }
            catch (Exception) { }
        }

        public async void registrar_comprobante_blc_fcc()
        {
            byte[] archivo_xml = null;
            byte[] archivo_cdr = null;
            int item = 1;
            int codcomprobante = -1;
            string numerocomprobante = "";
            
            try
            {
                comprobante = new clsComprobante()
                {
                    Pedido = new clsPedido()
                    {
                        Idpedido = int.Parse(dg_item_pedido.Rows[0].Cells[0].Value.ToString())
                    },
                    Fechaemsion = admcom.listar_fecha_actual(),
                    Seriefactura = lista_seriefactura[cb_tipocomprobante.SelectedIndex],
                    Cliente = cliente,
                    Tipoventa = lista_tipoventa[cb_tipoventa.SelectedIndex],
                    Tipooperacion = lista_tipooperacion[cb_tipooperacion.SelectedIndex],
                    Descripcion = txt_descripcion.Text,
                    Moneda = lista_moneda[cb_moneda.SelectedIndex],
                    Descuento = decimal.Parse(txt_descuento.Text),
                    Subtotal = decimal.Parse(txt_subtotal.Text),
                    Igv = decimal.Parse(txt_igv_total.Text),
                    Total = decimal.Parse(txt_total.Text),

                    Gravada = decimal.Parse(txt_descuento.Text) == 0?decimal.Round((dg_item_pedido.Rows.Cast<DataGridViewRow>().Where(
                                                                    x => int.Parse(x.Cells["idimpuesto"].Value.ToString()) == 1

                                                                  ).Select(x => decimal.Parse(x.Cells["valorventa"].Value.ToString())).Sum()),2):decimal.Round( decimal.Parse(txt_subtotal.Text),2),

                    Exonerada = decimal.Round((dg_item_pedido.Rows.Cast<DataGridViewRow>().Where(
                                                                    x => int.Parse(x.Cells["idimpuesto"].Value.ToString()) == 9

                                                                  ).Select(x => decimal.Parse(x.Cells["valorventa"].Value.ToString())).Sum()),2),

                    Inafecta =decimal.Round( (dg_item_pedido.Rows.Cast<DataGridViewRow>().Where(
                                                                    x => int.Parse(x.Cells["idimpuesto"].Value.ToString()) == 11

                                                                  ).Select(x => decimal.Parse(x.Cells["valorventa"].Value.ToString())).Sum()),2),
                    Gratuita =decimal.Round( (dg_item_pedido.Rows.Cast<DataGridViewRow>().Where(
                                                                    x => int.Parse(x.Cells["idimpuesto"].Value.ToString()) == 10

                                                                  ).Select(x => decimal.Parse(x.Cells["valorventa"].Value.ToString())).Sum()),2),

                    Comprobanterelacionado = new clsComprobante()
                    {
                        Idcomprobante = 0,
                        Numero = "-"
                    },
                    Usuario = usureg
                };

                lista_detallecomprobate = new List<clsDetalleComprobante>();

                foreach (DataGridViewRow row in dg_item_pedido.Rows)
                {

                    detallecomprobante = new clsDetalleComprobante()
                    {
                        Detallepedido = new clsDetallePedido()
                        {
                            Iddetallepedido = int.Parse(row.Cells["iddetallepedido"].Value.ToString()),
                            Producto = new clsProducto
                            {
                                IdProducto = int.Parse(row.Cells["idproducto"].Value.ToString()),
                                Nombre = row.Cells["nombreproducto"].Value.ToString(),
                                Codsunat = row.Cells["codprodsunat"].Value.ToString()
                            },
                            Unidad = new clsUnidadMedida()
                            {

                                Sigla = row.Cells["unidadmedida"].Value.ToString()
                            },
                            Cantidad = decimal.Parse(row.Cells["cantidad"].Value.ToString()),
                            Preciounitario = decimal.Parse(row.Cells["preciounitario"].Value.ToString())

                        },

                        Tipoimpuesto = new clsTipoImpuesto()
                        {
                            Idtipoimpuesto = int.Parse(row.Cells["idimpuesto"].Value.ToString()),
                            Codsunat = row.Cells["codsunat"].Value.ToString()
                        },

                        Subtotal = decimal.Parse(row.Cells["valorventa"].Value.ToString()),
                        Igv = decimal.Parse(row.Cells["_igv"].Value.ToString()),
                        Total = decimal.Parse(row.Cells["precioventa"].Value.ToString())
                    };

                    lista_detallecomprobate.Add(detallecomprobante);

                }

                comprobante.Lista_detallecomprobante = lista_detallecomprobate;
                codcomprobante = admcom.registrar_comprobante(comprobante, usureg);

                if (codcomprobante > 0)
                {

                    comprobante.Idcomprobante = codcomprobante;
                    numerocomprobante = admcom.listar_numerocomprobante_xidcomprobante(comprobante);

                    Facturacion fact = new Facturacion();
                    comprobante.Numero = numerocomprobante;
                    await fact.GeneraDocumento(cliente,comprobante,lista_detallecomprobate);

                    if (numerocomprobante != "")
                    {
                        txt_seriecorrelativo.Text = numerocomprobante;
                        comprobante.Numero = numerocomprobante;

                        documento_electronico = new DocumentoElectronico()
                        {
                            FechaEmision = comprobante.Fechaemsion.ToShortDateString(),
                            IdDocumento = comprobante.Numero,
                            TipoDocumento = comprobante.Seriefactura.Tipodocumento,
                            TipoOperacion = comprobante.Tipooperacion.Codsunat,
                            Moneda = comprobante.Moneda.CodSunat,
                            DescuentoGlobal = comprobante.Descuento,
                            Emisor = new Contribuyente()
                            {

                                NroDocumento = empresa.Ruc,
                                TipoDocumento = empresa.Tipodocidentidad.Codsunat,
                                Direccion = empresa.Direccion,
                                Departamento = empresa.Departamento,
                                Provincia = empresa.Provincia,
                                Distrito = empresa.Distrito,
                                NombreLegal = empresa.Razonsocial,
                                NombreComercial = "",
                                Ubigeo = empresa.Ubigeo
                            },
                            Receptor = new Contribuyente()
                            {

                                NroDocumento = cliente.Documento,
                                TipoDocumento = cliente.Tipodocidentidad.Codsunat,
                                NombreLegal = cliente.Razonsocial,
                                NombreComercial = "",
                                Direccion = cliente.Direccion
                            },

                            TotalVenta = comprobante.Total
                        };

                        documento_electronico.MontoEnLetras = conversion.enletras(comprobante.Total.ToString());
                        documento_electronico.Gratuitas = decimal.Round(comprobante.Gratuita,2);
                        documento_electronico.Gravadas = decimal.Round(comprobante.Gravada,2);
                        documento_electronico.Exoneradas = decimal.Round(comprobante.Exonerada,2);
                        documento_electronico.Inafectas = decimal.Round(comprobante.Inafecta,2);
                        documento_electronico.TotalIgv = decimal.Round(comprobante.Igv,2);
                        documento_electronico.DescuentoGlobal = decimal.Round(comprobante.Descuento,2);

                        foreach (clsDetalleComprobante det in comprobante.Lista_detallecomprobante)
                        {

                            detalle_documento_electronico = new DetalleDocumento()
                            {

                                Id = item,
                                Cantidad = det.Detallepedido.Cantidad,
                                Descripcion = det.Detallepedido.Producto.Nombre,
                                CodigoItem = item.ToString(),
                                PrecioUnitario = decimal.Round(det.Detallepedido.Preciounitario-igv.Valorinverso, 2),
                                PrecioReferencial = decimal.Round((det.Detallepedido.Preciounitario), 2),
                                SubTotalVenta = decimal.Round(det.Subtotal,2),
                                Impuesto = decimal.Round(det.Igv,2),                               
                                TipoImpuesto = det.Tipoimpuesto.Codsunat,
                                UnidadMedida = det.Detallepedido.Unidad.Sigla,
                                TotalVenta = decimal.Round(det.Total,2)

                            };

                            documento_electronico.Items.Add(detalle_documento_electronico);
                            item++;
                        }

                        //var invoice = GeneradorXML.GenerarInvoice(documento_electronico);
                        //var serializador = new Serializador();
                        //TramaXmlSinFirma = serializador.GenerarXml(invoice);

                        RutaArchivo = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Documentos\\" +
                        $"{documento_electronico.IdDocumento}.xml");

                        //File.WriteAllBytes(RutaArchivo, Convert.FromBase64String(TramaXmlSinFirma));


                        var tramaXmlSinFirma = Convert.ToBase64String(File.ReadAllBytes(RutaArchivo));

                        var firmadoRequest = new FirmadoRequest
                        {
                            TramaXmlSinFirma = tramaXmlSinFirma,
                            CertificadoDigital = Convert.ToBase64String(File.ReadAllBytes(herra.GetResourcesPath() + "\\" + empresa.Nombrecertificado)),
                            PasswordCertificado = empresa.Clavecertificado,
                            UnSoloNodoExtension = false

                        };

                        FirmarController enviar = new FirmarController();
                        var respuestaFirmado = enviar.FirmadoResponse(firmadoRequest);

                        if (!respuestaFirmado.Exito)
                            throw new ApplicationException(respuestaFirmado.MensajeError);

                        resumenfirmadig = respuestaFirmado.ResumenFirma;
                        firmadig = respuestaFirmado.ValorFirma;

                        
                        if (config != null)
                        {
                            if (config.Estadoenvio == 1)
                            {
                                var enviarDocumentoRequest = new EnviarDocumentoRequest
                                {
                                    Ruc = empresa.Ruc,
                                    UsuarioSol = empresa.Usuariosol,
                                    ClaveSol = empresa.Clavesol,
                                    EndPointUrl = empresa.Urlenvio,
                                    IdDocumento = comprobante.Numero,
                                    TipoDocumento = comprobante.Seriefactura.Tipodocumento,
                                    TramaXmlFirmado = respuestaFirmado.TramaXmlFirmado
                                };

                                var respuestaEnvio = new EnviarDocumentoResponse();
                                EnviarDocumentoController enviarDoc = new EnviarDocumentoController();
                                respuestaEnvio = enviarDoc.EnviarDocumentoResponse(enviarDocumentoRequest);

                                var rpta = (EnviarDocumentoResponse)respuestaEnvio;

                                if (rpta != null)
                                {
                                    if (rpta.Exito)
                                    {
                                        if (!string.IsNullOrEmpty(rpta.TramaZipCdr))
                                        {
                                            archivo_xml = Convert.FromBase64String(respuestaFirmado.TramaXmlFirmado);
                                            archivo_cdr = Convert.FromBase64String(rpta.TramaZipCdr);

                                            File.WriteAllBytes($"{Program.CarpetaXml}\\{rpta.NombreArchivo}.xml",
                                                archivo_xml);
                                            File.WriteAllBytes($"{Program.CarpetaCdr}\\R-{rpta.NombreArchivo}.zip",
                                                archivo_cdr);

                                            documento_electronico.FirmaDigital = respuestaFirmado.ValorFirma;

                                            mensajesunat = rpta.MensajeRespuesta;
                                            estadosunat = int.Parse(rpta.CodigoRespuesta);

                                            if (File.Exists($"{Program.CarpetaXml}\\{rpta.NombreArchivo}.xml") &&
                                               File.Exists($"{Program.CarpetaCdr}\\R-{rpta.NombreArchivo}.zip"))
                                            {
                                                archivo = new clsArchivo()
                                                {

                                                    Xml = File.ReadAllBytes($"{Program.CarpetaXml}\\{rpta.NombreArchivo}.xml"),
                                                    Zip = File.ReadAllBytes($"{Program.CarpetaCdr}\\R-{rpta.NombreArchivo}.zip")

                                                };
                                            }
                                            else
                                            {

                                                archivo = new clsArchivo()
                                                {

                                                    Xml = File.ReadAllBytes(RutaArchivo)

                                                };
                                            }
                                        }
                                        else
                                        {
                                            if (rpta.MensajeRespuesta != null)
                                            {
                                                if (rpta.MensajeRespuesta.IndexOf("1033") > 0)
                                                {
                                                    estadosunat = 0;
                                                    mensajesunat = rpta.MensajeRespuesta;
                                                }
                                                else { estadosunat = -1; mensajesunat = rpta.MensajeRespuesta; }
                                            }
                                            else
                                            {

                                                estadosunat = -1;
                                                mensajesunat = "No Enviado";
                                            }

                                            archivo = new clsArchivo()
                                            {
                                                Xml = File.ReadAllBytes(RutaArchivo)
                                            };

                                        }

                                    }
                                    else
                                    {
                                        if (rpta.MensajeRespuesta != null)
                                        {
                                            if (rpta.MensajeRespuesta.IndexOf("1033") > 0)
                                            {
                                                estadosunat = 0;
                                                mensajesunat = rpta.MensajeRespuesta;
                                            }
                                            else { estadosunat = -1; mensajesunat = rpta.MensajeRespuesta; }
                                        }
                                        else
                                        {

                                            estadosunat = -1;
                                            mensajesunat = "No Enviado";
                                        }

                                        archivo = new clsArchivo()
                                        {
                                            Xml = File.ReadAllBytes(RutaArchivo)

                                        };

                                    }

                                }
                                else
                                {
                                    estadosunat = -1;
                                    mensajesunat = "No Enviado";
                                    archivo = new clsArchivo()
                                    {
                                        Xml = File.ReadAllBytes(RutaArchivo)

                                    };
                                }

                            }
                            else
                            {

                                estadosunat = -1;
                                mensajesunat = "No Enviado";
                                archivo = new clsArchivo()
                                {

                                    Xml = File.ReadAllBytes(RutaArchivo)

                                };

                            }

                            // MessageBox.Show("Comprobante registrado correctamente..", "Información");
                            btn_imprimir_comprobante.PerformClick();
                        }
                        else
                        {

                            MessageBox.Show("Comprobante no se genero correctamente...", "Advertencia");
                        }


                    }
                    else
                    {

                        MessageBox.Show("Problemas para registar el comprobante...", "Advertencia");
                    }
                }
                
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error");
            }

        }

        public static Byte[] CargarImagen(string rutaArchivo)
        {
            if (rutaArchivo != "")
            {
                try
                {
                    FileStream Archivo = new FileStream(rutaArchivo, FileMode.Open);//Creo el archivo
                    BinaryReader binRead = new BinaryReader(Archivo);//Cargo el Archivo en modo binario
                    Byte[] imagenEnBytes = new Byte[(Int64)Archivo.Length]; //Creo un Array de Bytes donde guardare la imagen
                    binRead.Read(imagenEnBytes, 0, (int)Archivo.Length);//Cargo la imagen en el array de Bytes
                    binRead.Close();
                    Archivo.Close();
                    return imagenEnBytes;//Devuelvo la imagen convertida en un array de bytes
                }
                catch
                {
                    return new Byte[0];
                }
            }
            return new byte[0];
        }

        public void enviar_repositorio()
        {

            try
            {
                if (repositorio != null)
                {
                    repositorio = admrepo.listar_repositorio_xtscfm(repositorio);

                    if (repositorio.Repositorioid == 0)
                    {
                        if (admrepo.registrar_repositorio(repositorio, usureg) == 0)
                        {
                            MessageBox.Show("Problemas para registro de documento en repositorio...", "Advertencia");
                        }

                    }
                    else
                    {

                        if (admrepo.actualizar_repositorio(repositorio, usureg) == 0)
                        {
                            MessageBox.Show("Problemas para actualizar información...", "Información");
                        }

                    }

                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error");
            }
        }

        public string impresora_xdefecto()
        {

            string nombreimpresora = "";//Donde guardare el nombre de la impresora por defecto
            try
            {
                //Busco la impresora por defecto
                for (int i = 0; i < PrinterSettings.InstalledPrinters.Count; i++)
                {
                    PrinterSettings a = new PrinterSettings();
                    a.PrinterName = PrinterSettings.InstalledPrinters[i].ToString();
                    if (a.IsDefaultPrinter)
                    {
                        nombreimpresora = PrinterSettings.InstalledPrinters[i].ToString();

                    }
                }

                return nombreimpresora;
            }
            catch (Exception)
            {
                return nombreimpresora;
            }
        }
        private void btn_imprimir_comprobante_Click(object sender, EventArgs e)
        {
            byte[] LogoEmp = null;
            string datosAdicionales_CDB = "";
            string CodigoCertificado = "";
            string nombreArchivo = "";
            string rutapdf = "";
            string nombreimpresora = "";

            try
            {
                nombreimpresora = impresora_xdefecto();

                if (dg_item_pedido.Rows.Count > 0 && txt_seriecorrelativo.Text.Length > 0)
                {
                    if (nombreimpresora != "")
                    {

                        if (documento_electronico != null)
                        {
                            if (notacredito != null)
                            {
                                data = admcom.imprimir_comprobante(notacredito);
                                comprobante = notacredito;
                            }
                            else
                            {

                                data = admcom.imprimir_comprobante(comprobante);
                            }


                            if (data != null)
                            {
                                String[] cad = documento_electronico.IdDocumento.Split('-');
                                String[] fecha = documento_electronico.FechaEmision.Split('/');

                                nombreArchivo = documento_electronico.Emisor.NroDocumento;
                                nombreArchivo += "-" + fecha[2] + "-" + fecha[1] + "-" + fecha[0];
                                nombreArchivo += "-" + documento_electronico.IdDocumento;

                                datosAdicionales_CDB = documento_electronico.Emisor.NroDocumento + "|" +
                                                       documento_electronico.TipoDocumento + "|" +
                                                       cad[0].ToString() + "|" +
                                                       cad[1].ToString() + "|" +
                                                       documento_electronico.TotalIgv + "|" +
                                                       documento_electronico.TotalVenta + "|" +
                                                       fecha[2] + "-" + fecha[1] + "-" + fecha[0] + "|" +
                                                       documento_electronico.Receptor.TipoDocumento + "|" +
                                                       documento_electronico.Receptor.NroDocumento;

                                CodigoCertificado = datosAdicionales_CDB + "|" + resumenfirmadig;

                                QRCodeGenerator codigobarras = new QRCodeGenerator();
                                QRCodeData qrcode = codigobarras.CreateQrCode(CodigoCertificado, QRCodeGenerator.ECCLevel.Q);
                                QRCode qrco = new QRCode(qrcode);
                                System.Drawing.Bitmap bm = qrco.GetGraphic(20);

                                //BarcodePDF417 codigobarras = new BarcodePDF417();
                                //codigobarras.Options = BarcodePDF417.PDF417_USE_ASPECT_RATIO;
                                //codigobarras.ErrorLevel = 5;
                                //codigobarras.YHeight = 6f;
                                //codigobarras.SetText(CodigoCertificado);
                                //System.Drawing.Bitmap bm = new System.Drawing.Bitmap(codigobarras.CreateDrawingImage(System.Drawing.Color.Black, System.Drawing.Color.White));
                                bm.Save(AppDomain.CurrentDomain.BaseDirectory + "\\QR\\" + nombreArchivo + ".jpeg", System.Drawing.Imaging.ImageFormat.Jpeg);

                                LogoEmp = CargarImagen(AppDomain.CurrentDomain.BaseDirectory + "\\QR\\" + nombreArchivo + ".jpeg");

                                if (data.Tables[0].Rows.Count > 0)
                                {
                                    foreach (DataTable mel in data.Tables)
                                    {
                                        foreach (DataRow changesRow in mel.Rows)
                                        {
                                            changesRow["firma"] = LogoEmp;
                                        }
                                        if (mel.HasErrors)
                                        {
                                            foreach (DataRow changesRow in mel.Rows)
                                            {
                                                if ((int)changesRow["Item", DataRowVersion.Current] > 100)
                                                {
                                                    changesRow.RejectChanges();
                                                    changesRow.ClearErrors();
                                                }
                                            }
                                        }
                                    }


                                    frmImpresionComprobante frm = new frmImpresionComprobante();
                                    CRComprobante rpt = new CRComprobante();
                                    rpt.SetDataSource(data);
                                    CrystalDecisions.CrystalReports.Engine.PrintOptions rptoption = rpt.PrintOptions;
                                    frm.crystal_comprobante.ReportSource = rpt;                                    
                                   /* if (estaenlinealaimpresora(nombreimpresora))
                                    { rpt.PrintToPrinter(1, false, 1, 1); }
                                    else { frm.ShowDialog(this); }*/
                                    //frm.ShowDialog(this);
                                 
                                    if (comprobante.Seriefactura.Idserie == 3 || comprobante.Seriefactura.Idserie == 8)
                                    {
                                        rutapdf = AppDomain.CurrentDomain.BaseDirectory + "\\NOTA_CREDITO_PDF\\" + nombreArchivo + ".pdf";
                                        rpt.ExportToDisk(
                                            CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, rutapdf);
                                    }

                                    if (comprobante.Seriefactura.Idserie == 2)
                                    {
                                        rutapdf = AppDomain.CurrentDomain.BaseDirectory + "\\BOLETAS_PDF\\" + nombreArchivo + ".pdf";
                                        rpt.ExportToDisk(
                                            CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, rutapdf);
                                    }
                                    if (comprobante.Seriefactura.Idserie == 1)
                                    {
                                        rutapdf = AppDomain.CurrentDomain.BaseDirectory + "\\FACTURAS_PDF\\" + nombreArchivo + ".pdf";
                                        rpt.ExportToDisk(
                                            CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, rutapdf);
                                    }                                  
                                    
                                    if (archivo != null)
                                    {

                                        repositorio = new clsRepositorio();
                                        archivo.Pdf = File.ReadAllBytes(rutapdf);
                                        repositorio.Idcomprobante = comprobante.Idcomprobante;
                                        repositorio.Archivo = archivo;
                                        repositorio.Tipodocumento = comprobante.Seriefactura.Tipodocumento;
                                        repositorio.Serie = comprobante.Numero.Substring(0, comprobante.Numero.IndexOf("-"));
                                        repositorio.Correlativo = (comprobante.Numero.Substring(
                                                                    (comprobante.Numero.IndexOf("-") + 1),
                                                                        (comprobante.Numero.Length -
                                                                            (comprobante.Numero.IndexOf("-") + 1)
                                                                        )
                                                                    ));
                                        repositorio.Comprobante = comprobante.Numero;
                                        repositorio.Fechaemision = comprobante.Fechaemsion;
                                        repositorio.Monto = comprobante.Total;
                                        repositorio.Estadosunat = estadosunat;
                                        repositorio.Mensajesunat = mensajesunat;

                                        repositorio.Nombredocxml = comprobante.Numero + ".xml";
                                        repositorio.Rutaxml = RutaArchivo;
                                        repositorio.Nombredocpdf = nombreArchivo + ".pdf";
                                        repositorio.Rutapdf = rutapdf;
                                        repositorio.Pcorigen = SystemInformation.UserDomainName;
                                        repositorio.Usuariopc = SystemInformation.UserName;

                                        enviar_repositorio();

                                    }

                                    rptoption.ApplyPageMargins(new CrystalDecisions.Shared.PageMargins(0, 0, 0, 20));
                                    rpt.PrintOptions.PrinterName = nombreimpresora;
                                    rpt.PrintToPrinter(1, false, 1, 1);
                                    rpt.Close();
                                    rpt.Dispose();
                                }
                            }
                        }
                    }
                    else
                    {

                        MessageBox.Show("No se ha configurado la ticketera por defecto...", "Advertencia");
                    }

                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error");
            }
        }     

        private void btn_cobro_Click(object sender, EventArgs e)
        {
            int id = -1;

            try
            {
                if (comprobante != null)
                {
                    if (comprobante.Seriefactura.Idserie == 1 || comprobante.Seriefactura.Idserie == 2)
                    {
                        comprobante = admcom.listar_comprobante_xid(comprobante);

                        if (comprobante.Estado == 1)
                        {
                            if (comprobante.Total > comprobante.Totalcancelado)
                            {
                                id = admca.buscar_caja_abierta(usureg);

                                if (id > 0)
                                {
                                    caja = new clsCaja()
                                    {
                                        Idcaja = id

                                    };

                                    comprobante.Caja = caja;

                                    if (Application.OpenForms["frm_Cobro"] != null)
                                    {
                                        Application.OpenForms["frm_Cobro"].Activate();
                                    }
                                    else
                                    {
                                        frmCobro frm_cobro = new frmCobro();
                                        frm_cobro.usureg = usureg;
                                        frm_cobro.comprobante = comprobante;
                                        frm_cobro.ShowDialog();

                                        comprobante = admcom.listar_comprobante_xid(comprobante);

                                        if (comprobante.Total > comprobante.Totalcancelado)
                                        {
                                            btn_cobro.PerformClick();
                                        }
                                    }
                                }
                                else
                                {

                                    MessageBox.Show("Por favor aperture caja...", "Advertencia");
                                }
                            }
                            else
                            {
                                MessageBox.Show("Comprobante ya cancelado en su totalidad...", "Advertencia");

                            }
                        }
                        else
                        {
                            MessageBox.Show("Comprobante se encuentra anulado...", "Advertencia");

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Hubo un error...."+ex.Message.ToString());
            }
        }

        public void registrar_comprobante_ncc()
        {
            byte[] archivo_xml = null;
            byte[] archivo_cdr = null;
            int item = 1;
            int codcomprobante = -1;
            string numerocomprobante = "";            
            int index_tipocomprobante = 0;
                        
            try
            {
                if (comprobante.Seriefactura.Idserie == 1)
                {
                    index_tipocomprobante = 4;
                }

                if (comprobante.Seriefactura.Idserie == 2)
                {
                    index_tipocomprobante = 2;
                }
                notacredito = new clsComprobante()
                {
                    Pedido = new clsPedido()
                    {
                        Idpedido = int.Parse(dg_item_pedido.Rows[0].Cells[0].Value.ToString())
                    },
                    Fechaemsion = admcom.listar_fecha_actual(),
                    Seriefactura = lista_seriefactura[index_tipocomprobante],
                    Cliente = comprobante.Cliente,
                    Tipoventa = lista_tipoventa[cb_tipoventa.SelectedIndex],
                    Tipooperacion = lista_tipooperacion[cb_tipooperacion.SelectedIndex],
                    Descripcion = txt_descripcion.Text,
                    Moneda = lista_moneda[cb_moneda.SelectedIndex],
                    Descuento = decimal.Parse(txt_descuento.Text),
                    Subtotal = decimal.Parse(txt_subtotal.Text),
                    Igv = decimal.Parse(txt_igv_total.Text),
                    Total = decimal.Parse(txt_total.Text),

                    Gravada = (dg_item_pedido.Rows.Cast<DataGridViewRow>().Where(
                                                                    x => int.Parse(x.Cells["idimpuesto"].Value.ToString()) == 1

                                                                  ).Select(x => decimal.Parse(x.Cells["valorventa"].Value.ToString())).Sum()),

                    Exonerada = (dg_item_pedido.Rows.Cast<DataGridViewRow>().Where(
                                                                    x => int.Parse(x.Cells["idimpuesto"].Value.ToString()) == 9

                                                                  ).Select(x => decimal.Parse(x.Cells["valorventa"].Value.ToString())).Sum()),

                    Inafecta = (dg_item_pedido.Rows.Cast<DataGridViewRow>().Where(
                                                                    x => int.Parse(x.Cells["idimpuesto"].Value.ToString()) == 11

                                                                  ).Select(x => decimal.Parse(x.Cells["valorventa"].Value.ToString())).Sum()),
                    Gratuita = (dg_item_pedido.Rows.Cast<DataGridViewRow>().Where(
                                                                    x => int.Parse(x.Cells["idimpuesto"].Value.ToString()) == 10

                                                                  ).Select(x => decimal.Parse(x.Cells["valorventa"].Value.ToString())).Sum()),

                    Comprobanterelacionado = comprobante,
                    Usuario = usureg
                };

                lista_detallecomprobate = new List<clsDetalleComprobante>();

                foreach (DataGridViewRow row in dg_item_pedido.Rows)
                {

                    detallecomprobante = new clsDetalleComprobante()
                    {

                        Detallepedido = new clsDetallePedido()
                        {
                            Iddetallepedido = int.Parse(row.Cells["iddetallepedido"].Value.ToString()),
                            Producto = new clsProducto
                            {
                                IdProducto = int.Parse(row.Cells["idproducto"].Value.ToString()),
                                Nombre = row.Cells["nombreproducto"].Value.ToString()
                            },
                            Unidad = new clsUnidadMedida()
                            {

                                Sigla = row.Cells["unidadmedida"].Value.ToString()
                            },
                            Cantidad = decimal.Parse(row.Cells["cantidad"].Value.ToString()),
                            Preciounitario = decimal.Parse(row.Cells["preciounitario"].Value.ToString())

                        },

                        Tipoimpuesto = new clsTipoImpuesto()
                        {

                            Idtipoimpuesto = int.Parse(row.Cells["idimpuesto"].Value.ToString()),
                            Codsunat = row.Cells["codsunat"].Value.ToString()
                        },

                        Subtotal = decimal.Parse(row.Cells["valorventa"].Value.ToString()),
                        Igv = decimal.Parse(row.Cells["_igv"].Value.ToString()),
                        Total = decimal.Parse(row.Cells["precioventa"].Value.ToString())
                    };

                    lista_detallecomprobate.Add(detallecomprobante);

                }

                notacredito.Lista_detallecomprobante = lista_detallecomprobate;
                codcomprobante = admcom.registrar_comprobante(notacredito, usureg);

                if (codcomprobante > 0)
                {

                    notacredito.Idcomprobante = codcomprobante;
                    numerocomprobante = admcom.listar_numerocomprobante_xidcomprobante(notacredito);

                    if (numerocomprobante != "")
                    {
                        txt_seriecorrelativo.Text = numerocomprobante;
                        notacredito.Numero = numerocomprobante;

                        documento_electronico = new DocumentoElectronico()
                        {
                            FechaEmision = notacredito.Fechaemsion.ToShortDateString(),
                            IdDocumento = notacredito.Numero,
                            TipoDocumento = notacredito.Seriefactura.Tipodocumento,
                            TipoOperacion = notacredito.Tipooperacion.Codsunat,
                            Moneda = notacredito.Moneda.CodSunat,
                            DescuentoGlobal = notacredito.Descuento,
                            Emisor = new Contribuyente()
                            {

                                NroDocumento = empresa.Ruc,
                                TipoDocumento = empresa.Tipodocidentidad.Codsunat,
                                Direccion = empresa.Direccion,
                                Departamento = empresa.Departamento,
                                Provincia = empresa.Provincia,
                                Distrito = empresa.Distrito,
                                NombreLegal = empresa.Razonsocial,
                                NombreComercial = "",
                                Ubigeo = empresa.Ubigeo
                            },
                            Receptor = new Contribuyente()
                            {
                                NroDocumento = notacredito.Cliente.Documento,
                                TipoDocumento = notacredito.Cliente.Tipodocidentidad.Codsunat,
                                NombreLegal = notacredito.Cliente.Razonsocial,
                                NombreComercial = "",
                                Direccion = cliente.Direccion
                            },

                            TotalVenta = notacredito.Total,
                            Discrepancias=new List<Discrepancia>() {
                                new Discrepancia() {
                                    Descripcion=lista_discrepancia_ncc[0].Descripcion,
                                    NroReferencia=notacredito.Comprobanterelacionado.Numero,
                                    Tipo=lista_discrepancia_ncc[0].Codsunat
                                }
                            },
                            Relacionados=new List<DocumentoRelacionado>() {
                                new DocumentoRelacionado() {
                                     NroDocumento= notacredito.Comprobanterelacionado.Numero,
                                     TipoDocumento=notacredito.Comprobanterelacionado.Seriefactura.Tipodocumento
                                 }

                            }
                            
                        };

                        documento_electronico.MontoEnLetras = conversion.enletras(notacredito.Total.ToString());
                        documento_electronico.Gratuitas = notacredito.Gratuita;
                        documento_electronico.Gravadas = notacredito.Gravada;
                        documento_electronico.Exoneradas = notacredito.Exonerada;
                        documento_electronico.Inafectas = notacredito.Inafecta;
                        documento_electronico.TotalIgv = notacredito.Igv;
                        documento_electronico.DescuentoGlobal = notacredito.Descuento;

                        foreach (clsDetalleComprobante det in notacredito.Lista_detallecomprobante)
                        {

                            detalle_documento_electronico = new DetalleDocumento()
                            {

                                Id = item,
                                Cantidad = det.Detallepedido.Cantidad,
                                Descripcion = det.Detallepedido.Producto.Nombre,
                                CodigoItem = item.ToString(),
                                PrecioUnitario = det.Detallepedido.Preciounitario,
                                PrecioReferencial = decimal.Round((det.Detallepedido.Preciounitario / igv.Valorinverso), 2),
                                Impuesto = det.Igv,
                                TotalVenta = det.Subtotal,
                                TipoImpuesto = det.Tipoimpuesto.Codsunat,
                                UnidadMedida = det.Detallepedido.Unidad.Sigla
                            };

                            documento_electronico.Items.Add(detalle_documento_electronico);
                            item++;
                        }

                        var nota = GeneradorXML.GenerarCreditNote(documento_electronico);
                        var serializador = new Serializador();
                        TramaXmlSinFirma = serializador.GenerarXml(nota);

                        RutaArchivo = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Documentos\\" +
                        $"{documento_electronico.IdDocumento}.xml");
                        File.WriteAllBytes(RutaArchivo, Convert.FromBase64String(TramaXmlSinFirma));

                        var tramaXmlSinFirma = Convert.ToBase64String(File.ReadAllBytes(RutaArchivo));

                        var firmadoRequest = new FirmadoRequest
                        {
                            TramaXmlSinFirma = tramaXmlSinFirma,
                            CertificadoDigital = Convert.ToBase64String(
                                                                             File.ReadAllBytes(herra.GetResourcesPath() + "\\" + empresa.Nombrecertificado)
                                                                        ),
                            PasswordCertificado = empresa.Clavecertificado,
                            UnSoloNodoExtension = false

                        };

                        FirmarController enviar = new FirmarController();
                        var respuestaFirmado = enviar.FirmadoResponse(firmadoRequest);

                        if (!respuestaFirmado.Exito)
                            throw new ApplicationException(respuestaFirmado.MensajeError);

                        resumenfirmadig = respuestaFirmado.ResumenFirma;
                        firmadig = respuestaFirmado.ValorFirma;


                        if (config != null)
                        {
                            if (config.Estadoenvio == 1)
                            {
                                var enviarDocumentoRequest = new EnviarDocumentoRequest
                                {
                                    Ruc = empresa.Ruc,
                                    UsuarioSol = empresa.Usuariosol,
                                    ClaveSol = empresa.Clavesol,
                                    EndPointUrl = empresa.Urlenvio,
                                    IdDocumento = notacredito.Numero,
                                    TipoDocumento = notacredito.Seriefactura.Tipodocumento,
                                    TramaXmlFirmado = respuestaFirmado.TramaXmlFirmado
                                };

                                var respuestaEnvio = new EnviarDocumentoResponse();
                                EnviarDocumentoController enviarDoc = new EnviarDocumentoController();
                                respuestaEnvio = enviarDoc.EnviarDocumentoResponse(enviarDocumentoRequest);

                                var rpta = (EnviarDocumentoResponse)respuestaEnvio;

                                if (rpta != null)
                                {
                                    if (rpta.CodigoRespuesta != null)
                                    {
                                        if (rpta.CodigoRespuesta == "0")
                                        {
                                            if (rpta.Exito && !string.IsNullOrEmpty(rpta.TramaZipCdr))
                                            {
                                                archivo_xml = Convert.FromBase64String(respuestaFirmado.TramaXmlFirmado);
                                                archivo_cdr = Convert.FromBase64String(rpta.TramaZipCdr);

                                                File.WriteAllBytes($"{Program.CarpetaXml}\\{rpta.NombreArchivo}.xml",
                                                    archivo_xml);
                                                File.WriteAllBytes($"{Program.CarpetaCdr}\\R-{rpta.NombreArchivo}.zip",
                                                    archivo_cdr);
                                                documento_electronico.FirmaDigital = respuestaFirmado.ValorFirma;

                                                mensajesunat = rpta.MensajeRespuesta;
                                                estadosunat = int.Parse(rpta.CodigoRespuesta);

                                                if (File.Exists($"{Program.CarpetaXml}\\{rpta.NombreArchivo}.xml") &&
                                                   File.Exists($"{Program.CarpetaCdr}\\R-{rpta.NombreArchivo}.zip"))
                                                {
                                                    archivo = new clsArchivo()
                                                    {

                                                        Xml = File.ReadAllBytes($"{Program.CarpetaXml}\\{rpta.NombreArchivo}.xml"),
                                                        Zip = File.ReadAllBytes($"{Program.CarpetaCdr}\\R-{rpta.NombreArchivo}.zip")

                                                    };
                                                }
                                                else
                                                {

                                                    archivo = new clsArchivo()
                                                    {

                                                        Xml = File.ReadAllBytes(RutaArchivo)

                                                    };
                                                }

                                            }
                                            else
                                            {
                                                estadosunat = -1;
                                                mensajesunat = rpta.MensajeRespuesta;
                                                archivo = new clsArchivo()
                                                {

                                                    Xml = File.ReadAllBytes(RutaArchivo)

                                                };

                                            }
                                        }
                                        else
                                        {
                                            estadosunat = -1;
                                            mensajesunat = rpta.MensajeRespuesta;
                                            archivo = new clsArchivo()
                                            {

                                                Xml = File.ReadAllBytes(RutaArchivo)

                                            };

                                        }
                                    }
                                    else
                                    {
                                        estadosunat = -1;
                                        mensajesunat = rpta.MensajeRespuesta;
                                        archivo = new clsArchivo()
                                        {

                                            Xml = File.ReadAllBytes(RutaArchivo)

                                        };

                                    }
                                }
                                else
                                {

                                    estadosunat = -1;
                                    mensajesunat = "No Enviado";
                                    archivo = new clsArchivo()
                                    {

                                        Xml = File.ReadAllBytes(RutaArchivo)

                                    };
                                }
                            }
                            else
                            {

                                estadosunat = -1;
                                mensajesunat = "No Enviado";
                                archivo = new clsArchivo()
                                {

                                    Xml = File.ReadAllBytes(RutaArchivo)

                                };

                            }
                        }
                        else
                        {

                            estadosunat = -1;
                            mensajesunat = "No Enviado";
                            archivo = new clsArchivo()
                            {

                                Xml = File.ReadAllBytes(RutaArchivo)

                            };

                        }

                        MessageBox.Show("Comprobante registrado correctamente..", "Información");
                        btn_imprimir_comprobante.PerformClick();
                    }
                    else
                    {

                        MessageBox.Show("Comprobante no se genero correctamente...", "Advertencia");
                    }


                }
                else
                {

                    MessageBox.Show("Problemas para registar el comprobante...", "Advertencia");
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error");
            }

        }

        private void btn_cliente_Click(object sender, EventArgs e)
        {
            try
            {
                txt_ruc_dni_KeyDown(sender, new KeyEventArgs(Keys.F1));
            }
            catch (Exception) { }
        }

        private void cb_tipocomprobante_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (lista_seriefactura != null)
                {
                    switch (lista_seriefactura[cb_tipocomprobante.SelectedIndex].Idserie)
                    {
                        case 1:

                            cliente = null;
                            txt_ruc_dni.Text = string.Empty;
                            txt_razonsocial.Text = string.Empty;
                            txt_direccion.Text = string.Empty;
                            txt_ruc_dni.Focus();

                            break;
                        case 2:

                            if (cliente == null) { cliente = new clsCliente { Documento = "00000000" }; }
                            buscar_clientexnumerodocumento();

                            break;
                    }

                }
            }
            catch (Exception) { }
            
        }

        public bool estaenlinealaimpresora(string printerName)
        {
            string str = "";
            bool online = false;

            ManagementScope scope = new ManagementScope(ManagementPath.DefaultPath);

            scope.Connect();

            //Consulta para obtener las impresoras, en la API Win32
            SelectQuery query = new SelectQuery("select * from Win32_Printer ");

            ManagementClass m = new ManagementClass("Win32_Printer");

            ManagementObjectSearcher obj = new ManagementObjectSearcher(scope, query);

            //Obtenemos cada instancia del objeto ManagementObjectSearcher
            using (ManagementObjectCollection printers = m.GetInstances())
                foreach (ManagementObject printer in printers)
                {
                    if (printer != null)
                    {
                        //Obtenemos la primera impresora en el bucle
                        str = printer["Name"].ToString().ToLower();

                        if (str.Equals(printerName.ToLower()))
                        {
                            //Una vez encontrada verificamos el estado de ésta
                            if (printer["WorkOffline"].ToString().ToLower().Equals("true") || printer["PrinterStatus"].Equals(7))
                                //Fuera de línea
                                online = false;
                            else
                                //En línea
                                online = true;
                        }
                    }
                    else
                        throw new Exception("No fueron encontradas impresoras instaladas en el equipo");
                }
            return online;
        }

        private void groupPanel3_Click(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (chkBolsa.Checked)
            {
                
                if (admproducto.validaProductoBolsa() > 0)
                {
                    cantBolsas.Enabled = true;
                }
                else
                {
                    MessageBox.Show("No tiene registrado el item bolsa plastica");
                }
            }
            else
            {
                cantBolsas.Enabled = false;
            }
        }
    }   
}
