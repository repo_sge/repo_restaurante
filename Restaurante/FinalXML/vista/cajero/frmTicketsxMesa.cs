﻿using FinalXML.Administradores;
using FinalXML.Entidades;
using FinalXML.vista.mozo;
using FinalXML.vista.reporte;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalXML.vista.cajero
{
    public partial class frmTicketsxMesa : PlantillaBase
    {
        public clsUsuario usureg { get; set; }
        private clsMesa mesa = null;
        private clsProducto producto = null;
        private clsPedido pedidoparallevar = null;
        private clsTicket ticket = null;
        private DataTable mesas = null;
        private DataTable tickets = null;
        public  DataTable pedido = null;
        private clsAdmMesa admme = new clsAdmMesa();
        private clsAdmTicket admtick = new clsAdmTicket();
        private clsAdmPedido admped = new clsAdmPedido();
        private clsAdmProducto admpro = new clsAdmProducto();
        public clsPedido pedidoenmesa { get; set; }
        public int indexpedido { get; set; }
        public bool autorizado { get; set; }

        private DataSet data = null;
        public frmTicketsxMesa()
        {
            InitializeComponent();
        }

        private void frmTicketsxMesa_Load(object sender, EventArgs e)
        {
            indexpedido = -1;            
            dg_ticket.AutoGenerateColumns = false;
            dg_ticket.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            dg_ticket.DefaultCellStyle.Font = new Font("Segoe UI", 12);
            dg_pedido.DefaultCellStyle.Font =new Font("Segoe UI",12);
            //dg_pedido.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            cb_estado.SelectedIndex = 0;
            dt_fechaini.Value = DateTime.Now;
            dt_fechafin.Value = DateTime.Now;
            listar_mesa();
        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            lb_monto.Text = "0.00";
            if (cb_mesa.Items.Count > 0)
            {
                listar_tickets_xmesa();
            }
        }
        public void dg_ticket_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dg_ticket.Rows.Count > 0)
                {
                    if (e.RowIndex != -1)
                    {
                        limpiar_pedido();

                        if (indexpedido == -1)
                        {

                            indexpedido = e.RowIndex;
                            listar_pedido_xidpedido(indexpedido);


                        }
                        else
                        {

                            indexpedido = e.RowIndex;
                            listar_pedido_xidpedido(indexpedido);

                        }

                        cb_estado.Text = dg_ticket.Rows[e.RowIndex].Cells[estado.Index].Value.ToString();
                        cb_mesa.Text = dg_ticket.Rows[e.RowIndex].Cells[nombremesa.Index].Value.ToString();
                        dt_fechaini.Value = Convert.ToDateTime(dg_ticket.Rows[e.RowIndex].Cells[fecha.Index].Value.ToString());
                        dt_fechafin.Value = Convert.ToDateTime(dg_ticket.Rows[e.RowIndex].Cells[fecha.Index].Value.ToString());
                    }
                }
            }
            catch (Exception) { }
        }

        private void cb_estado_SelectedIndexChanged(object sender, EventArgs e)
        {
           /* dg_ticket.DataSource = null;
            dg_pedido.DataSource = null;
            lb_monto.Text = "0.00";
            pedidoenmesa = null;*/
        }

        private void btn_agregar_Click(object sender, EventArgs e)
        {
            try
            {
                if (cb_estado.SelectedIndex == 1 && dg_ticket.Rows.Count > 0 && dg_pedido.Rows.Count > 0)
                {
                    //DialogResult respuesta = MessageBox.Show("¿Desea agregar pedido la Mesa " + cb_mesa.SelectedValue + " ?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    //if (respuesta == DialogResult.Yes)
                    //{
                    if (Application.OpenForms["frm_DetallePedido"] != null)
                    {
                        Application.OpenForms["frm_DetallePedido"].Activate();
                    }
                    else
                    {
                        frmDetallePedido frm_detpedido = new frmDetallePedido();
                        frm_detpedido.frmticketxmesa = this;
                        frm_detpedido.usureg = usureg;
                        frm_detpedido.mesa = mesa;
                        frm_detpedido.pedidoenmesa = pedidoenmesa;
                        frm_detpedido.ShowDialog();
                        if (pedido != null)
                        {
                            columna_bloqueda();
                            btn_agregar.Focus();
                        }
                    }
                    //}
                }
                else
                {

                    MessageBox.Show("No se puede agregar a ticket´s " + cb_estado.Text, "Advertencia");

                }
            }
            catch (Exception) { }
        }

        private void dg_pedido_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int parallevar = 0;
            int atendido = 0;

            try
            {
                if (dg_pedido.Rows.Count > 0)
                {
                    if (e.RowIndex != -1)
                    {

                        if (e.ColumnIndex == 7)
                        {

                            DataGridViewCheckBoxCell ch = null;
                            ch = (DataGridViewCheckBoxCell)dg_pedido.Rows[e.RowIndex].Cells[e.ColumnIndex];

                            if (ch != null)
                            {
                                producto = new clsProducto()
                                {
                                    IdProducto = int.Parse(dg_pedido.Rows[e.RowIndex].Cells["COD PROD"].Value.ToString())
                                };

                                if ((bool)ch.Value)
                                {
                                    ch.Value = false;
                                    parallevar = 0;
                                }
                                else
                                {
                                    ch.Value = true;
                                    parallevar = 1;
                                }

                                if (admpro.producto_paracocina(producto) == 1)
                                {
                                    pedidoparallevar = new clsPedido()
                                    {
                                        Lista_detallepedido = new List<clsDetallePedido>() {

                                        new clsDetallePedido() {

                                            Iddetallepedido= int.Parse(dg_pedido.Rows[e.RowIndex].Cells["COD DETA"].Value.ToString()),
                                            Parallevar=parallevar
                                        }
                                    }
                                    };

                                    if (admped.actualizar_detallepedido_parallevar(pedidoparallevar, usureg) < 0)
                                    {
                                        MessageBox.Show("Problemas para actualizar pedido...", "Advertencia");
                                    }
                                }

                            }
                        }

                        if (e.ColumnIndex == 8)
                        {
                            DataGridViewCheckBoxCell ch = null;
                            ch = (DataGridViewCheckBoxCell)dg_pedido.Rows[e.RowIndex].Cells[e.ColumnIndex];

                            if (ch != null)
                            {
                                producto = new clsProducto()
                                {
                                    IdProducto = int.Parse(dg_pedido.Rows[e.RowIndex].Cells["COD PROD"].Value.ToString())
                                };

                                if ((bool)ch.Value)
                                {
                                    ch.Value = false;
                                    atendido = 0;
                                }
                                else
                                {
                                    ch.Value = true;
                                    atendido = 1;
                                }

                                if (admpro.producto_paracocina(producto) == 1)
                                {
                                    pedidoparallevar = new clsPedido()
                                    {
                                        Lista_detallepedido = new List<clsDetallePedido>() {

                                        new clsDetallePedido() {

                                            Iddetallepedido= int.Parse(dg_pedido.Rows[e.RowIndex].Cells["COD DETA"].Value.ToString()),
                                            Atendido=atendido
                                        }
                                    }
                                    };

                                    if (admped.actualizar_detalleordencocina_atendido(pedidoparallevar, usureg) < 0)
                                    {
                                        MessageBox.Show("Problemas para actualizar pedido...", "Advertencia");
                                    }
                                }

                            }
                        }
                    }
                }
            }
            catch (Exception) { }
        }
        private void btn_quitar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dg_pedido.Rows.Count > 0 && cb_estado.SelectedIndex == 1)
                {

                    if (dg_pedido.CurrentCell != null)
                    {
                        if (dg_pedido.CurrentCell.RowIndex != -1)
                        {

                            if (dg_pedido.Rows.Count == 1)
                            {
                                /***LLamar a anular pedido**************/
                                btn_anular_ticket.PerformClick();

                            }
                            else
                            {
                                // DialogResult respuesta = MessageBox.Show("¿Desea quitar pedido la Mesa " + cb_mesa.SelectedValue + " ?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                                //if (respuesta == DialogResult.Yes)
                                //{
                                autorizado = false;

                                if (Application.OpenForms["frmAutorizaAnulacion"] != null)
                                {
                                    Application.OpenForms["frmAutorizaAnulacion"].Activate();
                                }
                                else
                                {
                                    frmAutorizaAnulacion frm_autoriza = new frmAutorizaAnulacion();
                                    frm_autoriza.frm_ticketsxmesa = this;
                                    frm_autoriza.ShowDialog();

                                    if (autorizado)
                                    {
                                        pedidoenmesa.Lista_detallepedido = new List<clsDetallePedido>()
                                                                                {
                                                                                    new clsDetallePedido() {

                                                                                       Iddetallepedido=int.Parse(dg_pedido.Rows[dg_pedido.CurrentCell.RowIndex].Cells[dg_pedido.Columns["COD DETA"].Index].Value.ToString())

                                                                                    }
                                                                                };

                                        if (admped.eliminar_detallepedido(pedidoenmesa, usureg) > 0)
                                        {
                                            btn_buscar.PerformClick();
                                            dg_ticket_CellClick(sender, new DataGridViewCellEventArgs(2, indexpedido));
                                            dg_ticket.Rows[indexpedido].Cells[2].Selected = true;
                                            totales();
                                            columna_bloqueda();
                                        }
                                    }
                                }
                                //}
                            }

                        }
                    }
                }
                else
                {

                    MessageBox.Show("No se puede quitar a ticket´s " + cb_estado.Text, "Advertencia");

                }
            }
            catch (Exception) { }
        }

        private void btn_anular_ticket_Click(object sender, EventArgs e)
        {
            int filas_afectadas = -1;

            try
            {

                if (dg_pedido.Rows.Count > 0 && cb_estado.SelectedIndex == 1)
                {
                    if (dg_ticket.Rows.Count > 0)
                    {
                        if (dg_ticket.CurrentCell != null)
                        {
                            if (dg_ticket.CurrentCell.RowIndex != -1)
                            {
                                // DialogResult respuesta = MessageBox.Show("¿Desea anular ticket?...", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                                //if (respuesta == DialogResult.Yes)
                                //{
                                ticket = new clsTicket()
                                {
                                    Idticket = int.Parse(dg_ticket.Rows[dg_ticket.CurrentCell.RowIndex].Cells[idticket.Index].Value.ToString())
                                };

                                autorizado = false;

                                if (Application.OpenForms["frmAutorizaAnulacion"] != null)
                                {
                                    Application.OpenForms["frmAutorizaAnulacion"].Activate();
                                }
                                else
                                {
                                    frmAutorizaAnulacion frm_autoriza = new frmAutorizaAnulacion();
                                    frm_autoriza.frm_ticketsxmesa = this;
                                    frm_autoriza.ShowDialog();

                                    if (autorizado)
                                    {
                                        filas_afectadas = admtick.anular_ticket(ticket, usureg);

                                        if (filas_afectadas > 0)
                                        {
                                            MessageBox.Show("Ticket anulado correctamente...", "Información");
                                            indexpedido = indexpedido - 1;
                                            if (indexpedido < 0) indexpedido = 0;
                                            btn_buscar.PerformClick();

                                        }
                                        else
                                        {
                                            if (filas_afectadas == -3)
                                            {
                                                MessageBox.Show("La mesa actualmente tiene pedido...", "Información");
                                            }
                                            else
                                            {
                                                MessageBox.Show("Problemas para anular comprobante...", "Información");
                                            }
                                        }
                                    }
                                }

                                //}
                            }
                        }
                    }
                }
                else
                {

                    MessageBox.Show("No se puede anular ticket´s " + cb_estado.Text, "Advertencia");

                }
            }
            catch (Exception) { }
        }

        private void btn_imprimir_ticket_Click(object sender, EventArgs e)
        {
            string nombreimpresora = "";

            try
            {
                if (dg_pedido.Rows.Count > 0)
                {
                    if (dg_ticket.Rows.Count > 0)
                    {
                        if (dg_ticket.CurrentCell != null)
                        {
                            if (dg_ticket.CurrentCell.RowIndex != -1)
                            {

                                ticket = new clsTicket()
                                {
                                    Idticket = int.Parse(dg_ticket.Rows[dg_ticket.CurrentCell.RowIndex].Cells[idticket.Index].Value.ToString())
                                };

                                data = admtick.imprimir_ticket(ticket);

                                if (data != null)
                                {
                                    if (data.Tables[0].Rows.Count > 0)
                                    {
                                        nombreimpresora = impresora_xdefecto();

                                        if (nombreimpresora != "")
                                        {
                                            frmImpresionTicket frm = new frmImpresionTicket();
                                            CRTicket rpt = new CRTicket();
                                            rpt.SetDataSource(data);
                                            CrystalDecisions.CrystalReports.Engine.PrintOptions rptoption = rpt.PrintOptions;
                                            frm.crystal_imprimir_ticket.ReportSource = rpt;
                                            rptoption.ApplyPageMargins(new CrystalDecisions.Shared.PageMargins(0, 0, 0, 20));
                                            rpt.PrintOptions.PrinterName = nombreimpresora;
                                           /* if (estaenlinealaimpresora(nombreimpresora))
                                            { rpt.PrintToPrinter(1, false, 1, 1); }
                                            else { frm.ShowDialog(this); }   */
                                            //frm.ShowDialog(this);
                                            rpt.PrintToPrinter(1, false, 1, 1);
                                            rpt.Dispose();
                                            rpt.Close();                        
                                        }
                                        else
                                        {

                                            MessageBox.Show("No se ha configurado la ticketera por defecto...", "Advertencia");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception) { }
        }

        private void txt_numero_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void frmTicketsxMesa_Shown(object sender, EventArgs e)
        {
            txt_numero.Focus();
        }

        private void txt_numero_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {

                if (e.KeyCode == Keys.Enter)
                {
                    if (txt_numero.Text.Length > 0)
                    {
                        if (es_numero(txt_numero.Text))
                        {
                            listar_tickets_xnumero();
                            txt_numero.Text = string.Empty; 
                        }
                        else
                        {

                            txt_numero.Text = string.Empty;
                        }
                    }
                }
            }
            catch (Exception) { }
        }

        /*********************Mis Metodos*********************/

        public bool estaenlinealaimpresora(string printerName)
        {
            string str = "";
            bool online = false;

            ManagementScope scope = new ManagementScope(ManagementPath.DefaultPath);

            scope.Connect();

            //Consulta para obtener las impresoras, en la API Win32
            SelectQuery query = new SelectQuery("select * from Win32_Printer");

            ManagementClass m = new ManagementClass("Win32_Printer");

            ManagementObjectSearcher obj = new ManagementObjectSearcher(scope, query);

            //Obtenemos cada instancia del objeto ManagementObjectSearcher
            using (ManagementObjectCollection printers = m.GetInstances())
                foreach (ManagementObject printer in printers)
                {
                    if (printer != null)
                    {
                        //Obtenemos la primera impresora en el bucle
                        str = printer["Name"].ToString().ToLower();

                        if (str.Equals(printerName.ToLower()))
                        {
                            //Una vez encontrada verificamos el estado de ésta
                            if (printer["WorkOffline"].ToString().ToLower().Equals("true") || printer["PrinterStatus"].Equals(7))
                                //Fuera de línea
                                online = false;
                            else
                                //En línea
                                online = true;
                        }
                    }
                    else
                        throw new Exception("No fueron encontradas impresoras instaladas en el equipo");
                }
            return online;
        }
        public void listar_mesa()
        {
            try
            {
                mesas = admme.listar_mesa();

                if (mesas != null)
                {
                    if (mesas.Rows.Count > 0)
                    {
                        cb_mesa.DataSource = mesas;
                        cb_mesa.DisplayMember = "nombremesa";
                        cb_mesa.ValueMember = "idmesa";
                    }
                }
            }
            catch (Exception) { }
        }

        public void listar_tickets_xmesa() {

            int estado = -1;

            try
            {
                dg_ticket.DataSource = null;
                dg_pedido.DataSource = null;

                if (cb_estado.SelectedIndex == 0) estado = 2;
                if (cb_estado.SelectedIndex == 1) estado = 1;
                if (cb_estado.SelectedIndex == 2) estado = 3;

                tickets = admtick.listar_tickets_xmesa(
                        new clsTicket()
                        {
                            Estado = estado
                        }
                        ,
                        new clsMesa()
                        {
                            Idmesa = int.Parse(cb_mesa.SelectedValue.ToString())
                        },
                        dt_fechaini.Value,
                        dt_fechafin.Value

                    );

                if (tickets != null)
                {

                    if (tickets.Rows.Count > 0)
                    {

                        dg_ticket.DataSource = tickets;
                        dg_ticket.Refresh();
                        if (indexpedido != -1)
                        {
                            dg_ticket.Rows[indexpedido].Cells[2].Selected = true;

                        }
                    }
                }
            }
            catch (Exception) { }
        }

        public void limpiar_pedido()
        {
            try
            {
                dg_pedido.DataSource = null;
                lb_monto.Text = "0.00";
                pedidoenmesa = null;
                ticket = null;
            }
            catch (Exception) { }
        }

        public void listar_pedido_xidpedido(int index) {

            try
            {
                pedidoenmesa = admped.listar_pedido_xidpedidoxestado(new clsPedido()
                {
                    Idpedido = int.Parse(dg_ticket.Rows[index].Cells[idpedido.Index].Value.ToString())
                });

                if (pedidoenmesa != null)
                {
                    pedido = new DataTable();
                    pedido.Columns.Add("COD PEDI");
                    pedido.Columns.Add("COD DETA");
                    pedido.Columns.Add("COD PROD");
                    pedido.Columns.Add("PRODUCTO");
                    pedido.Columns.Add("PRECIO");
                    pedido.Columns.Add("CANTIDAD");
                    pedido.Columns.Add("TOTAL");
                    pedido.Columns.Add("PARA LLEVAR", typeof(bool));
                    pedido.Columns.Add("ATENDIDO", typeof(bool));
                    foreach (clsDetallePedido det in pedidoenmesa.Lista_detallepedido)
                    {
                        DataRow row = pedido.NewRow();

                        row["COD PEDI"] = pedidoenmesa.Idpedido.ToString();
                        row["COD DETA"] = det.Iddetallepedido.ToString();
                        row["COD PROD"] = det.Producto.IdProducto.ToString();
                        row["PRODUCTO"] = det.Producto.Nombre;
                        row["PRECIO"] = det.Preciounitario.ToString();
                        row["CANTIDAD"] = det.Cantidad.ToString().Replace(
                                                det.Cantidad.ToString().Substring(
                                                    det.Cantidad.ToString().IndexOf('.'),
                                                    det.Cantidad.ToString().Length - det.Cantidad.ToString().IndexOf('.')
                                                    ), "");
                        row["TOTAL"] = decimal.Round(det.Cantidad * det.Preciounitario, 2);
                        row["PARA LLEVAR"] = det.Parallevar;
                        row["ATENDIDO"] = det.Atendido;
                        pedido.Rows.Add(row);
                    }

                    dg_pedido.DataSource = null;
                    dg_pedido.DataSource = pedido;
                    dg_pedido.Columns[0].Visible = false;
                    dg_pedido.Columns[1].Visible = false;
                    dg_pedido.Columns[2].Visible = false;
                    columna_bloqueda();

                }
                else
                {

                    MessageBox.Show("Problemas para cargar detalle de pedido...", "Advertencia");
                }

                btn_agregar.Focus();
                totales();
            }
            catch (Exception) { }
        }



        public void columna_bloqueda()
        {
            try
            {
                if (dg_pedido.Rows.Count > 0)
                {

                    dg_pedido.Columns[0].ReadOnly = true;
                    dg_pedido.Columns[1].ReadOnly = true;
                    dg_pedido.Columns[2].ReadOnly = true;
                    dg_pedido.Columns[3].ReadOnly = true;
                    dg_pedido.Columns[4].ReadOnly = true;
                    dg_pedido.Columns[6].ReadOnly = true;
                    dg_pedido.Columns[7].ReadOnly = true;
                    dg_pedido.Columns[8].ReadOnly = true;
                }
            }
            catch (Exception) { }

        }

        public void totales()
        {
            try
            {
                if (dg_pedido.Rows.Count > 0)
                {
                    lb_monto.Text = (from x in pedido.AsEnumerable()
                                     select decimal.Parse(x.Field<string>("total"))).Sum().ToString();
                }
                else
                {

                    lb_monto.Text = "0.00";
                }
            }
            catch (Exception) { }
        }

        

        public string impresora_xdefecto()
        {

            string nombreimpresora = "";//Donde guardare el nombre de la impresora por defecto

            try
            {
                //Busco la impresora por defecto
                for (int i = 0; i < PrinterSettings.InstalledPrinters.Count; i++)
                {
                    PrinterSettings a = new PrinterSettings();
                    a.PrinterName = PrinterSettings.InstalledPrinters[i].ToString();
                    if (a.IsDefaultPrinter)
                    {
                        nombreimpresora = PrinterSettings.InstalledPrinters[i].ToString();

                    }
                }

                return nombreimpresora;
            }
            catch (Exception) { return nombreimpresora; }

       
        }

        public bool es_numero(object Expression)
        {
            try
            {
                bool isNum;
                double retNum;
                isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
                return isNum;
            }
            catch (Exception) { return false; }
        }

        public void listar_tickets_xnumero() {

            try
            {
                dg_ticket.DataSource = null;
                dg_pedido.DataSource = null;
                tickets = admtick.listar_tickets_xnumero(new clsTicket() {
                    Numero=txt_numero.Text
                });

                if (tickets != null) {

                    if (tickets.Rows.Count > 0) {

                        dg_ticket.DataSource = tickets;
                        dg_ticket.Refresh();
                    }
                }
            }
            catch (Exception) { }
        }      
    }
}
