﻿using FinalXML.Administradores;
using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalXML.vista.cajero
{
    public partial class frmCobro : PlantillaBase
    {
        public clsUsuario usureg { get; set; }
        public clsComprobante comprobante { get; set; }
        private clsAdmComprobante admcom = new clsAdmComprobante();
        private List<clsTipoCobro> lista_tipo_cobro = null;
        private clsAdmTipoCobro admtipoco = new clsAdmTipoCobro();
        private List<clsTarjeta> lista_tarjeta_cobro = null;
        private clsAdmTarjeta admtar = new clsAdmTarjeta();
        private List<clsBanco> lista_banco = null;
        private clsAdmBanco admba = new clsAdmBanco();
        private List<clsCuentaBancaria> lista_cuenta = null;
        private clsAdmCuentaBancaria admcuen = new clsAdmCuentaBancaria();
        private clsAdmCobro admco = new clsAdmCobro();
        private clsCobro cobro = null; 
        public clsComprobante notacredito { get; set; }
        public frmCobro()
        {
            InitializeComponent();
        }

        private void frmCobro_Load(object sender, EventArgs e)
        {
            try
            {
                if (comprobante != null)
                {

                    dtpk_fecha.Value = admcom.listar_fecha_actual();
                    txt_comprobante.Text = comprobante.Numero;
                    txt_monto_pendiente.Text = (decimal.Round(comprobante.Total - comprobante.Totalcancelado, 2)).ToString();
                    txt_parcial.Text = txt_monto_pendiente.Text;
                    txt_monto.Text = txt_monto_pendiente.Text;
                    cargar_tipo_cobro();

                }
            }
            catch (Exception) { }
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txt_operacion_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txt_monto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }


            if (e.KeyChar == '.' && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }

            if (!char.IsControl(e.KeyChar))
            {

                TextBox textBox = (TextBox)sender;

                if (textBox.Text.IndexOf('.') > -1 &&
                         textBox.Text.Substring(textBox.Text.IndexOf('.')).Length >= 4)
                {
                    e.Handled = true;
                }

            }
        }

        private void txt_monto_Leave(object sender, EventArgs e)
        {
            if (txt_monto.Text.Length == 0)
            {
                txt_monto.Text = "0.00";
            }
            txt_parcial.Text = decimal.Round(decimal.Parse(txt_monto_pendiente.Text) - decimal.Parse(txt_monto.Text), 2).ToString();
        }

        private void txt_monto_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                if (txt_monto.Text.Length == 0)
                {
                    txt_monto.Text = "0.00";
                }

                txt_parcial.Text = decimal.Round(decimal.Parse(txt_monto_pendiente.Text) - decimal.Parse(txt_monto.Text), 2).ToString();
                btn_guardar.Focus();
            }
        }

        private void txt_observacion_TextChanged(object sender, EventArgs e)
        {
            txt_observacion.CharacterCasing = CharacterCasing.Upper;
        }

        private void cb_tipo_pago_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cb_tipo_pago.Items.Count > 0)
                {

                    txt_ncc.Text = string.Empty;
                    txt_operacion.ReadOnly = true;
                    txt_operacion.Text = string.Empty;
                    txt_ncc.Text = string.Empty;
                    txt_monto.ReadOnly = false;

                    cb_tarjeta.Items.Clear();
                    cb_banco.Items.Clear();
                    cb_tarjeta.Items.Clear();
                    cb_cuenta.Items.Clear();

                    cb_tarjeta.Enabled = false;
                    cb_banco.Enabled = false;
                    cb_cuenta.Enabled = false;

                    if (cb_tipo_pago.SelectedItem.ToString().Contains("EFECTIVO"))
                    {
                        txt_operacion.Text = lista_tipo_cobro[cb_tipo_pago.SelectedIndex].Sigla + comprobante.Cliente.Documento;
                        txt_monto.Focus();
                    }

                    if (cb_tipo_pago.SelectedItem.ToString().Contains("TARJETA"))
                    {
                        cargar_tarjeta_cobro();
                        cb_tarjeta.Enabled = true;
                        txt_operacion.Text = "99999";
                        txt_operacion.ReadOnly = false;
                        txt_monto.Focus();

                    }

                    if (cb_tipo_pago.SelectedItem.ToString().Contains("DEPOSITO") || cb_tipo_pago.SelectedItem.ToString().Contains("TRANSFERENCIA"))
                    {
                        cargar_banco();
                        cb_banco.Enabled = true;
                        txt_operacion.ReadOnly = false;
                    }

                    if (cb_tipo_pago.SelectedItem.ToString().Contains("NOTA DE CREDITO"))
                    {

                        txt_monto.ReadOnly = true;

                        if (Application.OpenForms["frmNotaCreditoPendiente"] != null)
                        {
                            Application.OpenForms["frmNotaCreditoPendiente"].Activate();
                        }
                        else
                        {
                            frmNotaCreditoPendiente frm_notacredito = new frmNotaCreditoPendiente();
                            frm_notacredito.frm_cobro = this;
                            frm_notacredito.cliente = comprobante.Cliente;
                            frm_notacredito.ShowDialog();
                            if (notacredito != null)
                            {

                                txt_ncc.Text = notacredito.Numero;
                                txt_monto.Text = notacredito.Total.ToString();
                            }
                        }
                    }
                }
            }
            catch (Exception) { }
        }

        private void cb_banco_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cb_banco.Items.Count > 0)
                {
                    cb_cuenta.Items.Clear();
                    cb_cuenta.Enabled = false;
                    cargar_cuenta_xbanco();
                }
            }
            catch (Exception) { }
        }

        /***************Mis Metodos*******************/
        public void cargar_tipo_cobro()
        {
            try
            {
                lista_tipo_cobro = admtipoco.listar_tipocobro_xestado();

                if (lista_tipo_cobro != null)
                {
                    if (lista_tipo_cobro.Count > 0)
                    {
                        foreach (clsTipoCobro t in lista_tipo_cobro)
                        {
                            cb_tipo_pago.Items.Add(t.Nombre);
                        }

                        cb_tipo_pago.SelectedIndex = 0;
                    }

                }
            }
            catch (Exception) { }
        }

        public void cargar_tarjeta_cobro()
        {
            try
            {
                lista_tarjeta_cobro = admtar.listar_tarjetacobro();

                if (lista_tarjeta_cobro != null)
                {
                    if (lista_tarjeta_cobro.Count > 0)
                    {
                        foreach (clsTarjeta tar in lista_tarjeta_cobro)
                        {
                            cb_tarjeta.Items.Add(tar.Descripcion);
                        }

                        cb_tarjeta.SelectedIndex = 0;
                    }
                }
            }
            catch (Exception) { }
        }

        public void cargar_banco()
        {
            try
            {
                lista_banco = admba.listar_banco_xestado();

                if (lista_banco != null)
                {
                    if (lista_banco.Count > 0)
                    {
                        foreach (clsBanco b in lista_banco)
                        {
                            cb_banco.Items.Add(b.Nombre);

                        }
                        cb_banco.SelectedIndex = 0;
                    }
                }
            }
            catch (Exception) { }
        }

        public void cargar_cuenta_xbanco()
        {
            try
            {
                lista_cuenta = admcuen.listar_cuenta_x_banco(lista_banco[cb_banco.SelectedIndex]);

                if (lista_cuenta != null)
                {
                    if (lista_cuenta.Count > 0)
                    {
                        foreach (clsCuentaBancaria cu in lista_cuenta)
                        {
                            cb_cuenta.Items.Add(cu.Numero);
                        }
                        cb_cuenta.SelectedIndex = 0;
                        cb_cuenta.Enabled = true;
                    }

                }
            }
            catch (Exception) { }
        }

        private void btn_guardar_Click(object sender, EventArgs e)
        {
            bool correcto = true;

            try
            {
                if (comprobante != null)
                {
                    if (decimal.Parse(txt_monto.Text) > 0 && decimal.Parse(txt_monto.Text) <= decimal.Parse(txt_monto_pendiente.Text))
                    {
                        if (cb_tipo_pago.SelectedItem.ToString().Contains("TARJETA"))
                        {
                            if (txt_operacion.Text.Length > 0)
                            {
                                correcto = true;
                            }
                            else
                            {
                                MessageBox.Show("Ingrese el número de Operación...", "Advertencia");
                                correcto = false;
                            }

                        }

                        if (cb_tipo_pago.SelectedItem.ToString().Contains("DEPOSITO") || cb_tipo_pago.SelectedItem.ToString().Contains("TRANSFERENCIA"))
                        {
                            if (txt_operacion.Text.Length > 0 && cb_cuenta.Items.Count > 0)
                            {
                                correcto = true;
                            }
                            else
                            {
                                MessageBox.Show("Ingrese el número de Operación y Cuenta ...", "Advertencia");
                                correcto = false;
                            }

                        }

                        else if (cb_tipo_pago.SelectedItem.ToString().Contains("NOTA DE CREDITO"))
                        {
                            if (txt_ncc.Text.Length > 0)
                            {
                                correcto = true;
                            }
                            else
                            {

                                MessageBox.Show("Ingrese nota de credito...", "Advertencia");
                                correcto = false;

                            }
                        }

                        if (correcto)
                        {
                            cobro = new clsCobro()
                            {
                                Tipocobro = lista_tipo_cobro[cb_tipo_pago.SelectedIndex],
                                Fechacobro = admcom.listar_fecha_actual(),
                                Comprobante = comprobante,
                                Monto = decimal.Parse(txt_monto.Text),
                                Observacion = txt_observacion.Text,
                                Estado = 1
                            };

                            if (cb_tipo_pago.SelectedItem.ToString().Contains("EFECTIVO"))
                            {

                                cobro.Tarjetacobro = new clsTarjeta() { Idtarjeta = 0 };
                                cobro.Banco = new clsBanco() { Idbanco = 0 };
                                cobro.Cuentabancaria = new clsCuentaBancaria() { Idcuentabancaria = 0 };
                                cobro.Noperacion = txt_operacion.Text;
                                cobro.Notacredito = new clsComprobante() { Idcomprobante = 0 };

                            }

                            if (cb_tipo_pago.SelectedItem.ToString().Contains("TARJETA"))
                            {
                                cobro.Tarjetacobro = lista_tarjeta_cobro[cb_tarjeta.SelectedIndex];
                                cobro.Banco = new clsBanco() { Idbanco = 0 };
                                cobro.Cuentabancaria = new clsCuentaBancaria() { Idcuentabancaria = 0 };
                                cobro.Noperacion = txt_operacion.Text;
                                cobro.Notacredito = new clsComprobante() { Idcomprobante = 0 };
                            }

                            if (cb_tipo_pago.SelectedItem.ToString().Contains("DEPOSITO") || cb_tipo_pago.SelectedItem.ToString().Contains("TRANSFERENCIA"))
                            {

                                cobro.Tarjetacobro = new clsTarjeta() { Idtarjeta = 0 };
                                cobro.Banco = lista_banco[cb_banco.SelectedIndex];
                                cobro.Cuentabancaria = lista_cuenta[cb_cuenta.SelectedIndex];
                                cobro.Noperacion = txt_operacion.Text;
                                cobro.Notacredito = new clsComprobante() { Idcomprobante = 0 };

                            }

                            if (cb_tipo_pago.SelectedItem.ToString().Contains("NOTA DE CREDITO"))
                            {
                                cobro.Tarjetacobro = new clsTarjeta() { Idtarjeta = 0 };
                                cobro.Banco = new clsBanco() { Idbanco = 0 };
                                cobro.Cuentabancaria = new clsCuentaBancaria { Idcuentabancaria = 0 };
                                cobro.Noperacion = txt_operacion.Text;
                                cobro.Notacredito = notacredito;
                            }

                            if (admco.registrar_cobro(cobro, usureg) > 0)
                            {
                                //MessageBox.Show("Registro Correcto...", "Información");
                                this.Close();
                            }
                            else
                            {

                                MessageBox.Show("Problemas en el registro de cobro...", "Advertencia");

                            }


                        }
                    }
                    else
                    {


                        MessageBox.Show("Revisar el monto de cobro...", "Advertencia");

                    }
                }
            }
            catch (Exception) { }
        }

        private void frmCobro_Shown(object sender, EventArgs e)
        {            
            txt_monto.Focus();
        }

   
    }
}
