﻿using FinalXML.Administradores;
using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Interfaces
{
    interface IConfiguracionEnvio
    {
        int registrar_configuracionenvio(clsConfiguracionEnvio config);
        int actualizar_configuracionenvio(clsConfiguracionEnvio config,clsUsuario usureg);
        clsConfiguracionEnvio listar_configuracionenvio();
    }
}
