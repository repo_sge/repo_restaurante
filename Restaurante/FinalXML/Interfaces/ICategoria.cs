﻿using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Interfaces
{
    interface ICategoria
    {
        int registrar_categoria(clsCategoria categoria, clsUsuario usureg);
        int actualizar_categoria(clsCategoria categoria, clsUsuario usureg);
        DataTable _listar_categoria();
        List<clsCategoria> listar_categoria();
    }
}
