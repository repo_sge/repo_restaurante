﻿using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Interfaces
{
    interface IGrupo
    {
        int registrar_mesagrupo(clsGrupo grupo, clsMesa mesa, clsUsuario usureg);
        int registrar_grupo(clsUsuario usureg);
        DataTable listar_grupoxestado();
        int eliminar_mesagrupo(clsGrupo grupo, clsUsuario usureg);
    }
}
