﻿using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Interfaces
{
    interface ITipoCuentaBancaria
    {
        int registrar_tipocuenta(clsTipoCuentaBancaria tipocuenta, clsUsuario usureg);
        int actualizar_tipocuenta(clsTipoCuentaBancaria tipocuenta, clsUsuario usureg);
        DataTable listar_tipocuenta();
        DataTable listar_tipocuenta_xdescripcion(clsTipoCuentaBancaria tipocuenta);
        List<clsTipoCuentaBancaria> listar_tipocuenta_xestado();
    }
}
