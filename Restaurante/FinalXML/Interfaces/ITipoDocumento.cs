﻿using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Interfaces
{
    interface ITipoDocumento
    {
        /*Boolean Insert(clsTipoDocumento NuevoTipoDocumento);
        Boolean Update(clsTipoDocumento TipoDocumento);
        Boolean Delete(Int32 Codigo);*/

        clsTipoDocumento CargaTipoDocumento(Int32 Codigo);
        clsTipoDocumento BuscaTipoDocumento(String Sigla);
        DataTable ListaTipoDocumentos();
        DataTable ListaTipoDocumentosDeCaja();
        DataTable CargaTipoDocumentos();
        DataTable ListaDocumentoNota();
        DataTable ListaTipoDocumentosElectronicos();
    }
}
