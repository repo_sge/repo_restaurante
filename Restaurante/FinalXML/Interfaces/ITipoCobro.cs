﻿using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Interfaces
{
    interface ITipoCobro
    {
        int existe_sigla(clsTipoCobro tipocobro);
        int registrar_tipo_cobro(clsTipoCobro tipocobro, clsUsuario usureg);
        int actualizar_tipo_cobro(clsTipoCobro tipocobro, clsUsuario usureg);
        DataTable listar_tipocobro();
        DataTable listar_tipocobro_xnombre(clsTipoCobro tipocobro);
        List<clsTipoCobro> listar_tipo_cobro();
        List<clsTipoCobro> listar_tipocobro_xestado();
    }
}
