﻿using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Interfaces
{
    interface ITipoVenta
    {
        List<clsTipoVenta> listar_tipoventa();
    }
}
