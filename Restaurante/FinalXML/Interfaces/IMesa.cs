﻿using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Interfaces
{
    interface IMesa
    {
        int registrar_mesa(clsMesa mesa, clsUsuario usureg);
        int actualizar_mesa(clsMesa mesa, clsUsuario usureg);
        DataTable listar_mesa();
        List<clsMesa> listar_mesaxestado(clsPiso piso);
        List<clsMesa> listar_mesa_noenmesagrupo(clsPiso piso);
        DataTable listar_mesaxgrupo(clsGrupo grupo,clsPiso piso);
        DataTable listar_mesaxgruposinpedido(clsGrupo grupo, clsPiso piso);
        int cambiar_mesa(clsMesa mesa, clsPedido pedido, clsUsuario usureg);
        DataTable listar_grupodemesas(clsMesa mesa);        

    }
}
