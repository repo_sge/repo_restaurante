﻿using FinalXML.Administradores;
using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Interfaces
{
    interface IRepositorio
    {
        clsRepositorio listar_repositorio_xtscfm(clsRepositorio repositorio);
        int registrar_repositorio(clsRepositorio repositorio, clsUsuario usureg);
        int actualizar_repositorio(clsRepositorio repositorio, clsUsuario usureg);
        DataTable listar_repositorio_xtsfe(clsNumeracion numeracion);
        DataTable listar_repositorio_xtsfe_xcomprobante(clsNumeracion numeracion,clsComprobante comprobante);
        clsRepositorio listar_archivo_xrepositorio(clsRepositorio repositorio);
        clsRepositorio listar_archivo_xrepositorio_xcomprobante(clsComprobante comprobante);
    }
}
