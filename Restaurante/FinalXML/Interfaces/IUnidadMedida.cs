﻿using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Interfaces
{
    interface IUnidadMedida
    {
        int registrar_unidadmedida(clsUnidadMedida unidadmedida, clsUsuario usureg);
        int actualizar_unidadmedida(clsUnidadMedida unidadmedida, clsUsuario usureg);
        DataTable listar_unidadmedida();
        List<clsUnidadMedida> listar_unidadmedida_xestado();
    }
}
