﻿using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Interfaces
{
    interface IComprobante
    {
        int registrar_comprobante(clsComprobante comprobante, clsUsuario usureg);
        string listar_numerocomprobante_xidcomprobante(clsComprobante comprobante);
        DateTime listar_fecha_actual();
        DataSet imprimir_comprobante(clsComprobante comprobante);
        DataTable listar_ventas_xestado_xfecha(DateTime fechaincio, DateTime fechafin, int estado);
        DataTable listar_ventas_xestado_xfecha_xcomprobante(DateTime fechaincio, DateTime fechafin, int estado, clsComprobante comprobante);

        clsComprobante listar_comprobante_xid(clsComprobante comprobante);
        DataTable listar_detalle_comprobante_xidcomprobante(clsComprobante comprobante);
        int anular_comprobante(clsComprobante comprobante, clsUsuario usureg);
        DataTable listar_notas_xestado_xfecha(DateTime fechaincio, DateTime fechafin, int estado);

        /********************Notas de Credito***********************/
        DataTable listar_notas_xestado_xcliente(clsCliente cliente);

        /***********************Ventas********************************/
        DataTable listar_ventas_mesas_xmozo(DateTime fechaincio, DateTime fechafin);
        DataSet reporte_ventas_mesas_xmozo(DateTime fechaincio, DateTime fechafin);
    }
}
