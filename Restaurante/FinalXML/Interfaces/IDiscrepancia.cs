﻿using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.InterMySql
{
    interface IDiscrepancia
    {
        List<clsDiscrepancia> listar_discrepancia_ncc();
    }
}
