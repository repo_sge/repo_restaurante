﻿using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Interfaces
{
    interface IPedido
    {
        clsPedido listar_idpedidoxmesa(clsMesa mesa);
        int registrar_pedido(clsPedido pedido, clsUsuario usureg);
        clsPedido listar_pedido_xidpedido(clsPedido pedido);
        int registrar_detallepedido(clsPedido pedido, clsUsuario usureg);
        int actualizar_detallepedido(clsPedido pedido, clsUsuario usureg);
        int eliminar_detallepedido(clsPedido pedido, clsUsuario usureg);
        int actualizar_pedidoydetalle_anular(clsPedido pedido, clsUsuario usureg);
        int actualizar_detallepedido_parallevar(clsPedido pedido, clsUsuario usureg);
        int actualizar_detalleordencocina_atendido(clsPedido pedido, clsUsuario usureg);
        clsPedido listar_pedido_xidpedidoxestado(clsPedido pedido);

        /***************************Ordenes de Cocina**********************/
        DataTable listar_idordencocina();
        DataTable listar_detalle_ordencocina(int idordencocina);
        string listar_descripcion_xiddetallepedido(clsDetallePedido detallepedido);
        int actualizar_descripcion_xdetallepedidoid(clsDetallePedido detallepedido ,clsUsuario usureg);

        /*********************Comandas*********************/

        DataSet imprimir_comanda(clsPedido pedido);
    }
}
