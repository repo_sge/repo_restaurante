﻿using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Interfaces
{
    interface IBanco
    {
        int registrar_banco(clsBanco banco,clsUsuario usureg);
        int actualizar_banco(clsBanco banco, clsUsuario usureg);
        DataTable listar_banco();
        DataTable listar_banco_xnombre(clsBanco banco);
        List<clsBanco> listar_banco_xestado();
    }
}
