﻿using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Interfaces
{
    interface IUsuario
    {
        clsUsuario buscar_usuarioxdocumento(clsUsuario usuario);
        int registrar_usuario(clsUsuario usuario, clsUsuario usureg);
        int actualizar_usuario(clsUsuario usuario, clsUsuario usureg);
        DataTable listar_usuario();
        DataTable buscar_usuarioxnombreapellido(clsUsuario usuario);
        clsUsuario buscar_usuarioxcuentaclave(clsUsuario usuario);
        clsUsuario buscar_usuarioxcuentaclave_autorizado(clsUsuario usuario);
    }
}
