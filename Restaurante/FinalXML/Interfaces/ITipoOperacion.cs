﻿using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Interfaces
{
    interface ITipoOperacion
    {
        List<clsTipoOperacion> listar_tipooeracion_xestado();

        clsTipoOperacion listar_tipooeracion_xid(int id);
    }
}
