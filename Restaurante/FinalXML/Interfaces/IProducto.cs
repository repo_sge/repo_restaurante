﻿using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Interfaces
{
    interface IProducto
    {
        int registrar_producto(clsProducto producto, clsUsuario usureg);
        int actualizar_producto(clsProducto producto, clsUsuario usureg);
        DataTable listar_producto();
        DataTable listar_productoxnombreestado(clsProducto producto);
        clsProducto listar_productoxid(int id);
        DataTable listar_productoxnombre(clsProducto producto);
        DataTable listar_productoxcategorianombre(clsProducto producto);
        int producto_paracocina(clsProducto producto);
        DataTable listar_codproductosunatxdesc(String desc);
        int validaProductoBolsa();

    }
}
