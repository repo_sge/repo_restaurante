﻿using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Interfaces
{
    interface IUnidadCompra
    {
        int registrar_unidad_compra(clsUnidadCompra unidadmedidacompra, clsUsuario usureg);
        int actualizar_unidad_compra(clsUnidadCompra unidadmedidacompra, clsUsuario usureg);
        DataTable listar_unidad_compra_xingrediente(clsIngrediente ingrediente);
        List<clsUnidadCompra> listar_unidad_compra_xingrediente_xestado(clsIngrediente ingrediente);
    }
}
