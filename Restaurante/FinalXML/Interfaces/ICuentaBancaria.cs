﻿using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Interfaces
{
    interface ICuentaBancaria
    {
        int registrar_cuenta(clsCuentaBancaria cuenta, clsUsuario usureg);
        int actualizar_cuenta(clsCuentaBancaria cuenta, clsUsuario usureg);
        DataTable listar_cuenta();
        DataTable listar_cuenta_xbanco(clsBanco banco);
        List<clsCuentaBancaria> listar_cuenta_banco_xestado();
        List<clsCuentaBancaria> listar_cuenta_x_banco(clsBanco banco);
    }
}
