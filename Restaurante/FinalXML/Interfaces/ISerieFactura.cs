﻿using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Interfaces
{
    interface ISerieFactura
    {
        int actualizar_seriexnumeracion(clsSerieFactura serie, clsUsuario usureg);
        DataTable listar_seriefacturacion();
        List<clsSerieFactura> _listar_seriefacturacion();
    }
}
