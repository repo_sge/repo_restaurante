﻿using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Interfaces
{
    interface ITicket
    {
        int registrar_ticket(clsPedido pedido, clsUsuario usureg);
        clsTicket listar_ticket_xid(clsTicket ticket);
        int anular_ticket(clsTicket ticket, clsUsuario usureg);
        int eliminar_ticket_nofacturado(clsTicket ticket, clsUsuario usureg);
        DataTable listar_ticket_generado();
        DataSet imprimir_ticket(clsTicket ticket);
        DataTable ticket_afacturar(clsTicket ticket);
        int actualizar_estadoticket_anofacturado(clsTicket ticket, clsUsuario usureg);
        int actualizar_ticket_xidcomprobante(clsComprobante comprobante, clsUsuario usureg);
        DataTable listar_ticket_nofacturado(DateTime fecha);
        DataTable listar_tickets_xmesa(clsTicket ticket, clsMesa mesa, DateTime fechaini, DateTime fechafin);
        DataTable listar_tickets(DateTime fechaini, DateTime fechafin);
        DataTable listar_tickets_xnumero(clsTicket ticket);
        int actualizar_ticket(clsTicket ticket, clsUsuario usureg);
    }
}
