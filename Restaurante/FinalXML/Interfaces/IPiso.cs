﻿using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Interfaces
{
    interface IPiso
    {
        int registrar_piso(clsPiso piso, clsUsuario usureg);
        int actualizar_piso(clsPiso piso, clsUsuario usureg);
        DataTable listar_piso();
        List<clsPiso> listar_piso_xestado();
    }
}
