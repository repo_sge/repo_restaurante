﻿using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Interfaces
{
    interface IProveedor
    {
        int registrar_proveedor(clsProveedor proveedor, clsUsuario usureg);
        int actualizar_proveedor(clsProveedor proveedor, clsUsuario usureg);
        DataTable listar_proveedor();
        DataTable listar_proveedor_xrazonsocial(clsProveedor proveedor);
    }
}
