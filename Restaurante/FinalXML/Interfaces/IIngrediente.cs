﻿using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Interfaces
{
    interface IIngrediente
    {
        int registrar_ingrediente(clsIngrediente ingrediente, clsUsuario usureg);
        int actualizar_ingrediente(clsIngrediente ingrediente, clsUsuario usureg);
        DataTable listar_ingrediente();
        DataTable listar_ingrediente_xnombre(clsIngrediente ingrediente);
        DataTable listar_ingrediente_xnombre_xestado(clsIngrediente ingrediente);
    }
}
