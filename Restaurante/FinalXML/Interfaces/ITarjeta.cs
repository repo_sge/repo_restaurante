﻿using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Interfaces
{
    interface ITarjeta
    {
        int registrar_tarjeta_cobro(clsTarjeta tarjetacobro, clsUsuario usureg);
        int actualizar_tarjeta_cobro(clsTarjeta tarjetacobro, clsUsuario usureg);
        DataTable listar_tarjeta_cobro();
        List<clsTarjeta> listar_tarjetacobro();
    }
}
