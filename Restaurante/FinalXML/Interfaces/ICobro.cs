﻿using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Interfaces
{
    interface ICobro
    {
        int registrar_cobro(clsCobro cobro, clsUsuario usureg);
        int anular_cobro(clsCobro cobro, clsUsuario usureg);
    }
}
