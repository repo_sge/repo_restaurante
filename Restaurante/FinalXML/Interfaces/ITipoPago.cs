﻿using FinalXML.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML.Interfaces
{
    interface ITipoPago
    {
        int registrar_tipo_pago(clsTipoPago tipopago, clsUsuario usureg);
        int actualizar_tipo_pago(clsTipoPago tipopago, clsUsuario usureg);
        DataTable listar_tipo_pago();     
        List<clsTipoPago> listar_tipo_pago_estado();
    }
}
